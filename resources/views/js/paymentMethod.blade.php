
<script>
    $('.payment_method_modal_edit').on('click', function () {

        $.ajax({
            type: 'get',
            url: "{{ route('paymentMethod.edit') }}",
            data: {id: $(this).attr("data-id")}, //Add request data
            dataType: 'json',
            success: function (data) {
                console.log(data);
                console.log('saim');

                $('.payment_method_id').val(data.id);
                $('.payment_method_name').val(data.payment_method_name);
                $('.short_code').val(data.short_code);
                $('.exchange_rate').val(data.exchange_rate);
                $('.currency_id').val(data.currency_id);
                $('.sort_order').val(data.sort_order);

            }, error: function () {
                $('.payment_method_id').val('');
                $('.payment_method_name').val('');
                $('.short_code').val('');
                $('.exchange_rate').val('');
                $('.currency_id').val('');
                $('.sort_order').val('');


                console.log('error');
            }
        });

    });

    $('.edit_payment_method_save_modal').on('click',function(){
    $.ajax({
        type: 'post',
        url: "{{ route('paymentMethod.update') }}",
        data: {
            _token: '{{ csrf_token() }}',
            payment_method_id: $('.payment_method_id').val(),
            payment_method_name: $('#edit_payment_method_name').val(),
            short_code: $('#edit_short_code').val(),
            exchange_rate: $('#edit_exchange_rate').val(),
            currency_id: $('#edit_currency_id').val(),
            sort_order: $('#payment_method_modal_edit .sort_order').val(),
        },
        success: function (data) {
            window.location.reload();
        }, 
        error: function (err) {
            if(err.status === 422 ) {
                showValidationAlertMessage(err);
            } else {
                swal({
                  title: "Error",
                  icon: "error",
                  text: 'Something went wrong',
                });
            }
        }
    });
});
</script>
