<script>
    var supporting_documents = @json(config('constants.supporting_documents'));
    var payment_supporting_documents = @json(config('constants.payment_supporting_documents'));

    $(document).ready(function () {
        calc_total_payment();
        calc_total_pkr_payment();

        $('.table_lines_items_to_be_charged_edit td').each((i, v) => {
            if($(v).find('.form-control').is('select')) {
                $(v).tooltip({
                    title: $(v).find('.form-control').find(":selected").text() ? $(v).find('.form-control').find(":selected").text() : ''
                })
            } else {
                if($(v).find('.form-control').hasClass('number_formated_two')) {
                    $(v).tooltip({
                        title: $.number($(v).find('.form-control').val(), 6) ? $.number($(v).find('.form-control').val(), 6) : ''
                    })
                } else {
                    $(v).tooltip({
                        title: $(v).find('.form-control').val() ? $(v).find('.form-control').val() : ''
                    })
                }
            }
        })

    });

    function fnc(value, min, max) {
        if(parseInt(value) < 0 || isNaN(value)) 
            return 0; 
        else if(parseInt(value) > 100) 
            return "Number is greater than 100"; 
        else return value;
    }

    function ordinal_suffix_of(i) {
        var j = i % 10,
            k = i % 100;
        if (j == 1 && k != 11) {
            return i + "st";
        }
        if (j == 2 && k != 12) {
            return i + "nd";
        }
        if (j == 3 && k != 13) {
            return i + "rd";
        }
        return i + "th";
    }

    $contractProjects = null;
    //    $contractProjectsEdit = null;
    $(document).on('change', '.contractor_id', function (event) {
        $contractor_id = $('.contractor_id').val();
        $('#beneficiary_account_no').empty().selectpicker('refresh');
        if ($contractor_id != '' || $contractor_id != 'undefined' || $contractor_id != null) {
            $.ajax({
                url: "{{ route('payment.GetContractorContracts')  }}",
                method: 'get',
                data: {
                    'contractor_id': $contractor_id
                },
                success: function (response) {
                    if (response.contractor_financial_details) {
                        $('#beneficiary_account_no').append(response.contractor_financial_details).selectpicker('refresh');
                    } else {
                        $('#beneficiary_account_no').empty().selectpicker('refresh');
                    }

                    if(response.contractor) {
                        $('.vendor_ntn_div').prop('hidden', false);
                        $('.vendor_ntn').val(response.contractor.contractor_ntn);
                    } else {
                        $('.vendor_ntn_div').prop('hidden', true);
                        $('.vendor_ntn').val('');
                    }

                    if (response.vendor_contracts) {
                        $('.vendor_contracts').html(response.vendor_contracts);
                    } else {
                        $('.vendor_contracts').html('');
                    }
                },
                error: function (response) {
                    console.log(response);
                }
            });
        }
        else {
            swal("please Select Vendor");
        }
    });


    $(document).on('change', '.vendor_contracts', function (event) {
        $vendor_contracts = $('.vendor_contracts').val();

        if ($vendor_contracts != '' || $vendor_contracts != 'undefined' || $vendor_contracts != null) {
            $.ajax({
                url: "{{ route('payment.GetContractsDetails')  }}",
                method: 'get',
                data: {
                    'contract_id': $vendor_contracts
                },
                success: function (response) {
                    console.log('response');
                    console.log(response);


                    $contractProjects = response.contract_projects;

                    if (response.contract_exchange_rate_currency_code != '' || response.contract_exchange_rate_currency_code != 'undefined' || response.contract_exchange_rate_currency_code != null) {
                        $('.contract_change_currency').text(response.contract_exchange_rate_currency_code);
                        $('.contract_change_currency_from').val(response.contract_exchange_rate_currency_code_from);
                        $('.contract_change_currency_from_text').text(response.contract_exchange_rate_currency_code_from);
                        $('.contract_change_currency_from_id').val(response.contract_exchange_rate_currency_code_from_id);
                        $('.previous_payment_amount').val(response.previous_payment_amount);
                        $('.previous_payment_amount_pkr').val(response.previous_payment_amount_pkr);
                        $('.balance_payable_amount').val(response.balance_payable_amount);
                        $('.balance_payable_amount_pkr').val(response.balance_payable_amount_pkr);
                        $('.payment_origin').val(response.payment_origin);
                        $('.contract_currency').val(response.contract_currency);
                        $('.last_payment_date').val(response.last_payment_date);
                        $('.last_payment_amount').val(response.last_payment_amount);
                        $('.invoice_less_retention').val(response.less_retention+'%');
                        $('.invoice_performance_security_validity').datepicker("setDate", new Date(response.ps_validity));

                        $('#payment_request_no').text(ordinal_suffix_of(response.payment_request_no));
                        $('#invoice_withdraw_application_prefix').val(response.payment_request_no);
                        // $('#payment_request_no_prefix').attr('data-prefix', ordinal_suffix_of(response.payment_request_no));


                    }
                    else {

                        $('.contract_change_currency').text('__');
                        $('.contract_change_currency_from').val('__');
                        $('.contract_change_currency_from_text').val('__');
                    }
                    if (response.contract_amount != '' || response.contract_amount != 'undefined' || response.contract_amount != null) {
                        $('.contract_amount_payment').val(response.contract_amount);
                        $('.contract_revised_amount_payment').val(response.contract_revised_amount);
                    }
                    else {

                        $('.contract_amount_payment').val(0.00);
                        $('.contract_revised_amount_payment').val(0.00);

                        $('.previous_payment_amount').val(0);
                        $('.previous_payment_amount_pkr').val(0);
                        $('.balance_payable_amount').val(0);
                        $('.balance_payable_amount_pkr').val(0);
                        $('.payment_origin').val('');
                        $('.contract_currency').val('');
                        $('.last_payment_date').val('');
                        $('.last_payment_amount').val(0);
                    }

                },
                error: function (response) {
                    console.log('response');
                    console.log(response);


                }
            });
        }
        else {
            swal("please Select Vendor");
        }
    });


    //bank data
    $(document).on('change', '#beneficiary_account_no', function (event) {
        var account_id = $(this).val();

        $.ajax({
            url: "{{ route('payment.GetAccountData')  }}",
            method: 'get',
            data: {
                'account_id': account_id
            },
            success: function (response) {
                if (response) {
                    $('.beneficiary_bank_name').val(response.account_no);
                    $('.beneficiary_branch_name').val(response.branch_name);
                    $('.beneficiary_iban').val(response.iban);
                    $('.beneficiary_swift_code').val(response.swift_code);
                    $('.beneficiary_city').val(response.city);
                    $('.beneficiary_address').val(response.branch_address);


                } else {
                    $('.beneficiary_bank_name').val('');
                    $('.beneficiary_branch_name').val('');
                    $('.beneficiary_iban').val('');
                    $('.beneficiary_swift_code').val('');
                    $('.beneficiary_city').val('');
                    $('.beneficiary_address').val('');

                }
            },
            error: function (response) {
                console.log('response');
                console.log(response);


            }
        });
    });


    $(document).on('change', '.payment_project_city_id', (event) => {
        event.preventDefault();
        let city_id = event.target.value;
        let e = $(event.target).parent().parent().find('.payment_project_facility_id');
        let row = $(event.target).parent().parent();
        e.empty(); 

        // send request to fetch ficilities according to city
        fetch(`{{ route('payment.getFacilities') }}`, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "X-Requested-With": "XMLHttpRequest",
                    "X-CSRF-Token": $('input[name="_token"]').val()
                },
                body: JSON.stringify({
                    city: city_id,
                    project: row.find('.payment_project_id').val(),
                    contract: $('#vendor_contracts').val(),
                    measure: row.find('.payment_project_measure').val(),
                    subcategory: row.find('.payment_project_measure_sub_cate').val(),
                }),
            })
            .then(response => response.json())
            .then(response => {
                let options = ``;
                if(!$.isEmptyObject(response)) {
                    e.attr('readonly', false)
                    options += '<option value="">Select Facility</option>'; 
                    $.each(response, (i, v) => {
                        options += `<option value="${i}">${v}</option>`
                    })
                } else {
                    e.attr('readonly', true)
                    options += '<option value="">No Facilities Found</option>'
                }

                e.append(options) 
            })
            .catch(error => console.log(error));
    })


    //    payment_project_measure
    //create contract child table
    $(".add_lines_items_to_be_charged").click(function () {
            if (($('.contractor_id').val() != '') && ($('.vendor_contracts').val() != '' ) && ($('.invoice_amount').val() != '')) {
                var markup = `
                    <tr>
                        <td align="center" valign="center">
                            <input type='hidden'  name='checkFlag' class='checkFlag' value='1' />
                            <input name='record' id='check_all' type='checkbox'>
                        </td>
                        <td>
                            <select name='project_id[]' class='form-control payment_project_id' id='payment_project_id' required>
                                ${$contractProjects}
                            </select>
                        </td>
                        <td>
                            <select name='measure_id[]' class=' form-control payment_project_measure' id='payment_project_measure' required>
                                <option value=''>Select</option>
                            </select>
                        </td>
                        <td>
                            <select name='sub_cate_id[]' class='form-control payment_project_measure_sub_cate' id='payment_project_measure_sub_cate'>
                                <option value=''>Select</option>
                            </select>
                        </td>
                        <td>
                            <input readonly type='text' class=' form-control payment_project_province_name' id='payment_project_province_name'>
                            <input type='hidden' name='province_id[]' class=' form-control payment_project_province_id' id='payment_project_province_id'>
                        </td>
                        <td>
                            <input readonly type='text' class=' form-control payment_project_sub_cate_peas_name form-control' id='payment_project_sub_cate_peas_name'>
                            <input readonly type='hidden' name='sub_cate_peas_id[]' class='form-control payment_project_sub_cate_peas_id form-control' id='payment_project_sub_cate_peas_id'>
                        </td> 
                        <td>
                            <select name='city_id[]' id='payment_project_city_id' class='payment_project_city_id form-control'>
                            </select>
                        </td>

                        <td>
                            <select name='facility_id[]' id='payment_project_facility_id' class='payment_project_facility_id form-control'></select>
                        </td>
                        
                        <td>
                            <input type='text' name='payment_amount[]' class='payment_project_amount numberJs form-control text-right'>
                        </td> 
                        <td>
                            <input type='text' name='payment_amount_pkr[]' class='payment_project_amount_pkr numberJs form-control text-right' value='0'>
                        </td>
                        <td>
                            <input type='text' name='payment_project_description[]' class='form-control payment_project_description'>
                        </td>
                    </tr>`;
                $(".table_lines_items_to_be_charged tbody").append(markup);

                $('.numberJs').number(true, 6);

            //  $('.number_formated_two').number(true, 6);
            } else if(($('.invoice_amount').val() == '') && ($('.contractor_id').val() != '') && ($('.vendor_contracts').val() != '' )) {
                $.notify('Please enter invoice amount first in orde to add project details.')
                return;
            } else {
                $.notify('Please Select Contract And Vendor First');
                return;
            }
        var rowCount = $('.table_lines_items_to_be_charged >tbody >tr').length;
        if (rowCount > 0) {
            $('.contractor_id').css('pointer-events', 'none');
            $('.vendor_contracts').css('pointer-events', 'none');

        }else{
            $('.contractor_id').css('pointer-events', '');
            $('.vendor_contracts').css('pointer-events', '');
        }


        }
    );


    $(".add_lines_items_to_be_charged_edit").click(function () {

        $vendor_contracts = $('.vendor_contracts').val();
        if ($vendor_contracts != '' || $vendor_contracts != 'undefined' || $vendor_contracts != null) {
            $contractProjectsEdit = null;
            $.ajax({
                url: "{{ route('payment.GetContractsDetails')  }}",
                method: 'get',

                data: {
                    'contract_id': $vendor_contracts
                },
                success: function (response) {
                    console.log('response');
                    $contractProjectsEdit = response.contract_projects;

                    if (response.contract_exchange_rate_currency_code != '' || response.contract_exchange_rate_currency_code != 'undefined' || response.contract_exchange_rate_currency_code != null) {
                        $('.contract_change_currency').text(response.contract_exchange_rate_currency_code);
                        $('.contract_change_currency_from').val(response.contract_exchange_rate_currency_code_from);
                        $('.contract_change_currency_from_text').text(response.contract_exchange_rate_currency_code_from);
                        $('.contract_change_currency_from_id').val(response.contract_exchange_rate_currency_code_from_id);
                    }
                    else {

                        $('.contract_change_currency').text('__');
                        $('.contract_change_currency_from').val('__');
                        $('.contract_change_currency_from_text').text('__');
                    }
                    if (response.contract_amount != '' || response.contract_amount != 'undefined' || response.contract_amount != null) {
                        $('.contract_amount').val(response.contract_amount);
                        $('.contract_revised_amount').val(response.contract_revised_amount);
                    }
                    else {

                        $('.contract_amount').val(0.00);
                        $('.contract_revised_amount').val(0.00);
                    }
                    var markup = `<tr>
                        <td style="vertical-align: middle;">
                            <input type='hidden'  name='checkFlag' class='checkFlag' value='1' />
                            <input name='record' id='check_all' type='checkbox'>
                        </td>
                        <td>
                            <select name='project_id[]' class='form-control payment_project_id' id='payment_project_id' required>${$contractProjectsEdit}
                            </select>
                        </td>
                        <td>
                            <select name='measure_id[]' class=' form-control payment_project_measure' id='payment_project_measure' required>
                                <option value=''>Select</option>
                            </select>
                        </td>
                        <td>
                            <select name='sub_cate_id[]' class='form-control payment_project_measure_sub_cate' id='payment_project_measure_sub_cate'>
                                <option value=''>Select</option>
                            </select>
                        </td>
                        <td>
                            <input readonly type='text' class=' form-control payment_project_province_name' id='payment_project_province_name'>
                            <input type='hidden' name='province_id[]' class=' form-control payment_project_province_id' id='payment_project_province_id'>
                        </td>
                        <td>
                            <input readonly type='text' class=' form-control payment_project_sub_cate_peas_name form-control' id='payment_project_sub_cate_peas_name'>
                            <input readonly type='hidden' name='sub_cate_peas_id[]' class='form-control payment_project_sub_cate_peas_id form-control' id='payment_project_sub_cate_peas_id'>
                        </td> 
                        <td>
                            <select name='city_id[]' id='payment_project_city_id' class='payment_project_city_id form-control'></select>
                        </td>

                        <td>
                            <select name='facility_id[]' id='payment_project_facility_id' class='payment_project_facility_id form-control'></select>
                        </td>

                        <td>
                            <input type='text' name='payment_amount[]' class='payment_project_amount form-control numberJs text-right'>
                        </td> 
                            <td><input type='text' name='payment_amount_pkr[]'  class='payment_project_amount_pkr form-control numberJs text-right'>
                        </td>
                        <td>
                            <input type='text' name='payment_project_description[]' class='form-control payment_project_description'>
                        </td>
                    </tr>`;
                    
                    $(".table_lines_items_to_be_charged_edit tbody").append(markup);

                    // INITIATE NUMBER FORMAT 
                    $('.numberJs').number(true, 6);

                },
                error: function (response) {
                    console.log('response');
                    console.log(response);


                }
            });

        }
        else {
            swal("please Select Vendor");
            return false;
        }
        console.log('$getVendorProject');

//            $('.number_formated_two').number(true, 6);
//
        var rowCount = $('.table_lines_items_to_be_charged_edit >tbody >tr').length;
        if (rowCount > 0) {
            $('.contractor_id').css('pointer-events', 'none').css('background', 'gray');
            $('.vendor_contracts').css('pointer-events', 'none').css('background', 'gray');

        } else {
            $('.contractor_id').css('pointer-events', '').css('background', 'white').css('color', 'black');
            $('.vendor_contracts').css('pointer-events', '').css('background', 'white').css('color', 'black');
        }


    });


    // Find and remove selected table rows
    $(".delete_table_lines_items_to_be_charged").click(function () {
        $(".table_lines_items_to_be_charged").find('input[name="record"]').each(function () {
            if ($(this).is(":checked")) {
                $(this).parents("tr").remove();
            }

            var rowCount = $('.table_lines_items_to_be_charged >tbody >tr').length;
            if (rowCount > 0) {
                $('.contractor_id').css('pointer-events', 'none').css('background', 'gray');
                $('.vendor_contracts').css('pointer-events', 'none').css('background', 'gray');

            }else{
                $('.contractor_id').css('pointer-events', '').css('background', 'white').css('color', 'black');
                $('.vendor_contracts').css('pointer-events', '').css('background', 'white').css('color', 'black');
            }



        });

    });// Find and remove selected table rows edit


    // Find and remove selected table rows
    $(".delete_table_lines_items_to_be_charged_edit").click(function () {
        $(".table_lines_items_to_be_charged_edit").find('input[name="record"]').each(function () {
            if ($(this).is(":checked")) {
                $(this).parents("tr").remove();
                $('.select_all_checkboxes').prop('checked', false)
            }



        });
        var rowCount = $('.table_lines_items_to_be_charged_edit >tbody >tr').length;
        if (rowCount > 0) {
            $('.contractor_id').css('pointer-events', 'none').css('background', 'gray');
            $('.vendor_contracts').css('pointer-events', 'none').css('background', 'gray');

        } else {
            $('.contractor_id').css('pointer-events', '').css('background', 'white').css('color', 'black');
            $('.vendor_contracts').css('pointer-events', '').css('background', 'white').css('color', 'black');
        }

    });// Find and remove selected table rows edit


    //selected project measure


    $(document).on('change', '.payment_project_id', function (event) {
        var row = $(this).closest('tr');
        var project_id = $(this).val();
        var vendor_contracts = $('.vendor_contracts').val();
        
        if (project_id) {
            row.find('.payment_project_city_id').empty();
            row.find('.payment_project_facility_id').empty();
            $.ajax({
                url: "{{ route('payment.GetProjectMeasure')  }}",
                method: 'get',
                data: {
                    'project_id': project_id,
                    'contract_id': vendor_contracts
                },
                success: function (response) {
                    console.log('response');
                    console.log(response);
                    if (response) {
                        row.find('.payment_project_measure').html(response.project_measure);
                        row.find('.payment_project_province_id').val(response.province_id);
                        row.find('.payment_project_province_name').val(response.province_name);
                        row.find('.payment_project_sub_cate_peas_id').val(response.peas_id);
                        row.find('.payment_project_sub_cate_peas_name').val(response.peas_name);
                        row.find('.payment_project_city_id').append(response.cities);
                        row.find('.payment_project_city_id').trigger('change');
                    } else {
                        row.find('.payment_project_measure').empty();
                        row.find('.payment_project_city_id').empty();
                        row.find('.payment_project_facility_id').empty();
                        row.find('.payment_project_province_id').val(null);
                        row.find('.payment_project_province_name').val('');
                        row.find('.payment_project_sub_cate_peas_id').val(null);
                        row.find('.payment_project_sub_cate_peas_name').val('');
                    }
                },
                error: function (response) {
                    console.log('response');
                    console.log(response);


                }
            });
        } else {
            $.notify('Please select project', 'error')
        }
    });

    //get measure data
    $(document).on('change', '.payment_project_measure', function (event) {

        var row = $(this).closest('tr');
        var payment_project_id = row.find('.payment_project_id').val();
        var payment_project_measure = row.find('.payment_project_measure').val();
        var vendor_contracts = $('.vendor_contracts').val();
        $.ajax({
            url: "{{ route('payment.GetProjectMeasureSubcategory')  }}",
            method: 'get',
            data: {
                'project_id': payment_project_id,
                'contract_id': vendor_contracts,
                'payment_project_measure': payment_project_measure
            },
            success: function (response) {
                console.log('response');
                console.log(response);
                row.find('.payment_project_city_id').trigger('change');
                if (response) {
                    row.find('.payment_project_measure_sub_cate').html(response.sub_category_id);
                } else {
                    row.find('.payment_project_measure_sub_cate').empty();
                }
            },
            error: function (response) {
                console.log('response');
                console.log(response);


            }
        });
        highlightDuplicatesPaymentCreate();
        highlightDuplicatesPaymentEdit();
    });
    //files

    $(".add_supporting_documents").click(function () {

        $('.delete-contract-file').css('display', 'block');

        let supporting_document_options = '';

        $.each(supporting_documents, (i ,v) => {
            supporting_document_options += `<option value="${i}">${v}</option>`;
        })

        var markup = `
            <tr>
                <th>
                    <input type='checkbox' name='record'>
                </th>
                
                <th>
                    <input type="file" class="form-control-sm" name="file[]" class='file'>
                </th>
                
                <th>
                    <select class="form-control category" style="height: 35px !important;" name="category[]" onchange="otherCategory()" required>
                        <option value='' selected disabled>Select Category</option>
                        ${supporting_document_options}
                    </select>
                </th>

                <th style="text-align:center">
                    <input style="height: 35px !important;" type="text" class='form-control description' name="description[]">
                </th>

                <th style='text-align:center'>
                    <button type='button' class='add_supporting_documents_file text-white' id='add_supporting_documents_file' style='background: #001f3f;'>
                        <i class="fa fa-plus fa-sm"></i>
                    </button> 
                </th>
            </tr>
        `;

        $(".table_supporting_documents").append(markup);


    });


    $(".add_payment_supporting_documents").click(function () {

        $('.delete-contract-file').css('display', 'block');

        let payment_supporting_document_options = '';

        $.each(payment_supporting_documents, (i ,v) => {
            payment_supporting_document_options += `<option value="${i}">${v}</option>`;
        })

        var markup = `
            <tr>
                <td align="middle"><input type='checkbox' name='record'></td>
                <td>
                    <input type="file" name="file[]" class='file form-control-sm'>
                </td>
                <td>
                    <select class="form-control category" style="height: 35px !important;widtd: 100%!important;" name="category[]" onchange="otherCategory()" required>
                        <option value='' selected disabled>Select</option>
                        ${payment_supporting_document_options}
                    </select>
                </td>

                <td>
                    <input style="height: 35px !important;" dir="ltl" type="text" class='form-control description' name="description[]">
                </td>

                <td>
                    <button type='button' class='add_payment_supporting_documents_file text-white' id='add_payment_supporting_documents_file' style='background: #001f3f;'>
                        <i class="fa fa-plus"></i>
                    </button> 
                </td>   
            </tr>
        `;
        $(".table_payment_supporting_documents").append(markup);
    });

    function otherCategory() {
        event.preventDefault();
        let v = event.target.value;
        if(v == '20') {
            $(event.target).parent().append(`
                <input style="height: 35px !important;" type="text" class='form-control mt-1 other-category' name="others[]" placeholder="Enter Other Category">
            `);
        } else {
            $(event.target).parent().find('.other-category').remove();
        }


    }


    $(document).on('click', '.add_supporting_documents_file', function (event) {

        $(".table_supporting_documents_progress").css('display', '');
        $(document.body).css({'cursor': 'wait'});
        var row = $(this).closest('tr');
        var files = row.find("input[type='file']");
        var description = row.find('.description').val();
        var category = row.find('.category').val();
        var payment_id = $('.payment_id').val();
        var other = row.find('.other-category').val();
        if (files[0].files.length == 0) {
            swal("please upload File");
            return false;
        }

        var checkSize = files[0].files[0].size / 1024 / 1024;

        if (checkSize >= 30) {
            swal('Upload Max File Size is 30mb, Uploaded File Size:' + (checkSize.toFixed(2) ) + 'mb');
            row.find('.add_supporting_documents_file').css('display', '');
            $(document.body).css({'cursor': 'default'});
            return false;
        }
        var formData = new FormData();
        formData.append('file', files[0].files[0]);
        formData.append('description', description);
        formData.append('category', category);
        formData.append('other', other);
        formData.append('payment_id', payment_id);
        formData.append("_token", "{{ csrf_token() }}");

        console.log(formData);
        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = ((evt.loaded / evt.total) * 100);
                        $(".table_supporting_documents_progress_bar").width(percentComplete.toFixed(2) + '%');
                        $(".table_supporting_documents_progress_bar").html(percentComplete.toFixed(2) + '%');
                    }
                }, false);
                return xhr;
            },
            url: "{{route('supporting.documents.file.save')}}",

            // Form data
            data: formData,
            type: 'POST',
            //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $(".progress-bar").width('0%');
            },
            success: function (data) {
                console.log(data);
                if (data.flag) {
                    $('.table_supporting_documents_message').css('display', '');
                    $('.message').show();
                    setTimeout(function () {
                        $('.message').fadeOut('fast');
                    }, 2000);
                }
                if (!data.success) {
                    swal('File Format Have Issue');

                    $(".table_supporting_documents_progress").css('display', 'none');
                    $(document.body).css({'cursor': 'default'});
                    row.find('.contract-file').css('display', '');
                } else {
                    console.log(data);
                    console.log(data['success']['file']);
                    console.log(data['success']['category']);
                    console.log(data['path']);
                    $html = "<tr><td style='text-align: center'><input data-id='" + data['success']['id'] + "' value='" + data['success']['id'] + "' type='hidden' name='supporting_documents_progress[]'><input data-id='" + data['success']['id'] + "' value='" + data['success']['id'] + "' type='checkbox' name='record'></td><td style=''><a style='color: #007bff;margin-top: 0;white-space: -moz-pre-wrap !important;white-space: -pre-wrap; white-space: -o-pre-wrap;white-space: pre-wrap; word-wrap: break-word;white-space: -webkit-pre-wrap;  word-break: break-all;white-space: normal;'  href='" + data['path'] + "' target='_blank' download=" + data['success']['file_name'] + ">" + data['success']['file_name'] + "    <i class=' fa-lg fas fa-file-download'></i> </a></td><td><p>" + data['success']['category_name'] + "</p></td><td><p>" + data['success']['description'] + "</p></td>    <th style='text-align:center'></th></tr>";
                    $('.table_supporting_documents').append($html);
                    $('.number_formated_two').number(true, 6);
                    row.remove();
                    console.log('Upload completed');
                    $(".table_supporting_documents_progress").css('display', 'none');
                    $(document.body).css({'cursor': 'default'})
                }
                $('.number_formated_two').number(true, 6);
            },
            error: function (e) {
                console.log(e);
                row.find('.add_file_tr_contract_retain').css('display', '');
                $(document.body).css({'cursor': 'default'});
                $(".table_supporting_documents_progress").css('display', 'none');

            },
        });
        return false;
    });


    $(document).on('click', '.add_payment_supporting_documents_file', function (event) {

        $(".table_supporting_documents_progress_detail").css('display', '');
        $(document.body).css({'cursor': 'wait'});
        var row = $(this).closest('tr');
        var files = row.find("input[type='file']");
        var description = row.find('.description').val();
        var category = row.find('.category').val();
        var payment_id = $('.payment_id').val();
        var other = row.find('.other-category').val();

        if (files[0].files.length == 0) {
            swal("please upload File");
            return false;
        }

        var checkSize = files[0].files[0].size / 1024 / 1024;

        if (checkSize >= 30) {
            swal('Upload Max File Size is 30mb, Uploaded File Size:' + (checkSize.toFixed(2) ) + 'mb');
            row.find('.add_supporting_documents_file_detail').css('display', '');
            $(document.body).css({'cursor': 'default'});
            return false;
        }
        var formData = new FormData();
        formData.append('file', files[0].files[0]);
        formData.append('description', description);
        formData.append('category', category);
        formData.append('other', other);
        formData.append("_token", "{{ csrf_token() }}");
        formData.append('payment_id', payment_id);

        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = ((evt.loaded / evt.total) * 100);
                        $(".table_supporting_documents_progress_bar_detail").width(percentComplete.toFixed(2) + '%');
                        $(".table_supporting_documents_progress_bar_detail").html(percentComplete.toFixed(2) + '%');
                    }
                }, false);
                return xhr;
            },
            url: "{{route('payment.supporting.documents.file.save')}}",

            // Form data
            data: formData,
            type: 'POST',
            //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $(".progress-bar").width('0%');
            },
            success: function (data) {
                console.log(data);
                if (data.flag) {
                    $('.table_supporting_documents_message_detail').css('display', '');
                    $('.message').show();
                    setTimeout(function () {
                        $('.message').fadeOut('fast');
                    }, 2000);
                }
                if (!data.success) {
                    swal('File Format Have Issue');

                    $(".table_supporting_documents_progress_detail").css('display', 'none');
                    $(document.body).css({'cursor': 'default'});
                    row.find('.add_payment_supporting_documents_file').css('display', '');
                } else {
                    $html = "<tr><td style='text-align: center'><input data-id='" + data['success']['id'] + "' value='" + data['success']['id'] + "' type='hidden' name='payment_supporting_documents[]'><input data-id='" + data['success']['id'] + "' value='" + data['success']['id'] + "' type='checkbox' name='record'></td><td style=''><a style='color: #007bff;margin-top: 0;white-space: -moz-pre-wrap !important;white-space: -pre-wrap; white-space: -o-pre-wrap;white-space: pre-wrap; word-wrap: break-word;white-space: -webkit-pre-wrap;  word-break: break-all;white-space: normal;'  href='" + data['path'] + "' target='_blank' download=" + data['success']['file_name'] + ">" + data['success']['file_name'] + "    <i class=' fa-lg fas fa-file-download'></i> </a></td><td><p>" + data['success']['category_name'] + "</p></td><td><p>" + data['success']['description'] + "</p></td>    <th style='text-align:center'></th></tr>";
                    $('.table_payment_supporting_documents').append($html);
                    $('.number_formated_two').number(true, 6);
                    row.remove();
                    console.log('Upload completed');
                    $(".table_supporting_documents_progress_detail").css('display', 'none');
                    $(document.body).css({'cursor': 'default'})
                }
                $('.number_formated_two').number(true, 6);
            },
            error: function (e) {
                console.log(e);
                row.find('.add_payment_supporting_documents_file').css('display', '');
                $(document.body).css({'cursor': 'default'});
                $(".table_supporting_documents_progress_detail").css('display', 'none');

            },
        });
        return false;
    });


    $(".delete_supporting_documents").click(function () {
        $(".table_supporting_documents").find('input[name="record"]').each(function () {
            console.log($(this).attr('data-id'));
            if ($(this).is(":checked")) {
                deletePaymentDocumentFile($(this).attr('data-id'));
                $(this).parents("tr").remove();

            }
        });
    });   //files

    $(".delete_payment_supporting_documents").click(function () {
        $(".table_payment_supporting_documents").find('input[name="record"]').each(function () {
            console.log($(this).attr('data-id'));
            if ($(this).is(":checked")) {
                paymentSupportingDocumentsFileDelete($(this).attr('data-id'));
                $(this).parents("tr").remove();

            }
        });
    });


    function deletePaymentDocumentFile($id) {

        $.ajax({
            url: "{{ route('supporting.documents.file.delete') }}",
            method: 'get',
            data: {
                id: $id
            },
            success: function (response) {
                console.log(response);
                console.log("hello");
            }, error: function () {
                console.log('error');
            }
        });
    }
    function paymentSupportingDocumentsFileDelete($id) {

        $.ajax({
            url: "{{ route('payment.supporting.documents.file.delete') }}",
            method: 'get',
            data: {
                id: $id
            },
            success: function (response) {
                console.log(response);
                console.log("hello");
            }, error: function () {
                console.log('error');
            }
        });
    }

    //    Invoice Details

    $(document).on('change', '.table_lines_items_to_be_charged_edit', function (event) {

        $('.table_lines_items_to_be_charged_edit tr.duplicate').removeClass();
        calc_total_payment();
        calc_total_pkr_payment();
        highlightDuplicatesPaymentEdit();

        var invoice_amount = parseFloat(($('.invoice_amount').val().split(",").join("").replace('%', '')));
        var invoice_less_retention = $('.invoice_less_retention').val().split(",").join("").replaceAll('%', '');
        console.log([invoice_less_retention, invoice_amount]);


        var va = calculateRetentionOfPayment(invoice_less_retention, invoice_amount);
        $('.invoice_less_retention_amount').val(va[0]);
        $('.invoice_net_payable').val(va[1]);
        console.log(va);
    });
    $(document).on('change', '.table_lines_items_to_be_charged', function (event) {
        $('.table_lines_items_to_be_charged tr.duplicate').removeClass();

        calc_total_payment();
        calc_total_pkr_payment();
        highlightDuplicatesPaymentCreate();

        var invoice_amount = parseFloat(($('.invoice_amount').val().split(",").join("").replace('%', '')));
        var invoice_less_retention = $('.invoice_less_retention').val().split(",").join("").replaceAll('%', '');
        console.log([invoice_less_retention, invoice_amount]);


        var va = calculateRetentionOfPayment(invoice_less_retention, invoice_amount);
        $('.invoice_less_retention_amount').val(va[0]);
        $('.invoice_net_payable').val(va[1]);
        console.log(va);
    });
    
    $(document).on('change', '.invoice_amount', function (event) {

        var invoice_amount = parseFloat(($('.invoice_amount').val().split(",").join("").replace('%', '')));
        var invoice_less_retention = $('.invoice_less_retention').val().split(",").join("").replaceAll('%', '');
        console.log([invoice_less_retention, invoice_amount]);


        var va = calculateRetentionOfPayment(invoice_less_retention, invoice_amount);
        $('.invoice_less_retention_amount').val(va[0]);
        $('.invoice_net_payable').val(va[1]);
        console.log(va);
    });

    $(document).on('change', '.invoice_less_retention', function (event) {
        var invoice_amount = parseFloat(($('.invoice_amount').val().split(",").join("").replace('%', '')));
        var invoice_less_retention = $('.invoice_less_retention').val().split(",").join("").replaceAll('%', '');
        if (invoice_less_retention == '') {
            $('.invoice_less_retention').val('0' + '%');
            var va = calculateRetentionOfPayment(0, invoice_amount);
            $('.invoice_less_retention_amount').val(va[0]);
            $('.invoice_net_payable').val(va[1]);
        }
        else {
            var get_value = $('.invoice_less_retention').val(this.value.split(",").join("").replaceAll('%', '') + '%');
            var invoice_less_retention = $('.invoice_less_retention').val().split(",").join("").replaceAll('%', '');
            var va = calculateRetentionOfPayment(invoice_less_retention, invoice_amount);
            $('.invoice_less_retention_amount').val(va[0]);
            $('.invoice_net_payable').val(va[1]);
        }
    });

    function calculateRetentionOfPayment(percentage, invoice_amount) {

        if ((invoice_amount != 0) && (invoice_amount != '') || (percentage != 0) && (percentage != '')) {
            if ((percentage == 0) || (invoice_amount == '')) {

                return [0, invoice_amount];
            } else {
                var calculatedValue = (invoice_amount * percentage / 100);
                var net_payable = invoice_amount - calculatedValue;
                return [calculatedValue, net_payable];
            }
        }

    }
    //    document.addEventListener("DOMContentLoaded", function (event) {
    // do your stuff here
    //        $('.payment_project_city_id').selectpicker();

    //        alert(1);
    //    });


    function calc_total_payment() {


        var sum = 0;
        $(".payment_project_amount").each(function () {
            if (this.value.indexOf(',') > -1) {
                sum += parseFloat(this.value.split(",").join(""));
            } else if (!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);
            }
        });

        $('.payment_total_amount').val(sum);
        // contractPaymentAmountCalculatePercentCreate();

    }

    function calc_total_pkr_payment() {


        var sum = 0;
        $(".payment_project_amount_pkr").each(function () {
            if (this.value.indexOf(',') > -1) {

                sum += parseFloat(this.value.split(",").join(""));

            } else if (!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);

            }
        });
        $('.payment_total_amount_pkr').val(sum);

        // contractPaymentAmountCalculatePercentCreate();

    }


    $(document).on('change', '.payment_project_amount', function (event) {
        var row = $(this).closest('tr');
        var payment_project_id = row.find(".payment_project_id :selected").val();
        var payment_project_measure = row.find('.payment_project_measure :selected').val();
        var payment_project_measure_sub_cate = row.find('.payment_project_measure_sub_cate :selected').val();
        var payment_project_city_id = row.find('.payment_project_city_id :selected').val();
        var payment_project_facility_id = row.find('.payment_project_facility_id :selected').val();
        var payment_project_amount = row.find('.payment_project_amount').val();
        var payment_id = $('.payment_id').val();
        var vendor_contracts = $('.vendor_contracts').val();
        var contract_currency_id = $('.contract_change_currency_from_id').val();
        let balance_payable_amount = $('.balance_payable_amount').val();
        var sum = 0;

        $(".payment_project_amount").each(function () {
            if (this.value.indexOf(',') > -1) {
                sum += parseFloat(this.value.split(",").join(""));
            } else if (!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);
            }
        });

        $.ajax({
            url: "{{ route('payment.calculate.budget') }}",
            method: 'get',
            data: {
                project_id: payment_project_id,
                measure_id: payment_project_measure,
                sub_cate_id: payment_project_measure_sub_cate,
                city_id: payment_project_city_id,
                facility_id: payment_project_facility_id,
                contract_id: vendor_contracts,
                budget: payment_project_amount,
                payment_total_amount: sum,
                invoice_amount: $('.invoice_amount').val(),
                contract_currency_id
            },

            success: response => {
                if (response.success) {
                    row.css('background', 'white');
                    $('.remaining_balance').val(balance_payable_amount - sum)
                    $.notify(response.message, 'success')
                } else {
                    row.find('.payment_project_amount').val(0)
                    row.find('.payment_project_amount_pkr').val(0)
                    row.css('background', '#C70039');
                    calc_total_payment()
                    calc_total_pkr_payment()
                    swal('Warning', response.message, 'warning');
                }
            }, 
            error:  e => $.notify('Something went wrong.', 'error')
        });

        highlightDuplicatesPaymentCreate();
        highlightDuplicatesPaymentEdit();
    });

    $('.printpayment').on("click", function () {

        $('#paymentCreateForm').printThis({
            importCSS: true,
            loadCSS: true,
            canvas: true

        });
    });


    $(document).on('click', '.check-for-total', () => {
        event.preventDefault();

        let payment_total_amount = $('.payment_total_amount').val();
        let invoice_amount = $('.invoice_amount').val();

        if(Math.round(payment_total_amount) != Math.round(invoice_amount)) {
            swal('Warning', `Total Project(s) amount of (${payment_total_amount}) not equal to total invoice amount of (${invoice_amount}).`, 'warning');
            return;
        }

        $('.paymentForm').submit();
    })


    //check dublicated

    //contract checking row avoid dubicated
    function highlightDuplicatesPaymentCreate() {
        $('.table_lines_items_to_be_charged >tbody >tr').each(function (index1) {
            var row = $(this);

            var row_val1 = row.find("td:nth-child(2) select").val();
            var row_val2 = row.find("td:nth-child(3) select").val();
            var row_val3 = row.find("td:nth-child(4) select").val();
            var row_val4 = row.find("td:nth-child(7) select").val();
            var row_val5 = row.find("td:nth-child(8) select").val();

            $('.table_lines_items_to_be_charged >tbody >tr').each(function (index2) {

                var compare_row = $(this)
                var compare_row_val1 = compare_row.find("td:nth-child(2) select").val();
                var compare_row_val2 = compare_row.find("td:nth-child(3) select").val();
                var compare_row_val3 = compare_row.find("td:nth-child(4) select").val();
                var compare_row_val4 = compare_row.find("td:nth-child(7) select").val();
                var compare_row_val5 = compare_row.find("td:nth-child(8) select").val();

                if (index1 != index2 && row_val1 == compare_row_val1 && row_val2 == compare_row_val2 && row_val3 == compare_row_val3 && row_val4 == compare_row_val4 && row_val5 == compare_row_val5) {

                    row.addClass('duplicate')
                    compare_row.addClass('duplicate')
                }
            })
        });

        if ($('tr.duplicate').length > 0) {

            return false;
        } else {
            return true;
        }

    }
    function highlightDuplicatesPaymentEdit() {
        $('.table_lines_items_to_be_charged_edit >tbody >tr').each(function (index1) {
            var row = $(this);

            var row_val1 = row.find("td:nth-child(2) select").val();
            var row_val2 = row.find("td:nth-child(3) select").val();
            var row_val3 = row.find("td:nth-child(4) select").val();
            var row_val4 = row.find("td:nth-child(7) select").val();
            var row_val5 = row.find("td:nth-child(8) select").val();

            $('.table_lines_items_to_be_charged_edit >tbody >tr').each(function (index2) {

                var compare_row = $(this)
                var compare_row_val1 = compare_row.find("td:nth-child(2) select").val();
                var compare_row_val2 = compare_row.find("td:nth-child(3) select").val();
                var compare_row_val3 = compare_row.find("td:nth-child(4) select").val();
                var compare_row_val4 = compare_row.find("td:nth-child(7) select").val();
                var compare_row_val5 = compare_row.find("td:nth-child(8) select").val();

                if (index1 != index2 && row_val1 == compare_row_val1 && row_val2 == compare_row_val2 && row_val3 == compare_row_val3 && row_val4 == compare_row_val4 && row_val5 == compare_row_val5) {

                    row.addClass('duplicate')
                    compare_row.addClass('duplicate')
                }
            })
        });

        if ($('tr.duplicate').length > 0) {

            return false;
        } else {
            return true;
        }

    }


    $('.invoice_amount').on('change', () => {
        event.preventDefault();

        if ($('.table_lines_items_to_be_charged_edit tbody .payment_project_amount').not(':disabled').length > 0) {
            $('.table_lines_items_to_be_charged_edit tbody .payment_project_amount').not(':disabled').val('');
            $('.table_lines_items_to_be_charged_edit tbody .payment_project_amount').not(':disabled').trigger('change');
            $.notify('Invoice Amount changed. Please enter measure(s) amount again!', 'info');
        }


    })


    $(document).ready( function () {
        $("#paymentCreateForm").validate({
            errorElement: "div",
            errorPlacement: function ( error, element ) {
                // Add the `invalid-feedback` class to the error element
                error.addClass( "invalid-feedback" );
                error.insertAfter( element );
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
            }
        });
    });


</script>
