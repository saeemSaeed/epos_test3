<script>
    $('#exchangeRateForm').submit(function () {

        //Do stuff here
//        if ($('.currency_from option:selected').val() !== '' && $('.currency_to option:selected').val() !== '')
//            if ($('.currency_from option:selected').val() === $('.currency_to option:selected').val()) {
//
//                swal('currency cannot be same');
//                $('.currency_from').val('');
//                return false;
//
//
//            }


    });
    $('.currency_to').on('change', function () {

        if ($('.currency_from option:selected').val() !== '') {
//            if ($('.currency_from option:selected').val() === $('.currency_to option:selected').val()) {
//
//                swal('currency cannot be same');
//                $('.currency_to').val('');
//                return false;
//            }

        }
    });
    $('.currency_from').on('change', function () {

        if ($('.currency_from option:selected').val() !== '') {
//            if ($('.currency_from option:selected').val() === $('.currency_to option:selected').val()) {
//
//                swal('currency cannot be same');
//                $('.currency_from').val('');
//                return false;
//            }

        }
    });
    $('.currency_to').on('change', function () {

        if ($('.currency_to option:selected').val() !== '') {
//            if ($('.currency_from option:selected').val() === $('.currency_to option:selected').val()) {
//
//                swal('currency cannot be same');
//                $('.currency_to').val('');
//                return false;
//            }
        }
    });
    $(document).on('click', '.getExchangeRate', function (event) {


        $.ajax({
            type: 'get',
            url: "{{ route('exchangeRate.edit') }}",
            data: {id: $(this).attr("data-id")}, //Add request data
            dataType: 'json',
            success: function (data) {

                console.log(data.start_date);

                $('.exchange_rate_id').val(data.data.id);
                $('.start_date').val(data.start_date);
                $('.end_date').val(data.end_date);
                $('.exchange_rate').val(data.data.exchange_rate);
                $('.currency_from').val(data.data.currency_from);
                $('.currency_to').val(data.data.currency_to);
                $('.sort_order').val(data.data.sort_order);





//                $(".exchange_end_date").datepicker('setDate', data['data'][data.end_date]);



            }, error: function () {
                $('.exchange_rate_id').val('');
                $('.start_date').val('');
                $('.end_date').val('');
                $('.exchange_rate').val('');
                $('.currency_from').val('');
                $('.currency_to').val('');
                $('.sort_order').val('');

                console.log('error');
            }
        });

    });

    //validate the data
    $('.edit_save_exchange_modal').on('click', function () {
        $.ajax({
            type: 'post',
            url: "{{ route('exchangeRate.update') }}",
            data: {
                _token: '{{ csrf_token() }}',
                exchange_rate_id: $('.exchange_rate_id').val(),
                start_date: $('.start_date').val(),
                end_date: $('.end_date').val(),
                exchange_rate: $('.exchange_rate ').val(),
                currency_from: $('#currency_from_edit').val(),
                currency_to: $('#currency_to_edit').val(),
                sort_order: $('.sort_order').val(),
            },
            success: function (data) {
                window.location.reload();
            }, 
            error: function (err) {
                if(err.status === 422 ) {
                    showValidationAlertMessage(err);
                } else {
                    swal({
                      title: "Error",
                      icon: "error",
                      text: 'Something went wrong',
                    });
                }
            }
        });
    });
</script>
