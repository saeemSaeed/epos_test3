<script>
    // select all
        $('input[type=checkbox]').on('change', function () {
            if ($(this).prop('checked')) {
                $(this).prop('checked', true);
            } else {
                $(this).prop('checked', false);
            }
        });
        $('.selectAll').change(function () {
            if ($(this).prop('checked')) {
                $('.your_checkbox_class').prop('checked', true);
            } else {
                $('.your_checkbox_class').prop('checked', false);
            }
        });
//select only administrator create
        $('.admin_create_select').change(function () {
            if ($(this).prop('checked')) {
                $('.admin_create').prop('checked', true);
            } else {
                $('.admin_create').prop('checked', false);
            }
        });
//select only administrator delete
        $('.admin_delete_select').change(function () {
            if ($(this).prop('checked')) {
                $('.admin_delete').prop('checked', true);
            } else {
                $('.admin_delete').prop('checked', false);
            }
        });
//select only administrator edit
        $('.admin_edit_select').change(function () {
            if ($(this).prop('checked')) {
                $('.admin_edit').prop('checked', true);
            } else {
                $('.admin_edit').prop('checked', false);
            }
        });
//select only administrator list
        $('.admin_list_select').change(function () {
            if ($(this).prop('checked')) {
                $('.admin_list').prop('checked', true);
            } else {
                $('.admin_list').prop('checked', false);
            }
        });
//select only administrator show
        $('.admin_show_select').change(function () {
            if ($(this).prop('checked')) {
                $('.admin_show').prop('checked', true);
            } else {
                $('.admin_show').prop('checked', false);
            }
        });


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//select only epos create
        $('.epos_create_select').change(function () {
            if ($(this).prop('checked')) {
                $('.epos_create').prop('checked', true);
            } else {
                $('.epos_create').prop('checked', false);
            }
        });
//select only epos delete
        $('.epos_delete_select').change(function () {
            if ($(this).prop('checked')) {
                $('.epos_delete').prop('checked', true);
            } else {
                $('.epos_delete').prop('checked', false);
            }
        });
//select only epos edit
        $('.epos_edit_select').change(function () {
            if ($(this).prop('checked')) {
                $('.epos_edit').prop('checked', true);
            } else {
                $('.epos_edit').prop('checked', false);
            }
        });
//select only epos list
        $('.epos_list_select').change(function () {
            if ($(this).prop('checked')) {
                $('.epos_list').prop('checked', true);
            } else {
                $('.epos_list').prop('checked', false);
            }
        });
//select only epos show
        $('.epos_show_select').change(function () {
            if ($(this).prop('checked')) {
                $('.epos_show').prop('checked', true);
            } else {
                $('.epos_show').prop('checked', false);
            }
        });
//select only epos restore
        $('.epos_restore_select').change(function () {
            if ($(this).prop('checked')) {
                $('.epos_restore').prop('checked', true);
            } else {
                $('.epos_restore').prop('checked', false);
            }
        });
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//select only helper create
        $('.helper_create_select').change(function () {
            if ($(this).prop('checked')) {
                $('.helper_create').prop('checked', true);
            } else {
                $('.helper_create').prop('checked', false);
            }
        });
//select only helper delete
        $('.helper_delete_select').change(function () {
            if ($(this).prop('checked')) {
                $('.helper_delete').prop('checked', true);
            } else {
                $('.helper_delete').prop('checked', false);
            }
        });
//select only helper edit
        $('.helper_edit_select').change(function () {
            if ($(this).prop('checked')) {
                $('.helper_edit').prop('checked', true);
            } else {
                $('.helper_edit').prop('checked', false);
            }
        });
//select only helper list
        $('.helper_list_select').change(function () {
            if ($(this).prop('checked')) {
                $('.helper_list').prop('checked', true);
            } else {
                $('.helper_list').prop('checked', false);
            }
        });

//select only helper restore
        $('.helper_restore_select').change(function () {
            if ($(this).prop('checked')) {
                $('.helper_restore').prop('checked', true);
            } else {
                $('.helper_restore').prop('checked', false);
            }
        });
//select only dashboard list
        $('.dashboard_show_select').change(function () {
            if ($(this).prop('checked')) {
                $('.dashboard_show').prop('checked', true);
            } else {
                $('.dashboard_show').prop('checked', false);
            }
        });
// 
   

</script>