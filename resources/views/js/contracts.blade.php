
@push('js')
<script type="text/javascript" src="{{asset('assets/js/sweetalert.min.js')}}"></script>
@endpush
<script>

    function calc_total_pkr() {


        var sum = 0;
        $(".dymanic_contract_amount_pkr").each(function () {
            if (this.value.indexOf(',') > -1) {

                sum += parseFloat(this.value.split(",").join(""));

            } else if (!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);

            }
        });
        $('.contract_total_amount_pkr').val(sum);
        $('.contract_amount_pkr').val(sum);
        // contractPaymentAmountCalculatePercentCreate();

    }
    function calc_total() {

        highlightDuplicatescreate();


        var sum = 0;
        $(".dymanic_contract_amount").each(function () {
            if (this.value.indexOf(',') > -1) {

                sum += parseFloat(this.value.split(",").join(""));

            } else if (!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);

            }
        });
        $('.contract_total_amount').val(sum);
        $('.contract_amount').val(sum);

        // contractPaymentAmountCalculatePercentCreate();

    }
    $(document).ready(function () {

        //        $(".project_id_edit").on("mouseover", function () {
        $(document).on('mouseover', '.project_id_edit', function () {
            var row = $(this).closest('tr');
            $slected = row.find('.project_id_edit > option:selected').text();
            row.find('.project_id_edit').attr('title', $slected);

            //            $(this).attr('title', $slected);

        });
        $(document).on('mouseover', '.contract_measure_id_edit', function () {

            var row = $(this).closest('tr');

            $slected = row.find('.contract_measure_id_edit > option:selected').text();
            row.find('.contract_measure_id_edit').attr('title', $slected);
            //
            //            $slected=$("select.contract_measure_id_edit > option:selected").text();
            //            $(".contract_measure_id_edit").attr('title', $slected);

        });
        $(document).on('mouseover', '.subcategory_id_edit', function () {

            var row = $(this).closest('tr');

            $slected = row.find('.subcategory_id_edit > option:selected').text();
            row.find('.subcategory_id_edit').attr('title', $slected);
            //
            //            $slected=$("select.contract_measure_id_edit > option:selected").text();
            //            $(".contract_measure_id_edit").attr('title', $slected);

        });
        $(document).on('mouseover', '.subcategory_id_edit', function () {

            var row = $(this).closest('tr');

            $slected = row.find('.subcategory_id_edit > option:selected').text();
            row.find('.subcategory_id_edit').attr('title', $slected);
            //
            //            $slected=$("select.contract_measure_id_edit > option:selected").text();
            //            $(".contract_measure_id_edit").attr('title', $slected);

        });
        $(document).on('mouseover', '.contract_city_edit', function () {

            var row = $(this).closest('tr');

            $slected = row.find('.contract_city_edit > option:selected').text();
            row.find('.contract_city_edit').attr('title', $slected);
            //
            //            $slected=$("select.contract_measure_id_edit > option:selected").text();
            //            $(".contract_measure_id_edit").attr('title', $slected);

        });
        $(document).on('mouseover', '.description_edit', function () {

            var row = $(this).closest('tr');

            $slected = row.find('.description_edit > option:selected').text();
            row.find('.description_edit').attr('title', $slected);
            //
            //            $slected=$("select.contract_measure_id_edit > option:selected").text();
            //            $(".contract_measure_id_edit").attr('title', $slected);

        });

        //
        //        $(".description_edit").on("mouseover", function () {
        //
        //            $slected=$(".description_edit").val();
        //            $(".description_edit").attr('title', $slected);
        //
        //        });
        function calc_total_pkr() {


            var sum = 0;
            $(".dymanic_contract_amount_pkr").each(function () {
                if (this.value.indexOf(',') > -1) {

                    sum += parseFloat(this.value.split(",").join(""));

                } else if (!isNaN(this.value) && this.value.length != 0) {
                    sum += parseFloat(this.value);

                }
            });
            $('.contract_total_amount_pkr').val(sum);
            $('.contract_amount_pkr').val(sum);
            // contractPaymentAmountCalculatePercentCreate();

        }

        calc_total();
        calc_total_pkr();
        calc_total_edit();
        calc_total_edit_pkr();
        calc_total_payment_amount_edit();
        calc_total_payment_amount_edit_pkr();
        total_payment_percentage_edit();

        function checkValueTrueOrFalse() {
            $('.contract_table_dymanic >tbody >tr').each(function (index1) {
                var row = $(this);

                var row_val1 = row.find(".checkFlag").val();

                if (row_val1 == 0) {
                    swal("Project budget revision is submitted for approval, please get it approved before proceeding Or check measure budget!");


                    return false;
                }
            });
            // return false
        }


        function checkValueTrueOrFalseEdit() {

            $('.contract_table_dymanic_edit>tbody >tr').each(function (index1) {
                var row = $(this);

                var row_val1 = row.find(".checkFlag").val();

                if (row_val1 == 0) {

                    swal('Project budget revision is submitted for approval, please get it approved before proceeding Or Check measure budget');

                    return false;
                }
            });
            // return false
        }


        $(".contract_table_dymanic").on('change', function () {

            $('.contract_table_dymanic tr.duplicate').removeClass();
            highlightDuplicatescreate();
            calc_total_pkr();
            calc_total();
        });


        $(".contract_table_dymanic_edit").on('change', function () {

            $('.contract_table_dymanic_edit tr.duplicate').removeClass();
            highlightDuplicatesEdit();
        });

        function comparizonToDate(fit_start_time, fit_end_time) {
            if (fit_start_time !== '' && fit_end_time !== '') {
                if ((new Date(fit_start_time)) < (new Date(fit_end_time))) {

                    return true;
                } else {
                    return false;
                }

            }

        }


        $(".addContract").on('click', function (event) {
            checkPKrValuePayment(event);

            checkPKrValue(event);

            checkValueTrueOrFalse();
            var contract_planned_start_date = $('.contract_planned_start_date').val();
            var contract_planned_end_date = $('.contract_planned_end_date').val();

            if (!comparizonToDate(contract_planned_start_date, contract_planned_end_date)) {
                swal("Completion date should be greater than planed start date");
                return false;
            }

            var contract_amount = parseFloat($('.contract_amount').val().split(",").join(""));
            var contract_amount_pkr = parseFloat($('.contract_amount_pkr').val().split(",").join(""));
            var contract_total_amount = parseFloat($('.contract_total_amount').val().split(",").join(""));
            var total_payment_amount = parseFloat($('.total_payment_amount').val().split(",").join(""));
            var total_payment_amount_pkr = parseFloat($('.total_payment_amount_pkr').val().split(",").join(""));

            if (!highlightDuplicatescreate()) {
                swal("Please select unique measure and sub-category");
                calc_total();
                calc_total_pkr();
                calc_total_edit();
                calc_total_edit_pkr();
                calc_total_payment_amount_edit();
                calc_total_payment_amount_edit_pkr();
                total_payment_percentage_edit();
                return false;
            }
            if (total_payment_amount_pkr === contract_amount_pkr) {

                return true;
            } else {

                swal("Contract Amount(PKR) Should Match  Payment Plan(PKR)");
                calc_total();
                calc_total_pkr();
                calc_total_edit_pkr();
                calc_total_edit();
                calc_total_payment_amount_edit_pkr();
                calc_total_payment_amount_edit();
                total_payment_percentage_edit();
                return false;
            }


        });
        $('.addupdateContractEdit').on('click', function (event) {

            var contract_planned_start_date = $('.contract_planned_start_date_edit').val();
            var contract_planned_end_date = $('.contract_planned_end_date_edit').val();

            if (!comparizonToDate(contract_planned_start_date, contract_planned_end_date)) {
                swal("Completion date should be greater than planed start date");
                return false;
            }

            checkPKrValueEditPayment(event);

            checkPKrValueEdit(event);

            checkValueTrueOrFalseEdit();


            var contract_amount = parseFloat($('.contract_amount_edit').val().split(",").join(""));
            var contract_total_amount = parseFloat($('.contract_total_amount-edit').val().split(",").join(""));
            var contract_total_amount_edit_pkr = parseFloat($('.contract_total_amount-edit-pkr').val().split(",").join(""));
            var total_payment_amount_edit = parseFloat($('.total_payment_amount_edit').val().split(",").join(""));
            var total_payment_amount_edit_pkr = parseFloat($('.total_payment_amount_edit_pkr').val().split(",").join(""));
            if (!highlightDuplicatescreate()) {
                swal("Please select unique measure and sub-category");
                calc_total();
                calc_total_pkr();
                calc_total_edit();
                calc_total_edit_pkr();
                calc_total_payment_amount_edit();
                calc_total_payment_amount_edit_pkr();
                total_payment_percentage_edit();
                return false;
            }

            if ((total_payment_amount_edit_pkr === contract_total_amount_edit_pkr)) {
                return true;
            } else {

                swal("Contract Amount(PKR) Should Match  Payment Plan(PKR)");
                return false;
            }

        });

        // function checkExceedOrNot() {
        //     $(".checkFlag").each(function () {
        //
        //         if (this.value == 0) {
        //             swal("Kindly Resolved The Exceeded Value");
        //             return false;
        //         }
        //
        //     });
        // }

        $(".contract_table_dymanic_edit").on('change', function () {
            calc_total_edit();
            calc_total_edit_pkr();
        });



        $(document).on('change', '.contract_province_budget_edit_pkr', function () {
            calc_total_edit_pkr();
        });

        function calc_total_edit_pkr() {
            var sum = 0;
            $(".contract_province_budget_edit_pkr").each(function () {
                if (this.value.indexOf(',') > -1) {

                    sum += parseFloat(this.value.split(",").join(""));

                } else if (!isNaN(this.value) && this.value.length != 0) {
                    sum += parseFloat(this.value);

                }
            });
            $('.contract_total_amount-edit-pkr').val(sum);
            $('.contract_amount_pkr_edit').val(sum);
            contractPaymentAmountCalculatePercent();
        }

        function calc_total_edit() {
            var sum = 0;
            $(".contract_province_budget_edit").each(function () {
                if (this.value.indexOf(',') > -1) {

                    sum += parseFloat(this.value.split(",").join(""));

                } else if (!isNaN(this.value) && this.value.length != 0) {
                    sum += parseFloat(this.value);

                }
            });
            $('.contract_total_amount-edit').val(sum);
            $('.contract_amount_edit').val(sum);
            contractPaymentAmountCalculatePercent();
        }


        function ActiveProjectCallWhileDomRender() {
            $.ajax({
                url: "{{ route('constant.active.project') }}",
                method: 'get',
                success: function (response) {
                    console.log(response.ActiveProjects);
                    $html = "";
                    $html += "<option value=''>Select </option>";
                    $.each(response.ActiveProjects, function (index, val) {


                        $html += "<option value='" + val['id'] + "'>" + val['title'] + "</option>";

                    });
                    $activeProjectGenerate = $html;

                }
            });

        }

        // calling Active Project
        ActiveProjectCallWhileDomRender();


        // Get measure against project
        $(document).on('change', '.contracts_project_id', function (event) {
            contracts_project_id = null;
            var row = $(this).closest('tr');
            var contracts_project_id = row.find('.contracts_project_id :selected').val();

            $.ajax({
                url: "{{ route('contract.getActiveProjectMeasures') }}",
                method: 'get',
                data: {project_id: contracts_project_id},
                success: function (response) {
                    $html = "";
                    $html += "<option value=''>Select Measure</option>";
                    $.each(response.getActiveProjectMeasure, function (index, val) {
                        $html += "<option value='" + val['id'] + "'>" + val['name'] + "</option>";
                    });
                    // $activeProjectGenerate = $html;
                    row.find('.measures_contracts').html($html);
                    row.find('.province_contracts').val(response.province_contracts);
                    row.find('.province_contracts_id').val(response.province_contracts_id);
                    row.find('.sub_cate_peas').val(response.sub_cate_peas);
                    row.find('.sub_cate_peas_id').val(response.sub_cate_peas_id);
                    row.find('.dymanic_contract_amount').val(null);
                    row.find('.description').val("");
                    row.css('background', 'white').css('color', '');
                    //                    row.find('.sub_cate_city option').remove();

                    //                    $('.selectpicker option:selected').remove();

                    //                    row.find('.dymanic_contract_amount_pkr').val('').prop('readonly', true).css('background', '#5f5f5f');
                    row.find('#sub_cate_city_id').html(response.cities);


                    //                    row.find('.sub_cate_city').selectpicker();


                }
            });
        });
        // get measure_category
        $(document).on('change', '.measures_contracts', function (event) {

            var row = $(this).closest('tr');
            var measures_contracts_id = row.find('.measures_contracts').val();
            var contracts_project_id = row.find('.contracts_project_id').val();
            console.log(contracts_project_id);
            console.log(measures_contracts_id);
            $.ajax({
                url: "{{ route('contract.getActiveProjectMeasuresSelectedSubcategory') }}",
                method: 'get',
                data: {
                    project_id: contracts_project_id,
                    measures_id: measures_contracts_id
                },
                success: function (response) {
                    console.log('response');
                    console.log(response);
                    $html = "";
                    $html += "<option value=''>Select </option>";
                    $.each(response.ProjectSelectedMeasureSubcategory, function (index, val) {

                        $html += "<option value='" + val['id'] + "'>" + val['name'] + "</option>";

                    });

                    row.find('.sub_cate_contracts').html($html);

                }
            });
        });


        //edit contract


        // Get measure against project
        $(document).on('change', '.project_id_edit', function (event) {

            var row = $(this).closest('tr');
            var contracts_project_id = row.find('.project_id_edit :selected').val();

            $.ajax({
                url: "{{ route('contract.getActiveProjectMeasures') }}",
                method: 'get',
                data: {project_id: contracts_project_id},
                success: function (response) {
                    $html = "";
                    $html += "<option value=''>Select </option>";
                    $.each(response.getActiveProjectMeasure, function (index, val) {
                        $html += "<option value='" + val['id'] + "'>" + val['name'] + "</option>";
                    });
                    // $activeProjectGenerate = $html;
                    row.find('.contract_measure_id_edit').html($html);
                    row.find('.province_name_contracts_edit').val(response.province_contracts);
                    row.find('.province_id_contracts_edit').val(response.province_contracts_id);
                    row.find('.pea_name_contracts_edit').val(response.sub_cate_peas);
                    row.find('.pea_id_contracts_edit').val(response.sub_cate_peas_id);

                    row.find('.contract_province_budget_edit').val(0.00);
                    row.find('.description_edit').val("");
                    row.css('background', 'white').css('color', '');

                    console.log(response.cities);

                    row.find('#contract_city_edit').html(response.cities);
                    row.find('.contract_province_budget_edit_pkr').val(0).prop('readonly', true).css('background', '#d2d1d1');

                }
            });
        });


        // get measure_category edit
        $(document).on('change', '.contract_measure_id_edit', function (event) {

            var row = $(this).closest('tr');
            var measures_contracts_id = row.find('.contract_measure_id_edit').val();
            var contracts_project_id = row.find('.project_id_edit').val();

            $.ajax({
                url: "{{ route('contract.getActiveProjectMeasuresSelectedSubcategory') }}",
                method: 'get',
                data: {
                    project_id: contracts_project_id,
                    measures_id: measures_contracts_id
                },
                success: function (response) {
                    console.log('response');
                    console.log(response);
                    $html = "";
                    $html += "<option value=''>Select </option>";
                    $.each(response.ProjectSelectedMeasureSubcategory, function (index, val) {

                        $html += "<option value='" + val['id'] + "'>" + val['name'] + "</option>";

                    });

                    row.find('.subcategory_id_edit ').html($html);

                }
            });
        });

        $(document).on('change', '.sub_cate_city', (event) => {
            event.preventDefault();
            let city_id = event.target.value;
            let e = $(event.target).parent().parent().find('.sub_cate_facilities');
            e.empty(); 

            // send request to fetch ficilities according to city
            fetch(`{{ route('contract.getFacilities') }}?city=${city_id}`)
                .then(response => response.json())
                .then(response => {
                    let options = ``;
                    if(!$.isEmptyObject(response)) {
                        e.attr('readonly', false)
                        options += '<option value="">Select Facility</option>'; 
                        $.each(response, (i, v) => {
                            options += `<option value="${i}">${v}</option>`
                        })
                    } else {
                        e.attr('readonly', true)
                        options += '<option value="">No Facilities Found</option>'
                    }

                    e.append(options) 
                })
                .catch(error => console.log(error));
        })


        //create contract child table
        $(".add-row-contract").click(function () {


            if ($('.exchange_rate_flag').val() == 'true') {
                $('.delete-row-contract').css('display', 'block');

                // var markup = "<tr><th><input name='record' id='check_all'  type='checkbox'></th><td ><select name='project_id[]' class=' form-control contracts_project_id' id='contracts_project_id'>" + $activeProjectGenerate + "</select></td><td ><select name='measures_contracts[]' class=' form-control measures_contracts' id='measures_contracts'>" + $generateSubContract + "</select></td><td ><select name='sub_cate_contracts[]' class=' form-control sub_cate_contracts' id='sub_cate_contracts'>" + $generateSubContract + "</select></td><td width='15%'><select name='province_contracts[]' class=' form-control province_contracts' id='province_contracts' required>" + $generateProvince + "</select></td><td width='15%'><select name='sub_cate_peas[]' class=' form-control sub_cate_peas' id='sub_cate_peas'><option value=''>Select </option></select></td> <td width='15%'><select name='sub_cate_city[]' class=' form-control sub_cate_city' id='sub_cate_city'><option value=''>Select </option></select></td> <td width='15%'><input type='text' name='province_amount[]' oninput='this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');' class='control-form dymanic_contract_amount' dir='rtl' style=' background-color: #FFCCCB;'></td> <td width='18%'><input type='text' name='description[]' class='control-form' ></td></tr>";
                var markup = `
                    <tr>
                        <th>
                            <input type='hidden'  name='checkFlag' class='checkFlag' value='1' />
                            <input name='record' id='check_all'  type='checkbox'>
                        </th>
                        <td>
                            <select name='project_id[]' class='form-control contracts_project_id' id='contracts_project_id' required>${$activeProjectGenerate}</select>
                        </td>
                        <td>
                            <select name='measures_contracts[]' class='form-control measures_contracts' id='measures_contracts' required>
                                <option value=''>Select Measure</option>
                            </select>
                        </td>
                        <td >
                            <select name='sub_cate_contracts[]' class=' form-control sub_cate_contracts' id='sub_cate_contracts'>
                                <option value=''>Select Subcategory</option>
                            </select>
                        </td>
                        <td>
                            <input readonly  name='province_contracts[]' class=' form-control province_contracts' id='province_contracts'>
                            <input type='hidden' name='province_contracts_id[]' class=' form-control province_contracts_id' id='province_contracts_id'>
                        </td>
                        <td>
                            <input readonly name='sub_cate_peas[]' class='form-control sub_cate_peas' id='sub_cate_peas'>
                            <input type='hidden' name='sub_cate_peas_id[]' class=' form-control sub_cate_peas_id' id='sub_cate_peas_id'>
                        </td> 
                        <td>
                            <select name='sub_cate_city_id[]' id='sub_cate_city_id' class='sub_cate_city form-control'>
                                <option value="" selected>Select City</option>
                            </select>
                        </td> 
                        <td>
                            <select name='sub_cate_facility_id[]' id='sub_cate_facility_id' class='sub_cate_facilities form-control'>
                                <option value="" selected>Select Facility</option>
                            </select>
                        </td>
                        <td>
                            <input type='text' name='province_amount[]'  class='dymanic_contract_amount number_formated_two text-right form-control'>
                        </td>
                        <td>
                            <input type='text' name='province_amount_pkr[]' class='dymanic_contract_amount_pkr number_formated_two form-control text-right' readonly required>
                        </td> 
                        <td >
                            <input type='text' name='contract_child_description[]' class='form-control description'>
                        </td>
                    </tr>`;
                $(".contract_table_dymanic tbody").append(markup);
                $('.number_formated_two').number(true, 6);
            } else {
                swal('Exchange Rate Not Defined');
                return false;
            }
            var rowCount = $('.contract_table_dymanic >tbody >tr').length;
            if (rowCount > 0) {
                $('.currency_id_from').css('pointer-events', 'none');
                $('.currency_id').css('pointer-events', 'none');
                $('.exchange_rate_date').css('pointer-events', 'none');
                $('.add_exchange_rate_create_button').css('display', 'none');
            }


        });

        


        //ediit contract child table
        $(".add-row-contract-edit").click(function () {

            // var markup = "<tr><th><input name='record' id='check_all'  type='checkbox'></th><td ><select name='project_id[]' class=' form-control contracts_project_id' id='contracts_project_id'>" + $activeProjectGenerate + "</select></td><td ><select name='measures_contracts[]' class=' form-control measures_contracts' id='measures_contracts'>" + $generateSubContract + "</select></td><td ><select name='sub_cate_contracts[]' class=' form-control sub_cate_contracts' id='sub_cate_contracts'>" + $generateSubContract + "</select></td><td width='15%'><select name='province_contracts[]' class=' form-control province_contracts' id='province_contracts' required>" + $generateProvince + "</select></td><td width='15%'><select name='sub_cate_peas[]' class=' form-control sub_cate_peas' id='sub_cate_peas'><option value=''>Select </option></select></td> <td width='15%'><select name='sub_cate_city[]' class=' form-control sub_cate_city' id='sub_cate_city'><option value=''>Select </option></select></td> <td width='15%'><input type='text' name='province_amount[]' oninput='this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');' class='control-form dymanic_contract_amount' dir='rtl' style=' background-color: #FFCCCB;'></td> <td width='18%'><input type='text' name='description[]' class='control-form' ></td></tr>";
            if ($('.exchange_rate_flag').val() == 'true') {
                var markup = `
                        <tr>
                            <th>
                                <input type='hidden'  name='checkFlag' class='checkFlag' value='1' />
                                <input name='record' id='check_all'  type='checkbox'>
                                <input name='record' id='contract_id'  type='hidden'  class='contract_id'>
                            </th>
                            <td>
                                <select name='project_id_edit[]' class=' form-control project_id_edit' id='project_id_edit' required>${$activeProjectGenerate}</select>
                            </td>
                            <td>
                                <select name='measure_id_edit[]' class=' form-control contract_measure_id_edit' id='contract_measure_id_edit' required>
                                    <option value=''>Select Measure</option>
                                </select>
                            </td>
                            <td>
                                <select name='subcategory_id_edit[]' class=' form-control subcategory_id_edit' id='subcategory_id_edit'>
                                    <option value=''>Select Subcategory</option>
                                </select>
                            </td>
                            <td>
                                <input readonly name='province_name_contracts_edit[]' class=' form-control province_name_contracts_edit' id='province_name_contracts_edit'>
                                <input type='hidden' name='province_id_contracts_edit[]' class=' form-control province_id_contracts_edit' id='province_id_contracts_edit'>
                            </td>
                            <td>
                                <input readonly name='pea_name_contracts_edit[]' class=' form-control pea_name_contracts_edit' id='pea_name_contracts_edit'>
                                <input type='hidden' name='pea_id_contracts_edit[]' class=' form-control pea_id_contracts_edit' id='pea_id_contracts_edit'>
                            </td> 
                            <td>
                                <select name='contract_city_edit[]' id='contract_city_edit' class='sub_cate_city form-control'></select>
                            </td>
                            <td>
                                <select name='sub_cate_facility_id[${count_contractSubCategoryDivisions}]' id='sub_cate_facility_id' class='sub_cate_facilities form-control'>
                                    <option value="" selected>Select Facility</option>
                                </select>
                            </td>
                            <td>
                                <input type='text' name='contract_province_budget_edit[]' class='contract_province_budget_edit number_formated_two text-right' style='background-color: #FFCCCB;width:100% !important;'>
                            </td>
                            <td>
                                <input type='text' name='contract_province_budget_edit_pkr[]'  class='contract_province_budget_edit_pkr number_formated_two text-right'  style='background-color: #d2d1d1;width:100% !important;' readonly>
                            </td> 
                            <td>
                                <input type='text' name='description_edit[]' class='form-control description_edit' style='width:100% !important;'>
                            </td>
                        </tr>`;

                        count_contractSubCategoryDivisions++;
                $(".contract_table_dymanic_edit tbody").append(markup);
                $('.number_formated_two').number(true, 6);
            } else {
                swal('Exchange Rate Not Defined');
                return false;
            }
            var rowCount = $('.contract_table_dymanic_edit  >tbody >tr').length;
            if (rowCount > 0) {

                $('.currency_id_from_edit').css('pointer-events', 'none');
                $('.currency_id_edit').css('pointer-events', 'none');
                $('.exchange_rate_date_edit').css('pointer-events', 'none');
                $('.changeRateCreateButton').css('display', 'none');

            }
        });


        // Find and remove selected table rows
        $(".delete-row-contract").click(function () {
            $(".contract_table_dymanic").find('input[name="record"]').each(function () {
                if ($(this).is(":checked")) {
                    $(this).parents("tr").remove();
                }
                calc_total();
                calc_total_pkr();

            });
            var rowCount = $('.contract_table_dymanic >tbody >tr').length;
            if (rowCount == 0) {
                $('.currency_id_from').css('pointer-events', '');
                $('.currency_id').css('pointer-events', '');
                $('.exchange_rate_date').css('pointer-events', '');
                $('.add_exchange_rate_create_button').css('display', '');

            }
        });// Find and remove selected table rows edit



        function deleteContraactChild($id) {

            $.ajax({
                url: "{{ route('delete.contract.child.row') }}",
                method: 'get',
                data: {
                    id: $id
                },
                success: function (response) {
                    console.log(response);
                    console.log("hello");
                }, error: function () {
                    console.log('error');
                }
            });
        }

        $(".delete-row-contract-edit").click(function () {
            $(".contract_table_dymanic_edit").find('input[name="record"]').each(function () {
                if ($(this).is(":checked")) {
                    console.log($(this).val());
                    // deleteContraactChild($(this).val());
                    $(this).parents("tr").remove();
                }
                calc_total_edit_pkr();
                calc_total_edit();
            });

            var rowCount = $('.contract_table_dymanic_edit  >tbody >tr').length;
            if (rowCount == 0) {
                $('.currency_id_from_edit').css('pointer-events', '');
                $('.currency_id_edit').css('pointer-events', '');
                $('.exchange_rate_date_edit').css('pointer-events', '');
                $('.changeRateCreateButton').css('display', '');
            }

        });


        //file
        $(".add-contract-file-create").click(function () {

            $('.delete-contract-file').css('display', 'block');
            var markup = "<tr><th><input type='checkbox' name='record'></th><th ><input   type='file' style='font-size:12px !important;' class='contract-file'></th><th><select class='form-control category_project'  style='font-size: 14px !important;height: 35px !important;width: 100%!important;' name='category[]' required><option value='select'>Select</option><option value='Contract'>Contract</option><option value='Quotation'>Quotation</option><option value='PO'>PO</option></select></th><th><input style='font-size: 14px !important;height: 35px !important;' type='text' class='form-control number_formated_two contract_file_amount text-right' name='contract_file_amount[]'></th><th style='text-align:center'><input style='font-size: 14px !important;height: 35px !important;' type='text' class='form-control description_project' name='description[]'></th>  <th style='text-align:center'> <button type='button' class='add_file_tr_contract_on_create' id='add_file_tr_contract_on_create' style='background: #001f3f;color: white;'>+</button> </th></tr>";
            $(".contract_file_create").append(markup);
            $('.number_formated_two').number(true, 6);

        });
        $(".delete-contract-file").click(function () {
            $(".contract_file_create").find('input[name="record"]').each(function () {
                console.log($(this).attr('data-id'));
                if ($(this).is(":checked")) {
                    deleteContractFile($(this).attr('data-id'));
                    $(this).parents("tr").remove();

                }
            });
        });


        $(".add-contract-file-edit").click(function () {

            $('.delete-contract-file_edit').css('display', 'block');
            var markup = "<tr><th><input type='checkbox' name='record'></th><th ><input   type='file' style='font-size:9px !important;' class='contract-file-edit' ></th><th><select class='form-control category_project'  style='font-size: 9px !important;height: 35px !important;width: 100%!important;' name='category[]' required><option value='select'>Select</option><option value='Contract'>Contract</option><option value='Quotation'>Quotation</option><option value='PO'>PO</option></select></th><th><input style='   font-size: 14px !important;height: 35px !important;' type='text' class='form-control number_formated_two contract_file_amount text-right' name='contract_file_amount[]' ></th><th style='text-align:center'><input style='font-size: 9px !important;height: 35px !important;' type='text' class='form-control description_project' name='description[]'></th>    <th style='text-align:center'><button type='button' class='add_file_tr_contract' id='add_table_file' style='background: #001f3f;color: white;'>+</button></th></tr>";
            $(".contract_file_edit").append(markup);
            $('.number_formated_two').number(true, 6);
        });

        //file-retain
        var h = 0;
        $('.retain_file_date_' + h).datepicker({

            format: "dd-M-yyyy"
            , autoclose: true,
            startDate: "01-01-1947"
        });


        $(".add-contract-file-retain-create").click(function () {
            ++h;
            $('.delete-contract-retain-file').css('display', 'block');
            var markup = "<tr><th><input type='checkbox' name='record'></th><th ><input   type='file' style='font-size:12px !important;' class='file_retain'></th><th> <input placeholder='dd-mm-yyyy' type='text' class='form-control retain_file_date_" + h + "' name='retain_file_date' required></th><th><input style='   font-size: 14px !important;height: 35px !important;' type='text' class='form-control addendum_file_amount number_formated_two text-right' name='addendum_file_amount[]'></th><th style='text-align:center'><input style='font-size: 14px !important;height: 35px !important;' type='text' class='form-control description_project' name='description_retain[]'></th>   <th style='text-align:center'><button type='button' class='add_file_tr_contract_retain_create' id='add_table_file_retain' style='background: rgba(105,126,255,0.8)'>+</button></th></tr>";
            $(".contract_file_retain_create").append(markup);


            $('.retain_file_date_' + h).datepicker({

                format: "dd-M-yyyy", autoclose: true
                ,
                startDate: "01-01-1947"
            });
            $('.number_formated_two').number(true, 6);
        });
        $(".delete-contract-retain-file").click(function () {
            $(".contract_file_retain_create").find('input[name="record"]').each(function () {
                console.log($(this).attr('data-id'));
                if ($(this).is(":checked")) {
                    deleteContractFileRetain($(this).attr('data-id'));
                    $(this).parents("tr").remove();
                }
            });
        });


        // $(".add-contract-file-edit").click(function () {
        //
        //
        //     $('.delete-contract-file_edit').css('display', 'block');
        //
        //     var markup = "<tr><th><input type='checkbox' name='record'></th><th ><input   type='file' style='font-size:9px !important;' name='file' ></th><th> <div class='input-group date Pickerdate' id='retain_file_date_edit" + h + "' data-target-input='nearest'> <input placeholder='dd-mm-yy' type='text' class='form-control datetimepicker-input'  required data-target='#retain_file_date_edit" + h + "' name='retain_file_date_edit[]' style='text-align: center'> <div class='input-group-append' data-target='#retain_file_date_edit" + h + "' data-toggle='datetimepicker'><div class='input-group-text'><i class='fa fa-calendar'></i></div></div></div></th><th style='text-align:center'><input style='font-size: 9px !important;height: 35px !important;' type='text' class='form-control description_project' name='description[]'></th>    <th style='text-align:center'><button type='button' class='add_file_tr_contract' id='add_table_file' style='background: rgba(105,126,255,0.8)'>+</button></th></tr>";
        //     $(".contract_file_edit").append(markup);
        // });

        $(".add-contract-file-retain-edit").click(function () {
            var rowCount = $('table.contract_file_retain_edit tr:last').index() + 1;
            ++rowCount;
            console.log(rowCount);
            console.log('rowCount');
            $('.delete-contract-retain-file_edit').css('display', 'block');
            var markup = "<tr><th><input type='checkbox' name='record'></th><th ><input   type='file' style='font-size:9px !important;'></th><th><input type='text' id='retain_file_date_edit_" + rowCount + "' class='form-control retain_file_date_edit'  name='retain_file_date_edit'></th><th><input style='   font-size: 14px !important;height: 35px !important;' dir='rtl' type='text' class='form-control number_formated_two retain_file_amount' name='addendum_file_amount[]'></th><th style='text-align:center'><input style='font-size: 9px !important;height: 35px !important;' type='text' class='form-control description_project_retain' name='description_retain[]'></th><th style='text-align:center'><button type='button' class='add_file_tr_contract_retain' id='add_table_file_retain' style='background: rgba(105,126,255,0.8)'>+</button></th></tr>";

            $(".contract_file_retain_edit").append(markup);
            $('.number_formated_two').number(true, 6);
            $('#retain_file_date_edit_' + rowCount).datepicker({

                format: "dd-M-yyyy", autoclose: true
                ,
                startDate: "01-01-1947"
            });

        });


        //    $(".delete-project-file").click(function () {
        //
        //        $(".project_file").find('input[name="record"]').each(function () {
        //            console.log($(this).is(":checked"));
        //            if ($(this).is(":checked")) {
        //                swal($(this).attr('data-id'));
        //                console.log($(this).attr('data-id'));
        //                deleteProjectFile($(this).attr('data-id'));
        //                $(this).parents("tr").remove();
        //            }
        //
        //        });
        //    });
        $(".delete-contract-file-edit").click(function () {
            $(".contract_file_edit").find('input[name="record"]').each(function () {
                console.log($(this).attr('data-id'));
                if ($(this).is(":checked")) {

                    deleteContractFile($(this).attr('data-id'));
                    $(this).parents("tr").remove();
                }
            });
        });
        $(".delete-contract-file-retain-edit").click(function () {
            $(".contract_file_retain_edit").find('input[name="record"]').each(function () {
                console.log($(this).attr('data-id'));
                if ($(this).is(":checked")) {

                    deleteContractFileRetain($(this).attr('data-id'));
                    $(this).parents("tr").remove();
                }
            });
        });

        function deleteContractFile($id) {

            $.ajax({
                url: "{{ route('contract.file.delete') }}",
                method: 'get',
                data: {
                    id: $id
                },
                success: function (response) {
                    console.log(response);
                    console.log("hello");
                }, error: function () {
                    console.log('error');
                }
            });
        }

        function deleteContractFileRetain($id) {

            $.ajax({
                url: "{{ route('contract.file.retain.delete') }}",
                method: 'get',
                data: {
                    id: $id
                },
                success: function (response) {
                    console.log(response);
                    console.log("hello");
                }, error: function () {
                    console.log('error');
                }
            });
        }

        $('.contract-file').on('change', function () {
            // Maximum file size allowed
            var maxFileSize = 30;// 20kb
            // Load the file upload controller to a variable
            var fileUpload = $('.contract-file');
            //Check if the file upload controller has value
            if (fileUpload.val() == '') {
                return false;
            } else {
                checkSize = fileUpload[0].files[0].size / 1024 / 1024;
                //Check if the file size is less than maximum file size
//                if (checkSize < maxFileSize) {
//                    return true;
//                } else {
//                    swal('Upload Max File Size is 30mb, Uploaded File Size:' + (checkSize.toFixed(2) ) + 'mb');
//                    fileUpload.val('');
//                    return false;
//                }
            }
        });

        $('.file_retain').on('change', function () {
            // Maximum file size allowed
            var maxFileSize = 30;// 20kb
            // Load the file upload controller to a variable
            var fileUpload = $('.file_retain');
            //Check if the file upload controller has value
            if (fileUpload.val() == '') {
                return false;
            } else {
                checkSize = fileUpload[0].files[0].size / 1024 / 1024;
                //Check if the file size is less than maximum file size
//                if (checkSize < maxFileSize) {
//                    return true;
//                } else {
//                    swal('Upload Max File Size is 30mb, Uploaded File Size:' + (checkSize.toFixed(2) ) + 'mb');
//                    fileUpload.val('');
//                    return false;
//                }
            }
        });
        $(document).on('click', '.add_file_tr_contract_on_create', function (event) {


            var row = $(this).closest('tr');
            $(".file_progress").css('display', '');
            $(document.body).css({'cursor': 'wait'});
            var files = row.find(".contract-file");

            var category = row.find('.category_project').val();
            var contract_file_amount = row.find('.contract_file_amount').val();
            var description = row.find('.description_project').val();
            var add_file_tr_contract = row.find('.add_file_tr_contract').css('display', 'none');
            if (files[0].files.length == 0) {
                swal("please upload File");
                return false;
            }

            var checkSize = files[0].files[0].size / 1024 / 1024;

//            if (checkSize >= 30) {
//                swal('Upload Max File Size is 30mb, Uploaded File Size:' + (checkSize.toFixed(2) ) + 'mb');
//                row.find('.add_file_tr_contract').css('display', '');
//                $(document.body).css({'cursor': 'default'});
//                return false;
//            }

            var formData = new FormData();
            formData.append('file', files[0].files[0]);
            formData.append('category', category);
            formData.append('contract_file_amount', contract_file_amount);
            formData.append('description', description);
            formData.append("_token", "{{ csrf_token() }}");

            $.ajax({
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = ((evt.loaded / evt.total) * 100);
                            $(".progress-bar").width(percentComplete.toFixed(2) + '%');
                            $(".progress-bar").html(percentComplete.toFixed(2) + '%');
                        }
                    }, false);
                    return xhr;
                },
                url: "{{route('contract.file.single.create')}}",

                // Form data
                data: formData,
                type: 'POST',
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {

                    $(".progress-bar").width('0%');

                },
                success: function (data) {
                    if (data.flag) {
                        $('.file_contract_messege').css('display', '');
                        $('.message_contract').show();
                        setTimeout(function () {
                            $('.message_contract').fadeOut('fast');
                        }, 2000);
                    }
                    console.log(data);
                    if (!data.success) {
                        swal('File Format Have Issue');

                        $(".file_progress").css('display', 'none');
                        $(document.body).css({'cursor': 'default'});
                        row.find('.add_file_tr_contract').css('display', '');
                    } else {
                        console.log(data['success']['file']);
                        console.log(data['success']['category']);
                        console.log(data['path']);
                        $html = "<tr><td style='text-align: center'><input  value='" + data['success']['id'] + "' data-id='" + data['success']['id'] + "' type='hidden' name='record_contract[]'><input data-id='" + data['success']['id'] + "' value='" + data['success']['id'] + "' type='checkbox' name='record'></td><td ><a  style='color: #007bff;margin-top: 0;white-space: -moz-pre-wrap !important;white-space: -pre-wrap; white-space: -o-pre-wrap;white-space: pre-wrap; word-wrap: break-word;white-space: -webkit-pre-wrap;  word-break: break-all;white-space: normal;' href='" + data['path'] + "' target='_blank' download=" + data['success']['file_name'] + " >" + data['success']['file_name'] + "    <i class=' fa-lg fas fa-file-download'></i> </a></td><td style='text-align: center'>" + data['success']['category'] + "</td><td style='text-align:center'><input style=' font-size: 14px !important; height: 35px !important;' dir='rtl' type='text' class='form-control number_formated_two contract_file_amount tooltip_1' data-toggle='tooltip' data-original-title=" + data['success']['contract_file_amount'] + "   value=" + data['success']['contract_file_amount'] + "  name='contract_file_amount[]' readonly> </td><td>" + data['success']['description'] + "</td>    <th style='text-align:center'></th></tr>";
                        $('.contract_file_create ').append($html);
                        $('.number_formated_two').number(true, 6);
                        row.remove();
                        console.log('Upload completed');

                        $(".file_progress").css('display', 'none');
                        $(document.body).css({'cursor': 'default'})
                    }
                },
                error: function (e) {
                    console.log(e);
                    $(".file_progress").css('display', 'none');
                    $(document.body).css({'cursor': 'default'});
                    row.find('.add_file_tr_contract').css('display', '');

                }
            });


            return false;
        });
        $(document).on('click', '.add_file_tr_contract', function (event) {


            var row = $(this).closest('tr');
            $(".file_progress").css('display', '');
            $(document.body).css({'cursor': 'wait'});
            var files = row.find(".contract-file-edit");
            var category = row.find('.category_project').val();
            var contract_file_amount = row.find('.contract_file_amount').val();
            var description = row.find('.description_project').val();
            var add_file_tr_contract = row.find('.add_file_tr_contract').css('display', 'none');
            var checkSize = files[0].files[0].size / 1024 / 1024;

//            if (checkSize >= 30) {
//                swal('Upload Max File Size is 30mb, Uploaded File Size:' + (checkSize.toFixed(2) ) + 'mb');
//                row.find('.add_file_tr_contract').css('display', '');
//                $(document.body).css({'cursor': 'default'});
//                return false;
//            }

            var formData = new FormData();
            formData.append('file', files[0].files[0]);
            formData.append('category', category);
            formData.append('contract_id', $('#contract_id').val());
            formData.append('contract_file_amount', contract_file_amount);
            formData.append('description', description);
            formData.append("_token", "{{ csrf_token() }}");

            $.ajax({
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = ((evt.loaded / evt.total) * 100);
                            $(".progress-bar").width(percentComplete.toFixed(2) + '%');
                            $(".progress-bar").html(percentComplete.toFixed(2) + '%');
                        }
                    }, false);
                    return xhr;
                },
                url: "{{route('contract.file.single')}}",

                // Form data
                data: formData,
                type: 'POST',
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {

                    $(".progress-bar").width('0%');

                },
                success: function (data) {
                    if (data.flag) {
                        $('.file_contract_messege').css('display', '');
                        $('.message_contract').show();
                        setTimeout(function () {
                            $('.message_contract').fadeOut('fast');
                        }, 2000);
                    }
                    console.log(data);
                    if (!data.success) {
                        swal('File Format Have Issue');

                        $(".file_progress").css('display', 'none');
                        $(document.body).css({'cursor': 'default'});
                        row.find('.add_file_tr_contract').css('display', '');
                    } else {
                        console.log(data['success']['file']);
                        console.log(data['success']['category']);
                        console.log(data['path']);
                        $html = "<tr><td style='text-align: center'><input data-id='" + data['success']['id'] + "' type='checkbox' name='record'></td><td><a style='color: #007bff;margin-top: 0;white-space: -moz-pre-wrap !important;white-space: -pre-wrap; white-space: -o-pre-wrap;white-space: pre-wrap; word-wrap: break-word;white-space: -webkit-pre-wrap;  word-break: break-all;white-space: normal;'  href='" + data['path'] + "' target='_blank' download=" + data['success']['file_name'] + ">" + data['success']['file_name'] + "    <i class=' fa-lg fas fa-file-download'></i> </a></td><td><p>" + data['success']['category'] + "</p></td><td style='text-align:center'><input style=' font-size: 14px !important; height: 35px !important;' dir='rtl' type='text' class='form-control number_formated_two contract_file_amount tooltip_1'  data-toggle='tooltip' data-original-title=" + data['success']['contract_file_amount'] + " value=" + data['success']['contract_file_amount'] + "  name='contract_file_amount[]' readonly> </td><td>" + data['success']['description'] + "</td>    <th style='text-align:center'></th></tr>";
                        $('.contract_file_edit').append($html);
                        $('.number_formated_two').number(true, 6);
                        row.remove();
                        console.log('Upload completed');

                        $(".file_progress").css('display', 'none');
                        $(document.body).css({'cursor': 'default'})
                    }
                },
                error: function (e) {
                    console.log(e);
                    $(".file_progress").css('display', 'none');
                    $(document.body).css({'cursor': 'default'})
                    row.find('.add_file_tr_contract').css('display', '');

                }
            });


            return false;
        });
        $(document).on('click', '.add_file_tr_contract_retain', function (event) {

            $(".file_progress_retain").css('display', '');
            $(document.body).css({'cursor': 'wait'});
            var row = $(this).closest('tr');
            var files = row.find("input[type='file']");
            var retain_file_date_edit = row.find('.retain_file_date_edit').val();
            var retain_file_amount = row.find('.retain_file_amount').val();

            var description = row.find('.description_project_retain').val();

            var add_file_tr_contract = row.find('.add_file_tr_contract_retain').css('display', 'none');
            var checkSize = files[0].files[0].size / 1024 / 1024;

//            if (checkSize >= 30) {
//                swal('Upload Max File Size is 30mb, Uploaded File Size:' + (checkSize.toFixed(2) ) + 'mb');
//                row.find('.add_file_tr_contract_retain').css('display', '');
//                $(document.body).css({'cursor': 'default'});
//                return false;
//            }
            var formData = new FormData();
            formData.append('file_retain', files[0].files[0]);
            formData.append('contract_id', $('#contract_id').val());
            formData.append('retain_file_date_edit', retain_file_date_edit);
            formData.append('description_retain', description);
            formData.append('retain_file_amount', retain_file_amount);
            formData.append("_token", "{{ csrf_token() }}");

            $.ajax({
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = ((evt.loaded / evt.total) * 100);
                            $(".file_progress_retain-bar").width(percentComplete.toFixed(2) + '%');
                            $(".file_progress_retain-bar").html(percentComplete.toFixed(2) + '%');
                        }
                    }, false);
                    return xhr;
                },
                url: "{{route('contract.file.retain.single')}}",

                // Form data
                data: formData,
                type: 'POST',
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $(".progress-bar").width('0%');
                },
                success: function (data) {
                    console.log(data);
                    if (data.flag) {
                        $('.file_contract_messege_addendum').css('display', '');
                        $('.message').show();
                        setTimeout(function () {
                            $('.message').fadeOut('fast');
                        }, 2000);
                    }
                    if (!data.success) {
                        swal('File Format Have Issue');

                        $(".file_progress_retain").css('display', 'none');
                        $(document.body).css({'cursor': 'default'});
                        row.find('.add_file_tr_contract_retain').css('display', '');
                    } else {
                        console.log(data);
                        console.log(data['success']['file']);
                        console.log(data['success']['category']);
                        console.log(data['path']);
                        $html = "<tr><td style='text-align: center'><input data-id='" + data['success']['id'] + "' type='checkbox' name='record'></td><td ><a style='color: #007bff;margin-top: 0;white-space: -moz-pre-wrap !important;white-space: -pre-wrap; white-space: -o-pre-wrap;white-space: pre-wrap; word-wrap: break-word;white-space: -webkit-pre-wrap;  word-break: break-all;white-space: normal;'  href='" + data['path'] + "' target='_blank' download=" + data['success']['file_name'] + ">" + data['success']['file_name'] + "    <i class=' fa-lg fas fa-file-download'></i> </a></td><td><p style='text-align:center'>" + data['success']['retain_file_date'] + "</p></td><td style='text-align:center'><input style=' font-size: 14px !important; height: 35px !important;' dir='rtl' type='text' class='form-control number_formated_two retain_file_amount tooltip_1' value=" + data['success']['retain_file_amount'] + " data-toggle='tooltip' data-original-title=" + data['success']['retain_file_amount'] + "  name='retain_file_amount[]' readonly> </td><td><p>" + data['success']['description'] + "</p></td>    <th style='text-align:center'></th></tr>";
                        $('.contract_file_retain_edit').append($html);
                        $('.number_formated_two').number(true, 6);
                        row.remove();
                        console.log('Upload completed');
                        $(".file_progress_retain").css('display', 'none');
                        $(document.body).css({'cursor': 'default'})
                    }
                    $('.number_formated_two').number(true, 6);
                },
                error: function (e) {
                    console.log(e);
                    row.find('.add_file_tr_contract_retain').css('display', '');
                    $(document.body).css({'cursor': 'default'});
                    $(".file_progress_retain").css('display', 'none');

                },
            });
            return false;
        });
        $(document).on('click', '.add_file_tr_contract_retain_create', function (event) {

            $(".file_progress_retain").css('display', '');
            $(document.body).css({'cursor': 'wait'});
            var row = $(this).closest('tr');
            var files = row.find("input[type='file']");
            var retain_file_date_edit = row.find("input[name='retain_file_date']").val();
            var retain_file_amount = row.find('.addendum_file_amount').val();

            var description = row.find('.description_project').val();

            var add_file_tr_contract = row.find('.add_file_tr_contract_retain').css('display', 'none');
            if (files[0].files.length == 0) {
                swal("please upload File");
                return false;
            }

            var checkSize = files[0].files[0].size / 1024 / 1024;

//            if (checkSize >= 30) {
//                swal('Upload Max File Size is 30mb, Uploaded File Size:' + (checkSize.toFixed(2) ) + 'mb');
//                row.find('.add_file_tr_contract_retain').css('display', '');
//                $(document.body).css({'cursor': 'default'});
//                return false;
//            }
            var formData = new FormData();
            formData.append('file_retain', files[0].files[0]);
            formData.append('retain_file_date', retain_file_date_edit);
            formData.append('description_retain', description);
            formData.append('retain_file_amount', retain_file_amount);
            formData.append("_token", "{{ csrf_token() }}");
            console.log(formData);
            $.ajax({
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = ((evt.loaded / evt.total) * 100);
                            $(".file_progress_retain-bar").width(percentComplete.toFixed(2) + '%');
                            $(".file_progress_retain-bar").html(percentComplete.toFixed(2) + '%');
                        }
                    }, false);
                    return xhr;
                },
                url: "{{route('contract.file.retain.single.create')}}",

                // Form data
                data: formData,
                type: 'POST',
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {

                    $(".progress-bar").width('0%');

                },
                success: function (data) {
                    console.log(data);
                    if (data.flag) {
                        $('.file_contract_messege_addendum').css('display', '');
                        $('.message').show();
                        setTimeout(function () {
                            $('.message').fadeOut('fast');
                        }, 2000);
                    }
                    if (!data.success) {
                        swal('File Format Have Issue');

                        $(".file_progress_retain").css('display', 'none');
                        $(document.body).css({'cursor': 'default'});
                        row.find('.add_file_tr_contract_retain').css('display', '');
                    } else {
                        console.log(data);
                        console.log(data['success']['file']);
                        console.log(data['success']['category']);
                        console.log(data['path']);
                        $html = "<tr><td style='text-align: center'><input data-id='" + data['success']['id'] + "' value='" + data['success']['id'] + "' type='hidden' name='record_retain[]'><input data-id='" + data['success']['id'] + "' value='" + data['success']['id'] + "' type='checkbox' name='record'></td><td style=''><a style='color: #007bff;margin-top: 0;white-space: -moz-pre-wrap !important;white-space: -pre-wrap; white-space: -o-pre-wrap;white-space: pre-wrap; word-wrap: break-word;white-space: -webkit-pre-wrap;  word-break: break-all;white-space: normal;'  href='" + data['path'] + "' target='_blank' download=" + data['success']['file_name'] + ">" + data['success']['file_name'] + "    <i class=' fa-lg fas fa-file-download'></i> </a></td><td><p style='text-align:center'>" + data['success']['retain_file_date'] + "</p></td><td style='text-align:center'><input style=' font-size: 14px !important; height: 35px !important;' dir='rtl' type='text' class='tooltip_1 form-control number_formated_two retain_file_amount' value=" + data['success']['retain_file_amount'] + "  name='retain_file_amount[]' data-toggle='tooltip' data-original-title=" + data['success']['retain_file_amount'] + " readonly> </td><td><p>" + data['success']['description'] + "</p></td>    <th style='text-align:center'></th></tr>";
                        $('.contract_file_retain_create').append($html);
                        $('.number_formated_two').number(true, 6);
                        row.remove();
                        console.log('Upload completed');
                        $(".file_progress_retain").css('display', 'none');
                        $(document.body).css({'cursor': 'default'})
                    }
                    $('.number_formated_two').number(true, 6);
                },
                error: function (e) {
                    console.log(e);
                    row.find('.add_file_tr_contract_retain').css('display', '');
                    $(document.body).css({'cursor': 'default'});
                    $(".file_progress_retain").css('display', 'none');

                },
            });
            return false;
        });


        $(".custom-file-input").on("change", function () {

            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
    });


    //contract checking row avoid dubicated
    function highlightDuplicatescreate() {
        $('.contract_table_dymanic tbody > tr').each(function (index1) {
            console.log(index1);
            var row = $(this);
            var row_val1 = row.find("td:nth-child(2) select").val();
            var row_val2 = row.find("td:nth-child(3) select").val();
            var row_val3 = row.find("td:nth-child(4) select").val();
            var row_val4 = row.find("td:nth-child(7) select").val();
            var row_val5 = row.find("td:nth-child(8) select").val();

            $('.contract_table_dymanic > tbody > tr').each(function (index2) {
                var compare_row = $(this)
                var compare_row_val1 = compare_row.find("td:nth-child(2) select").val();
                var compare_row_val2 = compare_row.find("td:nth-child(3) select").val();
                var compare_row_val3 = compare_row.find("td:nth-child(4) select").val();
                var compare_row_val4 = compare_row.find("td:nth-child(7) select").val();
                var compare_row_val5 = compare_row.find("td:nth-child(8) select").val();

                if (index1 != index2 && row_val1 == compare_row_val1 && row_val2 == compare_row_val2 && row_val3 == compare_row_val3 && row_val4 == compare_row_val4 && row_val5 == compare_row_val5) {
                    row.addClass('duplicate')
                    compare_row.addClass('duplicate')
                }
            })
        });

        if ($('tr.duplicate').length > 0) {
            return false;
        } else {
            return true;
        }

    }


    function highlightDuplicatesEdit() {

        $('.contract_table_dymanic_edit >tbody >tr').each(function (index1) {
            var row = $(this);
            var row_val1 = row.find("td:nth-child(2) select").val();
            var row_val2 = row.find("td:nth-child(3) select").val();
            var row_val3 = row.find("td:nth-child(4) select").val();
            var row_val4 = row.find("td:nth-child(7) select").val();
            var row_val5 = row.find("td:nth-child(8) select").val();

            $('.contract_table_dymanic_edit >tbody >tr').each(function (index2) {

                var compare_row = $(this);
                var compare_row_val1 = compare_row.find("td:nth-child(2) select").val();
                var compare_row_val2 = compare_row.find("td:nth-child(3) select").val();
                var compare_row_val3 = compare_row.find("td:nth-child(4) select").val();
                var compare_row_val4 = compare_row.find("td:nth-child(7) select").val();
                var compare_row_val5 = compare_row.find("td:nth-child(8) select").val();

                if (index1 != index2 && row_val1 == compare_row_val1 && row_val2 == compare_row_val2 && row_val3 == compare_row_val3 && row_val4 == compare_row_val4 && row_val5 == compare_row_val5) {

                    row.addClass('duplicate');
                    compare_row.addClass('duplicate');
                }
            })
        })

        if ($('tr.duplicate').length > 0) {
            return false;
        } else {
            return true;
        }
    }


    ///checking budget on project/measure/sub-category level

    $(document).on('change', '.contract_table_dymanic', function (event) {
        var row = $(this).closest('tr');
        var contracts_project_id = row.find('.contracts_project_id :selected').val();
        var measures_contracts = row.find('.measures_contracts :selected').val();
        var sub_cate_contracts = row.find('.sub_cate_contracts :selected').val();
        var dymanic_contract_amount = row.find('.dymanic_contract_amount').val();
        var currency_id_from = $('.currency_id_from :selected').val();
        var exchange_rate_amount = $('.exchange_rate_amount_val').val();
        row.css('background', 'white').css('color', '');

        $.ajax({
            url: "{{ route('contract.checked.budget.project.measure.subcategory') }}",
            method: 'get',
            data: {
                contracts_project_id: contracts_project_id,
                measures_contracts: measures_contracts,
                sub_cate_contracts: sub_cate_contracts,
                dymanic_contract_amount: dymanic_contract_amount,
                contract_currency_id_from:currency_id_from,
                exchange_rate_amount:exchange_rate_amount
            },
            success: function (response) {
                if (response.flag) {
                    row.css('background', '#B22222').css('color', 'white');
                    row.find('.checkFlag').val(0);
                } else {
                    row.find('.checkFlag').val(1);
                }
            }, error: function () {
                console.log('error');
            }
        });
    });

    $(document).on('change', '.dymanic_contract_amount', function (event) {

        var row = $(this).closest('tr');

        var contracts_project_id = row.find('.contracts_project_id :selected').val();
        var measures_contracts = row.find('.measures_contracts :selected').val();
        var sub_cate_contracts = row.find('.sub_cate_contracts :selected').val();
        var dymanic_contract_amount = row.find('.dymanic_contract_amount').val();

        var exchange_rate_amount_val =   $('.exchange_rate_amount_val').val();
        var contract_currency_id_from = $('.currency_id_from').val();
        var contract_currency_id_to = $('.currency_id').val();

        var contract_id = $('#contract_id').val();

        row.css('background', 'white').css('color', '');

        var calculated=$.number( dymanic_contract_amount*exchange_rate_amount_val,6);


        $.ajax({
            url: "{{ route('contract.checked.budget.project.measure.subcategory') }}",
            method: 'get',
            data: {
                contract_id: contract_id,
                contracts_project_id: contracts_project_id,
                measures_contracts: measures_contracts,
                sub_cate_contracts: sub_cate_contracts,
                dymanic_contract_amount: dymanic_contract_amount,
                contract_currency_id_from: contract_currency_id_from,
                contract_currency_id_to: contract_currency_id_to,
                dymanic_contract_amount_pkr:calculated
            },
            success: function (response) {
                if(!response.currencyNotMatch){
                    row.find('.checkFlag').val(0);
                    row.find('.dymanic_contract_amount_pkr').val(0);
                    row.find('.dymanic_contract_amount').val(0);
                    swal('Please Select One Project Currency');
                    calc_total();
                    return false;

                }
                calc_total();

                if (!response.CheckProjectApprovedOrNot) {
                    swal('Project budget revision is submitted for approval, please get it approved before proceeding.');
                    row.find('.checkFlag').val(0);
                    row.find('.dymanic_contract_amount_pkr').val(0);
                    row.find('.dymanic_contract_amount').val(0);
                    if (parseInt($(this).val()) == 0) {
                        row.find('.contract_province_budget_edit_pkr').val(0);
                    } else {
                        row.find('.dymanic_contract_amount_pkr').val(0);
                        row.find('.dymanic_contract_amount_pkr').removeAttr("readonly").css('background', 'white');
                    }
                    return false;
                } else {
                    if (response.flag) {
                        row.css('background', '#B22222').css('color', 'white');
                        row.find('.checkFlag').val(0);
                        row.find('.dymanic_contract_amount').val(0);
                        row.find('.dymanic_contract_amount_pkr').val(0);
                        swal('Warning', `Amount entered is exceeding the project budget value.`, 'warning');

                        var sum = 0;
                        var contract_amount_pkr = 0;
                        $(".payemnt_province_amount").each(function () {
                            if (this.value.indexOf(',') > -1) {
                                sum += parseFloat(this.value.split(",").join(""));
                            } else if (!isNaN(this.value) && this.value.length != 0) {
                                sum += parseFloat(this.value);

                            }
                        });
                        //                        $(".contract_province_budget_edit_pkr").each(function () {
                        //                            if (this.value.indexOf(',') > -1) {
                        //
                        //                                contract_amount_pkr += parseFloat(this.value.split(",").join(""));
                        //
                        //                            } else if (!isNaN(this.value) && this.value.length != 0) {
                        //                                contract_amount_pkr += parseFloat(this.value);
                        //
                        //                            }
                        //                        });
                        //                        $('.contract_total_amount-edit-pkr').val($.number(contract_amount_pkr, 6));
                        //                        $('.contract_amount_pkr_edit').val($.number(contract_amount_pkr, 6));
                        $('.contract_total_amount ').val($.number(sum, 6));
                        $('.contract_amount ').val($.number(sum, 6));


                    } else {
                        row.find('.checkFlag').val(1);
                        //                        $('.currency_id_from').val()==$('.currency_id_from').val()
                        row.find('.dymanic_contract_amount_pkr').val($.number(dymanic_contract_amount * exchange_rate_amount_val,6));
                        //                        row.find('.dymanic_contract_amount_pkr').removeAttr("readonly").css('background', 'white');

                    }
                }



                var sum_1 = 0;
                $(".dymanic_contract_amount_pkr").each(function () {
                    if (this.value.indexOf(',') > -1) {

                        sum_1 += parseFloat(this.value.split(",").join(""));

                    } else if (!isNaN(this.value) && this.value.length != 0) {
                        sum_1 += parseFloat(this.value);

                    }
                });
                $('.contract_total_amount_pkr').val(sum_1);
                $('.contract_amount_pkr').val(sum_1);
                calc_total_payment_amount();
                total_payment_percentage();

            }, error: function () {
                console.log('error');
            }
        });


    });


    $(document).on('change', '.contract_province_budget_edit', function (event) {

        var row = $(this).closest('tr');

        var contracts_project_id = row.find('.project_id_edit :selected').val();
        var measures_contracts = row.find('.contract_measure_id_edit :selected').val();
        var sub_cate_contracts = row.find('.subcategory_id_edit :selected').val();
        var dymanic_contract_amount = row.find('.contract_province_budget_edit').val();
        var contract_province_budget_edit_pkr   = row.find('.contract_province_budget_edit_pkr').val();
        var contract_id = row.find('#contract_id').val();


        var exchange_rate_amount_val =  $('.exchange_rate_amount_val').val();
        var currency_id_from_edit = $('.currency_id_from_edit').val();
        var currency_id_edit = $('.currency_id_edit').val();
        var calcaulteAmount = $.number(dymanic_contract_amount*exchange_rate_amount_val,6);

        row.css('background', 'white').css('color', '');


        $.ajax({
            url: "{{ route('contract.checked.budget.project.measure.subcategory.update') }}",
            method: 'get',
            data: {
                contract_id: contract_id,
                contracts_project_id: contracts_project_id,
                measures_contracts: measures_contracts,
                sub_cate_contracts: sub_cate_contracts,

                dymanic_contract_amount: dymanic_contract_amount,
                contract_currency_id_from: currency_id_from_edit,
                contract_currency_id_to: currency_id_edit,
                contract_province_budget_edit_pkr: calcaulteAmount,
            },
            success: function (response) {
                console.log("_________|_______");
                console.log(response);
                console.log("_________|_!!______");
                console.log(calcaulteAmount);
                console.log("_______!!__|_!!______");
                console.log("_________|_______");
                if(!response.currencyNotMatch){
                    swal('Please Select One Project Currency');
                    row.find('.checkFlag').val(0);

                    row.find('.dymanic_contract_amount').val(0);
                    row.find('.contract_province_budget_edit_pkr').val(0);
                    calcaulteAmount=0;
                    calc_total();
                    return false;
                }
                if (!response.CheckProjectApprovedOrNot) {

                    swal('Project budget revision is submitted for approval, please get it approved before proceeding');
                    row.find('.checkFlag').val(0);
                    row.find('.dymanic_contract_amount').val(0);
                    row.find('.contract_province_budget_edit_pkr').val(0);
                    calcaulteAmount=0;
                    calc_total();
                    return false;
                } else {
                    if (response.flag) {

                        row.css('background', '#B22222').css('color', 'white');
                        row.find('.checkFlag').val(0);
                        row.find('.contract_province_budget_edit').val(0);

                        swal('Amount entered is exceeding the project budget value');

                        row.find('.contract_province_budget_edit_pkr').val(0).prop('readonly', true).css('background', '#868686');
                        var sum = 0;
                        var contract_amount_pkr = 0;
                        $(".contract_province_budget_edit").each(function () {

                            if (this.value.indexOf(',') > -1) {

                                sum += parseFloat(this.value.split(",").join(""));

                            } else if (!isNaN(this.value) && this.value.length != 0) {
                                sum += parseFloat(this.value);

                            }
                        });
                        $(".contract_province_budget_edit_pkr").each(function () {
                            if (this.value.indexOf(',') > -1) {

                                contract_amount_pkr += parseFloat(this.value.split(",").join(""));

                            } else if (!isNaN(this.value) && this.value.length != 0) {
                                contract_amount_pkr += parseFloat(this.value);

                            }
                        });
                        $('.contract_total_amount-edit-pkr').val($.number(contract_amount_pkr, 6));
                        $('.contract_amount_pkr_edit').val($.number(contract_amount_pkr, 6));

                        $('.contract_amount_edit').val($.number(sum, 6));
                        $('.contract_total_amount-edit').val($.number(sum, 6));

                    } else {


                        row.find('.contract_province_budget_edit_pkr').val( calcaulteAmount);
                        row.find('.checkFlag').val(1);
                        //                        row.find('.contract_province_budget_edit_pkr').removeAttr("readonly").css('background', 'white');
                        var contract_amount_pkrs = 0;
                        $(".contract_province_budget_edit_pkr").each(function () {
                            if (this.value.indexOf(',') > -1) {

                                contract_amount_pkrs += parseFloat(this.value.split(",").join(""));

                            } else if (!isNaN(this.value) && this.value.length != 0) {
                                contract_amount_pkrs += parseFloat(this.value);

                            }
                        });
                        $('.contract_total_amount-edit-pkr').val($.number(contract_amount_pkrs, 6));
                        $('.contract_amount_pkr_edit').val($.number(contract_amount_pkrs, 6));
                    }
                }

            }, error: function () {
                calcaulteAmount=0;
                console.log('error');
            }
        });

    });


    var count_i = 1;

    $("#payment_plan_create_button").click(function () {
        var rowCount = $('table#payment_plan_create tr:last').index() + 1;
        var contract_amount = parseFloat($('.contract_amount').val().split(",").join(""));
        var contract_total_amount = parseFloat($('.contract_total_amount').val().split(",").join(""));

        if ((contract_amount == 0) && (contract_total_amount == 0)) {
            swal('Kindly Define Contract Properly');
            return false;
        }
        var s = "000" + count_i;
        // return s.substr(s.length-4);
        var digit = s.substr(s.length - 4);
        var markup = "<tr><th>  <input name='checkFlag[]' class='checkFlag' value='0' type='hidden'><input name='record'   type='checkbox'></th><td ><input readonly style='text-align: center' type='text' value='" + digit + "' name='installment_no[]' class='installment_no form-control'></td><td  <td style='text-align: center'>   <input placeholder='dd-mm-yy' type='text' class='form-control contract_payment_date-" + count_i + "'  id='contract_payment_date-" + count_i + "'' data-target='#contract_payment_date-" + count_i + "' name='contract_payment_date[]' style='text-align: center' autocomplete='off' required></td> <td ><input type='text' name='percentage[]' class='form-control percentage' style='text-align: center' value='0.00%' ></td><td ><input type='text' name='paymentAmountpkr[]' class='paymentAmount_pkr form-control payment_amount_two_digit_pkr number_formated_two text-right' required ></td><td ><input type='text' name='paymentAmount[]' class='paymentAmount form-control payment_amount_two_digit text-right' required  ></td></tr>";


        $("#payment_plan_create").append(markup);
        $('#contract_payment_date-' + count_i).datepicker({

            format: "dd-M-yyyy", autoclose: true
            ,
            startDate: "01-01-1947"
        });
        count_i++;
        $('.number_formated_two').number(true, 6);
    });


    $("#payment_plan_edit_button").click(function () {
        var rowCount = $('table#payment_plan_edit tr:last').index() + 1;
        var contract_amount = parseFloat($('.contract_amount_edit').val().split(",").join(""));
        var contract_total_amount = parseFloat($('.contract_total_amount-edit').val().split(",").join(""));

        if (contract_amount !== contract_total_amount) {
            swal('Kindly Define Contract Properly');
            return false;
        }
        console.log(rowCount);
        console.log($("table#payment_plan_edit").find("tr").last().find('counter').val());
        var counter = $("#payment_plan_edit  >tbody tr:last input.counter").val();

        if (isNaN(counter)) {
            counter = 0;
        }
        parseInt(++counter);
        var s = "000" + counter;

        // return s.substr(s.length-4);
        var digit = s.substr(s.length - 4);
        var markup = "<tr><th>   <input name='checkFlag[]' class='checkFlag' type='hidden' value='1'><input name='counter[]' value='" + counter + "'  class='counter' type='hidden'><input name='record'   type='checkbox'></th><td ><input readonly style='text-align: center' type='text' value='" + digit + "' name='installment_no[]' class='installment_no form-control'></td><td  <td style='text-align: center'> <input placeholder='dd-mm-yy' type='text' class='form-control contract_payment_date-" + counter + "'  required id='contract_payment_date-" + counter + "' name='contract_payment_date[]'  style='text-align: center' required></td> <td ><input type='text' value='0.00%' name='percentage[]' class='form-control percentage_edit' style='text-align: center' ></td><td ><input type='text' name='paymentAmountPkr[]' class= 'payment_amount_two_digit paymentAmount_edit_pkr   form-control text-right'  value='0.00' required></td><td ><input type='text' name='paymentAmount_edit[]' class= 'payment_amount_two_digit paymentAmount_edit  form-control text-right'  value='0.00' required></td></tr>";


        $(".payment_plan_edit").append(markup);
        $('#contract_payment_date-' + counter).datepicker({

            format: "dd-M-yyyy", autoclose: true
            ,
            startDate: "01-01-1947"
        });
        i++;

    });
    //payment child row delete

    $(".delete-row-payment-plan").click(function () {
        $("#payment_plan_create").find('input[name="record"]').each(function () {
            if ($(this).is(":checked")) {
                $(this).parents("tr").remove();
            }

        });
        // var rowCount = $('table#payment_plan_create tr:last').index() + 1;
        // if (rowCount === 1) {
        //     i = 1;
        //     $('.total_payment_percentage').val(0 + "%");
        //     // $('.total_payment_amount').val(0.00)
        // }
        calc_total_payment_amount();
        calc_total_payment_amount_pkr();

        total_payment_percentage();
    });

    $(".delete-row-payment-plan_edit").click(function () {
        $("#payment_plan_edit").find('input[name="record"]').each(function () {
            if ($(this).is(":checked")) {
                $(this).parents("tr").remove();
            }

        });
        var rowCount = $('table#payment_plan_edit tr:last').index() + 1;
        // if (rowCount === 1) {
        //     i = 1;
        //     $('.total_payment_percentage_edit').val(0 + "%");
        //     // $('.total_payment_amount_edit').val(0.00)
        // }
        calc_total_payment_amount_edit();
        calc_total_payment_amount_edit_pkr();
        total_payment_percentage_edit();
    });

    function total_payment_percentage() {
        var sum = 0;
        var someVariable = 0;

        $(".percentage").each(function () {

            sum += parseFloat(this.value.split(",").join("").replace('%', ''));

        });

        $('.total_payment_percentage').val($.number(sum, 2) + '%');

    }
    $('#contract_payment_date-' + 0).datepicker({

        format: "dd-M-yyyy", autoclose: true
        ,
        startDate: "01-01-1947"
    });
    function total_payment_percentage_edit() {
        var sum = 0;
        var someVariable = 1;

        $(".percentage_edit").each(function (index, item) {


            sum += parseFloat(this.value.split(",").join("").replace('%', ''));
            $('#contract_payment_date-' + ++index).datepicker({

                format: "dd-M-yyyy", autoclose: true
                ,
                startDate: "01-01-1947"
            });

            $('#retain_file_date_edit-' + ++index).datepicker({

                format: "dd-M-yyyy", autoclose: true
                ,
                startDate: "01-01-1947"
            });

        });

        $('.total_payment_percentage_edit').val($.number(sum, 2) + '%');
    }

    function calc_total_payment_amount_pkr() {


        var sum = 0;
        $(".paymentAmount").each(function () {
            if (this.value.indexOf(',') > -1) {

                sum += parseFloat(this.value.split(",").join(""));

            } else if (!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);

            }
        });
        $('.total_payment_amount_pkr').val($.number(sum, 6));
    };

    function calc_total_payment_amount_pkr() {


        var sum = 0;
        $(".paymentAmount_pkr ").each(function () {
            if (this.value.indexOf(',') > -1) {

                sum += parseFloat(this.value.split(",").join(""));

            } else if (!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);

            }
        });
        $('.total_payment_amount_pkr').val($.number(sum, 6));

    }

    function calc_total_payment_amount() {


        var sum = 0;
        $(".paymentAmount").each(function () {
            if (this.value.indexOf(',') > -1) {

                sum += parseFloat(this.value.split(",").join(""));

            } else if (!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);

            }
        });
        $('.total_payment_amount').val($.number(sum, 6));

    }


    function calc_total_payment_amount_edit() {


        var sum = 0;
        $(".paymentAmount_edit").each(function () {
            if (this.value.indexOf(',') > -1) {

                sum += parseFloat(this.value.replaceAll(',', ''));

            } else if (!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);

            }
        });
        $('.total_payment_amount_edit').val($.number(sum, 6));

    }

    function calc_total_payment_amount_edit() {


        var sum = 0;
        $(".paymentAmount_edit").each(function () {
            if (this.value.indexOf(',') > -1) {

                sum += parseFloat(this.value.replaceAll(',', ''));

            } else if (!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);

            }
        });
        $('.total_payment_amount_edit').val($.number(sum, 6));

    }

    function calc_total_payment_amount_edit_pkr() {


        var sum = 0;
        $(".paymentAmount_edit_pkr").each(function () {
            if (this.value.indexOf(',') > -1) {

                sum += parseFloat(this.value.replaceAll(',', ''));

            } else if (!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);

            }
        });
        $('.total_payment_amount_edit_pkr').val($.number(sum, 6));

    }


    $(document).on('change', '.payment_plan_create', function (event) {
        total_payment_percentage();
        total_payment_percentage();

        calc_total_payment_amount();
        calc_total_payment_amount_pkr();


    });
    $(document).on('change', '.payment_plan_edit', function (event) {
        total_payment_percentage_edit();
        calc_total_payment_amount_edit();
        calc_total_payment_amount_edit_pkr();


    });
    $(document).on('change', '.calc_total_payment_amount_edit_pkr', function (event) {
        total_payment_percentage_edit();
        calc_total_payment_amount_edit();
        calc_total_payment_amount_edit_pkr();


    });

    $(document).on('change', '.percentage', function (event) {
        var row = $(this).closest('tr');
        var divided = 0;
        var getValue = $(this).val();
        var withoutPErctange = getValue.replaceAll('%', '');
        value = withoutPErctange + '%';

        row.find('.percentage').val($.number(withoutPErctange, 2) + '%');
        var paymentAmount = parseFloat($(this).val().split(",").join(""));
        var contract_total_amount = parseFloat($('.contract_total_amount').val().split(",").join(""));
        var contract_total_amount_pkr = parseFloat($('.contract_total_amount_pkr').val().split(",").join(""));
        if (contract_total_amount != 0) {
            row.find('.paymentAmount').val($.number(withoutPErctange * contract_total_amount / 100, 6));

        } else {

            row.find('.paymentAmount').val($.number(withoutPErctange * 1 / 100, 6));
        }


        if (contract_total_amount_pkr != 0) {
            row.find('.paymentAmount_pkr').val($.number(withoutPErctange * contract_total_amount_pkr / 100, 6));

        } else {

            row.find('.paymentAmount_pkr').val($.number(withoutPErctange * 1 / 100, 6));
        }


        total_payment_percentage();
        calc_total_payment_amount();
        if ($('.total_payment_percentage').val().replace('%', '').replace(',', '') > 100.00) {
            row.find('.percentage ').val(0.00);
            row.find('.paymentAmount ').val(0.00);
            swal('Payment plan amount should match the contract amount.');
            total_payment_percentage();
            calc_total_payment_amount();
        }

    });

    $(document).on('change', '.paymentAmount', function (event) {
        var row = $(this).closest('tr');
        var divided = 0;
        var paymentAmount = parseFloat($(this).val().split(",").join(""));
        var contract_total_amount = parseFloat($('.contract_total_amount').val().split(",").join(""));
        var exchangeRates = parseFloat($('.exchange_rate_amount_text').val());
        if (contract_total_amount != 0) {
            row.find('.percentage').val($.number(paymentAmount / contract_total_amount * 100, 2) + '%');
            row.find('.paymentAmount_pkr').val($.number(paymentAmount * exchangeRates, 6));
        } else {
            row.find('.percentage').val($.number(paymentAmount / 1 * 100, 2) + '%');
        }
        total_payment_percentage();
        calc_total_payment_amount();


        if ($('.total_payment_percentage').val().replace('%', '').replace(',', '') > 100.00) {
            row.find('.percentage ').val(0.00);
            row.find('.paymentAmount ').val(0.00);
            row.find('.paymentAmount_pkr').val(0.00)
            swal('Payment plan amount should match the contract amount.');
            total_payment_percentage();
            calc_total_payment_amount();
        }
    });


    $(document).on('change', '.paymentAmount_edit', function (event) {

        var row = $(this).closest('tr');
        var divided = 0;
        var paymentAmount = parseFloat($(this).val().split(",").join(""));
        var contract_total_amount = parseFloat($('.contract_total_amount-edit').val().split(",").join(""));
        var contract_total_amount_pkr = parseFloat($('.contract_total_amount-edit-pkr').val().split(",").join(""));
        var exchangeRates = parseFloat($('.exchange_rate_amount_text').val());
        if (contract_total_amount != 0) {
            row.find('.percentage_edit').val($.number(paymentAmount / contract_total_amount * 100, 6) + '%');
            row.find('.paymentAmount_edit_pkr').val($.number(paymentAmount * exchangeRates, 6));
        } else {
            row.find('.percentage_edit').val($.number(paymentAmount / 1 * 100, 6) + '%');
        }

        total_payment_percentage_edit();
        calc_total_payment_amount();

        if ($('.total_payment_percentage_edit').val().replace('%', '').replace(',', '') > 100.00) {
            row.find('.percentage_edit').val(0.00 + "%");
            row.find('.paymentAmount_edit').val('0.00');
            row.find('.paymentAmount_edit_pkr').val('0.00');

            swal('Payment plan amount should match the contract amount.');
            total_payment_percentage_edit();
            calc_total_payment_amount_edit();
        }
    });
    $(document).on('change', '.percentage_edit', function (event) {
        var row = $(this).closest('tr');
        var divided = 0;
        var getValue = $(this).val();
        var withoutPErctange = getValue.replaceAll('%', '');
        value = withoutPErctange + '%';

        row.find('.percentage_edit').val(value);
        // var paymentAmount = parseFloat($(this).val().split(",").join(""));
        var contract_total_amount = parseFloat($('.contract_total_amount-edit').val().split(",").join(""));
        var contract_total_amount_edit_pkr = parseFloat($('.contract_total_amount-edit-pkr').val().split(",").join(""));

        if (contract_total_amount != 0) {
            row.find('.paymentAmount_edit').val($.number(withoutPErctange * contract_total_amount / 100, 6));

        } else {

            row.find('.paymentAmount_edit').val($.number(withoutPErctange * 1 / 100, 6));
        }
        if (contract_total_amount_edit_pkr != 0) {
            row.find('.paymentAmount_edit_pkr').val($.number(withoutPErctange * contract_total_amount_edit_pkr / 100, 6));

        } else {

            row.find('.paymentAmount_edit_pkr').val($.number(withoutPErctange * 1 / 100, 6));
        }

        total_payment_percentage_edit();
        calc_total_payment_amount_edit();
        if ($('.total_payment_percentage_edit').val().replace('%', '').replace(',', '') > 100.00) {
            row.find('.percentage_edit').val(0.00);
            row.find('.paymentAmount_edit').val(0.00);
            swal('Payment plan amount should match the contract amount.');
            total_payment_percentage_edit();
            calc_total_payment_amount_edit();
        }

    });

    function contractPaymentAmountCalculatePercent() {
        // payment_plan_edit

        $(".payment_plan_edit > tbody > tr").each(function () {
            var paymentAmount_edit = ($(this).find('.paymentAmount_edit').val());

            var contract_total_amount = parseFloat($('.contract_total_amount-edit').val().split(",").join(""));
            if (contract_total_amount != 0) {
                $(this).find('.percentage_edit').val($.number(paymentAmount_edit / contract_total_amount * 100, 2) + '%');

            } else {

                $(this).find('.percentage_edit').val($.number(paymentAmount_edit / 1 * 100, 2) + '%');
            }

            var percentage_edit = ($(this).find('.percentage_edit').val());
            total_payment_percentage_edit();
            calc_total_payment_amount_edit();
        });
    }

    function contractPaymentAmountCalculatePercentCreate() {
        // payment_plan_edit

        $(".payment_plan_create > tbody > tr").each(function () {
            var paymentAmount_edit = ($(this).find('.paymentAmount').val());

            var contract_total_amount = parseFloat($('.contract_total_amount').val().split(",").join(""));
            if (contract_total_amount != 0) {
                $(this).find('.percentage').val($.number(paymentAmount_edit / contract_total_amount * 100, 2) + '%');

            } else {

                $(this).find('.percentage').val($.number(paymentAmount_edit / 1 * 100, 2) + '%');
            }


            total_payment_percentage();
            calc_total_payment_amount();

        });


    }


    //code for 0.0 pkr ammount

    function checkPKrValue(event) {
        $('.contract_table_dymanic > tbody > tr').each(function (index1) {
            var row = $(this);

            var row_val1 = row.find(".dymanic_contract_amount_pkr").val();

            if ((row_val1 === 0) || (parseFloat(row_val1) === 0.00) || (row_val1 == '')) {


                //                row.find(".dymanic_contract_amount_pkr").focus().css('background', '#B22222');
                event.preventDefault();
                return false;
            }
        });
        // return false
    }

    function checkPKrValueEdit(event) {
        $('.contract_table_dymanic_edit >tbody >tr').each(function (index1) {
            var row = $(this);

            var row_val1 = row.find(".contract_province_budget_edit_pkr").val();

            if ((parseFloat(row_val1) === 0) || (parseFloat(row_val1) === 0.00) || (row_val1 === '')) {


                row.find(".contract_province_budget_edit_pkr").focus().css('background', 'red');
                event.preventDefault();
                return false;
            }
        });
        // return false
    }


    function checkPKrValuePayment(event) {
        $('.payment_plan_create >tbody >tr').each(function (index1) {
            var row = $(this);

            var row_val1 = row.find(".paymentAmount_pkr").val();
            if ((parseFloat(row_val1) === 0) || (parseFloat(row_val1) === 0.00) || (row_val1 === '')) {


                row.find(".paymentAmount_pkr").focus().css('background', '#B22222');
                event.preventDefault();
                return false;
            }
        });
        // return false
    }


    function checkPKrValueEditPayment(event) {
        $('.payment_plan_edit >tbody >tr').each(function (index1) {
            var row = $(this);

            var row_val1 = row.find(".paymentAmount_edit_pkr").val();

            if ((parseFloat(row_val1) === 0) || (parseFloat(row_val1) == 0.00) || (row_val1 === '')) {

                row.find(".paymentAmount_edit_pkr").focus().css('background', 'red');
                event.preventDefault();

                return false;

            }
        });
        // return false
    }

    //    function myFunction(x) {
    //
    //        $('.my_custom_focus').val('');
    //       console.log('test');
    //    }


    //    currency change module


    $(document).on('change', '.currency_id_from', function () {

        $.ajax({
            url: "{{ route('currency.exchangerate.show.from') }}",
            method: 'get',
            data: {
                id: $('.currency_id_from').val(),

            }, success: function (response) {

                console.log(response);
                if (typeof response.currency_code === null) {
                    $('.currency_change_from').text('__');
                    $('.exchange_rate_amount_val').val(0);
                    $('.exchange_rate_flag').val(false);
                    $('.exchange_rate_id').val(null);
                    $('.exchange_rate_date').val(null);
                }
                else {
                    $('.currency_change_from').text(response.currency.currency_code);
                    $('.exchange_rate_amount_val').val(0);
                    $('.exchange_rate_flag').val(false);
                    $('.exchange_rate_id').val(null);
                    $('.exchange_rate_date').val(null);
                }


            }, error: function (response) {
                console.log(response);

            }

        });

    });


    $(document).on('change', '.currency_id_from_edit', function () {

        $.ajax({
            url: "{{ route('currency.exchangerate.show') }}",
            method: 'get',
            data: {
                id: $('.currency_id_from_edit').val(),

            }, success: function (response) {

                console.log(response);
                if (typeof response.currency_code === null) {
                    $('.currency_change_from').text('__');
                    $('.exchange_rate_amount_val').val(0);
                    $('.exchange_rate_flag').val(false);
                    $('.exchange_rate_id').val(null);
                    $('.exchange_rate_date_edit').val(null);
                }
                else {
                    $('.currency_change_from').text(response.currency.currency_code);
                    $('.exchange_rate_amount_val').val(0);
                    $('.exchange_rate_flag').val(false);
                    $('.exchange_rate_id').val(null);
                    $('.exchange_rate_date_edit').val(null);
                }


            }, error: function (response) {
                console.log(response);

            }

        });

    });


//from currency

    $(document).on('change', '.currency_id', function () {

        $.ajax({
            url: "{{ route('currency.exchangerate.show') }}",
            method: 'get',
            data: {
                id: $('.currency_id').val(),

            }, success: function (response) {

                console.log(response);
                if (typeof response.currency_code === null) {
                    $('.currency_change').text('__');
                    $('.exchange_rate_amount_val').val(0);
                    $('.exchange_rate_flag').val(false);
                    $('.exchange_rate_id').val(null);
                    $('.exchange_rate_date').val(null);
                }
                else {
                    $('.currency_change').text(response.currency.currency_code);
                    $('.exchange_rate_amount_val').val(0);
                    $('.exchange_rate_flag').val(false);
                    $('.exchange_rate_id').val(null);
                    $('.exchange_rate_date').val(null);
                }


            }, error: function (response) {
                console.log(response);

            }

        });

    });


    $(document).on('change', '.currency_id_edit', function () {

        $.ajax({
            url: "{{ route('currency.exchangerate.show') }}",
            method: 'get',
            data: {
                id: $('.currency_id_edit').val(),

            }, success: function (response) {

                console.log(response);
                if (typeof response.currency_code === null) {
                    $('.currency_change').text('__');
                    $('.exchange_rate_amount_val').val(0);
                    $('.exchange_rate_flag').val(false);
                    $('.exchange_rate_id').val(null);
                    $('.exchange_rate_date_edit').val(null);
                }
                else {
                    $('.currency_change').text(response.currency.currency_code);
                    $('.exchange_rate_amount_val').val(0);
                    $('.exchange_rate_flag').val(false);
                    $('.exchange_rate_id').val(null);
                    $('.exchange_rate_date_edit').val(null);
                }


            }, error: function (response) {
                console.log(response);

            }

        });

    });






    //contract exhcange form
    $(document).on('click', '.exchangerate_save', function () {

        //Do stuff here
        var current_exchange_rate_currency_from = $('.current_exchange_rate_currency_from option:selected').val();
        var current_exchange_rate_currency_to = $('.current_exchange_rate_currency_to option:selected').val();


        if (current_exchange_rate_currency_from !== '' && current_exchange_rate_currency_to !== '')
            if (current_exchange_rate_currency_from === current_exchange_rate_currency_to) {



            } else {

                var current_exchange_rate_sort_order = $('.current_exchange_rate_sort_order').val();
                var current_exchange_rate_start_date = $('.current_exchange_rate_start_date').val();
                var current_exchange_rate_end_date = $('.current_exchange_rate_end_date').val();
                var current_exchange_rate_exchange_rate_create = $('.current_exchange_rate_exchange_rate_create').val();


                var formData = new FormData();

                formData.append('sort_order', current_exchange_rate_sort_order);
                formData.append('start_date', current_exchange_rate_start_date);
                formData.append('end_date', current_exchange_rate_end_date);
                formData.append('currency_from', current_exchange_rate_currency_from);
                formData.append('currency_to', current_exchange_rate_currency_to);
                formData.append('exchange_rate', current_exchange_rate_exchange_rate_create);
                formData.append('_token', '{{ csrf_token() }}');

                $.ajax({
                    url: "{{route('exchangeRate.contract.store')}}",
                    // Form data
                    data: formData,
                    type: 'POST',
                    contentType: false,
                    dataType: 'json',
                    processData: false,
                    //Options to tell jQuery not to process data or worry about content-type.
                    success: function (data) {
                        console.log(data);
                        if (data.error == null) {
                            console.log(data);
                            $('#exchange_rate_modal').modal('toggle');
                            $(".print-error-msg").css('display', 'none');
                            $('.add_exchange_rate_messege').css('display', 'block').delay(2000).hide(0);
                            console.log(data['data']['currency_to']['id']);
                            $('.currency_id_from').val(data['data']['currency_from']['id']);
                            $('.currency_id').val(data['data']['currency_to']['id']);
                            $('#exchange_rate_date').val(data['start_date']);
//                                $(".exchange_rate_date").datepicker('setDate', data['start_date'],{
//                                    format: "dd-M-yyyy",
//                                    autoclose: true
//                                });
                            $('.currency_id_to').val(data['data']['exchange_rate']);
                            $('.exchange_rate_amount_val').val(data['data']['exchange_rate']);
                            $('.exchange_rate_id').val(data['data']['id']);
                            $('.currency_change').text(data['currency_code']);
                            $('.currency_change_from').text(data['currency_code_from']);
                            $('.exchange_rate_flag').val(true);
                            $('.current_exchange_rate_currency_from option:selected').val('');
                            $('.current_exchange_rate_currency_to option:selected').val('');
                            $('.current_exchange_rate_sort_order').val('');
                            $('.current_exchange_rate_start_date').val('');
                            $('.current_exchange_rate_end_date').val('');
                            $('.current_exchange_rate_exchange_rate_create').val('');


                        } else {
                            console.log(data.error);
                            printErrorMsg_errors(data.error);
                            $('.exchange_rate_flag').val(false);

                        }
                    },
                    error: function (reject) {
                        console.log('reject');
                        if (reject.status == 422) {
                            var errors = $.parseJSON(reject.responseText);
                            console.log(errors);
                            printErrorMsg_errors(errors);
                        }

                    }
                });


            }


    });
    $(document).on('click', '.add_exchange_rate_get_sort_order', function () {
        $.ajax({
            url: "{{route('exchangeRate.sort.order')}}",
            type: 'get',
            success: function (data) {
                $('.current_exchange_rate_sort_order').val(data['data'])

            },
            error: function (reject) {
                console.log('reject');
            }
        });

    });

    $(document).on('click', '.exchangerate_save_edit', function () {

        //Do stuff here
        var current_exchange_rate_currency_from = $('.current_exchange_rate_currency_from option:selected').val();
        var current_exchange_rate_currency_to = $('.current_exchange_rate_currency_to option:selected').val();


        if (current_exchange_rate_currency_from !== '' && current_exchange_rate_currency_to !== '')
            if (current_exchange_rate_currency_from === current_exchange_rate_currency_to) {


                swal("currency cannot be same!");
                $('.exchangerate_currency_from').val('');
                return false;


            } else {

                var current_exchange_rate_sort_order = $('.current_exchange_rate_sort_order').val();
                var current_exchange_rate_start_date = $('.current_exchange_rate_start_date').val();
                var current_exchange_rate_end_date = $('.current_exchange_rate_end_date').val();
                var current_exchange_rate_exchange_rate_create = $('.current_exchange_rate_exchange_rate_create').val();


                var formData = new FormData();

                formData.append('sort_order', current_exchange_rate_sort_order);
                formData.append('start_date', current_exchange_rate_start_date);
                formData.append('end_date', current_exchange_rate_end_date);
                formData.append('currency_from', current_exchange_rate_currency_from);
                formData.append('currency_to', current_exchange_rate_currency_to);
                formData.append('exchange_rate', current_exchange_rate_exchange_rate_create);
                formData.append('_token', '{{ csrf_token() }}');

                $.ajax({

                    url: "{{route('exchangeRate.contract.store')}}",

                    // Form data
                    data: formData,
                    type: 'POST',
                    contentType: false,
                    dataType: 'json',

                    processData: false,
                    //Options to tell jQuery not to process data or worry about content-type.

                    success: function (data) {
                        console.log(data);
                        if (data.error == null) {

                            $('#exchange_rate_modal_edit').modal('toggle');
                            $(".print-error-msg").css('display', 'none');
                            $('.add_exchange_rate_messege').css('display', 'block').delay(2000).hide(0);

                            $('.currency_id_from_edit').val(data['data']['currency_from']);
                            $('.currency_id_edit').val(data['data']['currency_to']['id']);
                            $('.exchange_rate_date').val(data['start_date']);

                            $('.currency_id_to').val(data['data']['exchange_rate']);
                            $('.exchange_rate_amount_val').val(data['data']['exchange_rate']);
                            $('.exchange_rate_id').val(data['data']['id']);
                            $('.currency_change').text(data['currency_code']);
                            $('.exchange_rate_flag').val(true);
                            $('.current_exchange_rate_currency_from option:selected').val('');
                            $('.current_exchange_rate_currency_to option:selected').val('');
                            $('.current_exchange_rate_sort_order').val('');
                            $('.current_exchange_rate_start_date').val('');
                            $('.current_exchange_rate_end_date').val('');
                            $('.current_exchange_rate_exchange_rate_create').val('');

                        } else {
                            console.log(data.error);
                            printErrorMsg_errors(data.error);
                            $('.exchange_rate_flag').val(false);

                        }
                    },
                    error: function (reject) {
                        console.log('reject');
                        if (reject.status == 422) {
                            var errors = $.parseJSON(reject.responseText);
                            console.log(errors);
                            printErrorMsg_errors(errors);
                        }

                    }
                });


            }


    });
    function printErrorMsg(msg) {
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display', 'block');
        $.each(msg, function (key, value) {
            $(".print-error-msg").find("ul").append('<li>' + value + '</li>');
        });
    }
    function printErrorMsg_errors(msg) {
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display', 'block');
        $.each(msg.errors, function (key, value) {

            $(".print-error-msg").find("ul").append('<li>' + value + '</li>');
        });
    }


</script>
