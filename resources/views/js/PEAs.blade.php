<script>
    $('.PEAs_modal_edit').on('click', function () {
        $.ajax({
            type: 'get',
            url: "{{ route('PEAs.edit') }}",
            data: {id: $(this).attr("data-id")}, //Add request data
            dataType: 'json',
            success: function (data) {
                $('.pea_id').val(data.id);
                $('.pea_name').val(data.pea_name);
                $('.province_id').val(data.province_id);
                $('.pea_description').val(data.pea_description);
                $('#pea_sort_order').val(data.sort_order);
            }, error: function () {
                $('.pea_id').val('');
                $('.pea_name').val('');
                $('.province_id').val('');
                $('.pea_description').val('');
                $('#pea_sort_order').val('');
                console.log('error');
            }
        });
        //edit pea contact info data
       $("#TextBoxContainerEdit").empty();
        $.ajax({

            type: 'get',
            url: "{{ route('PEAs.editContactInfo') }}",
            data: {id: $(this).attr("data-id")}, //Add request data
            dataType: 'json',
            success: function (data) {
                var html="";
            $.each(data, function(index) {
                       html+='<tr><td><input name = "check" type="checkbox"  /></td>' +'<td><input name = "contact_name_edit[]" placeholder="Name" type="text" value = "' +data[index].contact_name + '" class="form-control" required/></td><td><input name = "contact_designation_edit[]" placeholder="Designation" value = "' +data[index].contact_designation + '" type="text" class="form-control" /></td><td><input placeholder="Enter  Role" value = "' +data[index].contact_role + '" name = "contact_role_edit[]" type="text" class="form-control" /></td><td><input value = "' +data[index].contact_email + '" placeholder="Enter email" name = "contact_email_edit[]" type="email"  class="form-control" /></td><td><input placeholder="Enter Mobile" name = "contact_mobile_no_edit[]" value = "' +data[index].contact_mobile_no + '" type="text"  class="form-control" /></td><td><input placeholder="Enter Landline" value = "' +data[index].contact_landline + '" name = "contact_landline_edit[]" type="text" class="form-control" /></td></tr>';
                    });
                $("#TextBoxContainerEdit").append(html);
            }, error: function () {
                console.log('error');
            }
        });
    });

    $('.edit_peas_save_modal').on('click',function(){
        var contact_name = $('input[name="contact_name_edit[]"]').map(function(){
                return this.value;
            }).get();
        var contact_designation = $('input[name="contact_designation_edit[]"]').map(function(){
                return this.value;
            }).get();
        var contact_role = $('input[name="contact_role_edit[]"]').map(function(){
                return this.value;
            }).get();
        var contact_email = $('input[name="contact_email_edit[]"]').map(function(){
                return this.value;
            }).get();
        var contact_mobile_no = $('input[name="contact_mobile_no_edit[]"]').map(function(){
                return this.value;
            }).get();
        var contact_landline = $('input[name="contact_landline_edit[]"]').map(function(){
                return this.value;
            }).get();


    $.ajax({
        type: 'post',
        url: "{{ route('PEAs.update') }}",
        data: {
            _token: '{{ csrf_token() }}',
                pea_id: $('.pea_id').val(),
                pea_name: $('#edit_pea_name').val(),
                province_id: $('.province_id').val(),
                pea_description: $('#edit_pea_description').val(),
                    sort_order: $('#pea_sort_order').val(),
                'contact_name[]':contact_name,
                'contact_designation[]':contact_designation,
                'contact_role[]':contact_role,
                'contact_email[]':contact_email,
                'contact_mobile_no[]':contact_mobile_no,
                'contact_landline[]':contact_landline,

        },
        success: function (data) {
            window.location.reload();
        }, 
        error: function (err) {
            if(err.status === 422 ) {
                showValidationAlertMessage(err);
            } else {
                swal({
                  title: "Error",
                  icon: "error",
                  text: 'Something went wrong',
                });
            }
        }
    });
});
</script>
