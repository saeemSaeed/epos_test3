<script>
    $('.getProvinceRegion').on('click', function () {

        $.ajax({
            type: 'get',
            url: "{{ route('measure.edit') }}",
            data: { id:$(this).attr("data-id") }, //Add request data
            dataType: 'json',
            success: function (data) {
                console.log(data);

                $('.measure_id').val(data.id);
                $('.measure_name').val(data.name);
                $('.measure_code').val(data.code);
                $('.sort_order').val(data.sort_order);

            }, error: function () {
                $('.measure_id').val('');
                $('.measure_name').val('');
                $('.measure_code').val('');
                $('.sort_order').val('');

                console.log('error');
            }
        });

    });

$('.edit_measure_save_modal').on('click',function(){
    $.ajax({
        type: 'post',
        url: "{{ route('measure.update') }}",
        data: {
            _token: '{{ csrf_token() }}',
                measure_id: $('.measure_id').val(),
                name: $('.measure_name').val(),
                code: $('.measure_code').val(),
            sort_order: $('.sort_order').val(),

        },
        success: function (data) {
            window.location.reload();
        }, 
        error: function (err) {
            if(err.status === 422 ) {
                showValidationAlertMessage(err);
            } else {
                swal({
                  title: "Error",
                  icon: "error",
                  text: 'Something went wrong',
                });
            }
        }
    });
});


</script>
