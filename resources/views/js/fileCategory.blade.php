
<script>


    $('.getFileCategory').on('click', function () {
        $.ajax({
            type: 'get',
            url: "{{ route('filecategory.edit') }}",
            data: {id: $(this).attr("data-id")}, //Add request data
            dataType: 'json',
            success: function (data) {
              console.log(data);
                $('.fileCategory_id').val(data.id);
                $('.fileCategory_title').val(data.title);
            }, error: function () {
                $('.fileCategory_id').val('');
                $('.fileCategory_title').val('');
                console.log('error');
            }
        });
    });

$('.save_fileCategory').on('click',function(){
    $.ajax({
        type: 'post',
        url: "{{ route('filecategory.update') }}",
        data: {
            _token: '{{ csrf_token() }}',
            title: $('.fileCategory_title').val(),
            id: $('.fileCategory_id').val(),
        }, 
        success: function (data) {
            console.log(data);
            if($.isEmptyObject(data.errors)){
                   window.location.reload();
            }else{
                printErrorMsg(data.error);
            }
        }, error: function (reject) {
            console.log(reject);
                if( reject.status === 422 ) {
                    var errors = $.parseJSON(reject.responseText);
                         printErrorMsg(errors);
                }
            }
    });
});
function printErrorMsg (msg) {
    $(".print-error-msg").find("ul").html('');
    $(".print-error-msg").css('display','block');
    $.each( msg.errors, function( key, value ) {
        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
    });
}
</script>