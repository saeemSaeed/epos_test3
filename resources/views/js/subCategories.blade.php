<script>
    $('.sub_category_edit').on('click', function () {
        $.ajax({
            type: 'get',
            url: "{{ route('sub-categories.edit') }}",
            data: {id: $(this).attr("data-id")}, //Add request data
            dataType: 'json',
            success: function (data) {
                $('.sub_category_id').val(data.id);
                $('.sub_category_measure_id').val(data.has_measure ? data.has_measure.id : null);
                $('.sub_category_name').val(data.name);
                $('.sequence_number').val(data.sequence_number);

            }, error: function () {
                $('.sub_category_id').val('');
                $('.sub_category_measure_id').val('');
                $('.sub_category_name').val('');
                $('.sequence_number').val('');
            }
        });

    });

    $('.edit_subCategory_save_modal').on('click',function(){
    $.ajax({
        type: 'post',
        url: "{{ route('sub-categories.update') }}",
        data: {
            _token: '{{ csrf_token() }}',
            sub_category_id: $('.sub_category_id').val(),
            measure_id: $('#edit_measure_id').val(),
            name: $('#edit_sub_category_name').val(),
            sequence_number: $('.sequence_number').val(),
        },
        success: function (data) {
            window.location.reload();
        }, 
        error: function (err) {
            if(err.status === 422 ) {
                showValidationAlertMessage(err);
            } else {
                swal({
                  title: "Error",
                  icon: "error",
                  text: 'Something went wrong',
                });
            }
        }
    });
});
function printErrorMsg (msg) {
    $(".print-error-msg").find("ul").html('');
    $(".print-error-msg").css('display','block');
    $.each( msg.errors, function( key, value ) {
        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
    });
}
</script>