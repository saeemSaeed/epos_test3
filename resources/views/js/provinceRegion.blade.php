<script>
    $('.getProvinceRegion').on('click', function () {
        var row = $(this).closest('tr').attr('data-id');

        $.ajax({
            type: 'get',
            url: "{{ route('province-region.edit') }}",
            data: {id: row}, //Add request data
            dataType: 'json',
            success: function (data) {
                console.log(data);


                $('.province_region_id').val(data.id);
                $('.province_region_name').val(data.name);
                $('.province_region_short_code').val(data.short_code);
                $('.province_region_detail').val(data.detail);
                $('.province_region_sort_order').val(data.sort_order);
            }, error: function () {
                $('.province_region_id').val('');
                $('.province_region_name').val('');
                $('.province_region_short_code').val('');
                $('.province_region_detail').val('');
                $('.province_region_sort_order').val('');
                console.log('error');
            }
        });

    });

    $('.edit_province_region_save_modal').on('click', function () {
        $.ajax({
            type: 'post',
            url: "{{ route('province-region.update') }}",
            data: {
                _token: '{{ csrf_token() }}',
                province_region_id: $('.province_region_id').val(),
                name: $('.province_region_name').val(),
                short_code: $('.province_region_short_code').val(),
                detail: $('.province_region_detail').val(),
                sort_order: $('.province_region_sort_order').val(),
            },
            success: function (data) {
                window.location.reload();
            }, 
            error: function (err) {
                if(err.status === 422 ) {
                    showValidationAlertMessage(err);
                } else {
                    swal({
                      title: "Error",
                      icon: "error",
                      text: 'Something went wrong',
                    });
                }
            }
        });
    });
</script>
