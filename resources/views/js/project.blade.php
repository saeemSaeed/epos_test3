<script>
    $(document).ready(function () {


        $('.project_budget').on('change', function () {

            $('.project_budget').val($.number($(this).val(), 6));
            $('.total_budget').val($.number($(this).val(), 6));
            calc_total();
            calc_revised_total();


        });
        $('#projectFormEdit').submit(function () {

                var project_budget = parseFloat($('.project_budget').val().split(",").join(""));
                var province_total_amount = parseFloat($('.province_total_amount').val().split(",").join(""));
                if (!highlightDuplicates()) {
                    swal("Please select unqiue Measure And Sub-category");
                    return false;
                }

                if (project_budget === 0) {
                    swal("Please Enter Project Budget");
                    return false;
                }
                var check_is_approved = $('.is_approved').val();

                if (check_is_approved!=1) {

                    if (project_budget === province_total_amount) {

                        return true;
                    }
                    // else if (project_budget !== province_total_amount) {
                    //     swal("Project total budget and project budget is not matching, please correct calculations!");
                    //     return false;
                    // }
                    else {
         swal("Project measures amount doesn't match the Project budget, please correct calculations!");
                        return false;
                    }
                }

                var bugdet_revised = parseFloat($('.bugdet_revised').val().split(",").join(""));
                var revised_total_amount = parseFloat($('.revised_total_amount').val().split(",").join(""));

                if (bugdet_revised == revised_total_amount) {
                    return true;
                } else {
                    swal("Project measures revised amount doesn't match the Project budget, please correct calculations!");
                    swal("Warning", `Project measures revised amount ${bugdet_revised} doesn't match the Project budget ${revised_total_amount}. (Difference: ${bugdet_revised - revised_total_amount})`, "warning");
                    return false;
                }


            }
        );
        $('#projectForm').submit(function () {

            var project_budget = parseFloat($('.project_budget').val().split(",").join(""));
            province_total_amount = parseFloat($('.province_total_amount').val().split(",").join(""));
            if (!highlightDuplicatescreate()) {
                swal("Warning", "Please select unqiue Measure And Sub-category", "warning");
                return false;
            }
            if (project_budget === 0) {
                swal("Warning", "Please Enter Project Budget", "warning");
                return false;
            }
            if (project_budget === province_total_amount) {
                return true;
            }
            // else if (project_budget !== province_total_amount) {
            //     swal("Project total budget and project budget is not matching, please correct calculations!");
            //     return false;
            // }
            else {
                 swal("Warning", `Project measures amount doesn't match the Project budget of ${project_budget}.`, "warning");
                return false;
            }


        });

        $(document).on('change', '.project_province_select', function (event) {


            var provinceSelect = $(this).val();
//            $('.project_province_select_hidden').val($(this).val());
            $.ajax({
                url: "{{ route('constant.pea.find') }}",
                method: 'get',
                data: {
                    id: provinceSelect,
                },

                success: function (response) {
                    console.log(response);


                    $html_pea = "<option value=''>Select</option>";
                    $.each(response.provinceRegion, function (index, val) {

                        $html_pea += "<option value='" + val['id'] + "'>" + val['pea_name'] + "</option>";

                    });

                    $('.project_province_pea').html($html_pea);
                }
            });


        });
        $(document).on('change', '.measureSelect', function (event) {

            var row = $(this).closest('tr');
            var pay = row.find('.measureSelect').children("option:selected").val();

            $.ajax({
                url: "{{ route('constant.sub-cat.option') }}",
                method: 'get',
                data: {
                    id: pay,
                },

                success: function (response) {
                    $html = '';
                    if (response.showAllSubCategoriesOfmeasure.length === 0) {
                        $html += "<option value=''>Not Applicable</option>";
                    } else {
                        $html = '';
                        $html += "<option value=''>Select</option>";

                        $.each(response.showAllSubCategoriesOfmeasure, function (index, val) {

                            $html += "<option value='" + val['id'] + "'>" + val['name'] + "</option>";

                        });
                    }
                    row.find('.sub_cate').html($html);

//
//                        $html_pea = "";
//                        $.each(response.provinceRegion, function (index, val) {
//
//                            $html_pea += "<option value='" + val['id'] + "'>" + val['pea_name'] + "</option>";
//
//                        });
//
//                        row.find('.peas_select').html($html_pea);


//                        $('.measure').html($html);
                }
            });


        });


        var measure = {!! \App\Helpers\Helper::MeasureSelect()!!}

        $(document).on('change', '.provinceReviseAmount', function () {
            calc_total();
            calc_revised_total();

            $get_table_row_col = $(this).closest('tr').find('.provinceReviseAmount');

            current_table_row = $(this).closest('tr');

            if (checkBudgetRevised() === true) { // check if budget and measures are same
                calc_total();
                calc_revised_total();

                checkForContractRemaingAmount(current_table_row);

            } else {
                checkBudgeEdit();
                $get_table_row_col.val(0.00);
                console.log($get_table_row_col.val(0.00));
                calc_total();
                calc_revised_total();
//                checkBudgeEdit();
                $get_table_row_col.focus();


            }


        });

        function checkForContractRemaingAmount(row) {
            var project_id = $('.project_id').val();
            var sub_cate = row.find('.sub_cate').val();
            var measureSelect = row.find('.measureSelect').val();
            var provinceReviseAmount = row.find('.provinceReviseAmount').val();

            console.log({project_id, sub_cate, measureSelect, provinceReviseAmount});

            $.ajax({
                url: "{{ route('project.check.defining.budget.contract') }}",
                method: 'get',
                data: {
                    project_id: project_id,
                    sub_cate: sub_cate,
                    measureSelect: measureSelect,
                    provinceReviseAmount: provinceReviseAmount
                },
                success: function (response) {                
                    if (!response.flag) {
                        swal('Warning', response.message, 'warning');
                        row.find('.provinceReviseAmount').val(0.00);
                        return false;
                    } else {
                        $.notify('Amount added successfully!', 'success')
                    }
                    
                }, error: function () {
                    alert('Something went wrong.')
                }
            });
        }


        $(document).on('change', '.provinceBudgetAmount', function () {
            $(this).val($.number($(this).val(), 6));

            calc_total();
            calc_revised_total();

            $get_table_row_col = $(this).closest('tr').find('.provinceBudgetAmount');
            if (checkBudgeEdit() === true) {
                // checkBudgeEdit();
                calc_total();
                calc_revised_total();
                checkBudgeEdit();
            } else {
                // checkBudgeEdit();
                $get_table_row_col.val(0.00);
                console.log($get_table_row_col.val(0.00));
                calc_total();
                calc_revised_total();
                checkBudgeEdit();
                $get_table_row_col.focus();
            }
//            console.log($get_table_row_col.val());


        });
        $(document).on('change', '.provinceReviseAmount', function () {
            $(this).val($.number($(this).val(), 6));
            calc_total();
            calc_revised_total();

        });

        $(".add-row").click(function () {
            if (highlightDuplicatescreate()) {


                $('.delete-row_project_province').css('display', 'block');
                $('.province_total_amount_hide').css('display', 'block');
                $('.revised_total_amount_hide').css('display', 'block');
                var name = $("#name").val();
                var email = $("#email").val();
                var markup = `
                <tr>
                    <td>
                        <input name='row_count[]' type='hidden'>
                        <input type='checkbox' name='record'>
                    </td>
                    <td>${measure}</td>
                    <td>
                        <select class='sub_cate form-control' name='sub_cate[]'>
                            <option value=''>Select</option>
                        </select>
                    </td>
                    <td>
                        <input type='text' value='0.00' name='amount[]' class='form-control text-right provinceBudgetAmount number_formated_two' oninput='${this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1')}' style='background-color: #FFCCCB;'>
                    </td>
                    <td>
                        <input type='text' name='descriptionHasProvince[]' class='form-control'>
                    </td>
                </tr>`;
                $(".project_province").append(markup);
            } else {
                swal('Warning', 'Please select unqiue Measure & Sub-Category', 'warning');
            }
        });

        function checkBudgeEdit() {
            project_budget = parseFloat($('.project_budget').val().split(",").join(""));
            province_total_amount = parseFloat($('.province_total_amount').val().split(",").join(""));
            if (project_budget < province_total_amount) {

                swal("Warning", `Total Budget amount ${province_total_amount} exceed the Project Budget ${project_budget}. Exceeding amount ${project_budget - province_total_amount}.`, 'warning');
                return false;
            }
            return true;
        }

        function checkBudgetRevised() {
            project_budget = parseFloat($('.bugdet_revised').val().split(",").join(""));
            province_total_amount = parseFloat($('.revised_total_amount ').val().split(",").join(""));

            if (project_budget < province_total_amount) {
                swal("Warning", `Total Budget amount ${province_total_amount} exceeds the Project Budget ${project_budget}. Exceeding amount ${project_budget - province_total_amount}.`, 'warning');
                return false;
            }
            return true;
        }


        function checkBudgeCreate() {
            project_budget = parseFloat($('.project_budget').val().split(",").join(""));
            province_total_amount = parseFloat($('.province_total_amount').val().split(",").join(""));
            if (project_budget < province_total_amount) {
                swal("Warning", `Total Budget amount ${province_total_amount} exceed the Project Budget ${project_budget}. Exceeding amount ${project_budget - province_total_amount}.`, 'warning');
                return false;
            }
            return true;
        }

        calc_total();
        calc_revised_total();


        $(".project_province").on('change', function () {

            $('.project_province tr.duplicate').removeClass();
            highlightDuplicatescreate();

        });


        $(".project_province_edit").on('change', function () {
            $('.project_province_edit tr.duplicate').removeClass();
            highlightDuplicates();

        });


        function highlightDuplicatescreate() {
            $('.project_province >tbody >tr').each(function (index1) {
                var row = $(this)

                var row_val1 = row.find("td:nth-child(2) select").val()
                var row_val2 = row.find("td:nth-child(3) select").val()
                console.log(row);
                $('.project_province >tbody >tr').each(function (index2) {

                    var compare_row = $(this)
                    var compare_row_val1 = compare_row.find("td:nth-child(2) select").val()
                    var compare_row_val2 = compare_row.find("td:nth-child(3) select").val()

                    if (index1 != index2 && row_val1 == compare_row_val1 && row_val2 == compare_row_val2) {

                        row.addClass('duplicate')
                        compare_row.addClass('duplicate')
                    }
                })
            })


            if ($('tr.duplicate').length > 0) {
// swal('Duplicates found')
                return false;
            } else {
                return true;
            }
        }

        function highlightDuplicates() {
            debugger;
            $('.project_province_edit > tbody > tr').each(function (index1) {
                var row = $(this)
                var row_val1 = row.find("td:nth-child(2) select").val()
                var row_val2 = row.find("td:nth-child(3) select").val()
                console.log(row);
                $('.project_province_edit > tbody > tr').each(function (index2) {
                    debugger;
                    var compare_row = $(this)
                    var compare_row_val1 = compare_row.find("td:nth-child(2) select").val()
                    var compare_row_val2 = compare_row.find("td:nth-child(3) select").val()

                    if (index1 != index2 && row_val1 == compare_row_val1 && row_val2 == compare_row_val2) {
                        row.addClass('duplicate')
                        compare_row.addClass('duplicate')
                    }
                })
            })
            debugger;
            if ($('tr.duplicate').length > 0) {
// swal('Duplicates found')
                return false;
            } else {
                return true;
            }
        }

//        $(".project_province").on('change', function () {
//            calc_total();
//            calc_revised_total();
//            checkBudgeCreate();
//        });
//        $(".project_province_edit").on('change', function () {

//            calc_total();
//            calc_revised_total();
//            checkBudgeEdit();
//            if (!checkBudgeEdit()) {
////                $(this).val(0);
//                console.log("$get_table_row_col.val()");
//                console.log($get_table_row_col);
//                console.log($get_table_row_col.val("0.00"));
//                $get_table_row_col.val("0.00");
//                $get_table_row_col=null;
//
////                console.log( $(this).val());
////                console.log( $(this).val("0"));
//                calc_total();
//                calc_revised_total();
//            }

//        });


//        function calc_total() {
//            var sum2 = 0;
//            $(".provinceBudgetAmount").each(function () {
//                //add only if the value is number
//
//                if (this.value.indexOf(',') > -1) {
//
//                    sum2 += parseFloat(this.value.split(",").join(""));
//
//                } else if (!isNaN(this.value) && this.value.length != 0) {
//                    sum2 += parseFloat(this.value);
//
//                }
//            });
//
//            $(".province_total_amount").val($.number(sum2, 2));
//
//        }
//
//
//        function calc_revised_total() {
//            var sum2 = 0;
//            $(".provinceReviseAmount ").each(function () {
//                //add only if the value is number
//
//                if (this.value.indexOf(',') > -1) {
//
//                    sum2 += parseFloat(this.value.split(",").join(""));
//
//                } else if (!isNaN(this.value) && this.value.length != 0) {
//                    sum2 += parseFloat(this.value);
//
//                }
//            });
//
//            $(".revised_total_amount").val($.number(sum2, 2));
//
//        }

        function calc_total() {
            var sum2 = 0;

            $(".provinceBudgetAmount").each(function () {
//add only if the value is number

                if (this.value.indexOf(',') > -1) {

                    sum2 += parseFloat(this.value.split(",").join(""));

                } else if (!isNaN(this.value) && this.value.length != 0) {
                    sum2 += parseFloat(this.value);

                }
            });
            project_budget = parseFloat($('.project_budget').val().split(",").join(""));
            var unAllocatedAmount = project_budget - sum2;
            $(".province_total_amount").val($.number(sum2, 6));
            $(".province_unallocated_amount").val($.number(unAllocatedAmount, 6));
            $(".province_total_budget_amount").val($.number((sum2 + unAllocatedAmount), 6));


        }


        function calc_revised_total() {
            var sum2 = 0;

            $(".provinceReviseAmount").each(function ($index) {
                if (this.value.indexOf(',') > -1) {
                    sum2 += parseFloat(this.value.split(",").join(""));
                } else if (!isNaN(this.value) && this.value.length != 0) {
                    sum2 += parseFloat(this.value);
                }
            });

            var project_budget = parseFloat($('.project_budget').val().split(",").join(""));


            var unacllocatedRevisedBudget = project_budget - sum2;

            $(".revised_total_amount").val($.number(sum2, 6));
            $(".province_revised_unallocated_amount").val($.number(unacllocatedRevisedBudget, 6));
            var grandTotal = sum2 + unacllocatedRevisedBudget
            $(".province_revised_total_budget_amount").val($.number(grandTotal, 6));


        }

//        function calc_total() {
//            var sum = 0;
//            $(".provinceBudgetAmount").each(function () {
//                swal(parseFloat($.number($(this).val(),2)));
//                sum += parseFloat($(this).val() ? $(this).val(1) : 0);
//            });
//
//            $('.province_total_amount').val($.number(parseFloat(sum),2));
//        }

        //            province_total_amount

        // Find and remove selected table rows
        $(".delete-row_project_province").click(function () {
            $(".project_province").find('input[name="record"]').each(function () {
                if ($(this).is(":checked")) {
                    $(this).parents("tr").remove();
                }

            });
            calc_total();
            calc_revised_total();
        });


        $(".add-row-edit").click(function () {
            if (highlightDuplicates()) {


                var name = $("#name").val();
                var email = $("#email").val();
                var markup = "<tr><td><input type='checkbox'  name='record'></td><td>" + measure + "</td><td><select class='sub_cate form-control' id='sub_cate' name='sub_cate[]' style=''><option value=''>Select </option></select></td><td><input type='text' value='0.00' name='provinceBudgetAmount[]' class='form-control provinceBudgetAmount number_formated_two'  dir='rtl'  oninput='this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');'   style='background-color: #FFCCCB;' ></td><td><input readonly type='text' name='revise_amount[]' value='0.00' class='form-control provinceReviseAmount number_formated_two'  dir='rtl'  oninput='this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');'   style='background-color: #e9ecef;'></td><td><input type='text' name='descriptionHasProvince[]' class='form-control'  ></td></tr>";
            } else {
                swal('Please select unqiue Measure And Sub-category');
            }
            $(".project_province_edit").append(markup);


            var countRow = $('.project_province_edit >tbody >tr').length;
            if (countRow > 0) {
                $(".project_province_select").attr("style", "pointer-events: none;").css('background-color', '#FFCCCB');
                $(".project_province_pea").attr("style", "pointer-events: none;").css('background-color', '#FFCCCB');
            } else {
                $(".project_province_select").attr("style", "pointer-events: ;").css('background-color', '#FFCCCB');
                $(".project_province_pea").attr("style", "pointer-events: ;").css('background-color', '#FFCCCB');
            }
        });





        $(".add-row-edit-revised").click(function () {
            var name = $("#name").val();
            var email = $("#email").val();
            let attr = $(this).data('status');
            var markup = `
            <tr>
                <td>
                    <input type='checkbox' name='record'>
                </td>
                <td>${measure}</td>
                <td>
                    <select class='sub_cate form-control' id='sub_cate' name='sub_cate[]'>
                        <option value=''>Select</option>
                    </select>
                </td>
                <td>
                    <input type='text' value='0.00' name='provinceBudgetAmount[]' class='text-right form-control provinceBudgetAmount number_formated_two' ${attr ? 'readonly' : ''}  oninput='${this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1')};' style='background-color: #FFCCCB;' >
                </td>

                <td ${attr ? '' : 'hidden'}>
                    <input type='text' name='provinceReviseAmount[]' value='0.00' class='text-right form-control provinceReviseAmount number_formated_two' oninput='${this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1')}' style='background-color: #e9ecef;'>
                </td>

                <td>
                    <input type='text' name='descriptionHasProvince[]' class='form-control'>
                </td>
                
            </tr>`;

            $(".project_province_edit").append(markup);

            var countRow = $('.project_province_edit >tbody >tr').length;

            if (countRow > 0) {
                $(".project_province_select").attr("style", "pointer-events: none;").css('background-color', '#FFCCCB');
                $(".project_province_pea").attr("style", "pointer-events: none;").css('background-color', '#FFCCCB');
            } else {
                $(".project_province_select").attr("style", "pointer-events: ;").css('background-color', '#FFCCCB');
                $(".project_province_pea").attr("style", "pointer-events: ;").css('background-color', '#FFCCCB');
            }
        });

        $(".delete-row_project_province_edit").click(function () {
            var project_id = $('.project_id').val();
           
            $(".project_province_edit").find('input[name="record"]').each(function () {
      
                if ($(this).is(":checked")) {
                    var row = $(this).parents("tr");

                    // var measure_id = $( ".measureSelect option:selected" ).val();
                    var measure_id = $(this).closest("tr").find("#measure option:selected" ).val();
                    var subcategory = $(this).closest("tr").find("#sub_cate option:selected" ).val();

                    $.ajax({
                        type: 'get',
                        url: '/project/delete/child/'+$(this).attr("data-id"),
                        data : {
                            measure_id : measure_id,
                            subcategory_id : subcategory,
                            project_id : project_id,
                        },
                        success: function(data) {

                            console.log(data);
                            var countRow = $('.project_province_edit >tbody >tr').length;
                            if (countRow > 0) {
                                $(".project_province_select").attr("style", "pointer-events: none;").css('background-color', '#FFCCCB');
                                $(".project_province_pea").attr("style", "pointer-events: none;").css('background-color', '#FFCCCB');
                            } else {
                                $(".project_province_select").attr("style", "pointer-events: ;").css('background-color', '#FFCCCB');
                                $(".project_province_pea").attr("style", "pointer-events: ;").css('background-color', '#FFCCCB');
                            }


                            if(data.flag){
                                row.remove();
                                  calc_total();
            calc_revised_total();

                            }else{
                                swal(data.message);
                            }

                        },
                        error: function(data){
                        }
                    });

           


                }

            });

        });


    //file

    //file


    $(".add-project-file-create").click(function () {
//        swal(1);
        $('.delete-project-file').css('display', 'block');
        var markup = "<tr><th><input type='checkbox' style='text-align: center' name='record'></th><th ><input   type='file' style='font-size:12px !important;' name='file[]' ></th><th><select class='form-control category_project'  style='font-size: 14px !important;height: 35px !important;width: 100px!important;' name='category[]' required><option value='select'>Select</option><option value='Contract'>Contract</option><option value='Quotation'>Quotation</option><option value='PO'>PO</option></select></th><th style='text-align:center'><input style='font-size: 14px !important;height: 35px !important;' type='text' class='form-control description_project' name='description[]'></th>    <th style='text-align:center'></th></tr>";
        $(".project_file_create").append(markup);
    });
    $(".delete-project-file").click(function () {
        $(".project_file_create").find('input[name="record"]').each(function () {
            console.log($(this).attr('data-id'));
            if ($(this).is(":checked")) {

                $(this).parents("tr").remove();
            }
        });
    });


    $(".add-project-file_edit").click(function () {
        $('.delete-project-file_edit').css('display', 'block');
        var markup = "<tr><th><input type='checkbox' style='text-align: center' name='record'></th><th ><input   type='file' style='font-size:12px !important;' name='file' ></th><th><select class='form-control category_project'  style='font-size: 14px !important;height: 35px !important;width: 100px!important;' name='category[]' required><option value='select'>Select</option><option value='Contract'>Contract</option><option value='Quotation'>Quotation</option><option value='PO'>PO</option></select></th><th style='text-align:center'><input style='font-size: 14px !important;height: 35px !important;' type='text' class='form-control description_project' name='description[]'></th>    <th style='text-align:center'><button type='button' class='project_add_singe_file' id='project_add_singe_file' style='background: rgba(105,126,255,0.8)'>+</button></th></tr>";
        $(".project_file_edit").append(markup);
    });
    //    $(".delete-project-file").click(function () {
    //
    //        $(".project_file").find('input[name="record"]').each(function () {
    //            console.log($(this).is(":checked"));
    //            if ($(this).is(":checked")) {
    //                swal($(this).attr('data-id'));
    //                console.log($(this).attr('data-id'));
    //                deleteProjectFile($(this).attr('data-id'));
    //                $(this).parents("tr").remove();
    //            }
    //
    //        });
    //    });
    $(".delete-project-file_edit").click(function () {
        $(".project_file_edit").find('input[name="record"]').each(function () {
            console.log($(this).attr('data-id'));
            if ($(this).is(":checked")) {

                deleteProjectFile($(this).attr('data-id'));
                $(this).parents("tr").remove();
            }
        });
    });

    function deleteProjectFile($id) {
        $.ajax({
            url: "{{ route('project.delete.file') }}",
            method: 'get',
            data: {
                id: $id
            },
            success: function (response) {
                console.log(response);
                console.log("hello");
            }, error: function () {
                console.log('error');
            }
        });
    }

    $(document).on('click', '.project_add_singe_file', function (event) {


        var row = $(this).closest('tr');
        var files = row.find("input[type='file']");
        var category = row.find('.category_project').val();
        console.log(category);
        var description = row.find('.description_project').val();

        var formData = new FormData();
        formData.append('file', files[0].files[0]);
        formData.append('category', category);
        formData.append('project_id', $('.project_id').val());
        formData.append('description', description);
        formData.append("_token", "{{ csrf_token() }}");

        $.ajax({
            url: "{{route('project.file.single')}}",

            // Form data
            data: formData,
            type: 'POST',
            //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false,

            success: function (data) {
                console.log('project');
                console.log(data);
                console.log(data['success']['file']);
                console.log(data['success']['category']);
                console.log(data['path']);
                $html = "<tr><td valign='top' style='text-align: center'><input data-id='" + data['success']['id'] + "' type='checkbox' name='record'></td><td valign='top'><a style='float:left' href='" + data['path'] + "' target='_blank' download>" + data['success']['file'] + "    <i class=' fa-lg fas fa-file-download'></i> </a></td><td valign='top' style='text-align: center'><span>" + data['success']['category'] + "</span></td><td valign='top' style='text-align:left'><span>" + data['success']['description'] + "</span></td>    <td valign='top' style='text-align:center'></td></tr>";

                $('.project_file_edit').append($html);
                row.remove();
                console.log('Upload completed');
            },
            error: function (e) {
                console.log(e);
            },
        });
        return false;
    });


        $(".custom-file-input").on("change", function () {

            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
    });
</script>


<script>
    function checkAlreadyExists() {
        event.preventDefault();

        let title = $(event.target);


        title.addClass('loading');


        $.ajax({
            url: `{{ route('project.check.projectTitle') }}`,
            type: 'get',
            data: {
                title: title.val()
            },
            success: response => {
                console.log(response)
            },
            error: err => {
                title.val('')
                console.log(err)
                if(err.status == 409) {
                    swal('Duplication', err.responseJSON.message, 'error')
                }
            },
            complete: () => title.removeClass('loading')
        })
    }
</script>