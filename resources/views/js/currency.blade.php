<script>
    $('.getCurrency').on('click', function () {
        $.ajax({
            type: 'get',
            url: "{{ route('currency.edit') }}",
            data: {id: $(this).attr("data-id")}, //Add request data
            dataType: 'json',
            success: function (data) {
                $('.currency_id').val(data.id);
                $('.currency_name').val(data.currency_name);
                $('.currency_code').val(data.currency_code);
                $('.currency_symbol').val(data.currency_symbol);
                $('.sort_order').val(data.sort_order);


            }, error: function () {
                $('.currency_id').val('');
                $('.currency_name').val('');
                $('.currency_symbol').val('');
                $('.currency_code').val('');
                $('.sort_order').val('');



                console.log('error');
            }
        });

    });


    $('.edit_currency_save_modal').on('click',function(){
        $.ajax({
            type: 'post',
            url: "{{ route('currency.update') }}",
            data: {
                _token: '{{ csrf_token() }}',
                currency_id: $('.currency_id').val(),
                currency_name: $('.currency_name').val(),
                currency_code: $('.currency_code').val(),
                currency_symbol: $('.currency_symbol').val(),
                sort_order: $('.sort_order').val(),
            },
            success: function (data) {
                window.location.reload();
            }, 
            error: function (err) {
                if(err.status === 422 ) {
                    showValidationAlertMessage(err);
                } else {
                    swal({
                      title: "Error",
                      icon: "error",
                      text: 'Something went wrong',
                    });
                }
            }
        });
    });
</script>
