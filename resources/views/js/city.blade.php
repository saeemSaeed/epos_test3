<script>
     $(document).on('click', '.getcity', function (event) {
        // $('.getcity').on('click', function () {
        $.ajax({
            type: 'get',
            url: "{{ route('city.edit') }}",
            data: {id: $(this).attr("data-id")}, //Add request data
            dataType: 'json',
            success: function (data) {
                $('.city_id').val(data.id);
                $('.city_name').val(data.city_name);
                $('.province_id').val(data.has_province.id);
                $('.sort_order').val(data.sort_order);
                $('.short_code').val(data.short_code);
            }, error: function () {
                $('.city_id').val('');
                $('.city_name').val('');
                $('.sort_order').val('');
                $('.short_code').val('');
                
                swal({
                  title: "Error",
                  icon: "error",
                  text: 'Something went wrong',
                });

            }
        });
    });

$('.edit_city').on('click',function(){
    $.ajax({
        type: 'POST',
        url: "{{ route('city.update') }}",
        data: {
            _token: '{{ csrf_token() }}',
            city_name: $('.city_name').val(),
            city_id: $('.city_id').val(),
            province_id: $('.province_id').val(),
            sort_order: $('.sort_order').val(),
            short_code: $('.short_code').val(),
        },
        success: function (data) {
            window.location.reload();
        }, 
        error: function (err) {
            if(err.status === 422 ) {
                showValidationAlertMessage(err);
            } else {
                swal({
                  title: "Error",
                  icon: "error",
                  text: 'Something went wrong',
                });
            }
        }
    });
});
</script>
