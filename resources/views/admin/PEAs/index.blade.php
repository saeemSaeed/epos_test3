@extends('admin.layout.index')
@section('content')
<section class="bg-primary content-header"
    style="background-color:#ffc533 !important; padding-bottom: 20px; margin-top: 20px;">
    <div class="row">
        <div class="col-8">
            <h1>Project Execution Agency</h1>
        </div>
        <div class="col-4">
            <ol class="breadcrumb" style="color:#444;float: right">
                <li>
                    <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                </li>
                <li>
                    <i class="fa fa-angle-right" style="color: #ccc;"></i> Project Execution Agency &nbsp;
                </li>
            </ol>
        </div>
    </div>
</section>
<div class="container">
  <div class="row justify-content-between">
    <div  style="margin-top: 10px; margin-left: 10px">
        <button type="button" class="btn btn-block btn-success btn-flat" data-toggle="modal"
        data-target="#PEAs_modal">
        <i class="fa fa-plus"></i> PEA's
        </button>
    </div>
    @can('project-execution-agency-restore')
    <div style="margin-top: 10px; margin-right: 10px">
        <a href="{{ route('PEAs.Deleted') }}" class="btn btn-block btn-danger btn-flat"><i class="fas fa-trash-alt"></i></a>
    </div>
    @endcan
</div>
</div>
<div class="container-fluid" style="margin-top: 10px">
    <div class="card">
        <div class="card-body">
            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="peas" class="table table-bordered table-hover dataTable dtr-inline peas" role="grid"
                            aria-describedby="example2_info">
                            <thead style="    background-color: #65a3c6;
                                color: #2c2c2c;">
                                <tr role="row">
{{--                                    <th style="text-align: center">ID</th>--}}
                                    <th style="text-align: center;width: 10%">Sort Order</th>
                                    <th>PEA Name</th>
                                    <th>PEA Province/Region</th>
                                    <th>PEA Detail</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($peas as $index=>$pea)
                                <tr role="row" class="odd">
{{--                                    <td style='text-align: center'>{{++$index}}</td>--}}
                                    <td style="text-align: center">{{$pea->sort_order}}</td>
                                    <td>{{$pea->pea_name}}</td>
                                    <td>{{isset($pea->hasProvince->name)?$pea->hasProvince->name:''}}</td>
                                    <td>{{$pea->pea_description}}</td>
                                    <td data-id="{{$pea->id}}" style="text-align: center">
                                        <a data-id="{{$pea->id}}"
                                            class="PEAs_modal_edit"
                                            data-toggle="modal"
                                            data-target="#PEAs_modal_edit"
                                            title="Edit!"><i style="color: black;font-size: 14px!important"
                                        class="fas fa-pencil-alt"></i></a>&nbsp;&nbsp;
                                        <a
                                            onclick="return confirm('Are you sure?')"
                                            data-id="{{$pea->id}}"
                                            href="{{route('PEAs.delete',['id'=>$pea->id])}}"
                                            data-toggle="tooltip"
                                            title="Remove"><i style="color: black;font-size: 14px!important"
                                        class=" fa fa-trash"></i></a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
    @include('admin.PEAs.create')
    @include('admin.PEAs.edit')
    @endsection

    @section('javascript')
<script>
$(function () {
$("#btnAdd").bind("click", function () {
    var div = $("<tr />");
    div.html(GetDynamicTextBox(""));
        $("#TextBoxContainer").append(div);
    });

    $("body").on("click", ".remove", function () {
    $(this).closest("tr").remove();
    });
});
    function GetDynamicTextBox(value) {
    return '<td><input name = "check" type="checkbox" value = "' + value + '" /></td>' +'<td><input name = "contact_name[]" placeholder="Name" type="text" value = "' + value + '" class="form-control" required/></td>' + '<td><input name = "contact_designation[]" placeholder="Designation" type="text" value = "' + value + '" class="form-control" /></td>' + '<td><input placeholder="Enter  Role" name = "contact_role[]" type="text" value = "' + value + '" class="form-control" /></td>' + '<td><input placeholder="Enter email" name = "contact_email[]" type="email" value = "' + value + '" class="form-control" /></td>' +'<td><input placeholder="Enter Mobile" name = "contact_mobile_no[]" type="text" value = "' + value + '" class="form-control" /></td>' + '<td><input placeholder="Enter Landline" name = "contact_landline[]" type="text" value = "' + value + '" class="form-control" /></td>'
    }

$(function () {
$("#btnAddedit").bind("click", function () {
var div = $("<tr />");
    div.html(GetDynamicTextBoxedit(""));
    $("#TextBoxContainerEdit").append(div);
    });
    $("body").on("click", ".remove", function () {
    $(this).closest("tr").remove();
    });
    });
    function GetDynamicTextBoxedit(value) {
    return '<td><input name = "check" type="checkbox" value = "' + value + '" /></td>' +'<td><input name = "contact_name_edit[]" placeholder="Name" type="text" value = "' + value + '" class="form-control" required/></td>' + '<td><input name = "contact_designation_edit[]" placeholder="Designation" type="text" value = "' + value + '" class="form-control" /></td>' + '<td><input placeholder="Enter  Role" name = "contact_role_edit[]" type="text" value = "' + value + '" class="form-control" /></td>' + '<td><input placeholder="Enter email" name = "contact_email_edit[]" type="email" value = "' + value + '" class="form-control" /></td>' +'<td><input placeholder="Enter Mobile" name = "contact_mobile_no_edit[]" type="text" value = "' + value + '" class="form-control" /></td>' + '<td><input placeholder="Enter Landline" name = "contact_landline_edit[]" type="text" value = "' + value + '" class="form-control" /></td>'
    }



    </script>
    @endsection
