<div class="modal fade" id="PEAs_modal_edit">
    <form method="post">
        @csrf
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style=" background-color: #65a3c6;
                    color: #2c2c2c;">
                    <h4 class="modal-title">PEAs Form</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="form-group col-6">
                            <label>Sort Order &nbsp;</label>
                            <input type="text" placeholder="Sort Order"
                                   oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"

                                  id="pea_sort_order" class="pea_sort_order form-control @error('sort_order') is-invalid @enderror" name="sort_order">
                            @error('Sort Order') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group col-6">
                            <label> Name&nbsp;</label>
                            <input type="hidden" name="pea_id" class="form-control pea_id">
                            <input type="text" placeholder="Name" name="pea_name" id="edit_pea_name" class="form-control pea_name @error('pea_name') is-invalid @enderror">
                            @error('pea_name') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group col-6">
                            <label>Province&nbsp;</label>
                            <select name="province_id" class="form-control province_id @error('province_id') is-invalid @enderror" id="province_id">
                                <option value="">Choose an Option</option>
                                @foreach(\App\Helpers\Helper::province() as $province)
                                <option value="{{$province->id}}">{{$province->name}}</option>
                                @endforeach
                            </select>
                            @error('province_id') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <div class="form-group col-6">
                            <label> Description:&nbsp;</label>
                            <textarea type="text" placeholder="Description" name="pea_description" id="edit_pea_description"class="form-control pea_description @error('pea_description') is-invalid @enderror"></textarea>
                            @error('pea_description') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="card card-default collapsed-card">
                        <div class="card-header">
                            <h3 class="card-title">Contact Information</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool " data-card-widget="collapse"><i class="fas fa-plus"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <section class="container">
                                <div class="table table-responsive">
                                    <button id="btnAddedit" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add Row"><i class="fa fa-plus"></i> Add</button>
                                    <button type="button" class="btn btn-danger delete-row"><i class="fa fa-trash"></i></button>
                                    <table class="table table-responsive table-striped table-bordered" style="padding-top:1%">
                                        <thead>
                                            <tr>
                                                <td></td>
                                                <td>Name</td>
                                                <td>Designation</td>
                                                <td>Role</td>
                                                <td>Email</td>
                                                <td>Mobile No</td>
                                                <td>landline</td>
                                            </tr>
                                        </thead>
                                        <tbody id="TextBoxContainerEdit">

                                        </tbody>
                                    </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary edit_peas_save_modal">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">cancel</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </form>
    <!-- /.modal-dialog -->
</div>
