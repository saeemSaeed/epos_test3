@extends('admin.layout.index')
@section('content')
<section class="bg-primary content-header"
    style="background-color:#ffc533 !important; padding-bottom: 20px; margin-top: 20px;">
    <div class="row">
        <div class="col-8">
            <h1>Project Execution Agency</h1>
        </div>
        <div class="col-4">
            <ol class="breadcrumb" style="color:#444;float: right">
                <li>
                    <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                </li>
                <li>
                    <i class="fa fa-angle-right" style="color: #ccc;"></i> Project Execution Agency &nbsp;
                </li>
            </ol>
        </div>
    </div>
</section>

<div class="container-fluid" style="margin-top: 10px">
    <div class="card">
        <div class="card-body">
            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="peas" class="table table-bordered table-hover dataTable dtr-inline peas" role="grid"
                            aria-describedby="example2_info">
                            <thead style="    background-color: #65a3c6;
                                color: #2c2c2c;">
                                <tr role="row">
                                    <th>ID</th>

                                    <th style="text-align: center;width: 10%">Sort Order</th>
                                    <th>PEA Name</th>
                                    <th>PEA Province/Region</th>
                                    <th>PEA Detail</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($deletedPEAs as $index=>$pea)
                                <tr role="row" class="odd">
                                    <td style='text-align: center'>{{++$index}}</td>
                                    <td style="text-align: center">{{$pea->sort_order}}</td>
                                    <td>{{$pea->pea_name}}</td>
                                    <td>{{isset($pea->hasProvince->name)?$pea->hasProvince->name:''}}</td>
                                    <td>{{$pea->pea_description}}</td>
                                    <td class="sorting_1 dtr-control" style="text-align: center!important;">
                                        <a type="submit" style="border: none;background-color: transparent" data-toggle="tooltip" title="" href="{{route('PEAs.Restore',['id'=>$pea->id])}}" data-original-title="Restore"><i class="fas fa-trash-restore"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</div>
@include('admin.PEAs.create')
@include('admin.PEAs.edit')
@endsection
