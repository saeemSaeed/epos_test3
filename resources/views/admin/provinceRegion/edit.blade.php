<div class="modal fade" id="modal-default_edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style=" background-color: #65a3c6;
color: #2c2c2c;">
                <h4 class="modal-title">Province/Region</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger print-error-msg" style="display:none">
                    <ul>
                    </ul>
                </div>
                <form method="post">
                    @csrf
                    <input type="hidden" placeholder="Location Name" class="form-control province_region_id" name="id">
                    <div class="form-group">
                        <label>Sort Order &nbsp;</label>
                        <input type="text" placeholder="Sort Order"
                               oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"

                               class="province_region_sort_order form-control @error('province_region_sort_order') is-invalid @enderror" name="sort_order" >
                        @error('sort_order') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group">
                        <label>Name &nbsp;</label>
                        <input type="text" placeholder="Location Name" class="form-control @error('name') is-invalid @enderror province_region_name" name="name">
                        @error('name') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group">
                        <label>Short Code &nbsp;</label>
                        <input type="text" placeholder="Location Code" class="form-control province_region_short_code @error('short_code') is-invalid @enderror" name="short_code">
                        @error('short_code') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group">
                        <label>Detail &nbsp;</label>
                        <input type="text" placeholder="Location Detail" class="form-control province_region_detail @error('detail') is-invalid @enderror" name="detail">
                        @error('detail') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary edit_province_region_save_modal">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">cancel</button>
            </div>
        </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
