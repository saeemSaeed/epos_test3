<div class="modal fade" id="modal-default">
    <form action="{{route('province-region.store') }}" method="post">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style=" background-color: #65a3c6;
color: #2c2c2c;">
                <h4 class="modal-title">Province/Region</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                    @csrf
                <div class="form-group">
                    <label>Sort Order &nbsp;</label>
                    <input type="text" placeholder="Sort Order"
                           oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"

                           class="form-control @error('province_region_sort_order') is-invalid @enderror" name="sort_order" value="{{ old('sort_order') }}">
                    @error('Sort Order') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                    <div class="form-group">
                        <label>Name &nbsp;</label>
                        <input type="text" placeholder="Location Name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}">
                        @error('name') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group">
                        <label>Short Code &nbsp;</label>
                        <input type="text" placeholder="Location Code" class="form-control @error('short_code') is-invalid @enderror" name="short_code" value="{{ old('short_code') }}">
                        @error('short_code') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group">
                        <label>Detail &nbsp;</label>
                        <input type="text" placeholder="Location Detail" class="form-control @error('detail') is-invalid @enderror" name="detail" value="{{ old('detail') }}">
                        @error('detail') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">cancel</button>
            </div>
        </div>

        <!-- /.modal-content -->
    </div>
    </form>
    <!-- /.modal-dialog -->
</div>
