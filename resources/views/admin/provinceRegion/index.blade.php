@extends('admin.layout.index')
@section('content')
<section class="bg-primary content-header"
    style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;">
    <div class="row">
        <div class="col-8">
            <h1>Province/Region</h1>
        </div>
        <div class="col-4">
            <ol class="breadcrumb" style="color:#444;float: right">
                <li>
                    <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                </li>
                <li>
                    <i class="fa fa-angle-right" style="color: #ccc;"></i> Province/Region &nbsp;
                </li>
            </ol>
        </div>
    </div>
</section>

<div class="clearfix mt-3">
    <button type="button" class="btn btn-success btn-flat float-left" data-toggle="modal" data-target="#modal-default">
        <i class="fas fa-plus mr-1"></i> Create Province / Region
    </button>

    @can('province-region-restore')
        <a href="{{ route('provinceRegion.Deleted') }}" class="btn btn-danger btn-flat float-right"><i class="fas fa-trash-alt"></i></a>
    @endcan
</div>

<div class="mt-3">
    <div class="card shadow">
        <div class="card-body">
            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="province-region"
                            class="table table-bordered table-hover dataTable dtr-inline province-region"
                            role="grid" aria-describedby="example2_info">
                            <thead style="    background-color: #65a3c6;
                                color: #2c2c2c;">
                                <tr role="row">
{{--                                    <th style="text-align: center">ID</th>--}}
                                    <th style="text-align: center;width: 10%">Sort Order</th>
                                    <th style="text-align: center">Region Name</th>
                                    <th style="text-align: center">Region Stort Code</th>
                                    <th style="text-align: center">Region Details</th>
                                    <th style="text-align: center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($getAllProvinceRegion))
                                @foreach($getAllProvinceRegion as $index=>$province)
                             <tr  data-id="{{$province->id}}">
{{--                                    <td style="text-align: center">{{++$index}}</td>--}}
                                    <td style="text-align: center">{{$province->sort_order}}</td>
                                    <td>{{$province->name}}</td>
                                    <td>{{$province->short_code}}</td>
                                    <td>{{$province->detail}}</td>
                                    <td data-id="{{$province->id}}" style="text-align: center!important;">
                                        <a data-id="{{$province->id}}"
                                            class="getProvinceRegion"
                                            data-toggle="modal"
                                            data-target="#modal-default_edit"

                                            title="Edit!"><i style="color: black;font-size: 14px!important"
                                        class="fas fa-pencil-alt"></i></a>&nbsp;&nbsp;
                                        <a
                                            onclick="return confirm('Are you sure?')"
                                            data-id="{{$province->id}}"
                                            href="{{route('province-region.delete',['id'=>$province->id])}}"
                                            data-toggle="tooltip"
                                            title="Remove"><i style="color: black;font-size: 14px!important"
                                        class=" fa fa-trash"></i></a></td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
    @include('admin.provinceRegion.create')
    @include('admin.provinceRegion.edit')
    @endsection
