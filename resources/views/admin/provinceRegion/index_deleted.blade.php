@extends('admin.layout.index')
@section('content')
<section class="bg-primary content-header"
    style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;">
    <div class="row">
        <div class="col-8">
            <h1>Province/Region</h1>
        </div>
        <div class="col-4">
            <ol class="breadcrumb" style="color:#444;float: right">
                <li>
                    <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                </li>
                <li>
                    <i class="fa fa-angle-right" style="color: #ccc;"></i> Province/Region &nbsp;
                </li>
            </ol>
        </div>
    </div>
</section>
<div class="container-fluid" style="margin-top: 10px">
    <div class="card">
        <div class="card-body">
            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="province-region"
                            class="table table-bordered table-hover dataTable dtr-inline province-region"
                            role="grid" aria-describedby="example2_info">
                            <thead style="    background-color: #65a3c6;
                                color: #2c2c2c;">
                                <tr role="row">
{{--                                    <th style="text-align: center">ID</th>--}}
                                    <th style="text-align: center;width: 10%">Sort Order</th>
                                    <th style="text-align: center">Region Name</th>
                                    <th style="text-align: center">Region Stort Code</th>
                                    <th style="text-align: center">Region Details</th>
                                    <th style="text-align: center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($deletedprovinceRegion))
                                @foreach($deletedprovinceRegion as $index=>$province)
                                <tr>
{{--                                    <td style="text-align: center">{{++$index}}</td>--}}
                                    <td>{{$province->name}}</td>
                                    <td>{{$province->short_code}}</td>
                                    <td>{{$province->detail}}</td>
                                    <td class="sorting_1 dtr-control" style="text-align: center!important;">
                                        <a type="submit" style="border: none;background-color: transparent" data-toggle="tooltip" title="" href="{{route('provinceRegion.Restore',['id'=>$province->id])}}" data-original-title="Restore"><i class="fas fa-trash-restore"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</div>
@include('admin.provinceRegion.create')
@include('admin.provinceRegion.edit')
@endsection
