@extends('admin.layout.index')
@section('content')
	<section class="bg-primary content-header"
			 style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
	<div class="row">
		<div class="col-8">
			<h1>Reports</h1>
		</div>
		<div class="col-4">
			<ol class="breadcrumb" style="color:#444;float: right">
				<li>
					<i class="fa fa-dashboard"></i> Dashboard &nbsp;
				</li>
				<li>
					<i class="fa fa-angle-right" style="color: #ccc;"></i> Reports &nbsp;
				</li>
				
			</ol>
		</div>
	</div>
</section>
<div class="container" style="margin-top:10px">
	<div class="row">
		<div class="col-lg-3 col-6">
			<!-- small box -->
			<div class="small-box bg-info">
				<div class="inner">
					<strong><p>Overall <br>Summary All <br>Contract</p></strong>
				</div>
				<div class="icon">
					<i class=" fas fa-book"></i>
				</div>
			</div>
		</div>
		
		<div class="col-lg-3 col-6">
			<!-- small box -->
			<div class="small-box bg-warning">
				<div class="inner">
				<strong><p>Overall <br>Summary All <br>PEAs</p></strong>
				</div>
				<div class="icon">
				<i class=" fas fa-book"></i>
				</div>
			</div>
		</div>

		<div class="col-lg-3 col-6">
			<!-- small box -->
			<div class="small-box bg-danger">
				<div class="inner">
				<strong><p>Overall <br>Summary All <br>Category</p></strong>
								</div>
				<div class="icon">
					<i class=" fas fa-book"></i>
				</div>
			</div>
		</div>


	</div>
</div>
@endsection