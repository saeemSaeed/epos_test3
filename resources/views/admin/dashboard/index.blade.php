@extends('admin.layout.index')
@section('mystyle')
    <link href="{{asset('assets/css/components.min.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="container-fluid" style="padding-top:10px">
        <div class="row">
            @can('dashboard-user-count')
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
<span class="info-box-icon bg-info elevation-1"> <a href="{{route('user.index')}}"
                                                    target="_bank"><i
            class="fas fa-user"></i> </a></span>
                        <div class="info-box-content">
                            <span class="info-box-text">User</span>
                            <span class="info-box-number">
{{ $userCount }}
</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            @endcan
        <!-- /.col -->
            @can('dashboard-project-count')
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
<span class="info-box-icon bg-danger elevation-1">
<a href="{{route('project.index')}}">
<i class="fas fa-tasks"></i></a></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Projects</span>
                            <span class="info-box-number">{{ $projectCount }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            @endcan
        <!-- /.col -->
            <!-- fix for small devices only -->
            <div class="clearfix hidden-md-up"></div>
            @can('dashboard-contracts-count')
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
<span class="info-box-icon bg-success elevation-1"> <a href="{{route('contract.index')}}"> <i
            class="far fa-file-alt"></i></a></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Contracts</span>
                            <span class="info-box-number">{{ $contractCount }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            @endcan
        <!-- /.col -->
            @can('dashboard-payments-count')
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
            <span class="info-box-icon bg-warning elevation-1">
                <a href="{{route('payment.index')}}"><i
                    style="color: white"
                    class="fas fa-euro-sign"></i></a></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Payments</span>
                            <span class="info-box-number">{{ $payementsCount }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
        @endcan
        <!-- /.col -->
        </div>
        {{-- graph --}}
        @can('dashboard-project-budget-distribution-province-wise')
            <div class="card shadow rounded-0">
{{--                 <div class="card-header">
                    <h3 class="card-title">
                        <i class="far fa-chart-bar"></i>
                        Project Budget Distribution Province Wise
                    </h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div> --}}
                <div class="card-body">
                    {{-- <div class="chart has-fixed-height" id="bars_basic"></div> --}}
                    <div id="payment-chart"></div>
                </div>
            </div>
        @endcan
        {{-- Overall Fund Dispositions EUR --}}
        @can('dashboard-overall-fund-dispositions-EUR')

            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title">
                        Overall Fund Dispositions EUR
                    </h3>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead style="background-color: #282828; color: white">
                        <tr style="text-align: center">
                            <th>Category</th>
                            <th>Budget</th>
                            <th>Revised Budget</th>
                            {{-- <th>Spending</th>
                            <th>Balance</th> --}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($measuresAll as $measure)
                            <tr>
                                <td>{{ $measure->name }}</td>
                                <td align="right">{{ number_format(\App\Model\ProjectsHasProvince::where('measure_id',$measure->id)->sum('budget'),6) }}</td>
                                <td align="right">{{ number_format(\App\Model\ProjectsHasProvince::where('measure_id',$measure->id)->sum('revise_amount'),6) }}</td>
                                {{--<td style="text-align:right;">{{ number_format(\App\Model\payments::where('contractMeasures',$measure->id)->sum('invoice_amount'),2) }}</td>--}}
{{--                                <td style="text-align:right;">{{ number_format((\App\Model\ProjectsHasProvince::where('measure_id',$measure->id)->sum('budget'))-(\App\Model\payments::where('contractMeasures',$measure->id)->sum('contract_amount')),2) }}</td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot class="table-active">
                        <td style="text-align:center;">Total</td>
                        <td style="text-align:right;">{{ number_format(\App\Model\ProjectsHasProvince::sum('budget'),6) }}</td>
                        <td style="text-align:right;">{{ number_format(\App\Model\ProjectsHasProvince::sum('revise_amount'),6) }}</td>
                        {{--<td style="text-align:right;">{{ number_format(\App\Model\payments::sum('contract_amount'),2) }}</td>--}}
{{--                        <td style="text-align:right;">{{ number_format((\App\Model\ProjectsHasProvince::sum('budget'))-(\App\Model\payments::sum('contract_amount')),2)}}</td>--}}
                        </tfoot>
                    </table>
                </div>
            </div>
        @endcan
    <!-- Fund Dispositions -->
        @can('dashboard-fund-dispositions')
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title">
                        Fund Dispositions
                    </h3>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead style="background-color: #282828; color: white">
                        <tr style="text-align: center">
                            <th>Category</th>
                            @foreach($paymentMethod as $method)
                                <th>{{ $method->short_code }}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($measuresAll as $measure)
                            <tr>
                                <td>{{ $measure->name }}</td>
                                @foreach($paymentMethod as $method)
{{--                                    <td style="text-align:right;">{{ number_format(\App\Model\payments::where('contractMeasures',$measure->id)->where('contractPaymentMethod',$method->id)->sum('contract_amount'),2) }}</td>--}}
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot style="background-color: #282828; color: white">
                        <td style="text-align:center;">Total</td>
                        @foreach($paymentMethod as $method)
{{--                            <td style="text-align:right;">{{ number_format(\App\Model\payments::where('contractPaymentMethod',$method->id)->sum('contract_amount'),2) }}</td>--}}
                        @endforeach
                        </tfoot>
                    </table>
                </div>
            </div>
        @endcan
        {{-- Fund Dispositions AJK --}}
        @can('dashboard-fund-dispositions-AJK')
            @foreach($allProvinces as $province)
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            Fund Dispositions {{ $province->name }}
                        </h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead style="background-color: #282828; color: white">
                            <tr style="text-align: center">
                                <th>Category</th>
                                <th>Contracts</th>
                                <th>Payments</th>
                                <th>Balance</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($measuresAll as $measure)
                                <tr>
                                    <td>{{ $measure->name }}</td>
                                    {{--<td style="text-align:right;">{{ number_format(\App\Model\contractHasProvincDivision::where('measure_id',$measure->id)->where('province_id',$province->id)->sum('budget'),2) }}</td>--}}
                                    {{--<td style="text-align:right;">{{ number_format(\App\Model\paymentsHasSub::where('measure_id',$measure->id)->where('province_contracts',$province->id)->sum('province_amount'),2) }}</td>--}}
                                    {{--<td style="text-align:right;">{{ number_format((\App\Model\contractHasProvincDivision::where('measure_id',$measure->id)->where('province_id',$province->id)->sum('budget'))-(\App\Model\paymentsHasSub::where('measure_id',$measure->id)->where('province_contracts',$province->id)->sum('province_amount')),2) }}</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot style="background-color: #282828; color: white">
                            <td style="text-align:center;">Total</td>
                            {{--<td style="text-align:right;">{{ number_format(\App\Model\contractHasProvincDivision::where('province_id',$province->id)->sum('budget'),2) }}</td>--}}
                            {{--<td style="text-align:right;">{{ number_format(\App\Model\paymentsHasSub::where('province_contracts',$province->id)->sum('province_amount'),2) }}</td>--}}
                            {{--<td style="text-align:right;">{{ number_format((\App\Model\contractHasProvincDivision::where('province_id',$province->id)->sum('budget'))-(\App\Model\paymentsHasSub::where('province_contracts',$province->id)->sum('province_amount')),2) }}</td>--}}
                            </tfoot>
                        </table>
                    </div>
                </div>
            @endforeach
        @endcan
    </div>
@endsection
@section('javascript')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script>
        Highcharts.chart('payment-chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Project Budget Information'
            },
            xAxis: {
                categories: [
                    @foreach($projects as $k => $project)
                        '{{ $project->title }}',
                    @endforeach
                ],
                crosshair: true,
                endOnTick: false,
            },
            yAxis: {
                min: 0,
                endOnTick: false,
                title: {
                    text: 'Amount'
                }
            },
            tooltip: {
                headerFormat: '<span class="pl-5 font-weight-bold" style="font-size:12px; line-height: 2.5em">{point.key}</span><table>',
                pointFormat:    `<tr>
                                    <td style="color:{series.color}; padding:0">
                                        {series.name}: 
                                    </td>
                                    <td style="padding:0">
                                        <b>{point.y:.6f} {point.currency}</b>
                                    </td>
                                </tr>`,
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [
                {
                    name: 'Budget Amount',
                    data: [
                        @foreach($projects as $k => $project)
                           {
                                y: {{ $project->bugdet_revised > 0 ? $project->bugdet_revised : $project->budget }},
                                currency: '{{ $project->hasCurrency->currency_symbol ?? null }}'
                           },
                        @endforeach
                    ]

                }, 
                {
                    name: 'Contract Amount',
                    data: [
                        @foreach($projects as $k => $project)
                           {
                                y: {{ App\Model\contractHasProvincDivision::where('project_id', $project->id)->sum('budget') ?? 0 }},
                                currency: '{{ $project->hasCurrency->currency_symbol ?? null }}'
                           },
                        @endforeach
                    ]
                }, 
                {
                    name: 'Payment Amount',
                    data: [
                        @foreach($projects as $k => $project)
                           {
                                y: {{ App\Model\paymentsHasSub::where('project_id', $project->id)->sum('budget') ?? 0 }},
                                currency: '{{ $project->hasCurrency->currency_symbol ?? null }}'
                           },
                        @endforeach
                    ]
                }
            ],
            dataLabels: {
              enabled: true,
              formatter: function() {
                return this.point.currency.replace(':val', this.y);  
              }
            }
        });
    </script>



    {{-- <script type="text/javascript">
        var bars_basic_element = document.getElementById('bars_basic');
        if (bars_basic_element) {
            var bars_basic = echarts.init(bars_basic_element);
            bars_basic.setOption({
                color: ['#3398DB'],
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: [
                    {
                        type: 'category',
                        data: [
                            @foreach($provinceSum as $index=>$pro)
                                '{{ $index }}',
                            @endforeach
                        ],
                        axisLabel: {
                            formatter: function(value) {
                                return value.slice(0,6).concat('...')
                            }
                        },
                        axisTick: {
                            alignWithLabel: true
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: [
                    {
                        name: '',
                        type: 'bar',
                        barWidth: '65%',
                        data: [
                            @foreach($provinceSum as $index=>$pro)
                                '{{ $pro }}',
                            @endforeach
                        ]
                    }
                ]
            });
        }
    </script> --}}
@endsection
