@extends('admin.layout.index')
@section('content')
    <section class="bg-primary content-header"
        style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top:10px;">
        <div class="row">
            <div class="col-8">
                <h4>Contractors</h4>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444; float: right">
                    <li><i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li><i class="fa fa-angle-right" style="color: #ccc;"></i> Contractors &nbsp;</li>
                    <li class="active"><i class="fa fa-angle-right" style="color: #ccc;"></i> New Contractor</li>
                </ol>
            </div>
        </div>
    </section>
    <div class="row" style="padding-top:10px">
        <div class="col-12">
            <form action="{{ route('contractors.update') }}" method="post">
                @csrf
                <input type="hidden" class="form-control contractor_id" placeholder="Contractor Name"
                    value="{{ $contractor->id }}" name="contractor_id">
                <div class="card card-default collapsed-card">
                    <div class="card-header">
                        <h3 class="card-title">General <span style="color: red;">*</span></h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool " data-card-widget="collapse"><i
                                    class="fas fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <h4 style="text-align:center; padding-bottom:2%">Company Details</h4>
                        <div class="row">

                            <div class="col-6">
                                <div class="row form-group">
                                    <div class="col-3">
                                        <label class="col-form-label">Sort Order</label>
                                    </div>
                                    <div class="col-9">
                                        <input type="text" placeholder="Sort Order"
                                            oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"
                                            class="sort_order form-control @error('sort_order') is-invalid @enderror"
                                            name="sort_order" value="{{ $contractor->sort_order }}">
                                        @error('Sort Order')
                                        <div class="invalid-feedback">{{ $message }}</div> @enderror

                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-3">
                                        <label class="col-form-label">Name</label>
                                    </div>
                                    <div class="col-9">
                                        <input type="text" class="form-control contractor_name"
                                            placeholder="Contractor Name" name="contractor_name"
                                            value="{{ $contractor->contractor_name }}">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-3">
                                        <label class="col-form-label">NTN</label>
                                    </div>
                                    <div class="col-9">
                                        <input type="text" class="form-control contractor_ntn" placeholder="Contractor NTN"
                                            name="contractor_ntn" value="{{ $contractor->contractor_ntn }}">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-3">
                                        <label class="col-form-label">City</label>
                                    </div>
                                    <div class="col-9">
                                        <select name="contractor_city" id="contractor_city"
                                            class="form-control city col-form-label" value="{{ old('contractor_city') }}">
                                            <option
                                                value="{{ isset($contractor->hasCity->id) ? $contractor->hasCity->id : '' }}">
                                                {{ isset($contractor->hasCity->city_name) ? $contractor->hasCity->city_name : '' }}
                                            </option>
                                            @foreach (\App\Helpers\Helper::cities() as $city)
                                                <option value="{{ $city->id }}">{{ $city->city_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row form-group">
                                    <div class="col-3">
                                        <label class="col-form-label">Landline</label>
                                    </div>
                                    <div class="col-9">
                                        <input type="text" class="form-control contractor_landline phone_check"
                                            placeholder="Contractor Landline" name="contractor_landline"
                                            value="{{ $contractor->contractor_landline }}">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-3">
                                        <label class="col-form-label">Details</label>
                                    </div>
                                    <div class="col-9">
                                        <input type="text" class="form-control contractor_details"
                                            placeholder="Contractor Details" name="contractor_details"
                                            value="{{ $contractor->contractor_details }}">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-3">
                                        <label class="col-form-label">Address</label>
                                    </div>
                                    <div class="col-9">
                                        <input type="text" class="form-control contractor_address" name="contractor_address"
                                            placeholder="Contractor Address"
                                            value="{{ $contractor->contractor_address }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-default collapsed-card">
                    <div class="card-header">
                        <h3 class="card-title">Contact Information</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool " data-card-widget="collapse"><i
                                    class="fas fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="table table-responsive">
                            <button id="btnAdd" type="button" class="btn btn-primary" data-toggle="tooltip"
                                data-original-title="Add Row"><i class="fa fa-plus"></i> Add
                            </button>
                            <button type="button" class="btn btn-danger delete-row"><i class="fa fa-trash"></i></button>
                            <table class="table table-responsive table-striped table-bordered" style="padding-top:1%">
                                <thead>
                                    <tr>
                                        <td></td>
                                        <td>Name</td>
                                        <td>Designation</td>
                                        <td>Role</td>
                                        <td>Email</td>
                                        <td>Mobile No</td>
                                        <td>landline</td>
                                    </tr>
                                </thead>
                                <tbody id="TextBoxContainer">
                                    @foreach ($contractor->contractorContactInformation as $index => $contact)
                                        <tr>
                                            <td><input name="check" type="checkbox" value="" /></td>
                                            <td><input name="contact_name[]" type="text"
                                                    value="{{ $contact->contact_name }}" class="form-control" /></td>
                                            <td><input name="contact_designation[]" type="text"
                                                    value="{{ $contact->contact_designation }}" class="form-control" />
                                            </td>
                                            <td><input name="contact_role[]" type="text"
                                                    value="{{ $contact->contact_role }}" class="form-control" /></td>
                                            <td><input name="contact_email[]" type="text"
                                                    value="{{ $contact->contact_email }}" class="form-control" /></td>
                                            <td><input name="contact_mobile_no[]" type="text"
                                                    value="{{ $contact->contact_mobile_no }}" class="form-control" />
                                            </td>
                                            <td><input name="contact_landline[]" type="text"
                                                    value="{{ $contact->contact_landline }}" class="form-control" />
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <div class="card card-default collapsed-card">
                    <div class="card-header">
                        <h3 class="card-title">Financial Details</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool " data-card-widget="collapse"><i
                                    class="fas fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="text-right">
                            <button id="btnAddfinancial" type="button" class="btn btn-primary" data-toggle="tooltip"
                                title="Add Row"><i class="fa fa-plus"></i> Add</button>
                            <button type="button" class="btn btn-danger delete-row"><i class="fa fa-trash"></i></button>
                        </div>

                        <div class="mt-3">
                            <table class="table table-bordered table-hover table-striped table-sm" style="padding-top:1%">
                                <thead class="thead-light">
                                    <tr>
                                        <td>#</td>
                                        <td>Bank Name</td>
                                        <td>Account No</td>
                                        <td>IBAN</td>
                                        <td>Swift Code</td>
                                        <td>Branch Name</td>
                                        <td>Branch Code</td>
                                        <td>City</td>
                                        <td>Address</td>
                                    </tr>
                                </thead>
                                <tbody id="TextBoxContainerfinancial">
                                    @foreach ($contractor->ContractorFinancialDetails as $index => $financial)
                                        <tr>
                                            <td align="center" style="vertical-align: middle">
                                                <input name="check" type="checkbox" />
                                                <input type="hidden" name="id[{{ $index }}]"
                                                    value="{{ $financial->id }}">
                                                <input type="hidden" name="status[]" value="false">
                                            </td>

                                            <td>
                                                <input name="bank_name[{{ $index }}]" type="text"
                                                    value="{{ $financial->bank_name }}" class="form-control" />
                                            </td>

                                            <td>
                                                <input name="account_no[{{ $index }}]" type="text"
                                                    value="{{ $financial->account_no }}" class="form-control" />
                                            </td>

                                            <td>
                                                <input name="iban[{{ $index }}]" type="text"
                                                    value="{{ $financial->iban }}" class="form-control" />
                                            </td>

                                            <td>
                                                <input name="swift_code[{{ $index }}]" type="text"
                                                    value="{{ $financial->swift_code }}" class="form-control" />
                                            </td>

                                            <td>
                                                <input name="branch_name[{{ $index }}]" type="text"
                                                    value="{{ $financial->branch_name }}" class="form-control" />
                                            </td>

                                            <td>
                                                <input name="branch_code[{{ $index }}]" type="text"
                                                    value="{{ $financial->branch_code }}" class="form-control" />
                                            </td>

                                            <td>
                                                <select data-live-search="true" id="select-state" class="select-state"
                                                    name="city_id[{{ $index }}]" style="overflow: hidden;">
                                                    <option value="" selected>Select</option>
                                                    @foreach (\App\Helpers\Helper::cities() as $city)
                                                        <option value="{{ $city->id }}"
                                                            {{ $financial->city_id == $city->id ? 'selected' : '' }}>
                                                            {{ $city->city_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input name="branch_address[{{ $index }}]" type="text"
                                                    value="{{ $financial->branch_address }}" class="form-control" />
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <div class="col">
                    <button type="submit" class="btn btn-success btn-lg" style="float:right;">Save
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        var count_rows = {{ count($contractor->ContractorFinancialDetails) }} - 1;

        $('.select-state').selectpicker();
        $(function() {
            $("#btnAdd").bind("click", function() {
                var div = $("<tr />");
                div.html(GetDynamicTextBox(""));
                $("#TextBoxContainer").append(div);
            });
            $("body").on("click", ".remove", function() {
                $(this).closest("tr").remove();
            });
        });

        function GetDynamicTextBox(value) {
            return '<td><input name="check" type="checkbox" value = "' + value + '" /></td>' +
                '<td><input name = "contact_name[]" type="text" value = "' + value +
                '" class="form-control" required/></td>' +
                '<td><input name = "contact_designation[]" type="text" value = "' + value +
                '" class="form-control" /></td>' + '<td><input name = "contact_role[]" type="text" value = "' + value +
                '" class="form-control" /></td>' + '<td><input name = "contact_email[]" type="text" value = "' + value +
                '" class="form-control" /></td>' + '<td><input name = "contact_mobile_no[]" type="text" value = "' + value +
                '" class="form-control" /></td>' + '<td><input name = "contact_landline[]" type="text" value = "' + value +
                '" class="form-control" /></td>'
        }
        // Financial Details
        $(function() {
            $("#btnAddfinancial").bind("click", function() {
                var div = $("<tr />");
                div.html(GetDynamicTextBoxfinancial(""));
                $("#TextBoxContainerfinancial").append(div);
                $('.select-state').selectpicker();
            });
            $("body").on("click", ".remove", function() {
                $(this).closest("tr").remove();
            });
        });

        function GetDynamicTextBoxfinancial(value) {
            var cities = {!! json_encode(\App\Helpers\Helper::citiesOptions()) !!};
            count_rows++;

            return '<td align="center" style="vertical-align: middle"><input type="hidden" name="id[' + count_rows +
                ']" value=""><input name = "check" type="checkbox" value = "' + value +
                '" /><input type="hidden" name="status[]" value="false"></td>' +
                '<td><input name="bank_name[' + count_rows + ']" type="text" value = "' + value +
                '" class="form-control" required/></td>' +
                '<td><input name="account_no[' + count_rows + ']" type="text" value = "' + value +
                '" class="form-control" /></td>' +
                '<td><input name="iban[' + count_rows + ']" type="text" value = "' + value +
                '" class="form-control" /></td>' +
                '<td><input name="branch_name[' + count_rows + ']" type="text" value = "' + value +
                '" class="form-control" /></td>' +
                '<td><input name="branch_code[' + count_rows + ']" type="text" value = "' + value +
                '" class="form-control" /></td>' +
                '<td><input  name="swift_code[' + count_rows + ']" type="text" value = "' + value +
                '" class="form-control" /></td><td ><select data-live-search="true"  class="select-state" id="select-state" name="city_id[' +
                count_rows + ']" style="  overflow: hidden;">' +
                cities + '</select></td><td><input name="branch_address[' + count_rows + ']" type="text" value = "' +
                value +
                '" class="form-control" /></td>';
        }
        $(".delete-row").click(function() {
            $("table tbody").find('input[name="check"]').each(function() {
                if ($(this).is(":checked")) {
                    $(this).parents("tr").hide().find('[name="status[]"]').val("true");
                    $(this).parents("tr").find('input').each((k, v) => $(v).prop('required', false))
                }
            });
        });
    </script>
@endsection
