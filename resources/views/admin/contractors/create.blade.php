@extends('admin.layout.index')
@section('content')
    <section class="bg-primary content-header"
             style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top:10px;">
        <div class="row">
            <div class="col-8">
                <h4>Contractors</h4>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444; float: right">
                    <li><i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li><i class="fa fa-angle-right" style="color: #ccc;"></i> Contractors &nbsp;</li>
                    <li class="active"><i class="fa fa-angle-right" style="color: #ccc;"></i> New Contractor</li>
                </ol>
            </div>
        </div>
    </section>
    <div class="row" style="padding-top:10px">
        <div class="col-12">
            <form action="{{route('contractors.store') }}" method="post">
                @csrf
                <div class="card card-default collapsed-card">
                    <div class="card-header">
                        <h3 class="card-title">General <span style="color: red;">*</span></h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool " data-card-widget="collapse"><i
                                        class="fas fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <h4 style="text-align:center; padding-bottom:2%">Company Details</h4>
                        <div class="row">
                            <div class="col-6">
                                <div class="row form-group">
                                    <div class="col-3">
                                        <label class="col-form-label">Sort Order</label>
                                    </div>
                                    <div class="col-9">
                                        <input type="text" placeholder="Sort Order"
                                               oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"

                                               class=" sort_order form-control @error('sort_order') is-invalid @enderror"
                                               name="sort_order" value="{{ old('sort_order') }}">
                                        @error('Sort Order')
                                        <div class="invalid-feedback">{{ $message }}</div> @enderror

                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-3">
                                        <label class="col-form-label">Name</label>
                                    </div>
                                    <div class="col-9">
                                        <input type="text" class="form-control contractor_name"
                                               placeholder="Contractor Name" name="contractor_name" required>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-3">
                                        <label class="col-form-label">NTN</label>
                                    </div>
                                    <div class="col-9">
                                        <input type="text" class="form-control contractor_ntn"
                                               placeholder="Contractor NTN" name="contractor_ntn"
                                               value="{{ old('contractor_ntn') }}">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-3">
                                        <label class="col-form-label">City</label>
                                    </div>
                                    <div class="col-9">
                                        <select name="contractor_city" id="contractor_city"
                                                class="form-control city col-form-label sub_cate_city" data-live-search="true"
                                                value="{{ old('contractor_city') }}" required>
                                            <option value="">Choose an Option</option>
                                            @foreach(\App\Helpers\Helper::cities() as $city)
                                                <option value="{{$city->id}}">{{$city->city_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row form-group">
                                    <div class="col-3">
                                        <label class="col-form-label">Landline</label>
                                    </div>
                                    <div class="col-9">
                                        <input type="text" class="form-control contractor_landline phone_check"
                                               placeholder="Contractor Landline" name="contractor_landline"
                                               value="{{ old('contractor_landline') }}" required>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-3">
                                        <label class="col-form-label">Details</label>
                                    </div>
                                    <div class="col-9">
                                        <input type="text" class="form-control contractor_details"
                                               placeholder="Contractor Details" name="contractor_details"
                                               value="{{ old('contractor_details') }}">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-3">
                                        <label class="col-form-label">Address</label>
                                    </div>
                                    <div class="col-9">
                                    <textarea type="text" class="form-control contractor_address"
                                              name="contractor_address"
                                              placeholder="Contractor Address">{{ old('contractor_address') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-default collapsed-card">
                    <div class="card-header">
                        <h3 class="card-title">Contact Information</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool " data-card-widget="collapse"><i
                                        class="fas fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">

                            <div class="table table-responsive">
                                <button id="btnAdd" type="button" class="btn btn-primary" data-toggle="tooltip"
                                        data-original-title="Add Row"><i class="fa fa-plus"></i> Add
                                </button>
                                <button type="button" class="btn btn-danger delete-row"><i class="fa fa-trash"></i>
                                </button>
                                <table class="table table-responsive table-striped table-bordered"
                                       style="padding-top:1%">
                                    <thead>
                                    <tr>
                                        <td></td>
                                        <td>Name</td>
                                        <td>Designation</td>
                                        <td>Role</td>
                                        <td>Email</td>
                                        <td>Mobile No</td>
                                        <td>landline</td>
                                    </tr>
                                    </thead>
                                    <tbody id="TextBoxContainer">
                                    <td><input name="check" type="checkbox" value=""/></td>
                                    <td><input placeholder="Enter Name" name="contact_name[]" type="text" value=""
                                               class="form-control" required/></td>
                                    <td><input placeholder="Enter Designation" name="contact_designation[]" type="text"
                                               value="" class="form-control"/></td>
                                    <td><input placeholder="Enter Role" name="contact_role[]" type="text" value=""
                                               class="form-control"/></td>
                                    <td><input placeholder="Enter Email" name="contact_email[]" type="text" value=""
                                               class="form-control"/></td>
                                    <td><input placeholder="Enter Mobile no" name="contact_mobile_no[]" type="text"
                                               value="" class="form-control"/></td>
                                    <td><input placeholder="Enter Landline" name="contact_landline[]" type="text"
                                               value="" class="form-control"/></td>
                                    </tbody>
                                </table>
                            </div>

                    </div>
                </div>
                <div class="card card-default collapsed-card">
                    <div class="card-header">
                        <h3 class="card-title">Financial Details</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool " data-card-widget="collapse"><i
                                        class="fas fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">

                            <div class="">
                                <button id="btnAddfinancial" type="button" class="btn btn-primary" data-toggle="tooltip"
                                        data-original-title="Add Row"><i class="fa fa-plus"></i> Add
                                </button>
                                <button type="button" class="btn btn-danger delete-row"><i class="fa fa-trash"></i>
                                </button>
                                <table class="table  table-bordered"
                                      >
                                    <thead>
                                    <tr style="    width: auto">
                                        <td></td>
                                        <td style="text-align: center">Bank Name</td>
                                        <td style="text-align: center">Account No</td>
                                        <td style="text-align: center">IBAN</td>
                                        <td style="text-align: center">SWIFT Code</td>
                                        <td style="text-align: center">Branch Name</td>
                                        <td style="text-align: center">Branch Code</td>
                                        <td style="text-align: center;width: 20%!important;">City</td>
                                        <td style="text-align: center">Address</td>
                                    </tr>
                                    </thead>
                                    <tbody id="TextBoxContainerfinancial">
                                    <td><input name="check" type="checkbox" value=""/></td>
                                    <td><input placeholder="Enter Bank Number" name="bank_name[]" type="text" value=""
                                               class="form-control" required/></td>
                                    <td><input placeholder="Enter Bank Account " name="account_no[]" type="text"
                                               value="" class="form-control" required/></td>
                                    <td><input placeholder="Enter Bank IBAN" name="iban[]" type="text" value=""
                                               class="form-control"/></td>
                                    <td><input placeholder="Enter SWIFT CODE" name="swift_code[]" type="text" value=""
                                               class="form-control"/></td>
                                    <td><input placeholder="Enter Branch" name="branch_name[]" type="text" value=""
                                               class="form-control"/></td>
                                    <td><input placeholder="Enter Code" name="branch_code[]" type="text" value=""
                                               class="form-control"/></td>
                                    <td style="text-align: center;width:10%!important;"><select data-live-search="true" id="select-state"
                                                name="city_id[]"   class="form-control" style="width:100%">

                                            @foreach(\App\Helpers\Helper::cities() as $city)
                                                <option value="{{$city->id}}">{{$city->city_name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><input placeholder="Enter Address" name="branch_address[]" type="text" value=""
                                               class="form-control"/></td>
                                    </tbody>
                                </table>
                            </div>

                    </div>
                </div>
                <div class="col">
                    <button type="submit" class="btn btn-success btn-lg" style="float:right;"
                    >Save
                    </button>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('javascript')
    <script>
        {{-- Contact Information duplication --}}
$(function () {
            $("#btnAdd").bind("click", function () {
                var div = $("<tr />");
                div.html(GetDynamicTextBox(""));
                $("#TextBoxContainer").append(div);
            });
            $("body").on("click", ".remove", function () {
                $(this).closest("tr").remove();
            });
        });


        function GetDynamicTextBox(value) {
            return '<td><input name = "check" type="checkbox" value = "' + value + '" /></td>' + '<td><input name = "contact_name[]" placeholder="Name" type="text" value = "' + value + '" class="form-control" required/></td>' + '<td><input name = "contact_designation[]" placeholder="Designation" type="text" value = "' + value + '" class="form-control" /></td>' + '<td><input placeholder="Enter  Role" name = "contact_role[]" type="text" value = "' + value + '" class="form-control" /></td>' + '<td><input placeholder="Enter email" name = "contact_email[]" type="text" value = "' + value + '" class="form-control" /></td>' + '<td><input placeholder="Enter Mobile" name = "contact_mobile_no[]" type="text" value = "' + value + '" class="form-control" /></td>' + '<td><input placeholder="Enter Landline" name = "contact_landline[]" type="text" value = "' + value + '" class="form-control" /></td>'
        }
        // Financial Details
        $(function () {
            $("#btnAddfinancial").bind("click", function () {
                var div = $("<tr />");
                div.html(GetDynamicTextBoxfinancial(""));
                $("#TextBoxContainerfinancial").append(div);
                $('.select-state').selectpicker();
            });


            $("body").on("click", ".remove", function () {
                $(this).closest("tr").remove();
            });
        });

        function GetDynamicTextBoxfinancial(value) {
           var cities  ={!! json_encode(\App\Helpers\Helper::citiesOptions()) !!};


            return '<td><input name = "check" type="checkbox" value = "' + value + '" /></td>' + '<td><input placeholder="Enter Blank Name " name = "bank_name[]" type="text" value = "' + value + '" class="form-control" required/></td>' + '<td><input  placeholder="Enter Blank Account No " name = "account_no[]" type="text" value = "' + value + '" class="form-control" /></td>' + '<td><input  placeholder="Enter Blank IBAN " name = "iban[]" type="text" value = "' + value + '" class="form-control" /></td><td><input  placeholder="Enter SWIFT CODE" name = "swift_code[]" type="text" value = "' + value + '" class="form-control" /></td>' + '<td><input placeholder="Enter Blank Branch Name " name = "branch_name[]" type="text" value = "' + value + '" class="form-control" /></td>' + '<td><input placeholder="Enter Blank Branch Code " name = "branch_code[]" type="text" value = "' + value + '" class="form-control" /></td>' + '<td ><select data-live-search="true"  class="select-state" id="select-state" name="city_id[]" style="  overflow: hidden;">'+cities+'</select></td><td><input  placeholder="Enter Blank Branch Address " name = "branch_address[]" type="text" value = "' + value + '" class="form-control" /></td>'
        }


        $(".delete-row").click(function () {
            $("table tbody").find('input[name="check"]').each(function () {
                if ($(this).is(":checked")) {
                    $(this).parents("tr").remove();
                }

            });
        });


    </script>
@endsection
