@extends('admin.layout.index')
@section('content')
    <section class="bg-primary content-header"
             style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;">
        <div class="row">
            <div class="col-8">
                <h4>Contractors</h4>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li>
                        <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li>
                        <i class="fa fa-angle-right" style="color: #ccc;"></i> Contractors &nbsp;
                    </li>
                </ol>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row justify-content-between">

            <div style="margin-top: 10px; margin-left: 10px">
                @can('contractors-create')
                    <a type="button" href="{{ route('contractors.create') }}"
                       class="btn btn-block btn-success btn-flat">
                        <i class="fa fa-plus"></i> Contractors
                    </a>
                @endcan
            </div>
            @can('contractors-restore')
                <div style="margin-top: 10px; margin-right: 10px">
                    <a href="{{ route('contractors.Deleted') }}" class="btn btn-block btn-danger btn-flat"><i
                            class="fas fa-trash-alt"></i></a>
                </div>
            @endcan
        </div>
    </div>
    <div class="container-fluid" style="margin-top: 10px">
        <div class="card">
            <div class="card-body">
                <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12 col-md-6"></div>
                        <div class="col-sm-12 col-md-6"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="contractors"
                                   class="table table-bordered data-table-contractors" role="grid"
                                   aria-describedby="example2_info">
                                <thead style="    background-color: #65a3c6;
                                color: #2c2c2c;">
                                <tr role="row">
{{--                                    <th width="5% ">ID</th>--}}
                                    <th style=";width: 10%">Sort Order</th>
                                    <th width="15% ">Contractor Name</th>
                                    <th width="10% ">Contractor City</th>
                                    <th width="25% ">Contractor Address</th>
                                    <th width="15% ">Contractor Landline</th>

                                    <th width="10% ">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($contractors as $index=>$contractor)
                                    <tr role="row" class="odd">
{{--                                        <td style="text-align: center">{{++$index}}</td>--}}
                                        <td style="text-align: center">{{$contractor->sort_order}}</td>
                                        <td>{{$contractor->contractor_name}}</td>
                                        <td>{{isset($contractor->hasCity->city_name)?$contractor->hasCity->city_name:''}}</td>
                                        <td>{{$contractor->contractor_address}}</td>
                                        <td style="text-align: center"          > {{$contractor->contractor_landline}}</td>

                                        <td data-id="{{$contractor->id}}" style="text-align: center">
                                            @can('contractors-create')
                                                <a data-id="{{$contractor->id}}"
                                                   href="{{route('contractors.profile',['id'=>$contractor->id])}}"
                                                   title="Profile!"><i style="color: black;font-size: 14px!important"
                                                                       class="fas fa-eye"></i></a>&nbsp;&nbsp;
                                            @endcan
                                            @can('contractors-edit')
                                                <a data-id="{{$contractor->id}}"
                                                   href="{{route('contractors.edit',['id'=>$contractor->id])}}"
                                                   title="Edit!"><i style="color: black;font-size: 14px!important"
                                                                    class="fas fa-pencil-alt"></i></a>&nbsp;&nbsp;
                                            @endcan
                                            @can('contractors-delete')
                                                <a
                                                    onclick="return confirm('Are you sure?')"
                                                    data-id="{{$contractor->id}}"
                                                    href="{{route('contractors.delete',['id'=>$contractor->id])}}"
                                                    data-toggle="tooltip"
                                                    title="Remove"><i style="color: black"
                                                                      class=" fa fa-trash"></i></a>
                                            @endcan</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
@endsection
