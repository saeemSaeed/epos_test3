@extends('admin.layout.index')
@section('content')
<section class="bg-primary content-header" style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top:10px;">
    <div class="row">
        <div class="col-8">
            <h4>Contractors</h4>
        </div>
        <div class="col-4">
            <ol class="breadcrumb" style="color:#444; float: right">
                <li><i class="fa fa-dashboard"></i> Dashboard &nbsp;
                </li>
                <li><i class="fa fa-angle-right" style="color: #ccc;"></i> Contractors &nbsp;</li>
                <li class="active"><i class="fa fa-angle-right" style="color: #ccc;"></i> New Contractor</li>
            </ol>
        </div>
    </div>
</section>
<div class="row" style="padding-top:10px">
    <div class="col-12">
        <div class="card card-default collapsed-card">
            <div class="card-header">
                <h3 class="card-title">General <span style="color: red;">*</span></h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool " data-card-widget="collapse"><i class="fas fa-plus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <h4 style="text-align:center; padding-bottom:2%" >Company Details</h4>
                <div class="row">
                    <div class="col-6">
                        <div class="row form-group">
                            <div class="col-3">
                                <label class="col-form-label">Sort Order</label>
                            </div>
                            <div class="col-9">
                                <input type="text" class="form-control sort_order" placeholder="Contractor Name" value="{{$contractor->sort_order}}" readonly>
                            </div>
                        </div>


                        <div class="row form-group">
                            <div class="col-3">
                                <label class="col-form-label">Name</label>
                            </div>
                            <div class="col-9">
                                <input type="text" class="form-control contractor_name" placeholder="Contractor Name" value="{{$contractor->contractor_name}}" readonly>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-3">
                                <label class="col-form-label">NTN</label>
                            </div>
                            <div class="col-9">
                                <input type="text" class="form-control contractor_ntn" placeholder="Contractor NTN" name="contractor_ntn"  value="{{ $contractor->contractor_ntn }}" readonly>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-3">
                                <label class="col-form-label">City</label>
                            </div>
                            <div class="col-9">
                                <input type="text" class="form-control contractor_ntn" placeholder="Contractor City" name="contractor_ntn"  value="{{ $contractor->hasCity->city_name }}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row form-group">
                            <div class="col-3">
                                <label class="col-form-label">Landline</label>
                            </div>
                            <div class="col-9">
                                <input type="text" class="form-control contractor_landline phone_check"
                                placeholder="Contractor Landline" name="contractor_landline" value="{{  $contractor->contractor_landline }}" readonly>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-3">
                                <label class="col-form-label">Details</label>
                            </div>
                            <div class="col-9">
                                <input type="text" class="form-control contractor_details"
                                placeholder="Contractor Details" name="contractor_details" value="{{  $contractor->contractor_details }}" readonly>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-3">
                                <label class="col-form-label">Address</label>
                            </div>
                            <div class="col-9">
                                <textarea type="text" class="form-control contractor_address"
                                value="" readonly> {{  $contractor->contractor_address}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-default collapsed-card">
            <div class="card-header">
                <h3 class="card-title">Contact Information</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool " data-card-widget="collapse"><i class="fas fa-plus"></i></button>
                </div>
            </div>
            <div class="card-body">

                    <div class="table table-responsive">
                        <table class="table table-responsive table-striped table-bordered" style="padding-top:1%">
                            <thead>
                                <tr>
                                    <td></td>
                                    <td>Name</td>
                                    <td>Designation</td>
                                    <td>Role</td>
                                    <td>Email</td>
                                    <td>Mobile No</td>
                                    <td>landline</td>
                                </tr>
                            </thead>
                            <tbody id="TextBoxContainer">
                                @foreach($contractor->contractorContactInformation as $index=>$contact)
                                <tr>
                                    <td>{{ ++$index }}</td>
                                    <td><input readonly value = "{{ isset($contact->contact_name) ? $contact->contact_name: '---' }}" type="text" value = "" class="form-control" /></td>
                                    <td><input readonly value = "{{ isset($contact->contact_designation) ? $contact->contact_designation : '---' }}" type="text" value = "" class="form-control" /></td>
                                    <td><input readonly value = "{{ isset($contact->contact_role) ? $contact->contact_role : '---' }}" type="text" value = "" class="form-control" /></td>
                                    <td><input readonly value = "{{ isset($contact->contact_email) ? $contact->contact_emai : '---' }}" type="text" value = "" class="form-control" /></td>
                                    <td><input readonly value = "{{ isset($contact->contact_mobile_no) ? $contact->contact_mobile_no : '---' }}" type="text" value = "" class="form-control" /></td>
                                    <td><input readonly value = "{{ isset($contact->contact_landline) ? $contact->contact_landline : '---' }}" type="text" value = "" class="form-control" /></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

            </div>
        </div>
        <div class="card card-default collapsed-card">
            <div class="card-header">
                <h3 class="card-title">Financial Details</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool " data-card-widget="collapse"><i class="fas fa-plus"></i></button>
                </div>
            </div>
            <div class="card-body">

                    <div class="table table-responsive">
                        <table class="table table-responsive table-striped table-bordered" style="padding-top:1%">
                            <thead>
                                <tr>
                                    <td></td>
                                    <td>Bank Name</td>
                                    <td>Account No</td>
                                    <td>IBAN</td>
                                    <td>Swift Code</td>
                                    <td>Branch Name</td>
                                    <td>Branch Code</td>
                                    <td>City</td>
                                    <td>Address</td>
                                </tr>
                            </thead>
                            <tbody id="TextBoxContainerfinancial">
                                @foreach($contractor->ContractorFinancialDetails as $index=>$financial)
                                <tr>
                                    <td>{{ ++$index }}</td>
                                    <td><input readonly value = "{{ isset($financial->bank_name) ? $financial->bank_name: '---' }}" type="text" value = "" class="form-control" /></td>
                                    <td><input readonly value = "{{ isset($financial->account_no) ? : '---' }}" type="text" value = "" class="form-control" /></td>
                                    <td><input readonly value = "{{ isset($financial->iban) ? $financial->iban : '---' }}" type="text" value = "" class="form-control" /></td>
                                    <td><input readonly value = "{{ isset($financial->swift_code) ? $financial->swift_code : '---' }}" type="text" value = "" class="form-control" /></td>

                                    <td><input readonly value = "{{ isset($financial->branch_name) ? $financial->branch_name  : '---' }}" type="text" value = "" class="form-control" /></td>
                                    <td><input readonly value = "{{ isset($financial->branch_code) ? $financial->branch_code  : '---' }}" type="text" value = "" class="form-control" /></td>
                                    <td><select data-live-search="true" id="select-state" class="select-state" disabled
                                                name="city_id[]" style="  overflow: hidden;
"
                                        >
                                            <option value="">Select</option>
                                            @foreach(\App\Helpers\Helper::cities() as $city)
                                                <option value="{{$city->id}}" {{$financial->city_id==$city->id?"selected":""}}>{{$city->city_name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><input readonly value = "{{ isset($financial->branch_address) ? $financial->branch_address  : '---' }}" type="text" value = "" class="form-control" /></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

            </div>
        </div>
    </div>
</div>
@endsection
