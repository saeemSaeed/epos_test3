@extends('admin.layout.index')
@section('content')
<section class="bg-primary content-header"
    style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;">
    <div class="row">
        <div class="col-8">
            <h4>Contractors</h4>
        </div>
        <div class="col-4">
            <ol class="breadcrumb" style="color:#444;float: right">
                <li>
                    <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                </li>
                <li>
                    <i class="fa fa-angle-right" style="color: #ccc;"></i> Contractors &nbsp;
                </li>
            </ol>
        </div>
    </div>
</section>

<div class="container-fluid" style="margin-top: 10px">
    <div class="card">
        <div class="card-body">
            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="contractors"
                            class="table table-bordered data-table-contractors" role="grid"
                            aria-describedby="example2_info">
                            <thead style="    background-color: #65a3c6;
                                color: #2c2c2c;">
                                <tr role="row">
                                    <th  width="5% ">Sort Order</th>
{{--                                    <th width="5% ">ID</th>--}}
                                    <th  width="12% ">Contractor Name</th>
                                    <th width="10% ">Contractor City</th>
                                    <th width="25% ">Contractor Address</th>
                                    <th width="15% ">Contractor Landline</th>

                                    <th width="10% ">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($deletedContractors as $index=>$contractor)
                                <tr role="row" class="odd">
                                    <td style="text-align: center;width: 10%">{{++$index}}</td>
                                    <td style="text-align: center"  >{{$contractor->sort_order}}</td>
                                    <td>{{$contractor->contractor_name}}</td>
                                    <td>{{isset($contractor->hasCity->city_name)?$contractor->hasCity->city_name:''}}</td>
                                    <td>{{$contractor->contractor_address}}</td>
                                    <td>{{$contractor->contractor_landline}}</td>

                                    <td class="sorting_1 dtr-control" style="text-align: center!important;">
                                        <a type="submit" style="border: none;background-color: transparent" data-toggle="tooltip" title="" href="{{route('contractors.restore',['id'=>$contractor->id])}}" data-original-title="Restore"><i class="fas fa-trash-restore"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</div>
@endsection
