@extends('admin.layout.index')
@section('content')
    <style>
        label {
            font-size: 12px !important;
            color: #2a272a !important;
            margin-bottom: 0.1rem !important;
        }

        input {
            font-size: 14px !important;
            height: 30px !important;
        }

        select {
            font-size: 12px !important;
            height: 27px !important;
        }

        .form-group {
            margin-bottom: 0.5rem;
            padding-top: 5px !important;
        }

        .my_custom_focus :focus {

        }

        .fieldset {
            position: relative;
            border: 1px #d5d5d5 solid;
            padding: 1rem;
        }

        .fieldset h1 {
            position: absolute;
            top: 0;
            font-size: 16px;
            line-height: 1;
            margin: -9px 0 0 -0.5rem;
            background: #fff;
            padding: 0 0.5rem;
            color: #2d2d2d;
        }
    </style>
    <section class="bg-primary content-header" style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Contracts</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li><i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li><i class="fa fa-angle-right" style="color: #ccc;"></i> Contracts &nbsp;</li>
                    <li class="active"><i class="fa fa-angle-right" style="color: #ccc;"></i> newcontract</li>
                </ol>
            </div>
        </div>
    </section>
    {{--Start create form--}}
    <div style="display: none" class="alert alert-success alert-block add_exchange_rate_messege">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Successfully add Exchange</strong></div>
    <form method="post" action="{{route('contract.store')}}" id="contractForm" enctype="multipart/form-data">
        @csrf
        <div class="col" style="margin-top: 10px">
            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title">Create New Contract</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-5">

                            <div class="form-group"><label>Select Vendor:<span class="text-danger">*</span></label>
                                <select data-live-search="true" data-live-search-style="startsWith" id="select-state"
                                        class="form-control" name="contractor_id" required>
                                    <option value="">Select Vendor</option>
                                    @foreach($contractors as $contractor)
                                        <option value="{{$contractor->id}}">{{$contractor->contractor_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group mb-4 ">
                                <label>Contract Title/Reference Code:<span class="text-danger">*</span></label>
                                <input type="text" style="background-color: #FFCCCB;" class="form-control"
                                       value="{{ old('contract_title') }}" name="contract_title" required>
                            </div>
                            <section class="fieldset">
                                <h1>Exchange Rate</h1>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Currency From:<span class="text-danger">*</span></label>
                                            <select name="currency_id_from" id="currency_id_from"
                                                    class="form-control currency_id_from"
                                                    style="background-color: #FFCCCB;" required>
                                                <option value="">Select Currency</option>
                                                @foreach(\App\Helpers\Helper::currency() as $cur)
                                                    <option value="{{$cur->id}}">{{ucfirst($cur->currency_name)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Currency To:<span class="text-danger">*</span></label>
                                            <select name="currency_id" id="currency_id" class="form-control currency_id"
                                                    style="background-color: #FFCCCB;" required>
                                                <option value="">Select Currency</option>
                                                @foreach(\App\Helpers\Helper::currency() as $cur)
                                                    <option value="{{$cur->id}}">{{ucfirst($cur->currency_name)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <input type="hidden" class="exchange_rate_amount_val">
                                            <label>Exchange Rate Date:<span class="text-danger">*</span>
                                                &nbsp;</label>

                                            <input style=""
                                                   type="text"
                                                   name="exchange_rate_date" id="exchange_rate_date"
                                                   class="form-control exchange_rate_date"
                                                   required/>

                                            <input style=""
                                                   type="hidden"
                                                   name="exchange_rate_flag" id="exchange_rate_flag"
                                                   class="form-control exchange_rate_flag"
                                                   value="false"/>
                                            <input type="hidden" name="exchange_rate_id"
                                                   class=" form-control  exchange_rate_id"
                                                   readonly dir="rtl">


                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">

                                            <input type="hidden" class="exchange_rate_amount_val">

                                            <label style="display: inline">Exchange Rate:<span class="text-danger">*</span></label>
                                            @can('exchange-rate-create')
                                                <span class="add_exchange_rate_create_button">
                                                <button type="button" class="add_exchange_rate_get_sort_order" data-toggle="modal"
                                                        style="background: transparent;border: 0"
                                                        data-target="#exchange_rate_modal" >
                                                    <i class="fa fa-plus"
                                                       style="font-size: 10px !important;color: #3c8dbc !important"></i>
                                                </button>
                                                    </span>
                                            @endcan
                                            <input type="text" name="exchange_rate_amount_val"
                                                   class=" form-control exchange_rate_amount_text exchange_rate_amount_val"
                                                   readonly dir="rtl">


                                        </div>
                                    </div>
                                </div>
                            </section>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group"><label>Contract Amount (<span class="currency_change_from">__</span>):<span class="text-danger">*</span></label>
                                        <input type="text" style="background-color: #FFCCCB;" dir="rtl"
                                               data-type="amount" name="amount" id="amount"


                                               class="form-control contract_amount number_formated_two" readonly
                                        >

                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Contract Amount (<span class="currency_change">__</span>):<span class="text-danger">*</span></label>
                                        <input type="text" style="background-color: #FFCCCB;" dir="rtl"
                                               data-type="amount" name="amount_pkr" id="amount_pkr"


                                               class="form-control contract_amount_pkr number_formated_two" readonly
                                        >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Payment Origin:<span class="text-danger">*</span></label>
                                <select
                                        name="payment_id" id="payment_id"
                                        class="form-control"
                                        style="background-color: #FFCCCB;" required>
                                    <option value="">-CHOOSE-</option>
                                    @foreach(\App\Helpers\Helper::paymentMethod() as $pay)
                                        <option value="{{$pay->id}}">{{$pay->payment_method_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            {{--<div class="form-group">--}}
                            {{--<label>Contract Currency: &nbsp;</label>--}}
                            {{--<select name="currency_id" id="currency_id" class="form-control"--}}
                            {{--style="background-color: #FFCCCB;">--}}
                            {{--<option value="">Select Currency</option>--}}
                            {{--@foreach(\App\Helpers\Helper::currency() as $cur)--}}
                            {{--<option value="{{$cur->id}}">{{ucfirst($cur->currency_name)}}</option>--}}
                            {{--@endforeach--}}
                            {{--</select>--}}
                            {{--</div>--}}
                            <label>Performance Security/Bank Guarantee:</label>
                            <div style="border: 1px solid #caa0aa;padding: 15px">


                                <div class="row">
                                    <div class="col-6">
                                        <label>Select Currency:</label>

                                        <select class="form-control" style="font-size: 14px !important;height: 35px !important;" name="ps_currency">
                                            <option value="">Select Currency</option>
                                            @foreach(\App\Helpers\Helper::currency() as $cur)
                                                <option value="{{$cur->id}}">{{ucfirst($cur->currency_name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-6">
                                        <label> PS Amount</label>

                                        <input class="form-control number_formated_two text-right" type="text"
                                               style="font-size:12px !important;"
                                               id="ps_amount" name="ps_amount" value="{{  old('ps_amount') ?? 0 }}">

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <label> PS Validity:</label>

                                        <input placeholder="dd-mm-yyyy" type="text" class="form-control ps_validity"
                                               id="#ps_validity" name="ps_validity">

                                    </div>
                                    <div class="col-6">
                                        <label> Instrument Type</label>

                                        <select class="form-control" style="font-size: 14px !important;height: 35px !important;" name="instrument_type">
                                            <option value=''>Select</option>
                                            <option value="Bank Guarantee">Bank Guarantee</option>
                                            <option value="CBR">CBR</option>
                                            <option value="Insurance Bond">Insurance Bond</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-12">
                                        <label>Attachment</label>
                                        <input type="file" class="form-control-file"
                                               name="performance_security_file">
                                    </div>
                                </div>


                            </div>


                            <div class="form-group"><label>Contract Description</label>
                                <textarea class="form-control" id="contract_description" name="contract_description"
                                          placeholder="Description"></textarea>
                            </div>

                        </div>

                        <div class="col-7">
                            <div class="form-row">

                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Contract Signed on:<span class="text-danger">*</span></label>
                                        <input placeholder="dd-mm-yyyy" type="text" class="form-control contract_date" id="#contract_date" name="contract_date" required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Less Retention:<small>(If Applicable)</small></label>
                                        <input type="number" class="form-control text-right" name="less_retention" value="0" max="100" min="0">
                                    </div>
                                </div>
                            </div>


                            {{--<label>Contract Date</label>--}}
                            <div style="border: 1px solid #caa0aa;padding: 15px">
                                <div class="row">

                                    <div class="col-5">
                                        <label>Planned Start Date:<span class="text-danger">*</span></label>

                                        <input placeholder="dd-mm-yyyy" type="text" style="width:100%;"
                                               class="form-control contract_planned_start_date contract_planned contract_planned_start_dateChange"
                                               id="#contract_planned_start_date" name="planned_start_date"
                                               required 
                                        >

                                    </div>
                                    <div class="col-1"></div>
                                    <div class="col-5">
                                        <label> Planned Completion Date:<span class="text-danger">*</span></label>

                                        <input placeholder="dd-mm-yyyy" type="text"
                                               class="form-control contract_planned_end_date contract_planned contract_planned_end_date_dateChange"
                                               id="#contract_planned_end_date" name="planned_end_date"
                                               required 
                                        >
                                    </div>
                                    <div class="col-1"></div>
                                </div>

                                <div class="row">

                                    <div class="col-5">
                                        <label>Actual Start Date:<span class="text-danger">*</span></label>
                                        <input placeholder="dd-mm-yyyy" type="text"
                                               class="form-control contract_start_date"
                                               id="#contract_start_date" name="contract_start_date"
                                               required 
                                        >

                                    </div>
                                    <div class="col-1"></div>
                                    <div class="col-5">
                                        <label>Actual Completion Date:<span class="text-danger">*</span></label>

                                        <input placeholder="dd-mm-yyyy" type="text"
                                               class="form-control contract_end_date"
                                               id="#contract_end_date" name="contract_end_date"
                                               required 
                                        >


                                    </div>
                                    <div class="col-1"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="button" style="color:black"
                                        class=" btn btn-default btn-sm add-contract-file-create btn btn-default">
                                    Add File
                                    <li class="fa fa-file" style="color:#3c8dbc"></li>
                                </button>
                                <div style="display: none" class="file_contract_messege">
                                    <div class="alert alert-success alert-block message_contract">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>File Sucessfully Add</strong>
                                    </div>
                                </div>
                                <div class="progress file_progress" style="display: none">
                                    <div class="progress-bar"></div>
                                </div>
                                <div class="table-wrapper-scroll-y my-custom-scrollbar"
                                     style="height: 200px;border: darkgray solid 0.5px">

                                    <table class="contract_file_create table-bordered"
                                           style="margin-bottom: 0px;width: 100% !important;">
                                        <thead>
                                        <tr>
                                            <th colspan="6" style="background: #5f5f5f!important;color: white;">
                                                Add contract files


                                            </th>

                                        </tr>

                                        <tr>
                                            <th style="width:5% !important;"></th>
                                            <th style="width:33% !important;">File</th>
                                            <th style="width:20% !important;">Category</th>
                                            <th style="width: 20% !important;">Amount</th>
                                            <th style="width: 20% !important;">Description</th>
                                            <th style="width:2% !important;"></th>


                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                                <button type="button"
                                        class=" btn btn-default btn-sm delete-contract-file btn btn-default"
                                        style=";color:black">
                                    Delete Row
                                </button>
                            </div>


                            <div class="form-group">
                                <button type="button" style="color:black"
                                        class=" btn btn-default btn-sm add-contract-file-retain-create btn btn-default">
                                    Add File
                                    <li class="fa fa-file" style="color:#3c8dbc"></li>
                                </button>
                                <div style="display: none" class="file_contract_messege_addendum">
                                    <div class="alert alert-success alert-block message">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>File Successfully Add</strong>
                                    </div>
                                </div>
                                <div class="progress file_progress_retain" style="display: none">
                                    <div class="progress-bar file_progress_retain-bar"></div>
                                </div>
                                <div class="table-wrapper-scroll-y my-custom-scrollbar"
                                     style="height: 200px;border: darkgray solid 0.5px">

                                    <table class="contract_file_retain_create table-bordered"
                                           style="margin-bottom: 0px;width: 100% !important;">
                                        <thead>
                                        <tr>
                                            <th colspan="6" style="background: #5f5f5f!important;color: white;">
                                                Addendum Files

                                            </th>

                                        </tr>
                                        <tr>

                                            <th style="width:5% !important;"></th>
                                            <th style="width:33% !important;">File</th>
                                            <th style="width:20% !important;">Date</th>
                                            <th style="width: 20% !important;">Amount</th>
                                            <th style="width: 20% !important;">Description</th>
                                            <th style="width:2% !important;"></th>


                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                                <button type="button"
                                        class=" btn btn-sm delete-contract-retain-file btn btn-default"
                                        style="color:black">
                                    Delete Row
                                </button>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>


        <div class="col">
            <div class="card card-outline card-primary">
                <!-- /.card-header -->
                <div class="card-body">

                    <button type="button" class="add-row-contract" style="border: none">
                        <li class="fa fa-plus" style="color:#3c8dbc"></li> Add Row
                    </button>

                    <div class="table-responsive">
                        <table class="contract_table_dymanic table-sm table table-hover" style="border-collapse:separate; border-spacing:3px 3px; width: 200%">
                            <thead style="background: #cacfcf ;color:#404040">
                                <tr align="center">
                                    <th style="width: 2%">Sr.#</th>
                                    <th style="width:10%;">Project<span class="text-danger">*</span></th>
                                    <th style="width:10%;">Measure<span class="text-danger">*</span></th>
                                    <th style="width:10%;">Sub-Category</th>
                                    <th style="width:10%;">Province<span class="text-danger">*</span></th>
                                    <th style="width:10%;">PEA<span class="text-danger">*</span></th>
                                    <th style="width:10%;">City<span class="text-danger">*</span></th>
                                    <th style="width:10%;">Facility</th>
                                    <th style="width:10% !important;background:#16d2d4;color:white">
                                        Amount (<span class="currency_change_from">__</span>)
                                    </th>
                                    <th style="width:10% !important;background:#4966ff;color:white">
                                        Amount (<span class="currency_change">__</span>)
                                    </th>
                                    <th style="width:10%;">Description</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="7"></td>
                                    <td style="text-align: right"><b>Total</b></td>
                                    <td>
                                    {{--<div class="input-group-prepend">--}}
                                    {{--<button type="button" class="btn btn-default"--}}
                                    {{--style=" height: 30px;padding-top: 3px !important;">Total--}}
                                    {{--</button>--}}
                                    {{--</div>--}}
                                    <!-- /btn-group -->
                                        <input type="text" class="form-control contract_total_amount number_formated_two"
                                               style="text-align: right" readonly="">

                                    </td>
                                    <td>
                                    {{--<div class="input-group-prepend">--}}
                                    {{--<button type="button" class="btn btn-default"--}}
                                    {{--style=" height: 30px;padding-top: 3px !important;">Total--}}
                                    {{--</button>--}}
                                    {{--</div>--}}
                                    <!-- /btn-group -->
                                        <input type="text" class="form-control contract_total_amount_pkr number_formated_two"
                                               style="text-align: right" readonly="">

                                    </td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>  

                    <button type="button" class="delete-row-contract btn btn-default" style="display: none">Delete Row
                    </button>
                    <br>

                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <div class="card card-default collapsed-card-show">
                <div class="card-header">
                    <h3 class="card-title" style="">Create Payment Plan <span style="color: red;">*</span></h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool " data-card-widget="collapse"><i
                                    class="fas fa-plus"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <h4 style="text-align:center; padding-bottom:0%"><span style="">PAYMENT PLAN</span></h4>
                    <button id="payment_plan_create_button" style="color:black;margin-bottom: 5px" type="button"
                            class="btn btn-default"><i
                                class="fas fa-plus"></i>Add Plan
                    </button>
                    <div class="row">

                        <div class="col-12">
                            <table class=" table-sm payment_plan_create" id="payment_plan_create" style="  border-collapse:separate;
  border-spacing:3px 3px;">
                                <thead style="background: #cacfcf ;color:#404040">
                                <tr>
                                    <th style="width: 5%">
                                        Sr.#
                                    </th>
                                    <th style="width: 15%">
                                        Instalment No.
                                    </th>
                                    <th style="width: 15%">
                                        Schedule Date<span class="text-danger">*</span>
                                    </th>
                                    <th style="width: 10%">
                                        % age<span class="text-danger">*</span>
                                    </th>

                                    <th style="width: 15%;background:#4966ff;color:white;vertical-align: middle">
                                        Amount (<span
                                                class="currency_change">__</span>)
                                    </th>

                                    <th style="width: 15%;vertical-align:middle !important;text-align: center;background:#16d2d4;color:white">
                                        Amount (<span class="currency_change_from">__</span>)
                                    </th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                <tr>
                                    <th style="width: 5%">

                                    </th>
                                    <th style="width: 5%">

                                    </th>
                                    <th style="width: 15%;text-align:left">
                                        Total
                                    </th>
                                    <th style="width: 10%">
                                        <input class="form-control total_payment_percentage" style="text-align: center"
                                               type="text" readonly>
                                    </th>
                                    <th style="width: 10%">
                                        <input class="form-control total_payment_amount_pkr" type="text"
                                               style="text-align: right" readonly>
                                    </th>
                                    <th style="width: 10%">
                                        <input class="form-control total_payment_amount" type="text"
                                               style="text-align: right" readonly>
                                    </th>


                                </tr>
                                </tfoot>
                            </table>
                            <button type="button" class="delete-row-payment-plan btn btn-default">Delete Row</button>
                        </div>
                        <div class="col-4"></div>
                    </div>
                </div>
            </div>
            {{--End Sub Category form--}}
            {{--Start Buttons--}}
            <div class="col">
                <button id="addContract" type="submit" class="btn btn-block btn-success btn-lg addContract"
                        style="float:right;"
                        data-contractid="0">Save
                </button>
                <button id="cancelall" type="button" class="btn btn-block btn-primary btn-lg" style="float:right;">
                    Cancel
                </button>
            </div>
        </div>


    </form>
    @include('admin.contracts.exchangeRateForm')
    {{--End Buttons--}}
@endsection
