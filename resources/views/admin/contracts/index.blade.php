@extends('admin.layout.index')
@section('content')
    <section class="bg-primary content-header" style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Contracts</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li>
                        <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li>
                        <i class="fa fa-angle-right" style="color: #ccc;"></i> Contracts &nbsp;
                    </li>
                    <li class="active">
                        <i class="fa fa-angle-right" style="color: #ccc;"></i> newcontract
                    </li>
                </ol>
            </div>
        </div>
    </section>
    <div class="clearfix mx-2 mt-3">
        <div class="float-left">
            @can('contracts-create')
                <a href="{{ route('contract.create') }}" class="btn btn-success">
                    <i class="fa fa-plus"></i> New Contract
                </a>
            @endcan
        </div>

        <div class="float-right">
            @can('contracts-restore')
                <a href="{{ route('contract.contractDeleted') }}" class="btn btn-danger">
                    <i class="fas fa-trash-alt"></i>
                </a>
            @endcan
        </div>
    </div>
    <div class="container-fluid" style="margin-top: 10px">
        <div class="card">
            <div class="card-body">
                <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12 col-md-6"></div>
                        <div class="col-sm-12 col-md-6"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="contract_table" class=" table-bordered table-hover dataTable dtr-inline"
                                   role="grid" aria-describedby="example2_info">
                                <thead style="    background-color: #65a3c6;
                                color: #2c2c2c;">
                                <tr>
                                    <th style="width: 3%!important;">ID</th>
                                    <th style="width: 7%">Status</th>

                                    <th style="width:10%">Reference Code</th>

                                    <th style="width:20%">Description</th>
                                    <th style="width:10%">Vendor</th>
                                    <th style="width:10%">Original Amount</th>
                                    <th style="width:5%">Currency</th>
                                    <th style="width:10%">Revised Amount</th>
                                    <th style="width:10%">Paid Amount</th>

                                    <th style="width: 15%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($contracts as $index=>$contract)
                                    <tr>
                                        <td style="text-align: center">{{++$index}}</td>
                                        <td>
                                            @if($contract->status===0)
                                                <span class="badge badge-warning badge-pill"><i class="fas fa-exclamation-circle mr-1"></i> Pending </span>
                                            @elseif($contract->status===1)
                                                <span class="badge badge-success badge-pill"><i class="fas fa-check-circle mr-1"></i> Approved </span>
                                            @elseif($contract->status===2)
                                                <span class="badge badge-danger badge-pill"><i class="fas fa-times-circle mr-1"></i> Rejected </span>
                                            @endif
                                        </td>
                                        <td style="white-space: -moz-pre-wrap !important;  /* Mozilla, since 1999 */
    white-space: -pre-wrap;      /* Opera 4-6 */
    white-space: -o-pre-wrap;    /* Opera 7 */
    white-space: pre-wrap;       /* css-3 */
    word-wrap: break-word;       /* Internet Explorer 5.5+ */
    white-space: -webkit-pre-wrap; /* Newer versions of Chrome/Safari*/
    word-break: break-all;
    white-space: normal;">{{$contract->contract_title}}</td>

                                        <td style="white-space: -moz-pre-wrap !important;  /* Mozilla, since 1999 */
    white-space: -pre-wrap;      /* Opera 4-6 */
    white-space: -o-pre-wrap;    /* Opera 7 */
    white-space: pre-wrap;       /* css-3 */
    word-wrap: break-word;       /* Internet Explorer 5.5+ */
    white-space: -webkit-pre-wrap; /* Newer versions of Chrome/Safari*/
    word-break: break-all;
    white-space: normal;">{{$contract->contract_description}}</td>
                                        <td>{{$contract->hasContractor->contractor_name}}</td>
                                        <td style="text-align: right;" class="two_decimal">{{$contract->orginal_amount	}}</td>
                                        <td style="text-align: center;">{{$contract->hasExChangeRate->currencyFrom->currency_code	}}</td>
                                        <td style="text-align: right;" class="two_decimal">{{$contract->amount}}</td>
                                        <td style="text-align: right;" class="two_decimal">
                                            {{ $contract->payments()->sum('invoice_amount') ?? '0' }}
                                        </td>

                                        <td data-id="1" style="text-align: center">
                                            <a class="green btn-view-contract" data-toggle="tooltip"
                                               href="{{route('contract.show',['id'=>$contract->id])}}"
                                               data-original-title="Show Contract"><i
                                                        class="ace-icon fa fa-eye"></i></a>
                                            &nbsp;
                                            @if(\Illuminate\Support\Facades\Auth::user()->getRoleNames()[0]=='Admin')
                                                <a
                                                        class="green btn-edit-contract" data-toggle="tooltip" title=""
                                                        data-id="1" data-original-title="Edit"
                                                        href="{{route('contract.edit',['id'=>$contract->id])}}"
                                                ><i style='font-size:10px' class=" fas fa-pencil-alt"></i></a>&nbsp;
                                            @elseif(\Illuminate\Support\Facades\Auth::user()->getRoleNames()[0]=='CEO')
                                                <a
                                                        class="green btn-edit-contract" data-toggle="tooltip" title=""
                                                        data-id="1" data-original-title="Edit"
                                                        href="{{route('contract.edit',['id'=>$contract->id])}}"
                                                ><i style='font-size:10px' class=" fas fa-pencil-alt"></i></a>&nbsp;
                                            @else
                                                @if($contract->status!=1 AND $contract->status!=2)
                                                    <a
                                                            class="green btn-edit-contract" data-toggle="tooltip"
                                                            title=""
                                                            data-id="1" data-original-title="Edit"
                                                            href="{{route('contract.edit',['id'=>$contract->id])}}"
                                                    ><i style='font-size:10px' class=" fas fa-pencil-alt"></i></a>&nbsp;
                                                @endif
                                            @endif
                                            @if(\Illuminate\Support\Facades\Auth::user()->getRoleNames()[0]=='Admin')
                                                <a
                                                        href="{{route('contract.delete',['id'=>$contract->id])}}"
                                                        class="green btn-delete-contract" data-toggle="tooltip" title=""
                                                        data-id="1" data-original-title="Remove"><i
                                                            class="ace-icon fa fa-trash"></i></a>&nbsp;
                                            @elseif(\Illuminate\Support\Facades\Auth::user()->getRoleNames()[0]=='CEO')
                                                <a
                                                        href="{{route('contract.delete',['id'=>$contract->id])}}"
                                                        class="green btn-delete-contract" data-toggle="tooltip" title=""
                                                        data-id="1" data-original-title="Remove"><i
                                                            class="ace-icon fa fa-trash"></i></a>&nbsp;

                                            @endif
                                            {{--@can('contract-status')--}}
                                            @if(\Illuminate\Support\Facades\Auth::user()->getRoleNames()[0]=='Admin')
                                                <a class="green btn-revise-project" data-toggle="tooltip" title=""
                                                   href="{{route('contract.status',['id'=>$contract->id,'status'=>'revise'])}}"
                                                   data-original-title="Pending"><i
                                                            class="ace-icon fa fa-clock-o"></i></a>&nbsp;
                                            @elseif(\Illuminate\Support\Facades\Auth::user()->getRoleNames()[0]=='CEO')
                                                <a class="green btn-revise-project" data-toggle="tooltip" title=""
                                                   href="{{route('contract.status',['id'=>$contract->id,'status'=>'revise'])}}"
                                                   data-original-title="Pending"><i
                                                            class="ace-icon fa fa-clock-o"></i></a>&nbsp;

                                            @endif
                                            {{--@endcan--}}
                                            {{--@can('contract-status')--}}
                                            @if(\Illuminate\Support\Facades\Auth::user()->getRoleNames()[0]=='Admin')
                                                <a class="green btn-approve-project" data-toggle="tooltip" title=""
                                                   href="{{route('contract.status',['id'=>$contract->id,'status'=>'approve'])}}"
                                                   data-original-title="Approve"><i
                                                            class="ace-icon fa fa-check-square"></i></a>&nbsp;
                                            @elseif(\Illuminate\Support\Facades\Auth::user()->getRoleNames()[0]=='CEO')
                                                <a class="green btn-approve-project" data-toggle="tooltip" title=""
                                                   href="{{route('contract.status',['id'=>$contract->id,'status'=>'approve'])}}"
                                                   data-original-title="Approve"><i
                                                            class="ace-icon fa fa-check-square"></i></a>&nbsp;

                                            @endif
                                            {{--@endcan--}}
                                            {{--@can('projects-status')--}}
                                            @if(\Illuminate\Support\Facades\Auth::user()->getRoleNames()[0]=='Admin')
                                                <a class="green btn-reject-project" data-toggle="tooltip" title=""
                                                   href="{{route('contract.status',['id'=>$contract->id,'status'=>'reject'])}}"
                                                   data-original-title="Reject"
                                                   aria-describedby="tooltip136991"><i
                                                            class="ace-icon fa fa-arrow-left"></i></a>&nbsp;
                                                {{--@endcan--}}
                                            @elseif(\Illuminate\Support\Facades\Auth::user()->getRoleNames()[0]=='CEO')
                                                <a class="green btn-reject-project" data-toggle="tooltip" title=""
                                                   href="{{route('contract.status',['id'=>$contract->id,'status'=>'reject'])}}"
                                                   data-original-title="Reject"
                                                   aria-describedby="tooltip136991"><i
                                                            class="ace-icon fa fa-arrow-left"></i></a>&nbsp;

                                            @endif
                                            {{--<a--}}
                                            {{--class="green btn-show-payments" data-toggle="tooltip" title=""--}}
                                            {{--data-id="1" data-original-title="Show Payments"><i--}}
                                            {{--class="ace-icon fa fa-file-powerpoint-o"></i></a>--}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
@endsection