<div class="modal fade" id="exchange_rate_modal">
    <form>

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style=" background-color: #65a3c6;
color: #2c2c2c;">
                    <h4 class="modal-title">Exchange Rate</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    <div class="form-group">
                        <label>Sort Order&nbsp;</label>
                        <input

                                oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"
                                type="text" placeholder="Sort Order" name="sort_order" class="form-control  current_exchange_rate_sort_order

            @error('sort_order') is-invalid @enderror" value="{{ old('sort_order') }}">
                        @error('sort_order')
                        <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group">
                        <label>Start Date&nbsp;</label>
                        <input type="text" placeholder="Currency Name"
                               class="form-control @error('start_date') is-invalid @enderror current_exchange_rate_start_date"
                               name="start_date"
                               value={{ old('start_date') }}>
                        @error('start_date')
                        <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group">
                        <label>End Date&nbsp;</label>
                        <input type="text" placeholder="Currency Code"
                               class="form-control @error('end_date') is-invalid @enderror current_exchange_rate_end_date"
                               name="end_date"
                               value={{ old('end_date') }}>
                        @error('end_date')
                        <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>From &nbsp;</label>
                                <select name="currency_from"
                                        class="currency_from form-control @error('currency_from') is-invalid @enderror current_exchange_rate_currency_from"
                                        id="currency_from" value={{ old('currency_from') }}>
                                    <option value="">Select Currency</option>
                                    @foreach(\App\Helpers\Helper::currency() as $cur)
                                        <option value="{{$cur->id}}">{{$cur->currency_name}}</option>
                                    @endforeach
                                </select>
                                @error('currency_from')
                                <div class="invalid-feedback">{{ $message }}</div> @enderror
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>To&nbsp;</label>
                                <select name="currency_to"
                                        class="currency_to form-control @error('currency_to') is-invalid @enderror current_exchange_rate_currency_to"
                                        id="currency_to" value={{ old('currency_to') }}>
                                    <option value="">Select Currency</option>
                                    @foreach(\App\Helpers\Helper::currency() as $cur)
                                        <option value="{{$cur->id}}">{{$cur->currency_name}}</option>
                                    @endforeach
                                </select>
                                @error('currency_to')
                                <div class="invalid-feedback">{{ $message }}</div> @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Exchange Rate Value&nbsp;</label>
                        <input type="text" dir="rtl"
                               class="form-control exchange_rate_create @error('exchange_rate') is-invalid @enderror current_exchange_rate_exchange_rate_create"
                               name="exchange_rate"

                               value={{ old('exchange_rate') }}>
                        @error('exchange_rate')
                        <div class="invalid-feedback">{{ $message }}</div> @enderror

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary exchangerate_save">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">cancel</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </form>
    <!-- /.modal-dialog -->
</div>
