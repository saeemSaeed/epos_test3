@extends('admin.layout.index')
@section('content')
<section class="bg-primary content-header"
    style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
    <div class="row">
        <div class="col-8">
            <h1>Contracts</h1>
        </div>
        <div class="col-4">
            <ol class="breadcrumb" style="color:#444;float: right">
                <li>
                    <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                </li>
                <li>
                    <i class="fa fa-angle-right" style="color: #ccc;"></i> Contracts &nbsp;
                </li>
                <li class="active">
                    <i class="fa fa-angle-right" style="color: #ccc;"></i> newcontract
                </li>
            </ol>
        </div>
    </div>
</section>

<div class="container-fluid" style="margin-top: 10px">
    <div class="card">
        <div class="card-body">
            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="contract_table" class="table table-bordered table-hover dataTable dtr-inline"
                            role="grid" aria-describedby="example2_info">
                            <thead style="    background-color: #65a3c6;
                                color: #2c2c2c;">
                                <tr role="row">
                                    <th style="width: 5%">ID</th>
                                    <th>Status</th>
                                    <th>Reference Code</th>
                                    <th>Vendor</th>
                                    <th style="width: 15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($deletedContracts as $index=>$contract)
                                <tr role="row" class="odd">
                                    <td class="" style="text-align: center">{{++$index}}</td>
                                    <td class="">
                                        @if($contract->status===0)
                                        <span class="badge badge-warning "> Pending </span>
                                        @elseif($contract->status===1)
                                        <span class="badge badge-success "> Approved </span>
                                        @elseif($contract->status===-1)
                                        <span class="badge badge-danger"> Rejected </span>
                                        @endif
                                    </td>
                                    <td style="white-space: -moz-pre-wrap !important;  /* Mozilla, since 1999 */
    white-space: -pre-wrap;      /* Opera 4-6 */
    white-space: -o-pre-wrap;    /* Opera 7 */
    white-space: pre-wrap;       /* css-3 */
    word-wrap: break-word;       /* Internet Explorer 5.5+ */
    white-space: -webkit-pre-wrap; /* Newer versions of Chrome/Safari*/
    word-break: break-all;
    white-space: normal;">{{$contract->contract_title}}</td>
                                    <td>{{$contract->hasContractor->contractor_name}}</td>
                                    <td class="sorting_1 dtr-control" style="text-align: center!important;">
                                        <a type="submit" style="border: none;background-color: transparent" data-toggle="tooltip" title="" href="{{route('contract.restore',['id'=>$contract->id])}}" data-original-title="Restore"><i class="fas fa-trash-restore"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</div>
@endsection