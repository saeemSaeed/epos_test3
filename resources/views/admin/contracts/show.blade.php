@extends('admin.layout.index')
@section('content')

    <style>
        label {
            font-size: 12px !important;
            color: #2a272a !important;
            margin-bottom: 0.1rem !important;
        }

        input {
            font-size: 14px !important;
            height: 30px !important;
        }

        select {
            font-size: 12px !important;
            height: 27px !important;
        }

        .form-group {
            margin-bottom: 0.5rem;
            padding-top: 5px !important;
        }

        .fieldset {
            position: relative;
            border: 1px #d5d5d5 solid;
            padding: 1rem;
        }

        .fieldset h1 {
            position: absolute;
            top: 0;
            font-size: 16px;
            line-height: 1;
            margin: -9px 0 0 -0.5rem;
            background: #fff;
            padding: 0 0.5rem;
            color: #2d2d2d;
        }

        .fieldset h4 {
            position: absolute;
            top: 0;
            font-size: 16px;
            line-height: 1;
            margin: -9px 0 0 -0.5rem;
            background: #fff;
            padding: 0 0.5rem;
            color: #2d2d2d;
        }
    </style>
    <section class="bg-primary content-header"
             style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Contracts</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li><i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li><i class="fa fa-angle-right" style="color: #ccc;"></i> Contracts &nbsp;</li>
                    <li class="active"><i class="fa fa-angle-right" style="color: #ccc;"></i>Show</li>
                </ol>
            </div>
        </div>
    </section>
    {{--Start create form--}}

    <input type="hidden" name="contract_id" id="contract_id" value="{{$contract->id}}">
    <div class="col" style="margin-top: 10px">
        <div class="card card-outline card-primary">
            <div class="card-header">
                <h3 class="card-title">Edit Contract</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
                <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body" style="display: block;">
                <div class="row">
                    <div class="col-5">

                        <div class="form-group">
                            <label>Contract Title/Reference Code: &nbsp;</label>
                            <input type="text" style="background-color: #FFCCCB;" class="form-control"
                                   name="contract_title_edit" value="{{$contract->contract_title}}" readonly>
                        </div>
                        {{--<div class="text-center"><span><b><a class="btn-add-contractor" style="float:right;"--}}
                        {{--href="{{ route('vendor.create') }}">Add Vendor</a> </b></span>--}}
                        {{--</div>--}}
                        <div class="form-group"><label>Contract Vendor: &nbsp;</label>
                            <select data-live-search="true" data-live-search-style="startsWith" id="select-state"
                                    class="form-control" name="contractor_id_edit" disabled>
                                <option value="">Select Vendor</option>
                                @foreach($contractors as $contractor)
                                    <option value="{{$contractor->id}}"
                                            {{$contractor->id==$contract->contractor_id?"selected":''}}>{{$contractor->contractor_name}}</option>
                                @endforeach
                            </select>
                        </div>





                        <section class="fieldset">
                            <h4>Exchange Rate</h4>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">

                                        <label>Currency From: &nbsp;</label>
                                        <select name="currency_id_from_edit" id="currency_id_from_edit"
                                                class="form-control currency_id_from_edit"
                                                style="background-color: #FFCCCB;" disabled>
                                            <option value="">Select Currency</option>
                                            @foreach(\App\Helpers\Helper::currency() as $cur)
                                                <option value="{{$cur->id}}" {{$cur->id==$contract->hasExChangeRate->currency_from?"selected":""}}>{{ucfirst($cur->currency_name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Currency To: &nbsp;</label>
                                        <select name="currency_id_edit" id="currency_id_edit" class="form-control"
                                                style="background-color: #FFCCCB;" disabled>
                                            <option value="">Select Currency</option>
                                            @foreach(\App\Helpers\Helper::currency() as $cur)
                                                <option
                                                        value="{{$cur->id}}" {{$cur->id==$contract->hasExChangeRate->currency_to?"selected":""}}>{{ucfirst($cur->currency_name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">

                                        <label>Exchange Rate Date:
                                            &nbsp;</label>

                                        <input style=""


                                               value="{{$contract->hasExChangeRate->start_date?\Carbon\Carbon::parse($contract->hasExChangeRate->start_date)->format('d-M-Y'):''}}"
                                               name="exchange_rate_date" id="exchange_rate_date"
                                               class="form-control exchange_rate_date"
                                               readonly disabled/>

                                        <input style=""
                                               type="hidden"

                                               name="exchange_rate_flag" id="exchange_rate_flag"
                                               class="form-control exchange_rate_flag"
                                               value="{{$contract->hasExChangeRate->id?true:false}}" readonly/>


                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">

                                        <label>Exchange Rate:&nbsp;</label>
                                        <input type="text"
                                               value="{{$contract->hasExChangeRate->exchange_rate?$contract->hasExChangeRate->exchange_rate:false}}"
                                               class=" form-control exchange_rate_amount_text exchange_rate_amount_val"
                                               readonly dir="rtl">


                                    </div>
                                </div>
                            </div>
                        </section>

                        <section class="fieldset">
                            <h4>Contract Amount</h4>
                            <div class="row">
                                <div class="col-4">

                                </div>
                                <div class="col-4">
                                    <P style="text-align: center;background:#4966ff;color:white" >{{isset($contract->hasExChangeRate->currencyFrom->currency_code)
                                        ?$contract->hasExChangeRate->currencyFrom->currency_code:"__"}}</P>
                                </div>
                                <div class="col-4">
                                    <p style="text-align: center;background:#16d2d4;color:white">  <span class="currency_change">
												{{isset($contract->hasExChangeRate->currencyTo->currency_code)
                                        ?$contract->hasExChangeRate->currencyTo->currency_code:"__"}}</span></p>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-4">
                                    Original Amount

                                </div>
                                <div class="col-4">
                                    <input type="text" style="" dir="rtl"
                                           data-type="amount" name="amount_edit" id="amount_edit" placeholder="0.00"
                                           value="{{$contract->orginal_amount}}"
                                           class="form-control   number_formated_two" readonly
                                    >
                                </div>
                                <div class="col-4">
                                    <input type="text"  dir="rtl"
                                           data-type="amount" name="amount_pkr" id="amount_pkr" placeholder="0.00"
                                           value="{{$contract->orginal_amount_pkr}}"
                                           class="form-control    number_formated_two"
                                           readonly
                                    >
                                </div>
                            </div>


                            <div class="row" style="margin-top: 10px    ">
                                <div class="col-4">
                                    Revised Amount

                                </div>
                                <div class="col-4">
                                    <input type="text" style="background: none;"  dir="rtl"
                                           data-type="amount" id=""
                                           value="{{$contract->amount}}"
                                           class="form-control   number_formated_two"
                                           readonly
                                    >
                                </div>
                                <div class="col-4">
                                    <input type="text" style="background: none;"  dir="rtl"
                                           data-type="amount" id=""
                                           placeholder="0.00"
                                           value="{{$contract->amount_pkr   }}"
                                           class="form-control    number_formated_two"
                                           readonly

                                    >
                                </div>
                            </div>

                        </section>
                        <div class="form-group">
                            <label>Payment Origin: &nbsp;</label>
                            <select
                                    name="payment_id_edit" id="payment_id_edit" disabled
                                    class="form-control"
                                    style="background-color: #FFCCCB;">
                                <option value="">-CHOOSE-</option>
                                @foreach(\App\Helpers\Helper::paymentMethod() as $pay)
                                    <option
                                            value="{{$pay->id}}" {{$pay->id==$contract->payment_id?"selected":""}}>{{$pay->payment_method_name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <label>Performance Security/Bank Guarantee:</label>


                        <div style="border: 1px solid #caa0aa;padding: 15px">


                            <div class="row">
                                <div class="col-6">
                                    <label> Select Currency</label>

                                    <select class="form-control" style="   font-size: 14px !important;
                    height: 35px !important;"

                                            name="ps_currency"
                                            disabled>
                                        <option value="">Select Currency</option>
                                        @foreach(\App\Helpers\Helper::currency() as $cur)
                                            <option value="{{$cur->id}}" {{$cur->id==$contract->ps_currency?"selected":""}}>{{ucfirst($cur->currency_name)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6">
                                    <label> PS Amount</label>

                                    <input class="form-control number_formated_two" type="text"
                                           style="font-size:12px !important;text-align:right"
                                           id="ps_amount" name="ps_amount" value="{{$contract->ps_amount}}" readonly>

                                </div>

                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <label> Instrument Type</label>

                                    <select class="form-control" style="   font-size: 14px !important;
                    height: 35px !important;" value="{{$contract->instrument_type}}"

                                            name="instrument_type"
                                            disabled>
                                        <option value=''>Select</option>
                                        <option value="Bank Guarantee" {{ $contract->instrument_type=="Bank Guarantee"?"selected":'' }}>
                                            Bank Guarantee
                                        </option>
                                        <option value="CBR" {{ $contract->instrument_type=="CBR"?"selected":'' }}>CBR
                                        </option>
                                        <option value="Insurance Bond" {{ $contract->instrument_type=="Insurance Bond"?"selected":'' }}>
                                            Insurance Bond
                                        </option>
                                    </select>
                                </div>
                                <div class="col-6">
                                    <label> PS Validity</label>

                                    <input placeholder="dd-mm-yy" type="text"
                                           class="form-control "
                                           name="ps_validity"
                                           value="{{$contract->ps_validity?\Carbon\Carbon::parse($contract->ps_validity)->format('d-M-Y'):''}}"
                                           readonly>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">

                                    <label>Attachment update</label>


                                </div>
                                <div class="col-6">
                                    <label>Attachment Download</label><br>
                                    @if($contract->performance_security_file)
                                        <a href="{{ URL::to( '/files/contract/performance_security_file/' . $contract->performance_security_file)  }}"
                                           target="_blank" download="{{$contract->performance_security_file_name}}">
                                            {{$contract->performance_security_file_name}}
                                            <i class=" fa-lg fas fa-file-download"></i>
                                        </a>
                                    @endif
                                </div>
                            </div>


                            <div class="row">
                                {{--<div class="col-6">--}}
                                {{--<label> Instrument Type</label>--}}
                                {{--<input type="text" readonly value="{{$contract->instrument_type}}" rea>--}}

                                {{--</div>--}}
                                <div class="col-6">
                                    <label>Attachment</label><br>
                                    <a href="{{ URL::to( '/files/contract/performance_security_file/' . $contract->performance_security_file)  }}"
                                       target="_blank"
                                       download="{{$contract->performance_security_file_name}}"> {{$contract->performance_security_file_name}}
                                        <i class=" fa-lg fas fa-file-download"></i>
                                    </a>

                                </div>
                            </div>


                        </div>
                        <div class="form-group"><label>Contract Description</label>
                            <textarea class="form-control" id="contract_description_edit" readonly
                                      name="contract_description_edit"
                                      placeholder="Description">{{$contract->contract_description?$contract->contract_description:''}}


                            </textarea>
                        </div>

                    </div>

                    <div class="col-7">
                        <div class="form-row">

                            <div class="col-6">
                                <div class="form-group">
                                    <label>Contract Signed on:</label>
                                    <input placeholder="dd-mm-yyyy" type="text" class="form-control contract_date" id="#contract_date" name="contract_date_edit" value="{{$contract->contract_date ? \Carbon\Carbon::parse($contract->contract_date)->format('d-M-Y') : ''}}" readonly>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="form-group">
                                    <label>Less Retention:<small>(If Applicable)</small></label>
                                    <input type="number" class="form-control" name="less_retention" value="{{ $contract->less_retention }}" placeholder="xxx" dir="rtl" readonly>
                                </div>
                            </div>
                        </div>

                        <div style="border: 1px solid #caa0aa;padding: 15px">
                            <div class="row">
                                <div class="col-5">
                                    <label> Planned Start Date</label>

                                    <div class="input-group date Pickerdate"
                                         id="contract_planned_start_date_edit"
                                         data-target-input="nearest">
                                        <input placeholder="dd-mm-yy" type="text"
                                               class="form-control datetimepicker-input"
                                               readonly
                                               value="{{$contract->planned_start_date?\Carbon\Carbon::parse($contract->planned_start_date)->format('d-M-Y'):''}}"

                                               data-target="#contract_planned_start_date_edit"
                                               name="contract_planned_start_date_edit">
                                        <div class="input-group-append"
                                             data-target="#contract_planned_start_date_edit"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-1"></div>
                                <div class="col-5">
                                    <label> Planned Completion Date</label>
                                    <div class="input-group date Pickerdate" id="contract_planned_end_date_edit"
                                         data-target-input="nearest">
                                        <input placeholder="dd-mm-yy" type="text"
                                               readonly class="form-control datetimepicker-input"

                                               value="{{$contract->planned_end_date?\Carbon\Carbon::parse($contract->planned_end_date)->format('d-M-Y'):''}}"

                                               data-target="#contract_planned_end_date_edit"
                                               name="contract_planned_end_date_edit">
                                        <div class="input-group-append"
                                             data-target="#contract_planned_end_date_edit"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-1"></div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-5">
                                    <label>Actual Start Date</label>
                                    <div class="input-group date Pickerdate" id="contract_start_date_edit"
                                         data-target-input="nearest">
                                        <input placeholder="dd-mm-yy" type="text"
                                               class="form-control datetimepicker-input"

                                               readonly
                                               value="{{$contract->contract_start_date?\Carbon\Carbon::parse($contract->contract_start_date)->format('d-M-Y'):''}}"

                                               data-target="#contract_start_date_edit"
                                               name="contract_start_date_edit">
                                        <div class="input-group-append" data-target="#contract_start_date_edit"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-1"></div>
                                <div class="col-5">
                                    <label>Actual Completion Date</label>
                                    <div class="input-group date Pickerdate" id="contract_end_date_edit"
                                         data-target-input="nearest">
                                        <input placeholder="dd-mm-yy" type="text"
                                               class="form-control datetimepicker-input"

                                               value="{{$contract->contract_end_date?\Carbon\Carbon::parse($contract->contract_end_date)->format('d-M-Y'):''}}"
                                               readonly
                                               data-target="#contract_end_date_edit"
                                               name="contract_end_date_edit">
                                        <div class="input-group-append" data-target="#contract_end_date_edit"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-1"></div>
                            </div>
                        </div>
                        <br>
                        {{--                            <div class="form-group"><label>Performance Security/Bank Guarentee</label>--}}
                        {{--                                <input type="text" name="performance_security_bank_guarentee"--}}
                        {{--                                       id="performance_security_bank_guarentee"--}}
                        {{--                                       class="form-control"--}}
                        {{--                                       >--}}
                        {{--                            </div>--}}
                        {{--                            <div class="form-group"><label>Contract Description</label>--}}
                        {{--                                <textarea class="form-control" id="contract_description" name="contract_description"--}}
                        {{--                                          placeholder="Description"></textarea>--}}
                        {{--                            </div>--}}
                        <div class="form-group">

                            <div class="table-wrapper-scroll-y my-custom-scrollbar"
                                 style="height: 200px!important;border: darkgray solid 0.5px">
                                <table class="contract_file_edit  table-bordered"
                                       style="width:100%;margin-bottom: 0px">

                                    <thead>
                                    <tr>
                                        <th colspan="5" style="background: #5f5f5f!important;color: white;">Add contract
                                            files
                                        </th>

                                    </tr>


                                    <tr>
                                        <th style="width:5% !important;"></th>
                                        <th style="width:35% !important;">File</th>
                                        <th style="width:20% !important;">Category</th>
                                        <th style="width: 20% !important;">Amount</th>
                                        <th style="width: 20% !important;">Description</th>


                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($contract->hasContractFile as $file)

                                        <tr>

                                            {{--{{$file}}--}}
                                            <td style="text-align: center"><input data-id="{{$file->id}}"
                                                                                  type='checkbox' name='record'>
                                            </td>
                                            <td>
                                                @if( !empty($file->file))
                                                    <a href="{{ URL::to( '/files/contract/' . $file->file)  }}" style="    margin-top: 0; white-space: -moz-pre-wrap !important;  /* Mozilla, since 1999 */
    white-space: -pre-wrap;      /* Opera 4-6 */
    white-space: -o-pre-wrap;    /* Opera 7 */
    white-space: pre-wrap;       /* css-3 */
    word-wrap: break-word;       /* Internet Explorer 5.5+ */
    white-space: -webkit-pre-wrap; /* Newer versions of Chrome/Safari*/
    word-break: break-all;
    white-space: normal;"
                                                       target="_blank"
                                                       download="{{$file->file_name}}"> {{$file->file_name}}
                                                        <i class=" fa-lg fas fa-file-download"></i>
                                                    </a>
                                                @endif
                                            </td>

                                            <td style="text-align:center">{{$file->category}}</td>

                                            <th style="text-align:center"><input style="   font-size: 14px !important;
                    height: 35px !important;
                " dir="rtl" type="text" class='form-control number_formated_two' data-toggle="tooltip"
                                                                                 data-original-title="{{$file->contract_file_amount}}"
                                                                                 value="{{$file->contract_file_amount}}"
                                                                                 name="[]" readonly></th>
                                            <td style="text-align:left"><p>{{$file->description}}</p></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>


                        <div class="form-group">

                            <div class="table-wrapper-  scroll-y my-custom-scrollbar"
                                 style="height: 200px!important;border: darkgray solid 0.5px">
                                <table class="contract_file_retain_edit  table-bordered"
                                       style="width:100%;margin-bottom: 0px">

                                    <thead>
                                    <tr>
                                        <th colspan="5" style="background: #5f5f5f!important;color: white;">
                                            Add Addendum
                                        </th>

                                    </tr>
                                    <tr>

                                        <th style="width:5% !important;"></th>
                                        <th style="width:35% !important;">File</th>
                                        <th style="width:20% !important;">Date</th>
                                        <th style="width: 20% !important;">Amount</th>
                                        <th style="width: 20% !important;">Description</th>


                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $counter_r=1;@endphp
                                    @foreach($contract->ContractsHasRetainFile as $file)
                                        <tr>
                                            {{--{{$file}}--}}
                                            <td style="text-align: center"><input data-id="{{$file->id}}"
                                                                                  type='checkbox' name='record'>
                                            </td>
                                            <td>
                                                @if( !empty($file->file))

                                                    <a href="{{ URL::to( '/files/contract/retain' . $file->file)  }}"
                                                       style="    margin-top: 0;white-space: -moz-pre-wrap !important;  /* Mozilla, since 1999 */
    white-space: -pre-wrap;      /* Opera 4-6 */
    white-space: -o-pre-wrap;    /* Opera 7 */
    white-space: pre-wrap;       /* css-3 */
    word-wrap: break-word;       /* Internet Explorer 5.5+ */
    white-space: -webkit-pre-wrap; /* Newer versions of Chrome/Safari*/
    word-break: break-all;
    white-space: normal;"
                                                       target="_blank"
                                                       download="{{$file->file_name}}"> {{$file->file_name}}
                                                        <i class=" fa-lg fas fa-file-download"></i>
                                                    </a>
                                                @endif
                                            </td>
                                            <td style="text-align:center">   {{$file->retain_file_date?\Carbon\Carbon::parse($file->retain_file_date)->format('d-M-Y'):''}}
                                            </td>


                                            <td><input style="   font-size: 14px !important;
                    height: 35px !important;
                " dir="rtl" type="text" class='form-control number_formated_two' value="{{$file->retain_file_amount}}"
                                                       data-toggle="tooltip"
                                                       data-original-title="{{$file->retain_file_amount}}"
                                                       name="contract_file_amount[]" readonly></td>

                                            <td style="text-align:left"><p class="">{{$file->description}}</p></td>
                                        </tr>
                                        @php $counter_r++;@endphp
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <div class="col">
            <div class="card card-outline card-primary">
                <!-- /.card-header -->
                <div class="card-body">
                    <form>

                    </form>
                    <table class="contract_table_dymanic_edit  table-sm" id="contract_table_dymanic-edit" style="  border-collapse:separate;
                        border-spacing:3px 3px;">
                        <thead style="background: #cacfcf ;color:#404040">
                        <tr>
                            <th style="width: 2%">Sr.#</th>
                            <th style="width:10%;text-align:center;">Project</th>
                            <th style="width:10%;text-align:center;">Measure</th>
                            <th style="width:10%;text-align:center;">Sub-Category</th>
                            <th style="width:10%;text-align:center;">Province</th>
                            <th style="width:10%;text-align:center;">PEA</th>
                            <th style="width:10%;text-align:center;">City</th>
                            <th style="width:15% !important;text-align:center;background:#4966ff;color:white">Amount
                                (<span class="currency_change_from">{{isset($contract->hasExChangeRate->currencyFrom->currency_code)?$contract->hasExChangeRate->currencyFrom->currency_code:'__'}}</span>)
                            </th>
                            <th style="width:15% !important;text-align:center;background:#16d2d4 ;color:white">Amount
                                (<span
                                        class="currency_change">{{isset($contract->hasExChangeRate->currencyTo->currency_code)?$contract->hasExChangeRate->currencyTo->currency_code:'__'}}</span>)
                            </th>
                            <th style="width:10%;text-align:center;">Description</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contract->contractBelongsToProvinceDivision as $con)
                            <tr>
                                <th>
                                    <input name='record' id='check_all' type='checkbox' readonly>
                                </th>
                                <td
                                ><select name='project_id_edit[]' class=' form-control project_id_edit ' disabled
                                         id='project_id_edit'>
                                        @if(!is_null($activeProject))
                                            @foreach($activeProject as $sub )
                                                <option
                                                        value="{{$sub->id}}" {{$sub->id===$con->project_id?'selected':''}}>{{$sub->title}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </td>
                                <td
                                ><select name='measure_id_edit[]' class=' form-control contract_measure_id_edit'
                                         id='contract_measure_id_edit' disabled>

                                        @if(!is_null($con->project_id))
                                            @foreach(\App\Helpers\Helper::getProjectMeasures($con->project_id) as $sub )
                                                <option
                                                        value="{{$sub['id']}}" {{$con->measure_id==$sub['id']?'selected':''}}>
                                                    {{$sub['name']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </td>
                                <td
                                >
                                    {{--                                            {{$con->project_id}}--}}
                                    {{--                                            {{$con->measure_id}}--}}
                                    <select name='subcategory_id_edit[]'
                                            class=' form-control subcategory_id_edit '
                                            id='subcategory_id_edit' disabled>
                                        @if((!is_null($con->project_id) ANd !is_null($con->measure_id)))
                                            @forelse(\App\Helpers\Helper::getProjectMeasuresSelectedSubcategory($con->project_id,$con->measure_id) as $index=>$sub )

                                                <option
                                                        value="{{$sub['id']}}"
                                                        {{$con->sub_category_id==$sub['id']?'selected':''}}
                                                >{{$sub['name']}}

                                                </option>
                                            @empty
                                                <option>UnAllocated</option>

                                            @endforelse
                                        @else
                                            <option>UnAllocated</option>

                                        @endif
                                    </select>
                                </td>
                                <td><input type="text" readonly name='province_name_contracts_edit[]'
                                           class=' form-control province_name_contracts_edit'
                                           id='province_name_contracts_edit'
                                           value="{{($con->province_id)?$con->hasProvince->name:""}}">
                                    <input type="hidden" readonly name='province_id_contracts_edit[]'
                                           class="province_id_contracts_edit"
                                           value="{{($con->province_id)?$con->hasProvince->id:""}}"
                                           id='province_id_contracts_edit'>


                                </td>

                                <td><input type="text" readonly name='pea_name_contracts_edit[]'
                                           class=' form-control pea_name_contracts_edit'
                                           id='pea_name_contracts_edit'
                                           value="{{($con->pea_id)?$con->hasPeas->pea_name:''}}">
                                    <input type="hidden" readonly name='pea_id_contracts_edit[]'
                                           class='pea_id_contracts_edit form-control '
                                           value="{{($con->pea_id)?$con->hasPeas->id:""}}"
                                           id='pea_id_contracts_edit[]'>


                                </td>
                                <td><select class="form-control     contract_city_edit" name="contract_city_edit[]"
                                            id="contract_city_edit" disabled>
                                        <option value="">Select</option>

                                        @foreach(\App\Helpers\Helper::GetProvinceCity($con->province_id) as $index => $citys)
                                            @foreach(explode(',', $con->city_id) as $key)
                                                @foreach($citys as $index => $city)

                                                    @if( $key==$city->id)
                                                        <option
                                                                value="{{$city->id}}" {{ $key==$city->id?'selected':''}}>{{$city->city_name}}</option>

                                                    @endif


                                                @endforeach

                                            @endforeach
                                            @if( $key!=$city->id)
                                                <option
                                                        value="{{$city->id}}">{{$city->city_name}}</option>
                                            @endif
                                        @endforeach
                                    </select>


                                </td>

                                @if($contract->hasExChangeRate->currency_from==App\Model\Projects::find($con->project_id)->currency_id)
                                    <td><input type="text"
    
                                               name='contract_province_budget_edit[]'
                                               class='form-control contract_province_budget_edit number_formated_two'
                                               dir='rtl'
                                               value="{{$con->budget}}" readonly
                                               style='width:100% !important;    background-color: #FFCCCB;'></td>
                                    <td><input type="text"
    
                                               name='contract_province_budget_edit_pkr[]'
                                               class='form-control contract_province_budget_edit_pkr  number_formated_two'
                                               dir='rtl'
                                               value="{{$con->budget_pkr}}" readonly
                                               style='width:100% !important;    background-color: #d2d1d1;'></td>
                                @else
                                    <td><input type="text"

                                           name='contract_province_budget_edit[]'
                                           class='form-control contract_province_budget_edit number_formated_two'
                                           dir='rtl'
                                           value="{{$con->budget_pkr}}" readonly
                                           style='width:100% !important;    background-color: #FFCCCB;'></td>
                                    <td><input type="text"
    
                                               name='contract_province_budget_edit_pkr[]'
                                               class='form-control contract_province_budget_edit_pkr  number_formated_two'
                                               dir='rtl'
                                               value="{{$con->budget}}" readonly
                                               style='width:100% !important;    background-color: #d2d1d1;'></td>
                                @endif
                                <td><input type='text' name='description_edit[]'
                                           class='form-control description_edit'
                                           value="{{$con->description}}" readonly></td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="6"></td>
                            <td colspan="" style="text-align: right;"><b>Total</b></td>
                            <td><input type="text" class="form-control contract_total_amount-edit number_formated_two"
                                       style=" text-align:right"
                                       readonly=""></td>
                            <td><input type="text"
                                       class="form-control contract_total_amount-edit-pkr number_formated_two"
                                       style=" text-align:right"
                                       readonly=""></td>
                            <td></td>
                        </tr>
                        </tfoot>
                    </table>
                    <br>
                {{--<br>--}}
                {{--<div class="input-group mb-3" style="float: right;width: 250px">--}}
                {{--<div class="input-group-prepend">--}}
                {{--<button type="button" class="btn btn-default"--}}
                {{--style=" height: 30px;padding-top: 3px !important;">Total--}}
                {{--</button>--}}
                {{--</div>--}}
                {{--<!-- /btn-group -->--}}
                {{--<input type="text" class="form-control contract_total_amount-edit number_formated_two"--}}
                {{--style="width: 250px; text-align:right"--}}
                {{--readonly="">--}}
                {{--</div>--}}
                <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <div class="card card-default collapsed-card-show">
                <div class="card-header">
                    <h3 class="card-title">Show Payment Plan <span style="color: red;">*</span></h3>
                    <div class="card-tools">

                    </div>
                </div>
                <div class="card-body">
                    <h4 style="text-align:center; padding-bottom:0%"><span style="">PAYMENT PLAN</span></h4>

                    <div class="row">

                        <div class="col-8">
                            <table class=" table-sm payment_plan_edit" id="payment_plan_edit" style="  border-collapse:separate;
                        border-spacing:3px 3px;">
                                <thead style="background: #cacfcf ;color:#404040">
                                <tr>
                                    <th style="width: 5%">
                                        Sr.#
                                    </th>
                                    <th style="width: 10%">
                                        Instalment No.
                                    </th>
                                    <th style="width: 15%">
                                        Schedule Date
                                    </th>
                                    <th style="width: 10%">
                                        % age
                                    </th>
                                    <th style="width: 15%;background:#16d2d4;color:white;vertical-align: middle">
                                        Amount (<span
                                                class="currency_change">{{isset($contract->hasExChangeRate->currencyTo->currency_code)?$contract->hasExChangeRate->currencyTo->currency_code:'__'}}</span>)
                                    </th>
                                    <th  style="width: 15%;vertical-align:middle !important;text-align: center;background: #4966ff;color:white">  Amount (<span class="currency_change_from">{{isset($contract->hasExChangeRate->currencyFrom->currency_code)?$contract->hasExChangeRate->currencyFrom->currency_code:"__"}}</span>)

                                    </th>

                                </tr>
                                </thead>
                                <tbody>
                                @php $counter=1;@endphp
                                @foreach($contract->hasContractPayment as $index=>$payment)
                                    <tr>
                                        <th>
                                            <input name='record' type='checkbox'></th>
                                        <td>
                                            <input readonly style='text-align: center' type='text'
                                                   value="{{sprintf("%04d",$counter)}}"
                                                   name='installment_no_edit[]'
                                                   class='installment_no form-control'>
                                            <input readonly style='text-align: center' type='hidden'
                                                   value="{{$payment->contract_id}}" name='contract_id[]'
                                                   class='contract_id form-control'>

                                        </td>

                                        <td style='text-align: center'>

                                            <input placeholder='dd-mm-yy' type='text'
                                                   class='form-control datetimepicker-input' readonly
                                                   data-target="#contract_payment_date-{{$counter}}"
                                                   name='contract_payment_date_edit[]'
                                                   style="text-align: center"
                                                   value="{{$payment->schedule_date?\Carbon\Carbon::parse($payment->schedule_date)->format('d-M-Y'):''}}"
                                            >


                                        </td>
                                        <td>
                                            <input type='text' name='percentage[]' readonly
                                                   value="{{$payment->percentage.'%'}}"
                                                   class='form-control percentage_edit'
                                                   style='text-align: center'
                                                   readonly></td>

                                        <td>
                                            <input type='text' name='paymentAmountPkr[]'
                                                   value="{{$payment->amount_pkr}}"
                                                   class='paymentAmount_edit_pkr form-control number_formated_two'
                                                   dir='rtl' readonly></td>
                                        <td>
                                            <input type='text' name='paymentAmount[]'
                                                   value="{{$payment->amount}}"
                                                   class='paymentAmount_edit form-control number_formated_two'
                                                   dir='rtl' readonly></td>

                                    </tr>
                                    @php $counter++;@endphp
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th style="width: 5%">

                                    </th>
                                    <th style="width: 5%">

                                    </th>
                                    <th style="width: 15%;text-align: right!important">
                                        Total
                                    </th>
                                    <th style="width: 10%">
                                        <input class="form-control total_payment_percentage_edit"
                                               style="text-align: center" type="text" readonly>
                                    </th>


                                    <th style="width: 10%">
                                        <input class="form-control total_payment_amount_edit_pkr" type="text"
                                               style="text-align: right" readonly>
                                    </th>
                                    <th style="width: 10%">
                                        <input class="form-control total_payment_amount_edit" type="text"
                                               style="text-align: right" readonly>
                                    </th>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                        <div class="col-4"></div>
                    </div>
                </div>
            </div>
            {{--End Sub Category form--}}
            {{--Start Buttons--}}
            {{--                    <div class="col">--}}
            {{--                        <button id="addupdateContract" type="submit" class="btn btn-block btn-success btn-lg"--}}
            {{--                                style="float:right;"--}}
            {{--                                data-contractid="0">Save--}}
            {{--                        </button>--}}
            {{--                        <button id="cancelall" type="button" class="btn btn-block btn-primary btn-lg"--}}
            {{--                                style="float:right;">--}}
            {{--                            Cancel--}}
            {{--                        </button>--}}
            {{--                    </div>--}}
        </div>

@endsection
