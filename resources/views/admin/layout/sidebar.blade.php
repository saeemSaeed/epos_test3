<style>
    .header {

        color: #4b646f;
        background: #1a2226
    }

    .fa {
        display: inline-block;
        font-size: 10px !important;

    }

    .nav-sidebar .nav-header:not(:first-of-type) {
        padding: .3rem 1rem .5rem;
    }


</style>
<aside class="main-sidebar sidebar-dark-primary">
    <!-- Brand Logo -->
    <a href="{{route('home')}}" class="brand-link" style="background: #3c8dbc !important;">
        {{--<img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">--}}
        <h4 class="brand-text font-weight-bold mb-0">GOPA</h4>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-header" style="background-color: #1a2226ed">Main Navigation</li>
                
                <li class="nav-item">
                    <a href="{{ route('dashboard.index') }}" class="nav-link {{ Request::is('admin/dashboard*') ? 'active' : '' }}">
                        <i class="fas fa-tachometer-alt mr-2"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>

                @can(['users-list', 'role-list'])
                    <li class="nav-header" style="background-color: #1a2226ed">ADMINISTRATION</li>
                @endcan

                @can('users-list')
                    <li class="nav-item">
                        <a href="{{ route('user.index') }}" class="nav-link {{ Request::is('admin/user*') ? 'active' : '' }}">
                            <i class="fas fa-user-alt mr-2"></i>
                            <p>
                                Users
                            </p>
                        </a>
                    </li>
                @endcan

                @can('role-list')
                    <li class="nav-item">
                        <a href="{{ route('roles.index') }}" class="nav-link {{ Request::is('roles*') ? 'active' : '' }}">
                            <i class="fas fa-tasks mr-2"></i>
                            <p>
                                Roles
                            </p>
                        </a>
                    </li>
                @endcan

                <li class="nav-header" style="background-color: #1a2226ed">GOPA Health</li>
                @can('projects-list')
                    <li class="nav-item">
                        <a href="{{ route('project.index') }}" class="nav-link {{ Request::is('project*') ? 'active' : '' }}">
                            <i class="fas fa-file-alt mr-2"></i>
                            <p>
                                Projects
                            </p>
                        </a>
                    </li>
                @endcan

                {{--@can('contracts-list')--}}
                    <li class="nav-item">
                        <a href="{{ route('contract.index') }}" class="nav-link {{ Request::is('admin/contract*') ? 'active' : '' }}">
                            <i class="fas fa-file-alt mr-2"></i>
                            <p>
                                Contracts
                            </p>
                        </a>
                    </li>
                {{--@endcan--}}
                {{----}}
                {{--@can('payments-list')--}}
                    <li class="nav-item">
                        <a href="{{ route('payment.index') }}" class="nav-link {{ Request::is('admin/payment*') ? 'active' : '' }}">
                            <i class="fa fa-balance-scale mr-2"></i>
                            <p>
                                Payments
                            </p>
                        </a>
                    </li>
                        <li class="nav-header" style="background-color: #1a2226ed">Reports</li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="fas fa-clipboard"></i>&nbsp;
                        &nbsp;

                        <p>
                            Project wise
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="fas fa-clipboard"></i>&nbsp;
                        &nbsp;

                        <p>
                            Province wise
                        </p>
                    </a>
                </li>
                {{--@endcan--}}

                <li class="nav-header" style="background-color: #1a2226ed">HELPER</li>
                {{--@can('reports-list')--}}

                {{--@endcan--}}
                {{--@can('province-region-list')--}}
                    <li class="nav-item">
                        <a href="{{ route('province-region.index') }}" class="nav-link {{ Request::is('admin/province-region*') ? 'active' : '' }}">
                            <i class="fas fa-images mr-2"></i>
                            <p>
                                Province/Region
                            </p>
                        </a>
                    </li>
                {{--@endcan--}}
                {{--@can('project-execution-agency-list')--}}
                    <li class="nav-item">
                        <a href="{{ route('PEAs.index') }}" class="nav-link {{ Request::is('admin/PEAs*') ? 'active' : '' }}">
                            <i class="fas fa-flag-checkered mr-2"></i> 
                            <p>
                                PEA's
                            </p>
                        </a>
                    </li>
                {{--@endcan--}}
                {{--@can('contractors-list')--}}
                    <li class="nav-item">
                        <a href="{{ route('contractors.index') }}" class="nav-link {{ Request::is('admin/contractors*') ? 'active' : '' }}">
                            <i class="fas fa-file-signature mr-2"></i>
                            <p>
                                Vendors
                            </p>
                        </a>
                    </li>
                {{--@endcan--}}
                {{--<li class="nav-item">--}}
                {{--<a href="{{ route('vendor.index') }}" class="nav-link">--}}
                {{--<i class="fa fa-industry" aria-hidden="true"></i>--}}
                {{--&nbsp;&nbsp;--}}
                {{--<p>--}}

                {{--Vendors--}}
                {{--</p>--}}
                {{--</a>--}}
                {{--</li>--}}
                {{--@can('measures-list')--}}
                    <li class="nav-item">
                        <a href="{{ route('measure.index') }}" class="nav-link {{ Request::is('admin/measure*') ? 'active' : '' }}">
                            <i class="fa fa-barcode mr-2"></i> 
                            <p>
                                Measure's
                            </p>
                        </a>
                    </li>
                {{--@endcan--}}
                {{--@can('sub-categories-list')--}}
                    <li class="nav-item">
                        <a href="{{ route('sub-categories.index') }}" class="nav-link {{ Request::is('admin/sub-categories*') ? 'active' : '' }}">
                            <i class="fas fa-clone mr-2"></i>
                            <p>
                                Sub Categories
                            </p>
                        </a>
                    </li>
                {{--@endcan--}}
                {{--@can('payment-method-list')--}}
                    <li class="nav-item">
                        <a href="{{ route('paymentMethod.index') }}" class="nav-link {{ Request::is('admin/paymentMethod*') ? 'active' : '' }}">
                            <i class="fas fa-credit-card mr-2"></i>
                            <p>
                                Payment Method
                            </p>
                        </a>
                    </li>
                {{--@endcan--}}
                {{--@can('currency-list')--}}
                    <li class="nav-item">
                        <a href="{{ route('currency.index') }}" class="nav-link {{ Request::is('admin/currency*') ? 'active' : '' }}">
                            <i class="fas fa-money-bill-alt mr-2"></i>
                            <p>
                                Currency
                            </p>
                        </a>

                    </li>
                {{--@endcan--}}
                {{--@can('exchange-rate-list')--}}
                    <li class="nav-item">
                        <a href="{{ route('exchangeRate') }}" class="nav-link {{ Request::is('admin/exchange_rate*') ? 'active' : '' }}">
                            <i class="fas fa-exchange-alt mr-2"></i>
                            <p>
                                Exchange Rate
                            </p>
                        </a>
                    </li>
                {{--@endcan--}}
                {{--@can('city-list')--}}
                    <li class="nav-item">

                        <a href="{{ route('city.index') }}" class="nav-link {{ Request::is('admin/city*') ? 'active' : '' }}">
                            <i class="fas fa-city mr-2"></i>
                            <p>
                                City
                            </p>
                        </a>
                    </li>
                {{--@endcan--}}
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>