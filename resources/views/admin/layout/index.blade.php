<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GOPA |@yield('title')</title>
    <link rel="icon"
          href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAqElEQVRYR+2WYQ6AIAiF8W7cq7oXd6v5I2eYAw2nbfivYq+vtwcUgB1EPPNbRBR4Tby2qivErYRvaEnPAdyB5AAi7gCwvSUeAA4iis/TkcKl1csBHu3HQXg7KgBUegVA7UW9AJKeA6znQKULoDcDkt46bahdHtZ1Por/54B2xmuz0uwA3wFfd0Y3gDTjhzvgANMdkGb8yAyY/ro1d4H2y7R1DuAOTHfgAn2CtjCe07uwAAAAAElFTkSuQmCC">
    <link rel="stylesheet" href="{{asset('admin/plugins/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet"
          href="{{asset('admin/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{asset('admin/plugins/jqvmap/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('admin/dist/css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('admin/plugins/daterangepicker/daterangepicker.css')}}">


    {{--    <link rel="stylesheet" href="{{asset('admin/plugins/css/font-awesome.min.css')}}">--}}
    {{--    <link href="{{asset('assets/css/components.min.css')}}" rel="stylesheet" type="text/css">--}}
    @yield('mystyle')
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <style>
            @media print
            {
                .no-print, .no-print *
                {
                    display: none !important;
                }
            }
        .my-custom-scrollbar {
            position: relative;
            max-height: 200px;
            overflow: auto;
        }

        .table-wrapper-scroll-y {
            display: block;
        }

        .my-custom-scrollbar thead th {
            position: sticky;
            background: #cacfcf !important;
            top: 0;
        }

        .content-header {
            padding: 15px;
        }

        #loading {
            position: fixed;
            width: 100%;
            height: 100%;
            background: #fff url(/img/giphy.gif) no-repeat center center;
            z-index: 9999;
        }

        th {
            text-align: center !important;
        }

        .form-control {

            /*padding: .375rem .75rem;*/
            /*padding-bottom: .0rem .0rem !important;*/
            padding: .0rem .75rem !important;

        }

        tr.duplicate td {
            background-color: #90A4AE;
        }
    </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed text-sm nav-flat" style="width: 100% !important;">
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="background: #3c8dbc !important; padding: 15px;">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-arrow-left text-white"></i></a>
            </li>
        </ul>
        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Messages Dropdown Menu -->
            <!-- Notifications Dropdown Menu -->
            <li class="nav-link">
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle text-white text-capitalize font-weight-bold" data-toggle="dropdown">
                            <img src="https://ui-avatars.com/api/?name={{ urlencode(auth()->user()->full_name) }}&background=ffffff&color=111" class="user-image">
                            <span class="hidden-xs">{{ auth()->user()->full_name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="https://ui-avatars.com/api/?name={{ urlencode(auth()->user()->full_name) }}&background=3C8DBC&color=fff" class="img-circle">
                                <p class="text-capitalize">
                                    {{ auth()->user()->full_name }}
                                    <small>Member since {{ auth()->user()->created_at->isoFormat('MMMM Do YYYY') }}</small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="float-left">
                                    <a href="#" class="btn btn-default btn-flat" hidden>Profile</a>
                                </div>
                            
                                <div class="float-right">
                                    <a href="javascript:void(0)" class="btn btn-default btn-flat" onclick="$('#logout-form').submit();">Sign out</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" hidden>
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->
    <!-- Main Sidebar Container -->
@include('admin.layout.sidebar')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content">
            <div id="loading"></div>
            <div class="container-fluid">
            @include('message')
            <!-- /.content-header -->
                <!-- Main content -->
            @yield('content')
            <!-- /.content -->
            </div>
        </section>
    </div>
    <!-- /.content-wrapper -->
    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<!-- jQuery -->
<script src="{{asset('admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('admin/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('admin/plugins/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('admin/plugins/jquery-number-master/jquery.number.js')}}"></script>
<!-- JQVMap -->
<!-- jQuery Knob Chart -->
{{--<script src="{{asset('admin/plugins/jquery-knob/jquery.knob.min.js')}}"></script>--}}
<!-- daterangepicker -->
<script src="{{ asset('admin/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('admin/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('js/jquery-validation/dist/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/notify.min.js') }}"></script>

<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('admin/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
{{--<script src="{{asset('admin/plugins/summernote/summernote-bs4.min.js')}}"></script>--}}
<!-- overlayScrollbars -->
<script src="{{asset('admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('admin/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('admin/dist/js/demo.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('admin/dist/js/pages/dashboard.js')}}"></script>
<script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
{{--<script type="text/javascript" src="{{asset('assets/js/jquery.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>--}}
<script type="text/javascript" src="{{asset('assets/js/echarts.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/printThis-master/printThis.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.prefix-input.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>

<script>
    
    $('.numberJs').number(true, 6);

    $(document).ready(function() {
        $(document).ajaxStart(() => {
            $.blockUI({
                message: '<i class="fas fa-sync-alt fa-4x fa-spin"></i>',
                css: { backgroundColor: 'transparent', border: '0px', color: '#fff'}
            })
        });

        $(document).ajaxStop($.unblockUI);
    })

    window.addEventListener('load', function () {
        jQuery('#loading').fadeOut();
        $("body").tooltip({selector: '[data-toggle=tooltip]'});
    });
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $(document).ready(function () {


        $('[data-toggle="tooltip"]').tooltip();


        function comparizonToDate_start(fit_start_time, fit_end_time) {
            if (fit_start_time !== '' && fit_end_time !== '') {
                if ((new Date(fit_start_time)) < (new Date(fit_end_time))) {

                    return true;
                } else {
                    return false;
                }

            }

        }

        $().tooltip({
            customClass: 'number_formated_four',
            html: true,
        });


        $("body").tooltip({selector: '[data-toggle=tooltip]'});
        $('.exchange_rate_date').datepicker({

            format: "dd-M-yyyy",
            autoclose: true,
            startDate: "01-01-1947"

        }).on('changeDate', function (ev) {

            var to = $('.currency_id').val();
            var from = $('.currency_id_from').val();
            var exchange_rate_date = $('.exchange_rate_date').val();

            if ((to != '') && (from != '') && (exchange_rate_date != '')) {

                    $.ajax({
                        url: "{{ route('exchangerate.from.to') }}",
                        method: 'get',
                        data: {
                            to: $('.currency_id').val(),
                            from: $('.currency_id_from').val(),
                            exchange_rate_date: $('.exchange_rate_date').val(),

                        }, success: function (response) {

                            console.log(response);
                            if (response.exchange_rates == null) {
                                $('.exchange_rate_amount_val').val(0);
                                $('.exchange_rate_id').val(null);

                                $('.exchange_rate_flag').val(false);
                                swal('Exchange Rates Cannot Define on This Date');
                            }
                            else {
                                $('.exchange_rate_flag').val(true);
                                $('.exchange_rate_amount_val').val(response.exchange_rates);
                                $('.exchange_rate_id').val(response.exchange_rate_id);
                            }


//                $('.exchange_rate_date').html(response.exchange_rate);

                        }, error: function (response) {
                            console.log(response);

                        }

                    });


            } else {
                swal('Please Select Currency From,Currency To,Exchange Rate Date');
            }
        });


        $('.exchange_rate_date_edit').datepicker({

            format: "dd-M-yyyy",
            autoclose: true
            ,
            startDate: "01-01-1947"
        }).on('changeDate', function (ev) {

            var to = $('.currency_id_edit').val();
            var from = $('.currency_id_from_edit').val();
            var exchange_rate_date = $('.exchange_rate_date_edit').val();

            if ((to != '') && (from != '') && (exchange_rate_date != '')) {

                $.ajax({
                    url: "{{ route('exchangerate.from.to') }}",
                    method: 'get',
                    data: {
                        to: to,
                        from: from,
                        exchange_rate_date: exchange_rate_date,

                    }, success: function (response) {

                        console.log(response);
                        if (response.exchange_rates == null) {
                            $('.exchange_rate_amount_val').val(0);
                            $('.exchange_rate_id').val(null);

                            $('.exchange_rate_flag').val(false);
                            swal('Exchange Rates Cannot Define on This Date');
                        }
                        else {
                            $('.exchange_rate_flag').val(true);
                            $('.exchange_rate_amount_val').val(response.exchange_rates);
                            $('.exchange_rate_id').val(response.exchange_rate_id);
                        }


//                $('.exchange_rate_date').html(response.exchange_rate);

                    }, error: function (response) {
                        console.log(response);

                    }

                });
            } else {
                swal('Please Select Currency From,Currency To,Exchange Rate Date');
            }
        });

        $('.ps_validity_edit').datepicker({

            format: "dd-M-yyyy",
            autoclose: true,
            startDate: "01-01-1947"

        });
        $('.ps_validity').datepicker({

            format: "dd-M-yyyy",
            autoclose: true,
            startDate: "01-01-1947"

        });
        $('input, :input').attr('autocomplete', 'off');

        $('#planned_start_date').datepicker({
            format: "dd-M-yyyy",
            autoclose: true,
            todayHighlight: true,
        }).on('changeDate', function(){
            $('#planned_end_date').datepicker('setStartDate', new Date($(this).val()));
        });

        $('#planned_end_date').datepicker({
            format: "dd-M-yyyy",
            autoclose: true,
            todayHighlight: true,
        }).on('changeDate', function(){
            $('#planned_start_date').datepicker('setEndDate', new Date($(this).val()));
        });

        $('#project_start_date').datepicker({
            format: "dd-M-yyyy",
            autoclose: true,
            todayHighlight: true,
        }).on('changeDate', function(){
            $('#revised_expected_completion_date').datepicker('setStartDate', new Date($(this).val()));
            $('#project_end_date').datepicker('setStartDate', new Date($(this).val()));
        });


        $('#project_end_date').datepicker({
            format: "dd-M-yyyy",
            autoclose: true,
            todayHighlight: true,
        }).on('changeDate', function(){
            $('#project_start_date').datepicker('setEndDate', new Date($(this).val()));
        });

        $('#revised_expected_completion_date').datepicker({
            format: "dd-M-yyyy",
            autoclose: true,
            todayHighlight: true,
        }).on('changeDate', function(){
            $('#project_start_date').datepicker('setEndDate', new Date($(this).val()));
        });

        $('#revise_date').datepicker({

            format: "dd-M-yyyy",
            autoclose: true,
            todayHighlight: true,
        });


        $('.contract_date').datepicker({
            format: "dd-M-yyyy", 
            autoclose: true,
            todayHighlight: true,
        });

        $('.contract_date-2').datepicker({

            format: "dd-M-yyyy", autoclose: true
            ,
            startDate: "01-01-1947"
        });


        $('.contract_planned_start_date').datepicker({

            format: "dd-M-yyyy",

            autoclose: true
            ,
            startDate: "01-01-1947"

        }).on('changeDate', function (ev) {
            var contract_planned_start_date = $('.contract_planned_start_date').val();
            var contract_planned_end_date = $('.contract_planned_end_date').val();

            if (contract_planned_start_date !== '' && contract_planned_end_date !== '') {
                if (!comparizonToDate_start(contract_planned_start_date, contract_planned_end_date)) {
                    swal("Planned Start Date Cannot Greater Than Planned Start Date");
                    $('.contract_planned_start_date').datepicker('setDate', null);
                    return false;
                }
            }

        });

        $('.contract_planned_end_date').datepicker({

            format: "dd-M-yyyy", autoclose: true
            ,
            startDate: "01-01-1947"

        }).on('changeDate', function (ev) {
            var contract_planned_start_date = $('.contract_planned_start_date').val();
            var contract_planned_end_date = $('.contract_planned_end_date').val();

            if (contract_planned_start_date !== '' && contract_planned_end_date !== '') {
                if (!comparizonToDate_start(contract_planned_start_date, contract_planned_end_date)) {
                    swal("Planned Start Date Cannot Greater Than Planned Start Date");
                    $('.contract_planned_end_date').datepicker('setDate', null);
                    return false;
                }
            }

        });
        $('.contract_start_date').datepicker({

            format: "dd-M-yyyy", 
            autoclose: true,
        }).on('changeDate', function(){
            $('.contract_end_date').datepicker('setStartDate', new Date($(this).val()));
        });

        $('.contract_end_date').datepicker({

            format: "dd-M-yyyy", autoclose: true
            ,
            startDate: "01-01-1947"

        });
        $('.contract_date_edit').datepicker({

            format: "dd-M-yyyy", autoclose: true
            ,
            startDate: "01-01-1947"

        });
        $('.contract_planned_start_date_edit').datepicker({

            format: "dd-M-yyyy", autoclose: true
            ,
            startDate: "01-01-1947"

        }).on('changeDate', function (ev) {
            var contract_planned_start_date = $('.contract_planned_start_date_edit').val();
            var contract_planned_end_date = $('.contract_planned_end_date_edit').val();

            if (contract_planned_start_date !== '' && contract_planned_end_date !== '') {
                if (!comparizonToDate_start(contract_planned_start_date, contract_planned_end_date)) {
                    swal("Planned Start Date Cannot Greater Than Planned Start Date");
                    $('.contract_planned_start_date_edit').datepicker('setDate', null);
                    return false;
                }
            }
            ;
        });
        $('.contract_planned_end_date_edit').datepicker({

            format: "dd-M-yyyy", autoclose: true
            ,
            startDate: "01-01-1947"

        }).on('changeDate', function (ev) {
            var contract_planned_start_date = $('.contract_planned_start_date_edit').val();
            var contract_planned_end_date = $('.contract_planned_end_date_edit').val();

            if (contract_planned_start_date !== '' && contract_planned_end_date !== '') {
                if (!comparizonToDate_start(contract_planned_start_date, contract_planned_end_date)) {
                    swal("Planned Start Date Cannot Greater Than Planned Start Date");
                    $('.contract_planned_end_date_edit').datepicker('setDate', null);
                    return false;
                }
            }
            ;
        });


        $('.current_exchange_rate_start_date').datepicker({

            format: "dd-M-yyyy", autoclose: true,
            startDate: "01-01-1947"

        });
        $('.current_exchange_rate_end_date').datepicker({

            format: "dd-M-yyyy", autoclose: true,
            startDate: "01-01-1947"

        });

        $('.contract_start_date_edit').datepicker({

            format: "dd-M-yyyy", autoclose: true,
            startDate: "01-01-1947"

        });
        $('.contract_end_date_edit').datepicker({
            format: "dd-M-yyyy", autoclose: true,
            startDate: "01-01-1947"

        });
        $('.ps_validity_edit').datepicker({

            format: "dd-M-yyyy", autoclose: true
            ,
            startDate: "01-01-1947"
        });
        //payment


        $('.invoice_date').datepicker({

            format: "dd-M-yyyy", autoclose: true
            ,
            startDate: "01-01-1947"
        });

        $('.payment_details_date').datepicker({

            format: "dd-M-yyyy", autoclose: true,
            startDate: "01-01-1947"

        });
        $('.invoice_performance_security_validity').datepicker({

            format: "dd-M-yyyy", autoclose: true,
            startDate: "01-01-1947"

        });
        $('.payment_details_date').datepicker({

            format: "dd-M-yyyy", autoclose: true
            ,
            startDate: "01-01-1947"
        });

        $('.exchange_start_date').datepicker({
            format: "dd-M-yyyy", 
            autoclose: true,
            todayHighlight: true,
        }).on('changeDate', function(){
            $('.exchange_end_date').datepicker('setStartDate', new Date($(this).val()));
        });

        $('.exchange_end_date').datepicker({

            format: "dd-M-yyyy", 
            autoclose: true,
            todayHighlight: true,
        }).on('changeDate', function(){
            $('.exchange_start_date').datepicker('setStartDate', new Date($(this).val()));
        });

        $('.invoice_net_payable').number(true, 6);
        $('.invoice_net_payable').number(true, 6);
        $('.invoice_less_retention_amount').number(true, 6);
//        Invoice Amount:


        $(".message").show("slow").delay(5000).hide("slow");
        $('.number_formated_four').number(true, 6);
        $('.number_formated_two').number(true, 6);
        $('.two_decimal').number(true, 2);

        $(document).on('change', '.contractfileamount', function (event) {
            $(this).val($.number($(this).val(), 6));
        });


        $(document).on('change', '.payment_amount_two_digit', function (event) {
            $(this).val($.number($(this).val(), 6));
        });
        $(document).on('change', '.exchange_rate_create', function (event) {
            $(this).val($.number($(this).val(), 4));
        });
        $(document).on('change', '.dymanic_contract_amount', function (event) {
            $(this).val($.number($(this).val(), 6));
        });
        $(document).on('change', '.contract_province_budget_edit', function (event) {
            $(this).val($.number($(this).val(), 6));
        });

        //        $(".project_budget").on('change', function () {
        //            $(this).val(parseFloat(this.val()).toFixed(2));
        //        });
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="tooltip"]').tooltip();
        $('.phone_check').keyup(function () {
            var foo = $(this).val().split("-").join(""); // remove hyphens
            if (foo.length > 0) {
                foo = foo.match(new RegExp('.{1,3}', 'g')).join("-");
            }
            $(this).val(foo);
        });
        $('#select-state').selectpicker();
        $('#state_payment').selectpicker();
        $('#project_select').selectpicker();
        $('#user_table').DataTable();
        $('#role_table').DataTable();
        $('.project_table').DataTable();
        $('.data-table-city').DataTable();
        $('.data-table-contractors').DataTable();
        $('#exchangeRate').DataTable();
        $('.currency').DataTable();
        $('.paymentMethod').DataTable();
        $('.subCategory').DataTable();
        $('.measures').DataTable();
        $('.vendors').DataTable();
        $('.peas').DataTable();
        $('.province-region').DataTable();
        $('#payment_table').DataTable();
        $('#contract_table').DataTable();
    });

    // VALIDATION MESSAGES 
    function showValidationAlertMessage(data) {
        var response = JSON.parse(data.responseText);
        var errorString = '';
        $.each( response.errors, function( key, value) {
            errorString += value+"\n";
        });

        swal({
          title: "Warning",
          icon: "warning",
          text: errorString,
        });
    }
</script>
@include('js.datatabe_custom')
@include('js.payment')
@include('js.fileCategory')
@include('js.provinceRegion')
@include('js.project')
@include('js.measure')
@include('js.subCategories')
@include('js.city')
@include('js.exchangeRate')
@include('js.currency')
@include('js.paymentMethod')
@include('js.PEAs')
@include('js.contracts')
@include('js.select')
@yield('javascript')
@stack('js')

</body>
</html>
