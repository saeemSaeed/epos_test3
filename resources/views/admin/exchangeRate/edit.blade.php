<div class="modal fade" id="default_edit_exchangeRate">
    <form method="post">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style=" background-color: #65a3c6;
color: #2c2c2c;">
                    <h4 class="modal-title">Exchange Rate</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul>
                        </ul>
                    </div>
                    <div class="form-group">

                        <div class="form-group">
                            <label>Sort Order&nbsp;</label>
                            <input

                                oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"
                                type="text" placeholder="Sort Order" name="sort_order" class="form-control sort_order

            @error('sort_order') is-invalid @enderror" >
                            @error('sort_order') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <label>Start Date&nbsp;</label>
                        <input type="text" name="start_date"  id="exchange_start_date" class="form-control @error('start_date') is-invalid @enderror start_date exchange_start_date" >
                        <input type="hidden" name="exchange_rate_id" class="form-control exchange_rate_id " >
                        @error('start_date') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group">
                        <label>End Date&nbsp;</label>
                        <input type="text" name="end_date"    id="exchange_end_date" class="form-control end_date @error('end_date') is-invalid @enderror exchange_end_date" >
                        @error('end_date') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group">
                        <label>Exchange Rate&nbsp;</label>
                        <input type="text" placeholder="00.00" name="exchange_rate" class="form-control exchange_rate_create exchange_rate @error('exchange_rate') is-invalid @enderror" >
                        @error('exchange_rate') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>From &nbsp;</label>
                                <select name="currency_from" class="currency_from form-control @error('currency_from') is-invalid @enderror" id="currency_from_edit">
                                    <option value="" >Select Currency</option>
                                    @foreach(\App\Helpers\Helper::currency() as $cur)
                                        <option value="{{$cur->id}}">{{$cur->currency_name}}</option>
                                    @endforeach
                                </select>
                                @error('currency_from') <div class="invalid-feedback">{{ $message }}</div> @enderror
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>To&nbsp;</label>
                                <select name="currency_to" class="currency_to exchange_rate_edit form-control @error('currency_to') is-invalid @enderror" id="currency_to_edit">
                                    <option value="">Select Currency</option>
                                    @foreach(\App\Helpers\Helper::currency() as $cur)
                                        <option value="{{$cur->id}}">{{$cur->currency_name}}</option>
                                    @endforeach
                                </select>
                                @error('currency_to') <div class="invalid-feedback">{{ $message }}</div> @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary edit_save_exchange_modal">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">cancel</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </form>
    <!-- /.modal-dialog -->
</div>
