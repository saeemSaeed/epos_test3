<div class="modal fade" id="exchange_rate_modal">
    <form action="{{route('exchangeRate.store') }}" method="post" id="exchangeRateForm">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style=" background-color: #65a3c6;
color: #2c2c2c;">
                    <h4 class="modal-title">Exchange Rate</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Sort Order:<span class="text-danger">*</span></label>
                        <input oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');" type="text" placeholder="Sort Order" name="sort_order" readonly class="form-control current_exchange_rate_sort_order

            @error('sort_order') is-invalid @enderror" value="{{ old('sort_order') }}">
                        @error('sort_order') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group">
                        <label>Start Date:<span class="text-danger">*</span></label>
                        <input type="text" placeholder="Currency Name" class="form-control @error('start_date') is-invalid @enderror exchange_start_date" name="start_date" value="{{ old('start_date') }}" required>
                        @error('start_date') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group">
                        <label>End Date:<span class="text-danger">*</span></label>
                        <input type="text" placeholder="Currency Code" class="form-control @error('end_date') is-invalid @enderror exchange_end_date" name="end_date" value="{{ old('end_date') }}" required>
                        @error('end_date') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>From:<span class="text-danger">*</span></label>
                                <select name="currency_from" class="currency_from form-control @error('currency_from') is-invalid @enderror" id="currency_from" value="{{ old('currency_from') }}" required>
                                    <option value="">Select Currency</option>
                                    @foreach(\App\Helpers\Helper::currency() as $cur)
                                        <option value="{{$cur->id}}">{{$cur->currency_name}}</option>
                                    @endforeach
                                </select>
                                @error('currency_from') <div class="invalid-feedback">{{ $message }}</div> @enderror
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>To:<span class="text-danger">*</span></label>
                                <select name="currency_to" class="currency_to form-control @error('currency_to') is-invalid @enderror" id="currency_to" value="{{ old('currency_to') }}" required>
                                    <option value="">Select Currency</option>
                                    @foreach(\App\Helpers\Helper::currency() as $cur)
                                        <option value="{{$cur->id}}">{{$cur->currency_name}}</option>
                                    @endforeach
                                </select>
                                @error('currency_to') <div class="invalid-feedback">{{ $message }}</div> @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Exchange Rate Value:<span class="text-danger">*</span></label>
                        <input type="text" placeholder="00.00" class="form-control text-right exchange_rate_create @error('exchange_rate') is-invalid @enderror" name="exchange_rate" oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');" value="{{ old('exchange_rate') }}" required>
                        @error('exchange_rate') <div class="invalid-feedback">{{ $message }}</div> @enderror

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">cancel</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </form>
    <!-- /.modal-dialog -->
</div>
