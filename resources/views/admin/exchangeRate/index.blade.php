@extends('admin.layout.index')
@section('content')
<section class="bg-primary content-header"
    style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
    <div class="row">
        <div class="col-8">
            <h1>Exchange Rate</h1>
        </div>
        <div class="col-4">
            <ol class="breadcrumb" style="color:#444;float: right">
                <li>
                    <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                </li>
                <li>
                    <i class="fa fa-angle-right" style="color: #ccc;"></i> Exchange Rate &nbsp;
                </li>
            </ol>
        </div>
    </div>
</section>
<div class="container">
    <div class="row justify-content-between">
        <div  style="margin-top: 10px; margin-left: 10px">
            @can('exchange-rate-create')
            <button type="button" class="btn btn-block btn-success btn-flat add_exchange_rate_get_sort_order" data-toggle="modal"
            data-target="#exchange_rate_modal">
            <i class="fa fa-plus"></i> Add Exchange Rate
            </button>
            @endcan
        </div>
        @can('exchange-rate-restore')
        <div  style="margin-top: 10px; margin-right: 10px">
            <a href="{{ route('exchangeRate.Deleted') }}" class="btn btn-block btn-danger btn-flat"><i class="fas fa-trash-alt"></i></a>
        </div>
        @endcan
    </div>
</div>
<div class="container-fluid" style="margin-top: 10px">
    <div class="card">
        <div class="card-body">
            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="exchangeRate" class="table table-bordered table-hover exchangeRate " role="grid"
                            aria-describedby="example2_info">
                            <thead style="background-color: #65a3c6;color: #2c2c2c">
                                <tr role="row">
                                    <th style=";width: 10%">Sort Order</th>
{{--                                    <th>ID</th>--}}
                                    <th>Start date</th>
                                    <th>End date</th>
                                    <th>Currency From</th>
                                    <th>Currency To</th>
                                    <th>Exchange rate</th>
                                    <th style="width: 10%;text-align: center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($exchangeRates))
                                @foreach($exchangeRates as $index=>$exchangeRate)
                                <tr role="row" class="odd">
{{--                                    <td style="width:5%;text-align: center">{{++$index}}</td>--}}
                                    <td style="text-align: center">{{$exchangeRate->sort_order}}</td>



                                    <td style="text-align: center">{{$exchangeRate->start_date?\Carbon\Carbon::parse($exchangeRate->start_date)->format('d-M-Y'):''}}</td>
                                    <td  style="text-align: center">{{$exchangeRate->end_date?\Carbon\Carbon::parse($exchangeRate->end_date)->format('d-M-Y'):''}}</td>
                                    <td>{{$exchangeRate->currencyFrom->currency_name}}</td>
                                    <td>{{$exchangeRate->currencyTo->currency_name}}</td>
                                    <td style="text-align: right" class="number_formated_four">{{($exchangeRate->exchange_rate)}}</td>
                                    <td data-id="{{$exchangeRate->id}}" style=";text-align: center  ">
                                        @can('exchange-rate-edit')
                                        <a data-id="{{$exchangeRate->id}}"
                                            class="getExchangeRate"
                                            data-toggle="modal"
                                            data-target="#default_edit_exchangeRate"
                                            title="edit!"><i style="color: black;font-size: 14px!important"
                                        class="fas fa-pencil-alt"></i></a>&nbsp;&nbsp;
                                        @endcan
                                        @can('exchange-rate-delete')
                                        <a
                                            onclick="return confirm('Are you sure?')"
                                            data-id="{{$exchangeRate->id}}"
                                            href="{{route('exchangeRate.delete',['id'=>$exchangeRate->id])}}"
                                            data-toggle="tooltip"
                                            title="Remove"><i
                                            style="color: black;font-size: 14px!important"
                                        class=" fa fa-trash"></i></a>
                                    @endcan</td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</div>
@include('admin.exchangeRate.create')
@include('admin.exchangeRate.edit')
@endsection
