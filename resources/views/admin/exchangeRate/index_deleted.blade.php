@extends('admin.layout.index')
@section('content')
<section class="bg-primary content-header"
    style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
    <div class="row">
        <div class="col-8">
            <h1>Exchange Rate</h1>
        </div>
        <div class="col-4">
            <ol class="breadcrumb" style="color:#444;float: right">
                <li>
                    <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                </li>
                <li>
                    <i class="fa fa-angle-right" style="color: #ccc;"></i> Exchange Rate &nbsp;
                </li>
            </ol>
        </div>
    </div>
</section>
<div class="container-fluid" style="margin-top: 10px">
    <div class="card">
        <div class="card-body">
            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="exchangeRate" class="table table-bordered table-hover exchangeRate " role="grid"
                            aria-describedby="example2_info">
                            <thead style="background-color: #65a3c6;color: #2c2c2c">
                                <tr role="row">
{{--                                    <th>ID</th>--}}
                                    <th style=";width: 10%">Sort Order</th>
                                    <th>Start date</th>
                                    <th>End date</th>
                                    <th>Currency From</th>
                                    <th>Currency To</th>
                                    <th>Exchange rate</th>
                                    <th style="width: 10%;text-align: center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($deletedexchangeRates))
                                @foreach($deletedexchangeRates as $index=>$exchangeRate)
                                <tr role="row" class="odd">
{{--                                    <td style="width:5%;text-align: center">{{++$index}}</td>--}}
                                    <td style="text-align: center">{{$exchangeRate->sort_order}}</td>
                                    <td>{{$exchangeRate->start_date}}</td>
                                    <td>{{$exchangeRate->end_date}}</td>
                                    <td>{{$exchangeRate->currencyFrom->currency_name}}</td>
                                    <td>{{$exchangeRate->currencyTo->currency_name}}</td>
                                    <td style="text-align: right" class="number_formated_four">{{($exchangeRate->exchange_rate)}}</td>
                                    <td class="sorting_1 dtr-control" style="text-align: center!important;">
                                        <a type="submit" style="border: none;background-color: transparent" data-toggle="tooltip" title="" href="{{route('exchangeRate.restore',['id'=>$exchangeRate->id])}}" data-original-title="Restore"><i class="fas fa-trash-restore"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</div>
@include('admin.exchangeRate.create')
@include('admin.exchangeRate.edit')
@endsection
