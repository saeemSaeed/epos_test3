@extends('admin.layout.index')

@section('content')
    <section class="bg-primary content-header" style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Payments</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li><i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li><i class="fa fa-angle-right" style="color: #ccc;"></i> Payments &nbsp;</li>
                    <li class="active"><i class="fa fa-angle-right" style="color: #ccc;"></i> Add Payment</li>
                </ol>
            </div>
        </div>
    </section>

    <form class="paymentForm" action="{{route('payment.store') }}" method="post" id="paymentCreateForm" enctype="multipart/form-data">
        @csrf
        <div class="form-row mt-3">
            {{--Start create form--}}
            <div class="col-12">
                <div class="card card-outline card-primary shadow rounded-0 mb-4">
                    <!-- card-header -->
                    <div class="card-header">
                        <h2 class="card-title font-weight-bold"><i class="fa fa-ellipsis-v mr-1"></i> General Information</h2>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-md-5 col-12">
                                {{-- VENDOR --}}
                                <div class="form-group">
                                    <label>Vendor:<span class="text-danger">*</span></label>
                                    <select id="select_state_payment" class="form-control contractor_id" name="contractor_id" required>
                                        <option value="" selected>Select a Vendor</option>
                                        @foreach ($contractor_id as $index=>$obj)
                                            <option value=" {{$obj->hasOneContractor->id}}">
                                                {{$obj->hasOneContractor->contractor_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                {{-- NTN --}}
                                <div class="form-group vendor_ntn_div" hidden>
                                    <label>Vendor NTN:</label>
                                    <input type="text" class="form-control vendor_ntn" readonly>
                                </div>

                                {{-- Contract Title --}}
                                <div class="form-group">
                                    <label>Contract Title/Reference Code:<span class="text-danger">*</span></label>
                                    <select id="vendor_contracts" class="form-control vendor_contracts" name="contract_id" required>
                                    </select>
                                </div>

                                {{-- CONTRACT DETAILS --}}
                                <fieldset class="border p-2">
                                    <legend class="w-auto px-2 text-muted font-weight-bold" style="font-size: 19px;">Contract Details</legend>
                                    <div class="form-row">
                                        <div class="col-4"></div>
                                        <div class="col-4">
                                            <P class="text-center text-white p-1" style="background:#16d2d4">Amount(<span class="contract_change_currency_from_text">__</span>)</P>
                                        </div>
                                        <div class="col-4">
                                            <P class="text-center text-white p-1" style="background:#4966ff">Amount(<span class="contract_change_currency">__</span>)</P>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-4">
                                            <label class="mt-3">Contract Amount:</label>
                                        </div>
                                        <div class="col-4">
                                            <input type="text" class="form-control contract_amount_payment number_formated_two border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0" style="background:transparent; direction: rtl;" value="0.00" readonly>
                                        </div>

                                        <div class="col-4">
                                            <input type="text" class="form-control contract_revised_amount_payment number_formated_two border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0" style="background:transparent;direction: rtl" value="0.00" readonly>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-4">
                                            <label class="mt-3">Previous Payment:</label>
                                        </div>
                                        <div class="col-4">
                                            <input type="text" class="form-control previous_payment_amount number_formated_two border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0" style="background:transparent;" dir="rtl" readonly>
                                        </div>
                                        <div class="col-4">
                                            <input type="text" class="form-control previous_payment_amount_pkr number_formated_two border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0" style="background:transparent;" dir="rtl" readonly>
                                        </div>
                                    </div>
                                    <div class="form-row form-group">
                                        <div class="col-4">
                                            <label class="mt-3">Balance Payable:</label>
                                        </div>
                                        <div class="col-4">
                                            <input type="text" class="form-control balance_payable_amount number_formated_two border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0" style="background:transparent;" dir="rtl" readonly>
                                        </div>
                                        <div class="col-4">
                                            <input type="text" class="form-control balance_payable_amount_pkr number_formated_two border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0" style="background:transparent;" dir="rtl" readonly>
                                        </div>
                                    </div>


                                    <div class="form-row form-group">
                                        <div class="col-3">
                                            <label class="mt-3">Last Pmt Amount:</label>
                                        </div>
                                        <div class="col-3">
                                            <input type="text" class="form-control number_formated_two last_payment_amount border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0 bg-transparent" value="0" dir="rtl" readonly>
                                        </div>
                                        
                                        <div class="col-3">
                                            <label class="mt-3">Last Pmt Date:</label>
                                        </div>
                                        <div class="col-3">
                                            <input type="text" class="form-control last_payment_date border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0 bg-transparent text-right" value="(Empty)" readonly>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-3">
                                            <label class="mt-3">Remaining Balance:</label>
                                        </div>
                                        <div class="col-3">
                                            <input type="text" class="form-control remaining_balance number_formated_two border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0 bg-transparent" dir="rtl" data-toggle="tooltip" title="Remaining balance after current total of charges" value="0" readonly>
                                        </div>

                                        <div class="col-3">
                                            <label class="mt-3">Contract Currency:</label>
                                        </div>
                                        <div class="col-3">
                                            <input type="text" class="form-control contract_currency text-center border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0 bg-transparent" value="(Empty)" readonly>
                                        </div>
                                    </div>
                                </fieldset>

                            </div>

                            <div class="col-md-7 col-12">
                                <fieldset class="border p-2">
                                    <legend class="w-auto px-2 text-muted font-weight-bold" style="font-size: 19px;">Invoice Details</legend>
                                    <div class="form-row">
                                        <div class="col-md-2 form-group">
                                            <label>Currency:<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control contract_change_currency_from" style="background-color: #e9ecef" placeholder="(Empty)" readonly>
                                            <input type="hidden" name="invoice_currency" class="contract_change_currency_from_id">
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label>Invoice Amount:<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control invoice_amount numberJs text-right" value="{{ old('invoice_amount') ?? 0 }}" name="invoice_amount" required>
                                        </div>
                                        <div class="col-3 form-group">
                                            <label>Invoice Date:<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control invoice_date" value="{{ old('invoice_date') ?? \Carbon\Carbon::now('PKT')->format('d-M-Y') }}" name="invoice_date" placeholder="d/m/y" required>
                                        </div>
                                        <div class="col-3 form-group">
                                            <label>Invoice No:<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" value="{{ old('invoice_no') }}" name="invoice_no" placeholder="xxxx" required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 form-group">
                                            <label>Less Retention <small>(If Applicable)</small>:</label>
                                            <input type="text" class="form-control invoice_less_retention" oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');" dir="rtl" name="invoice_less_retention" placeholder="%" readonly>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Less Retention Amount:</label>
                                            <input type="text" class="form-control invoice_less_retention_amount" dir="rtl" name="invoice_less_retention_amount" placeholder="0" readonly>
                                        </div>
                                        <div class="col-md-8">
                                            <label>Withdraw Application / Payment Request No:<span class="text-danger">*</span></label>
                                                <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="payment_request_no">--</span>
                                                    <input type="hidden" name="payment_count_no" id="invoice_withdraw_application_prefix">
                                                </div>
                                                <input type="text" class="form-control" value="{{ old('payment_request_no_prefix') }}" id="payment_request_no_prefix" placeholder="0" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Fee:<span class="text-danger">*</span></label>
                                            <div class="input-group mb-3">
                                                <input type="number" class="form-control text-right" name="invoice_withdraw_application" value="0" maxlength="5" onkeyup="this.value = fnc(this.value, 0, 100)" value="{{ old('invoice_withdraw_application') }}">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6 form-group">
                                            <label>Performance Security Validity:<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control invoice_performance_security_validity" name="invoice_performance_security_validity" placeholder="d/m/y">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Net Payable Amount:<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control invoice_net_payable" name="invoice_net_payableinvoice_net_payable" readonly dir="rtl" placeholder="0">
                                        </div>
                                        <div class="col-12 form-group">
                                            <label>Invoice Description:</label>
                                            <textarea class="form-control" name="invoice_description" rows="4">{{ old('invoice_description') }}</textarea>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div style="display: none" class="table_supporting_documents_message">
                                        <div class="alert alert-success alert-block message">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            <strong>File Successfully Add</strong>
                                        </div>
                                    </div>
                                    <div class="progress table_supporting_documents_progress" style="display: none">
                                        <div class="progress-bar table_supporting_documents_progress_bar"></div>
                                    </div>
                                    
                                    <div class="clearfix text-right">
                                        <button type="button" class=" btn btn-primary btn-sm add_supporting_documents mb-2">
                                            <i class="fas fa-plus fa-sm mr-1"></i>
                                            Add File
                                        </button>
                                        <button type="button" class=" btn btn-default btn-sm delete_supporting_documents mb-2">
                                            <i class="fas fa-trash-alt mr-1"></i> Delete Row
                                        </button>

                                    </div>

                                    <div class="table-wrapper-scroll-y my-custom-scrollbar" style="max-height: 20vh">
                                        <table class="table_supporting_documents table-bordered table-hover w-100">
                                            <thead>
                                                <tr>
                                                    <th class="p-2 text-white font-weight-light" colspan="5" style="background: #222222 !important;">
                                                        Supporting Document(s)
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th style="width:5% !important;"></th>
                                                    <th style="width:35% !important;">File</th>
                                                    <th style="width:30% !important;">Category</th>
                                                    <th style="width: 30% !important;">Description</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody align="center"></tbody>
                                        </table>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            {{--End create form--}}
            {{--Satrt Sub Category form--}}
            <div class="col-12">
                <div class="card card-outline card-primary shadow rounded-0 mb-4">
                    <!-- /.card-header -->
                    <div class="card-header">
                        <h3 class="card-title font-weight-bold">Beneficiary Details</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-md-3 form-group">
                                <label>Bank Name:<span class="text-danger">*</span></label>
                                <select data-live-search="true" class="form-control select-state beneficiary_account_no"
                                        id="beneficiary_account_no" name="beneficiary_account_id" required>
                                    <option value="">Select Account</option>
                                </select>

                            </div>
                            <div class="col-md-3 form-group">
                                <label>Account Number:</label>
                                <input type="text" class="form-control beneficiary_bank_name"
                                       name="beneficiary_bank_name" readonly>
                            </div>
                            <div class="col-md-3 form-group">
                                <label>Branch Name:</label>
                                <input type="text" class="form-control beneficiary_branch_name"
                                       name="beneficiary_branch_name" readonly>
                            </div>

                            <div class="col-md-3 form-group">
                                <label>IBAN:</label>
                                <input type="text" class="form-control beneficiary_iban"
                                       name="beneficiary_iban" readonly>
                            </div>
                            <div class="col-md-3 form-group">
                                <label>SWIFT Code:</label>
                                <input type="text" class="form-control beneficiary_swift_code"
                                       name="beneficiary_swift_code" readonly>
                            </div>

                            <div class="col-md-3 form-group">
                                <label>City:</label>
                                <input type="text" class="form-control beneficiary_city"
                                       name="beneficiary_city" readonly>
                            </div>
                            <div class="col-md-6 form-group">
                                <label>Address:</label>
                                <input type="text" class="form-control beneficiary_address"
                                       name="beneficiary_address" readonly>
                            </div>

                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            {{--End Sub Category form--}}

            {{--Satrt Sub Category form--}}
            <div class="col-12">
                <div class="card card-outline card-primary shadow rounded-0 mb-4">
                    <!-- /.card-header -->
                    <div class="card-header">
                        <h3 class="card-title" style="font-weight: bold;">Project and Measures / Lines Items to be
                            Charged</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    

                    <div class="card-body">
                        <div class="text-right mb-2">
                            <button type="button" value="Add Row" id="add_lines_items_to_be_charged" class="no-print btn btn-primary btn-sm rounded-0 add_lines_items_to_be_charged">
                                <li class="fa fa-plus mr-1"></li>
                                Add New
                            </button>

                            <button type="button" id="delete_table_lines_items_to_be_charged" class="no-print btn btn-default btn-sm rounded-0 delete_table_lines_items_to_be_charged">
                                <i class="fas fa-trash-alt mr-1"></i> Delete Row
                            </button>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-hover table-bordered table-sm table_lines_items_to_be_charged" style="width: 250%" id="table_lines_items_to_be_charged">
                                {{-- TABLE HEADER --}}
                                <thead align="center" class="thead-light">
                                        <th scope="col" align="center" valign="center" width="2%">
                                            <input type="checkbox" 
                                                   onclick="$(`.table_lines_items_to_be_charged tbody input[name='record']`).not(':disabled').prop('checked', $(this).prop('checked'))" 
                                                   class="select_all_checkboxes">
                                        </th>
                                        <th scope="col" width="10%">Project</th>
                                        <th scope="col" width="10%">Measure</th>
                                        <th scope="col" width="10%">Sub-Category</th>
                                        <th scope="col" width="5%">Province</th>
                                        <th scope="col" width="10%">PEA</th>
                                        <th scope="col" width="5%">City</th>
                                        <th scope="col" width="5%">Facility</th>
                                        <th scope="col" width="15%" style="color: white;background-color:  #17a2b8; vertical-align: middle">
                                            Amount (<span class="contract_change_currency_from_text">__</span>)
                                        </th>
                                        <th scope="col" width="15%" style="color:white;background-color: #007bff; vertical-align: middle">
                                            Amount(<span class="contract_change_currency">__</span>)
                                        </th>

                                        <th scope="col" style="width: 10%; vertical-align: middle">Description</th>
                                </thead>
                                {{-- TABLE BODY --}}
                                <tbody></tbody>   
                                {{-- TABLE FOOTER --}}
                                <tfoot>
                                    <tr>
                                        <td colspan="7"></td>
                                        <td valign="center" align="right">
                                            <b>Total</b>
                                        </td>
                                        <td><input type="text"
                                                   class="form-control payment_total_amount number_formated_two"
                                                   style=" text-align:right"
                                                   readonly=""></td>
                                        <td><input type="text"
                                                   class="form-control payment_total_amount_pkr number_formated_two"
                                                   style=" text-align:right"
                                                   readonly=""></td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            {{--End Sub Category form--}}

            {{--Satrt Sub Category form--}}
            <div class="col-12">
                <div class="card card-outline card-primary shadow rounded-0 mb-4">
                    <!-- /.card-header -->
                    <div class="card-header">
                        <h3 class="card-title" style="font-weight: bold;">Payment Details</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->

                    </div>
                    <div class="card-body" style="display: block;">

                        <div class="row">
                            <div class="col-5">
                                <div class="form-row">
                                    <div class="col-6 form-group">
                                        <label>Payment Amount(<span class="contract_change_currency_from_text">__</span>):<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control numberJs text-right" value="{{ old('payment_details_amount') ?? 0 }}" name="payment_details_amount" required>
                                    </div>

                                    <div class="col-6 form-group">
                                        <label>Payment Date:<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control payment_details_date text-right" value="{{ old('payment_details_date') ?? \Carbon\Carbon::now('PKT')->format('d-M-Y') }}" placeholder="Enter Payment Date" name="payment_details_date" autocomplete="off" required>
                                    </div>

                                    <div class="col-6 form-group">
                                        <label>Payment Currency:<span class="text-danger">*</span></label>
                                        <select class="form-control" name="payment_details_currency" required>
                                            <option value="" selected>Select Currency</option>
                                            @foreach($currency as $curr)
                                                <option value="{{$curr->id}}" {{ old('payment_details_currency') == $curr->id ? 'selected' : '' }}>{{$curr->currency_code}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-6 form-group">
                                        <label>Purpose Code:</label>
                                        <input type="text" class="form-control" name="payment_details_code" value="{{ old('payment_details_code') }}">
                                    </div>

                                    <div class="col-12 form-group">
                                        <label>Remarks:</label>
                                        <textarea class="form-control" name="payment_details_remarks" rows="5">{{ old('payment_details_remarks') }}</textarea>
                                    </div>
                                </div>  
                            </div>


                            <div class="col-7">
                                <div style="display: none" class="table_supporting_documents_message_detail">
                                    <div class="alert alert-success alert-block message">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>File Successfully Add</strong>
                                    </div>
                                </div>
                                <div class="progress table_supporting_documents_progress_detail" style="display: none">
                                    <div class="progress-bar table_supporting_documents_progress_bar_detail"></div>
                                </div>
                                <div class="text-right mb-2">
                                    <button type="button" class="no-print btn btn-primary btn-sm rounded-0 add_payment_supporting_documents">
                                        <li class="fa fa-plus mr-1"></li>
                                        Add New
                                    </button>

                                    <button type="button" class="no-print btn btn-default btn-sm rounded-0 delete_payment_supporting_documents">
                                        <i class="fas fa-trash-alt mr-1"></i> Delete Row
                                    </button>
                                </div>  

                                <div class="table table-sm table-wrapper-scroll-y my-custom-scrollbar">
                                    <table class="table_payment_supporting_documents table-bordered" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th colspan="5" class="p-2 text-white font-weight-light" style="background: #222222 !important;">
                                                    Payment Supporting Documents
                                                </th>
                                            </tr>
                                            <tr>
                                                <th style="width:5% !important;"></th>
                                                <th style="width:30% !important;">File</th>
                                                <th style="width:30% !important;">Category</th>
                                                <th style="width: 30% !important;">Description</th>
                                                <th style="width:5% !important;"></th>
                                            </tr>
                                        </thead>
                                        <tbody align="center"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <div class="col-12 clearfix form-group">
                <button id="cancelall" type="button" class="btn btn-light active rounded-0"><i class="fas fa-times text-danger mr-1"></i> Cancel</button>
                <button type="submit" class="btn btn-success rounded-0 check-for-total"><i class="fas fa-cloud-upload-alt mr-1"></i> | Create Payment</button>
            </div>
        </div>
    </form>

    {{--End Buttons--}}
    @push('javascript')
    @endpush
@endsection