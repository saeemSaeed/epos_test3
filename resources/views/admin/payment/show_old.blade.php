@extends('admin.layout.index')
@section('content')
    <style>
        .fieldset {
            position: relative;
            border: 1px #ddd dotted;
            padding: 1rem;
        }

        .fieldset h6 {
            /*font-weight: bold;*/
            font-size: 10px;
            margin-top: 5px;
            margin-bottom: 2px;
        }

        .card h6 {
            /*font-weight: 0;*/
            font-size: 10px;
            margin-top: 5px;
            margin-bottom: 2px;
        }

        .table th, td {
            font-size: 10px;
        }

        .fieldset h1 {
            position: absolute;
            top: 0;
            font-size: 16px;
            line-height: 1;
            margin: -9px 0 0 -0.5rem; /* half of font-size up top */
            background: #fff;
            padding: 0 0.5rem;
            color: #ccc;
        }

        input {
            /*font-size: 12px !important;*/
            height: 30px !important;
            color:black !important;
            font-size: 12px !important;
        }

        select {
            height: 30px !important;
            color:black !important;
            font-size: 12px !important;
        }

        .form-group {
            margin-bottom: 0.5rem;
            padding-top: 5px !important;
        }

    </style>



    <section class="bg-primary content-header"
             style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Payments</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li><i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li><i class="fa fa-angle-right" style="color: #ccc;"></i> Payments &nbsp;</li>
                    <li class="active"><i class="fa fa-angle-right" style="color: #ccc;"></i> Add Payment</li>
                </ol>
            </div>
        </div>
    </section>
    <button type="button" class="btn btn-default printpayment" style="float: right"><i class="fa fa-print"  aria-hidden="true"></i>
        Print
    </button>
    <form id="paymentCreateForm"
          enctype="multipart/form-data">
        @csrf
        {{--Start create form--}}
        <div class="col" style="margin-top: 10px">
            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title" style="font-weight: bold;">Payment</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-5">
                            <div class="form-group">
                                <input type="hidden" class="payment_id" value="{{$payment->id}}">
                                <h1 style="font-size: 10px;">Vendor: &nbsp;</h1>
                                <select data-live-search="true" data-live-search-style="startsWith"
                                        id="select_state_payment"
                                        style="pointer-events:none;background: #a2a2a2;color:#e2e2e2"
                                        class="form-control form-control-sm contractor_id" name="contractor_id">
                                    <option value="">Select a state...</option>
                                    @foreach ($contractor_id as $index=>$obj)
                                        <option value=" {{$obj->hasOneContractor->id}}" {{$payment->contractor_id==$obj->hasOneContractor->id?'selected':''}}>   {{$obj->hasOneContractor->contractor_name}}</option>

                                    @endforeach


                                </select>


                            </div>

                            <div class="form-group">
                                <h1 style="font-size: 10px;">Contract Title/Reference Code: &nbsp;</h1>
                                <select data-live-search="true" data-live-search-style="startsWith"
                                        id="vendor_contracts"
                                        style="pointer-events:none;background: #a2a2a2;color:#e2e2e2"
                                        class="form-control form-control-sm vendor_contracts" name="contract_id">
                                    @foreach($get_all_vendor_contract as $index=>$arr)
                                        <option value=" {{$arr->id}}" {{$payment->contractor_id==$arr->id?'selected':''}}>   {{$arr->contract_title}}</option>


                                    @endforeach


                                </select>


                            </div>

                            <section class="fieldset">
                                <h1>Contract Details</h1>
                                <div class="form-row">
                                    <div class="col-4">

                                    </div>
                                    <div class="col-4">

                                        <P style="text-align: center;background:#16d2d4;color:white"><span class="currency_change_from">   Amount({{$contract_currency}})</span></P>
                                    </div>
                                    <div class="col-4">
                                        <P style="text-align: center;background:#4966ff;color:white"><span class="currency_change_from">  Amount({{$currency_name}})</span></P>


                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-4">
                                        <h6 style="padding-top:17px; ">Contract Amount:</h6>
                                    </div>
                                    <div class="col-4">
                                        <input type="text" class="form-control  contract_amount_payment number_formated_two"
                                               style="direction: rtl;
                                               width: 100%;
                                                border-radius: 0px;
                                                 border-bottom: 1px solid #8b8b8b;
                                                  border-top: 0px;
                                                  border-left: 0px;border-right: 0px;background: transparent"
                                               value="{{$contract_amount}}" readonly>
                                    </div>
                                    <div class="col-4">
                                        <input type="text"
                                               class="form-control form-control-sm contract_revised_amount_payment number_formated_two"
                                               style="direction: rtl;
                                                border-radius: 0px;
                                                  width: 100%;
                                          border-bottom: 1px solid #8b8b8b;
                                               border-top: 0px; border-left:
                                                0px;border-right: 0px;background: none"
                                               placeholder="" value="{{$contract_revised_amount}}" readonly>


                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-4">
                                        <h6 style="padding-top:17px;">Previous Payment:</h6>
                                    </div>
                                    <div class="col-4">
                                        <input type="text" class="form-control form-control-sm previous_payment_amount number_formated_two"
                                               style="background:transparent;direction: rtl;
                                                border-radius: 0px;
                                                border-bottom: 1px solid #8b8b8b;
                                                 border-top: 0px;
                                                 border-left:
                                                 0px;border-right: 0px;"
                                               readonly value="{{$previous_payment_amount}}">
                                    </div>
                                    <div class="col-4">
                                        <input type="text" class="form-control form-control-sm previous_payment_amount_pkr number_formated_two"
                                               style="background:transparent;direction: rtl; border-radius: 0px; border-bottom: 1px solid #8b8b8b; border-top: 0px; border-left: 0px;border-right: 0px;"
                                               readonly value="{{$previous_payment_amount_pkr}}">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-4   ">
                                        <h6 style="padding-top:17px;">Balance Payable:</h6>
                                    </div>
                                    <div class="col-4">
                                        <input type="text" class="form-control form-control-sm balance_payable_amount number_formated_two"
                                               style="background:transparent;direction: rtl; border-radius:
                                               0px; border-bottom: 1px solid #8b8b8b;
                                               border-top: 0px; border-left: 0px;border-right: 0px;"
                                               readonly value="{{$balance_payable_amount}}">
                                    </div>
                                    <div class="col-4">
                                        <input type="text" class="form-control form-control-sm balance_payable_amount_pkr number_formated_two"
                                               style="background:transparent;direction: rtl; border-radius: 0px; border-bottom: 1px solid #8b8b8b; border-top: 0px; border-left: 0px;border-right: 0px;"
                                               readonly value="{{$balance_payable_amount_pkr}}">
                                    </div>
                                </div>


                                <div class="form-row">
                                    <div class="col-3">
                                        <h6 style="padding-top:17px;">contract Currency:</h6>
                                    </div>
                                    <div class="col-3">
                                        <input type="text" class="form-control form-control-sm contract_currency"
                                               style="background:transparent; border-radius: 0px; border-bottom: 1px solid
                                               #8b8b8b; border-top: 0px; border-left: 0px;border-right: 0px; "
                                               value="{{$contract_currency}}" readonly>
                                    </div>
                                    <div class="col-3">
                                        <h6 style="padding-top:17px;">Last Payment Date: </h6>
                                    </div>
                                    <div class="col-3">
                                        <input type="text" class="form-control form-control-sm last_payment_date"
                                               style="background:transparent; border-radius: 0px; border-bottom: 1px solid #8b8b8b;
                                               border-top: 0px; border-left: 0px;border-right: 0px;"
                                               value="{{$last_payment_date}}" readonly>
                                    </div>
                                </div>
                                <div class="form-row" style="">
                                    <div class="col-3">
                                        <h6 style="padding-top:17px;">Payment Origin:</h6>
                                    </div>
                                    <div class="col-3">
                                        <input type="text" class="form-control form-control-sm payment_origin"
                                               style="background:transparent; border-radius: 0px; border-bottom: 1px solid #8b8b8b; border-top: 0px; border-left: 0px;border-right: 0px;"
                                               value="{{$payment_origin}}" readonly>
                                    </div>
                                    <div class="col-3">
                                        <h6 style="padding-top:17px;">Last Payment Amount:</h6>
                                    </div>
                                    <div class="col-3">
                                        <input type="text" class="form-control form-control-sm last_payment_amount"
                                               style="background:transparent;direction: rtl; border-radius: 0px; border-bottom: 1px solid #8b8b8b; border-top: 0px; border-left: 0px;border-right: 0px;"
                                               value="{{$last_payment_amount}}" readonly>
                                    </div>
                                </div>
                            </section>

                        </div>
                        <div class="form-group col-0"></div>
                        <div class="col-7">
                            <section class="fieldset" style="margin-top: 22px;">
                                <h1>Invoice Details</h1>
                                <div class="form-row">
                                    <div class="col-3">
                                        <h6>Currency:</h6>
                                        <input type="text"
                                               class="form-control form-control-sm contract_change_currency_from"
                                               placeholder=""
                                               style="background-color: #e9ecef" readonly
                                               value=" {{$payment->invoiceCurrency->currency_code}}">
                                        <input type="hidden"
                                               class="form-control form-control-sm contract_change_currency_from_id"
                                               placeholder=""
                                               style="background-color: #e9ecef" name="invoice_currency"
                                               value=" {{$payment->invoiceCurrency->id}}">
                                    </div>
                                    <div class="col-4">
                                        <h6>Invoice Amount:</h6>
                                        <input type="text" class="form-control form-control-sm invoice_amount" dir="rtl"
                                               name="invoice_amount" required readonly

                                               value=" {{$payment->invoice_amount}}">
                                    </div>
                                    <div class="col">
                                        <h6>Invoice Date:</h6>
                                        <input type="text" class="form-control form-control-sm invoice_date"
                                               name="invoice_date" readonly
                                               value="{{$payment->invoice_date?\Carbon\Carbon::parse($payment->invoice_date)->format('d-M-Y'):''}}">
                                    </div>
                                    <div class="col">
                                        <h6>Invoice No:</h6>
                                        <input type="text" class="form-control form-control-sm" name="invoice_no"
                                               readonly value="{{$payment->invoice_no}}">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-3">
                                        <h6>Less Retention (If Applicable):</h6>
                                        <input type="text" class="form-control form-control-sm invoice_less_retention"
                                               oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"
                                               dir="rtl" name="invoice_less_retention"
                                            readonly   value="{{$payment->invoice_less_retention}}">
                                    </div>
                                    <div class="col-4">
                                        <h6>Less Retention Amount EUR:</h6>
                                        <input type="text"
                                               class="form-control form-control-sm invoice_less_retention_amount"
                                               readonly dir="rtl" name="invoice_less_retention_amount"
                                               value="{{$payment->invoice_less_retention_amount}}">
                                    </div>
                                    <div class="col-5">
                                        <h6>Withdraw Application/Payment Request No:</h6>
                                        <input type="text" class="form-control form-control-sm"
                                               name="invoice_withdraw_application"
                                            readonly   value="{{$payment->invoice_withdraw_application}}">

                                    </div>

                                    <div class="col-md-12">
                                            <label>Fee:<span class="text-danger">*</span></label>
                                            <div class="input-group mb-3">
                                                <input type="number" class="form-control text-right" name="invoice_withdraw_application" placeholder="0" maxlength="5" onkeyup="this.value = fnc(this.value, 0, 100)">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">%</span>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        <h6>Performance Security Validity:</h6>
                                        <input type="text" readonly
                                               class="form-control form-control-sm  "
                                               name="invoice_performance_security_validity"
                                               value="{{$payment->invoice_performance_security_validity?
                                               \Carbon\Carbon::parse($payment->invoice_performance_security_validity)->format('d-M-Y'):''}}">
                                    </div>
                                    <div class="col-4">
                                        <h6>Net Payable Amount(EUR):</h6>
                                        <input type="text" class="form-control form-control-sm invoice_net_payable"
                                               name="invoice_net_payable"
                                               dir="rtl" readonly
                                               value="{{$payment->invoice_amount-$payment->invoice_less_retention_amount}}">
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <h6>Invoice Description:</h6>
                                        <textarea class="form-control" style="width: 100%;height: 70%"
                                              readonly    name="invoice_description"> </textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div style="display: none" class="table_supporting_documents_message">
                                        <div class="alert alert-success alert-block message">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            <strong>File Successfully Add</strong>
                                        </div>
                                    </div>
                                    <div class="progress table_supporting_documents_progress" style="display: none">
                                        <div class="progress-bar table_supporting_documents_progress_bar"></div>
                                    </div>

                                    <div class="table-wrapper-scroll-y my-custom-scrollbar" style="max-height:120px ">
                                        <table class="table_supporting_documents table-bordered"
                                               style="margin-bottom: 0px;width: 100% !important;font-size: 10px">
                                            <thead>
                                            <tr>
                                                <th colspan="5" style="background: #5f5f5f!important;color: white;">
                                                    Supporting Documents
                                                </th>
                                            </tr>
                                            <tr>
                                                <th style="width:5% !important;"></th>
                                                <th style="width:35% !important;">File</th>
                                                <th style="width:30% !important;">Category</th>

                                                <th style="width: 30% !important;">Description</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($payment->PaymentSupportingDocuments as $file)

                                                <tr>

                                                    {{--{{$file}}--}}
                                                    <td style="text-align: center"><input data-id="{{$file->id}}"
                                                                                          type='checkbox' name='record'>
                                                    </td>
                                                    <td>
                                                        @if( !empty($file->file))
                                                            <a href="{{ URL::to( '/files/payment/supporting/' . $file->file)  }}"
                                                               style="
                                                            margin-top: 0; white-space: -moz-pre-wrap !important;  /* Mozilla, since 1999 */
    white-space: -pre-wrap;      /* Opera 4-6 */
    white-space: -o-pre-wrap;    /* Opera 7 */
    white-space: pre-wrap;       /* css-3 */
    word-wrap: break-word;       /* Internet Explorer 5.5+ */
    white-space: -webkit-pre-wrap; /* Newer versions of Chrome/Safari*/
    word-break: break-all;
    white-space: normal;"
                                                               target="_blank"
                                                               download="{{$file->file_name}}"> {{$file->file_name}}
                                                                <i class=" fa-lg fas fa-file-download"></i>
                                                            </a>
                                                        @endif
                                                    </td>

                                                    <td style="text-align:center">{{$file->category}}</td>


                                                    <td style="text-align:left"><p>{{$file->description?$file->description:''}}</p></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        {{--End create form--}}
        {{--Satrt Sub Category form--}}
        <div class="col">
            <div class="card card-outline card-primary">
                <!-- /.card-header -->
                <div class="card-header">
                    <h3 class="card-title" style="font-weight: bold;">Beneficiary Details</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->

                </div>
                <div class="card-body" style="display: block;">
                    <div class="form-row">
                        <div class="col-2">
                            <h6>Account No:</h6>
                            <select data-live-search="true" class="form-control  select-state beneficiary_account_no"
                                    id="beneficiary_account_no" name="beneficiary_account_id" required style="pointer-events:none">
                                <option value="">Select</option>
                                @foreach($contractor_financial_details as $account)
                                    <option value="{{$account->id}}" {{$payment->ContractorFinancialDetails->id==$account->id?'selected':''}}>{{$account->account_no}}</option>
                                @endforeach
                            </select>

                            {{--<input type="text" class="form-control form-control-sm beneficiary_account_no" name="beneficiary_account_no"  placeholder="">--}}
                        </div>
                        <div class="col">
                            <h6>Bank Name:</h6>
                            <input type="text" class="form-control form-control-sm beneficiary_bank_name"
                                   name="beneficiary_bank_name" readonly
                                   value="{{$payment->ContractorFinancialDetails->bank_name }}">
                        </div>
                        <div class="col">
                            <h6>Branch Name:</h6>
                            <input type="text" class="form-control form-control-sm beneficiary_branch_name"
                                   name="beneficiary_branch_name" readonly
                                   value="{{$payment->ContractorFinancialDetails->branch_name }}">
                        </div>

                        <div class="col">
                            <h6>IBAN:</h6>
                            <input type="text" class="form-control form-control-sm beneficiary_iban"
                                   name="beneficiary_iban" readonly
                                   value="{{$payment->ContractorFinancialDetails->iban }}">
                        </div>
                        <div class="col">
                            <h6>SWIFT Code:</h6>
                            <input type="text" class="form-control form-control-sm beneficiary_swift_code"
                                   name="beneficiary_swift_code" readonly
                                   value="{{$payment->ContractorFinancialDetails->swift_code }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-2">
                            <h6>City:</h6>
                            <input type="text" class="form-control form-control-sm beneficiary_city"
                                   name="beneficiary_city" readonly
                                   value="{{$payment->ContractorFinancialDetails->hasCity->city_name }}">
                        </div>
                        <div class="col">
                            <h6>Address:</h6>
                            <input type="text" class="form-control form-control-sm beneficiary_address"
                                   name="beneficiary_address" readonly
                                   value="{{$payment->ContractorFinancialDetails->branch_address }}">
                        </div>

                    </div>

                </div>
                <!-- /.card-body -->


            </div>
            <!-- /.card -->
        </div>
        {{--End Sub Category form--}}
        {{--Satrt Sub Category form--}}
        <div class="col">
            <div class="card card-outline card-primary">
                <!-- /.card-header -->
                <div class="card-header">
                    <h3 class="card-title" style="font-weight: bold;">Project and Measures / Lines Items to be
                        Charged</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <div class="card-body">



                    <table class=" table-sm table_lines_items_to_be_charged_edit"
                           id="table_lines_items_to_be_charged_edit"
                           style="width: 100%!important;border-collapse:separate;
                        border-spacing:3px 3px;">
                        <thead style="">
                        <tr style="    background: #cacfcf;
    color: #404040;vertical-align: middle">
                            <th style="width: 2%;text-align:center;vertical-align: middle ">Sr.#</th>
                            <th style="width: 10%;text-align:center;vertical-align: middle">Project</th>
                            <th style="width: 10%;text-align:center;vertical-align: middle">Measure</th>
                            <th style="width: 10%;text-align:center;vertical-align: middle">Sub-Category</th>
                            <th style="width: 10%;text-align:center;vertical-align: middle">Province</th>
                            <th style="width: 10%;text-align:center;vertical-align: middle">PEA</th>
                            <th style="width: 10%;text-align:center;vertical-align: middle">City</th>
                            <th style="color: white;background-color:  #17a2b8;width: 15%!important;text-align:center;vertical-align: middle">
                                Amount(<span class="contract_change_currency_from_text">({{$contract_currency}})</span>)
                            </th>
                            <th style="color:white;background-color: #007bff;width: 15%!important;text-align:center;vertical-align: middle">
                                Amount(<span class="contract_change_currency">({{$currency_name}})</span>)
                            </th>

                            <th style="width: 10%;text-align:center;vertical-align: middle">Description</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($payment->hasPaymentsHasSub as $pay)
                            <tr>
                                <th><input type='hidden' name='checkFlag' class='checkFlag' value='1'/>
                                    <input
                                            name='record' id='check_all' type='checkbox'></th>
                                <td><select name='project_id[]' class='form-control payment_project_id'
                                            id='payment_project_id' required>

                                        @if(!is_null($payment->contract_id))
                                            @foreach(\App\Helpers\Helper::GetContractProjects($payment->contract_id) as $sub )
                                                <option
                                                        value="{{$sub['id']}}" {{$pay->project_id   ==$sub['id']?'selected':''}}>
                                                    {{$sub['title']}}</option>
                                            @endforeach
                                        @endif

                                    </select></td>
                                <td><select name='measure_id[]' class=' form-control payment_project_measure'
                                            id='payment_project_measure' readonly style="pointer-events:none">
                                        <option value=''>Select</option>
                                        @if(!is_null($pay->project_id)|| !is_null($pay->contract_id))
                                            @foreach(\App\Helpers\Helper::GetContractProjectsMeasure($pay->contract_id,$pay->project_id) as $index=>$sub )
                                                <option
                                                        value="{{$sub['id']}}" {{$pay->measure_id==$sub['id']?'selected':''}}>
                                                    {{$sub['name']}}</option>
                                            @endforeach
                                        @endif

                                    </select></td>
                                <td><select name='sub_cate_id[]' class='form-control payment_project_measure_sub_cate'
                                            id='payment_project_measure_sub_cate' readonly style="pointer-events:none">
                                        <option value=''>Select</option>
                                        @if(!is_null($pay->project_id)|| !is_null($pay->contract_id) || !is_null($pay->measure_id))
                                            @foreach(\App\Helpers\Helper::GetContractProjectsMeasureSubCategory($pay->contract_id,$pay->project_id,$pay->measure_id) as $index=>$sub )
                                                <option
                                                        value="{{$sub['id']}}" {{$pay->sub_category_id==$sub['id']?'selected':''}}>
                                                    {{$sub['name']}}</option>
                                            @endforeach
                                        @endif
                                    </select></td>
                                <td><input readonly type='text' class=' form-control payment_project_province_name'
                                           id='payment_project_province_name' value="{{$pay->hasProvince->name}}">
                                    <input type='hidden' name='province_id[]' readonly
                                           class=' form-control payment_project_province_id'
                                           id='payment_project_province_id' value="{{$pay->hasProvince->id}}">
                                </td>
                                <td><input readonly type='text'
                                           class=' form-control payment_project_sub_cate_peas_name form-control'
                                           id='payment_project_sub_cate_peas_name' value="{{$pay->hasPeas->name}}"  >
                                    <input readonly type='hidden' name='sub_cate_peas_id[]'
                                           class=' form-control payment_project_sub_cate_peas_id form-control'
                                           id='payment_project_sub_cate_peas_id'
                                           value="{{$pay->hasPeas->id}}">
                                </td>
                                <td><select data-live-search='true' data-live-search-style='startsWith' name='city_id[]'
                                            id='payment_project_city_id' class='payment_project_city_id form-control'
                                            style='background-color: #FFCCCB;pointer-events:none'>
                                        @if(!is_null($pay->project_id)|| !is_null($pay->contract_id) || !is_null($pay->measure_id))
                                            @foreach(\App\Helpers\Helper::GetContractProjectsMeasureSubCategoryCity($pay->contract_id,$pay->project_id,$pay->measure_id) as $city)

                                                <option
                                                        value="{{$city['id']}}" {{$pay->city_id==$city['id']?'selected':''}}>
                                                    {{$city['city_name']}}</option>
                                            @endforeach
                                        @endif
                                    </select></td>
                                <td><input type='text' name='payment_amount[]'
                                           class='payment_project_amount number_formated_two form-control' dir='rtl'
                                           style='width:100% !important;text-align:right;' value="{{$pay->budget}}" readonly>
                                </td>
                                <td><input type='text' name='payment_amount_pkr[]'
                                           class='payment_project_amount_pkr number_formated_two form-control' dir='rtl'
                                           style='width:100% !important;text-align:right;' value="{{$pay->budget_pkr}}" readonly>
                                </td>
                                <td><input type='text' name='payment_project_description[]'
                                           class='form-control payment_project_description'
                                           value="{{$pay->description}}" readonly></td>
                            </tr>

                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="6"></td>
                            <td colspan="" style="text-align: right;"><b>Total</b></td>
                            <td><input type="text"
                                       class="form-control payment_total_amount number_formated_two"
                                       style=" text-align:right"
                                       readonly=""></td>
                            <td><input type="text"
                                       class="form-control payment_total_amount_pkr number_formated_two"
                                       style=" text-align:right"
                                       readonly=""></td>
                            <td></td>
                        </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
        {{--End Sub Category form--}}
        {{--Satrt Sub Category form--}}
        <div class="col">
            <div class="card card-outline card-primary">
                <!-- /.card-header -->
                <div class="card-header">
                    <h3 class="card-title" style="font-weight: bold;">Payment Details</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->

                </div>
                <div class="card-body" style="display: block;">

                    <div class="row">
                        <div class="col-5">
                            <div class="row">
                                <div class="col-6">

                                    <h6>Payment Amount(EUR):</h6>
                                    <input type="text" class="form-control form-control-sm payment_details_amount"
                                           dir="rtl"
                                           name="payment_details_amount" readonly
                                           value="{{$payment->payment_details_amount}}"></div>
                                <div class="col-6">

                                    <h6>Payment Date:</h6>
                                    <input type="text" class="form-control form-control-sm payment_details_date"
                                           name="payment_details_date" readonly
                                           value="{{$payment->payment_details_date?\Carbon\Carbon::parse($payment->payment_details_date)->format('d-M-Y'):''}}">

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">

                                    <h6>Payment Currency:</h6>
                                    <select class="form-control form-control-sm "
                                            name="payment_details_currency" style="pointer-events: none"
                                            required>
                                        <option>select Currency</option>
                                        @foreach($currency as $curr)
                                            <option value="{{$curr->id}}" {{$payment->payment_details_currency==$curr->id?'selected':''}}>{{$curr->currency_code}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6">
                                    <h6>Purpose Code</h6>
                                    <input type="text" class="form-control form-control-sm" name="payment_details_code"
                                           required value="{{$payment->payment_details_code}}" readonly>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <h2 style="font-size: 9px; margin-bottom: 0px;">Remarks:</h2>
                                    <textarea class="form-control"
                                              style="width:100% ;height: auto"
                                             readonly  name="payment_details_remarks">{{$payment->payment_details_remarks}}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-7">
                            <div style="display: none" class="table_supporting_documents_message_detail">
                                <div class="alert alert-success alert-block message">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>File Successfully Add</strong>
                                </div>
                            </div>
                            <div class="progress table_supporting_documents_progress_detail" style="display: none">
                                <div class="progress-bar table_supporting_documents_progress_bar_detail"></div>
                            </div>

                            <div class="table table-sm table-wrapper-scroll-y my-custom-scrollbar" style="max-height: 180px">
                                <table class="table_payment_supporting_documents table-bordered"
                                       style="width: 100%;font-size: 10px">
                                    <thead>
                                    <tr>
                                        <th colspan="5"
                                            style="background: #5f5f5f!important;color: white;">
                                            Payment Supporting Documents
                                        </th>
                                    </tr>
                                    <tr>
                                        <th style="width:5% !important;"></th>
                                        <th style="width:30% !important;">File</th>
                                        <th style="width:30% !important;">Category</th>

                                        <th style="width: 30% !important;">Description</th>
                                        <th style="width:5% !important;"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($payment->PaymentSupportingFileDetial as $file)

                                        <tr>

                                            {{--{{$file}}--}}
                                            <td style="text-align: center"><input data-id="{{$file->id}}"
                                                                                  type='checkbox' name='record'>
                                            </td>
                                            <td>
                                                @if( !empty($file->file))
                                                    <a href="{{ URL::to( '/files/payment/supporting/detail' . $file->file)  }}"
                                                       style="
                                                            margin-top: 0; white-space: -moz-pre-wrap !important;  /* Mozilla, since 1999 */
    white-space: -pre-wrap;      /* Opera 4-6 */
    white-space: -o-pre-wrap;    /* Opera 7 */
    white-space: pre-wrap;       /* css-3 */
    word-wrap: break-word;       /* Internet Explorer 5.5+ */
    white-space: -webkit-pre-wrap; /* Newer versions of Chrome/Safari*/
    word-break: break-all;
    white-space: normal;"
                                                       target="_blank"
                                                       download="{{$file->file_name}}"> {{$file->file_name}}
                                                        <i class=" fa-lg fas fa-file-download"></i>
                                                    </a>
                                                @endif
                                            </td>

                                            <td style="text-align:center">{{$file->category}}</td>


                                            <td style="text-align:left"><p>{{$file->description?$file->description:''}}</p></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>


                        </div>
                    </div>
                </div>
            </div>


            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </form>

    {{--End Buttons--}}
    @push('javascript')
    @endpush
@endsection