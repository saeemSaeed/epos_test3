@extends('admin.layout.index')
@section('content')
<section class="bg-primary content-header"
style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
<div class="row">
	<div class="col-8">
		<h1>Payments</h1>
	</div>
	<div class="col-4">
		<ol class="breadcrumb" style="color:#444;float: right">
			<li>
				<i class="fa fa-dashboard"></i> Dashboard &nbsp;
			</li>
			<li>
				<i class="fa fa-angle-right" style="color: #ccc;"></i> Payments &nbsp;
			</li>
		</ol>
	</div>
</div>
</section>

<div class="clearfix mx-2 mt-3">
	@can('projects-create')
		<div class="float-left">
			<a href="{{ route('payment.create') }}" class="btn btn-success">
				<i class="fa fa-plus mr-1"></i> New Payment
			</a>
		</div>
	@endcan
	
	@can('payments-restore')
		<div class="float-right">
			<a href="{{ route('payment.paymentDeleted') }}" class="btn btn-danger">
				<i class="fas fa-trash-alt"></i>
			</a>
		</div>
	@endcan
</div>


<div class="container-fluid" style="margin-top: 10px">
	<div class="card">
		<div class="card-body">
			<div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<div class="col-sm-12 col-md-6"></div>
					<div class="col-sm-12 col-md-6"></div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="table-responsive">
							<table id="payment_table" class="table table-bordered table-hover dataTable dtr-inline" role="grid" aria-describedby="example2_info">
								<thead style="background-color: #65a3c6;color: #2c2c2c">
									<tr role="row">
										<th>ID</th>
										<th>Payment Date</th>
										<th>Payment Request No</th>
										<th>Vendor</th>
										<th>Contract</th>
										<th>Payment Currency</th>
										<th>Payment Amount</th>
										<th>Payment Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody align="center">
									@foreach($payments as $index=>$payment)
									<tr role="row" class="odd">
										<td>{{ ++$index }}</td>
										<td>{{ \Carbon\Carbon::parse($payment->invoice_date)->format('d-M-Y') ?? '---' }}</td>
										<td>{{ App\Helpers\Helper::addOrdinalNumberSuffix($payment->payment_count_no) }}</td>
										<td>
											<a href="{{ route('contractors.profile', ['id' => $payment->PaymentHasContractor->id]) }}" title="View Contractor" target="_blank">
												{{ $payment->PaymentHasContractor->contractor_name }}
												<i class="fas fa-external-link-alt fa-sm ml-1"></i>
											</a>
										</td>
										<td>
											<a href="{{ route('contract.show', $payment->PaymentHasContract->id) }}" title="View Contract" target="_blank">
												{{ $payment->PaymentHasContract->contract_title }}
												<i class="fas fa-external-link-alt fa-sm ml-1"></i>
											</a>
										</td>
										<td>
											{{$payment->invoiceCurrency->currency_name }} 
											<small class="text-muted">({{ $payment->invoiceCurrency->currency_symbol }})</small>
										</td>
										<td>{{ number_format($payment->invoice_amount, 6) }}</td>
										<td>    
											@if($payment->status===0)
											<span class="badge badge-warning badge-pill"><i class="fas fa-exclamation-circle mr-1"></i> Pending </span>
											@elseif($payment->status===1)
											<span class="badge badge-success badge-pill"><i class="fas fa-check-circle mr-1"></i> Approved </span>
											@elseif($payment->status===2)
											<span class="badge badge-danger badge-pill"><i class="fas fa-times-circle mr-1"></i> Rejected </span>
											@endif
										</td>
										<td class="sorting_1 dtr-control">
											<div class="dropdown">
											  	<button type="button" class="btn btn-link btn-sm text-muted" data-toggle="dropdown">
											   	<i class="fas fa-cog"></i>
											  	</button>
											  <div class="dropdown-menu">
											  		{{-- view --}}
											  		@can('payments-show')
													   <a class="dropdown-item" href="{{ route('payment.show',$payment->id) }}">
													   	<i class="fas fa-eye mr-1 text-primary"></i> View Payment
													   </a>
											  		@endcan

											  		{{-- edit --}}
											  		@if($payment->status == 0)
												  		@can('payments-edit')
														   <a class="dropdown-item" href="{{ route('payment.edit',$payment->id) }}">
														   	<i class="fas fa-pencil-alt mr-1 text-dark"></i> Edit Payment
														   </a>
												  		@endcan
											  		@endif

											  		{{-- delete --}}
											  		@can('payments-delete')
													   <a class="dropdown-item" href="{{ route('payment.delete',$payment->id) }}">
													   	<i class="fas fa-times mr-1 text-danger"></i> Delete Payment
													   </a>
											  		@endcan


											  		@can('payments-status')
											  			@if($payment->status == 0) {{-- pending --}}
												  			<a class="dropdown-item" href="{{ route('payment.status', ['id'=>$payment->id, 'status'=>'approve']) }}">
														   	<i class="fas fa-check-circle mr-1 text-success"></i> Approve Payment
														   </a>

														   <a class="dropdown-item" href="{{ route('payment.status', ['id'=>$payment->id, 'status'=>'reject']) }}">
														   	<i class="fas fa-times-circle mr-1 text-danger"></i> Reject Payment
														   </a>
											  			@endif


											  		@endcan
											  </div>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection