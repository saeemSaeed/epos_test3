@extends('admin.layout.index')
@section('content')
    <section class="bg-primary content-header"
             style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Payments</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li>
                        <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li>
                        <i class="fa fa-angle-right" style="color: #ccc;"></i> Payments &nbsp;
                    </li>

                </ol>
            </div>
        </div>
    </section>
   
    <div class="container-fluid" style="margin-top: 10px">
        <div class="card">

            <div class="card-body">
                <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12 col-md-6"></div>
                        <div class="col-sm-12 col-md-6"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="payment_table" class="table table-bordered table-hover dataTable dtr-inline"
                                   role="grid" aria-describedby="example2_info">
                                <thead style="background-color: #65a3c6;color: #2c2c2c">
                                <tr role="row">
                                    <th>ID</th>
                                    <th>Payment Status</th>
                                    <th>Payment Date</th>
                                    <th>Payment Origin</th>
                                    <th>Payment Amount(EUR)</th>
                                    <th>Payment Amount</th>
                                    <th>Payment Currency</th>
                                    <th>Payment Category</th>
                                    <th>Action</th>

                                </tr>
                                </thead>
                                <tbody>

                                @foreach($deletedPayments as $index=>$payment)
                                    <tr role="row" class="odd">
                                        <td>{{++$index}}</td>
                                        <td>    @if($payment->status===0)
                                                <span class="badge badge-warning"> Pending </span>
                                            @elseif($payment->status===1)
                                                <span class="badge badge-success "> Approved </span>

                                            @elseif($payment->status===2)
                                                <span class="badge badge-danger "> Rejected </span>

                                            @endif</td>
                                        <td>{{$payment->payment_date}}</td>
                                        <td>{{isset($payment->hasPaymentMethod->payment_method_name)?$payment->hasPaymentMethod->payment_method_name:''}}</td>
                                        <td style="text-align: right">{{number_format($payment->contract_amount,2)}}</td>
                                        <td style="text-align: right">{{number_format($payment->payment_exchange_rate,2)}}</td>
                                        <td>{{isset($payment->hasCurrency->currency_name)?$payment->hasCurrency->currency_name:''}}</td>
                                        <td>{{isset($payment->hasMeasure->name)?$payment->hasMeasure->name:''}}</td>
                                         <td class="sorting_1 dtr-control" style="text-align: center!important;">
                                             <a type="submit" style="border: none;background-color: transparent" data-toggle="tooltip" title="" href="{{route('payment.restore',['id'=>$payment->id])}}" data-original-title="Restore"><i class="fas fa-trash-restore"></i></a>
                                        </td>

                                    </tr>

                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
@endsection