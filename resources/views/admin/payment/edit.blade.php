@extends('admin.layout.index')
@section('content')
    <section class="bg-primary content-header"
             style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Payments</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li><i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li><i class="fa fa-angle-right" style="color: #ccc;"></i> Payments &nbsp;</li>
                    <li class="active"><i class="fa fa-angle-right" style="color: #ccc;"></i> Update Payment</li>
                </ol>
            </div>
        </div>
    </section>

    <form class="paymentForm" action="{{route('payment.update', $payment->id) }}" method="post" id="paymentCreateForm" enctype="multipart/form-data">
        @csrf
        <div class="form-row mt-3">
            {{-- PRINT INVOICE --}}
            <div class="col-md-12">
                <div class="clearfix">

                    <h6 class="float-left mt-2 ml-1">
                        <i class="fas fa-history mr-1"></i> Last Updated: <b>{{ $payment->updated_at->diffForHumans() }}</b> 
                    </h6>

                    <button type="button" class="btn btn-primary rounded-0 float-right printpayment mb-2">
                        <i class="fas fa-print mr-1" aria-hidden="true"></i> Print Invoice
                    </button>
                </div>
            </div>
            {{--Start create form--}}
            <div class="col-12">
                <div class="card card-outline card-primary shadow rounded-0 mb-4">
                    <div class="card-header">
                        <h2 class="card-title font-weight-bold"><i class="fa fa-ellipsis-v mr-2"></i> General Information</h2>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-sm-5 col-12">
                                <div class="form-group col-md-12">
                                    <input type="hidden" class="payment_id" value="{{$payment->id}}">
                                    <label>Vendor:</label>
                                    <select id="select_state_payment" class="form-control contractor_id" name="contractor_id" disabled>
                                        <option value="">Select a state...</option>
                                        @foreach ($contractor_id as $index=>$obj)
                                            <option value="{{$obj->hasOneContractor->id}}" {{$payment->contractor_id==$obj->hasOneContractor->id?'selected':''}}>
                                                {{$obj->hasOneContractor->contractor_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                {{-- NTN --}}
                                <div class="col-12">
                                    <div class="form-group vendor_ntn_div">
                                        <label>Vendor NTN:</label>
                                        <input type="text" class="form-control vendor_ntn" value="{{ $payment->PaymentHasContractor->contractor_ntn ?? ''}}" readonly>
                                    </div>
                                </div>

                                <div class="form-group col-md-12">
                                    <label>Contract Title/Reference Code:</label>
                                    <select id="vendor_contracts" class="form-control vendor_contracts" name="contract_id" disabled>
                                        @foreach($get_all_vendor_contract as $index=>$arr)
                                            <option value="{{ $arr->id }}" {{$payment->contractor_id==$arr->id?'selected':''}}>
                                                {{$arr->contract_title}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <fieldset class="border p-2">
                                    <legend class="w-auto px-2 text-muted font-weight-bold" style="font-size: 19px;">Contract Details</legend>
                                    <div class="form-row">
                                        <div class="col-4"></div>
                                        <div class="col-4">
                                            <P class="text-center text-white p-1" style="background:#16d2d4">Amount({{$contract_currency}})</P>
                                        </div>
                                        <div class="col-4">
                                            <P class="text-center text-white p-1" style="background:#4966ff">Amount({{ $currency_name }})</P>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-4">
                                            <label class="mt-3">Contract Amount:</label>
                                        </div>
                                        <div class="col-4">
                                            <input type="text" class="form-control contract_amount_payment number_formated_two bg-transparent border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0" value="{{$contract_amount}}" dir="rtl" readonly>
                                        </div>

                                        <div class="col-4">
                                            <input type="text" class="form-control contract_revised_amount_payment number_formated_two border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0 bg-transparent" value="{{$contract_revised_amount}}" dir="rtl" readonly>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-4">
                                            <label class="mt-3">Previous Payment:</label>
                                        </div>

                                        <div class="col-4">
                                            <input type="text" class="form-control previous_payment_amount number_formated_two border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0 bg-transparent" dir="rtl" value="{{$previous_payment_amount}}" readonly>
                                        </div>

                                        <div class="col-4">
                                            <input type="text" class="form-control previous_payment_amount_pkr number_formated_two border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0 bg-transparent" value="{{$previous_payment_amount_pkr}}" dir="rtl" readonly>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-4   ">
                                            <label class="mt-3">Balance Payable:</label>
                                        </div>
                                        <div class="col-4">
                                            <input type="text" class="form-control balance_payable_amount number_formated_two border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0 bg-transparent" value="{{$balance_payable_amount}}" dir="rtl" readonly>
                                        </div>
                                        <div class="col-4">
                                            <input type="text" class="form-control balance_payable_amount_pkr number_formated_two border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0 bg-transparent" value="{{$balance_payable_amount_pkr}}" dir="rtl" readonly>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-3">
                                            <label class="mt-3">Last Pmt Amount:</label>
                                        </div>
                                        <div class="col-3">
                                            <input type="text" class="form-control last_payment_amount border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0 bg-transparent" value="{{ number_format($last_payment_amount, 6) }}" title="{{ number_format($last_payment_amount, 6) }}" dir="rtl" readonly>
                                        </div>

                                        <div class="col-3">
                                            <label class="mt-3">Last Pmt Date:</label>
                                        </div>
                                        <div class="col-3">
                                            <input type="text" class="form-control last_payment_date border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0 bg-transparent text-right" value="{{ $last_payment_date }}" readonly>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-3">
                                            <label class="mt-3">Remaining Balance:</label>
                                        </div>
                                        <div class="col-3">
                                            <input type="text" class="form-control remaining_balance number_formated_two border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0 bg-transparent" dir="rtl" data-toggle="tooltip" title="Remaining balance after current total of charges" value="0" readonly>
                                        </div>

                                        <div class="col-3">
                                            <label class="mt-3">Contract Currency:</label>
                                        </div>

                                        <div class="col-3">
                                            <input type="text" class="form-control contract_currency text-center border-left-0 border-top-0 border-right-0 border-bottom border-dark rounded-0 bg-transparent" value="{{$contract_currency}}" readonly>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="col-md-7 col-12">
                                <fieldset class="border p-2">
                                    <legend class="w-auto px-2 text-muted font-weight-bold" style="font-size: 19px;">Invoice Details</legend>
                                    <div class="form-row">
                                        <div class="col-md-2 form-group">
                                            <label>Currency:<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control contract_change_currency_from" style="background-color: #e9ecef" placeholder="(Empty)" value="{{ $payment->invoiceCurrency->currency_code }}" readonly>
                                            <input type="hidden" value="{{ $payment->invoiceCurrency->id }}" name="invoice_currency">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Invoice Amount:<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control numberJs text-right invoice_amount" placeholder="Empty" name="invoice_amount" value="{{ $payment->invoice_amount }}" required>
                                        </div>
                                        <div class="col-2 form-group">
                                            <label>Invoice Date:<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control invoice_date" name="invoice_date" placeholder="d/m/y" value="{{ \Carbon\Carbon::parse($payment->invoice_date)->format('d-M-Y') ?? '' }}" required>
                                        </div>
                                        <div class="col-2 form-group">
                                            <label>Invoice No:<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" name="invoice_no" placeholder="xxxx" value="{{ $payment->invoice_no }}" required>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6 form-group">
                                            <label>Less Retention <small>(If Applicable)</small>:</label>
                                            <input type="text" class="form-control invoice_less_retention" oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');" dir="rtl" name="invoice_less_retention" placeholder="%" value="{{ $payment->invoice_less_retention }}" readonly>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Less Retention Amount:</label>
                                            <input type="text" class="form-control invoice_less_retention_amount" dir="rtl" name="invoice_less_retention_amount" placeholder="0" value="{{ $payment->invoice_less_retention_amount }}" readonly>
                                        </div>
                                        <div class="col-md-8">
                                            <label>Withdraw Application / Payment Request No:<span class="text-danger">*</span></label>
                                                <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="payment_request_no">
                                                        {{ App\Helpers\Helper::addOrdinalNumberSuffix($payment->payment_count_no) }}
                                                    </span>
                                                    <input type="hidden" name="payment_count_no" id="invoice_withdraw_application_prefix">
                                                </div>
                                                <input type="text" class="form-control" name="invoice_withdraw_application" id="payment_request_no_prefix" placeholder="xx/xx/xxxx" value="{{$payment->invoice_withdraw_application}}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Fee:<span class="text-danger">*</span></label>
                                            <div class="input-group mb-3">
                                                <input type="number" class="form-control text-right" name="invoice_withdraw_application" placeholder="0" maxlength="5" onkeyup="this.value = fnc(this.value, 0, 100)" value="{{ $payment->invoice_withdraw_application ?? 0 }}">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6 form-group">
                                            <label>Performance Security Validity:<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control invoice_performance_security_validity" name="invoice_performance_security_validity" placeholder="d/m/y" value="{{ \Carbon\Carbon::parse($payment->invoice_performance_security_validity)->format('d-M-Y') ?? ''}}">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Net Payable Amoun:<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control invoice_net_payable" name="invoice_net_payableinvoice_net_payable" readonly dir="rtl" placeholder="0" value="{{ $payment->invoice_amount-$payment->invoice_less_retention_amount }}">
                                        </div>
                                        <div class="col-12 form-group">
                                            <label>Invoice Description:</label>
                                            <textarea class="form-control" name="invoice_description" rows="4">{{ $payment->invoice_description }}</textarea>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div style="display: none" class="table_supporting_documents_message">
                                        <div class="alert alert-success alert-block message">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            <strong>File Successfully Add</strong>
                                        </div>
                                    </div>
                                    <div class="progress table_supporting_documents_progress" style="display: none">
                                        <div class="progress-bar table_supporting_documents_progress_bar"></div>
                                    </div>
                                    
                                    <div class="clearfix text-right">
                                        <button type="button" class=" btn btn-primary btn-sm add_supporting_documents mb-2">
                                            <i class="fas fa-plus fa-sm mr-1"></i>
                                            Add File
                                        </button>
                                        <button type="button" class=" btn btn-default btn-sm delete_supporting_documents mb-2">
                                            <i class="fas fa-trash-alt mr-1"></i> Delete Row
                                        </button>

                                    </div>

                                    <div class="table-wrapper-scroll-y my-custom-scrollbar" style="max-height: 20vh">
                                        <table class="table_supporting_documents table-bordered table-hover w-100">
                                            <thead>
                                                <tr>
                                                    <th class="p-2 text-white font-weight-light" colspan="5" style="background: #222222 !important;">
                                                        Supporting Document(s)
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th style="width:5% !important;"></th>
                                                    <th style="width:35% !important;">File</th>
                                                    <th style="width:30% !important;">Category</th>
                                                    <th style="width: 30% !important;">Description</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody align="center">
                                                @foreach($payment->PaymentSupportingDocuments as $file)
                                                    <tr>
                                                        <td>
                                                            <input data-id="{{$file->id}}" type='checkbox' name='record'>
                                                        </td>

                                                        <td>
                                                            @if( !empty($file->file))
                                                                <a href="{{ URL::to( '/files/payment/supporting/' . $file->file) }}" target="_blank" download="{{$file->file_name}}" title="{{$file->file_name}}"> 
                                                                    <i class="fas fa-cloud-download-alt mr-1"></i>
                                                                    Download File
                                                                </a>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            {{ 
                                                                $file->category == 9 ? $file->other_category : config('constants.supporting_documents')[$file->category] 
                                                            }}
                                                        </td>
                                                        <td style="text-align:left">
                                                            <p>{{ $file->description }}</p>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </div>
                            </div>  
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            {{--End create form--}}
            {{--Satrt Sub Category form--}}
            <div class="col-12">
                <div class="card card-outline card-primary shadow rounded-0 mb-4">
                    <!-- /.card-header -->
                    <div class="card-header">
                        <h3 class="card-title font-weight-bold">
                            <i class="fas fa-file-invoice mr-2"></i> Beneficiary Details
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>

                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-md-3 form-group">
                                <label>Bank Name:<span class="text-danger">*</span></label>
                                <select data-live-search="true" class="form-control select-state beneficiary_account_no"
                                        id="beneficiary_account_no" name="beneficiary_account_id" required>
                                    <option value="">Select Account</option>
                                    @foreach($contractor_financial_details as $id => $name)
                                        <option value="{{$id}}" {{$payment->ContractorFinancialDetails->id==$id?'selected':null}}>{{$name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-3 form-group">
                                <label>Account No:</label>
                                <input type="text" class="form-control beneficiary_bank_name"
                                       name="beneficiary_bank_name" value="{{$payment->ContractorFinancialDetails->account_no }}" readonly>
                            </div>
                            <div class="col-md-3 form-group">
                                <label>Branch Name:</label>
                                <input type="text" class="form-control beneficiary_branch_name"
                                       name="beneficiary_branch_name" value="{{$payment->ContractorFinancialDetails->branch_name }}" readonly>
                            </div>

                            <div class="col-md-3 form-group">
                                <label>IBAN:</label>
                                <input type="text" class="form-control beneficiary_iban"
                                       name="beneficiary_iban" value="{{$payment->ContractorFinancialDetails->iban }}" readonly>
                            </div>
                            <div class="col-md-3 form-group">
                                <label>SWIFT Code:</label>
                                <input type="text" class="form-control beneficiary_swift_code"
                                       name="beneficiary_swift_code" value="{{$payment->ContractorFinancialDetails->swift_code }}" readonly>
                            </div>

                            <div class="col-md-3 form-group">
                                <label>City:</label>
                                <input type="text" class="form-control beneficiary_city"
                                       name="beneficiary_city" value="{{$payment->ContractorFinancialDetails->hasCity->city_name }}" readonly>
                            </div>
                            <div class="col-md-6 form-group">
                                <label>Address:</label>
                                <input type="text" class="form-control beneficiary_address"
                                       name="beneficiary_address" value="{{$payment->ContractorFinancialDetails->branch_address }}" readonly>
                            </div>

                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            {{--End Sub Category form--}}

            {{--Satrt Sub Category form--}}
            <div class="col-12">
                <div class="card card-outline card-primary shadow rounded-0 mb-4">
                    <!-- /.card-header -->
                    <div class="card-header">
                        <h3 class="card-title font-weight-bold">
                            <i class="fas fa-file-invoice-dollar mr-2"></i> 
                            Project and Measures / Lines Items to be Charged
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <div class="card-body">
                        <div class="text-right mb-2">
                            <button type="button" value="Add Row" id="add_lines_items_to_be_charged_edit" class="no-print btn btn-primary btn-sm rounded-0 add_lines_items_to_be_charged_edit">
                                <li class="fa fa-plus mr-1"></li>
                                Add New
                            </button>

                            <button type="button" id="delete_table_lines_items_to_be_charged_edit" class="no-print btn btn-default btn-sm rounded-0 delete_table_lines_items_to_be_charged_edit" hidden>
                                <i class="fas fa-trash-alt mr-1"></i> Delete Row
                            </button>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-hover table-bordered table-sm table_lines_items_to_be_charged_edit" style="width: 250%" id="table_lines_items_to_be_charged_edit">
                                {{-- TABLE HEADER --}}
                                <thead align="center" class="thead-light">
                                        <th align="center" valign="center" width="1%">
                                            <input type="checkbox" onclick="$(`.table_lines_items_to_be_charged_edit tbody input[name='record']`).not(':disabled').prop('checked', $(this).prop('checked'))" class="select_all_checkboxes">
                                        </th>
                                        <th scope="col" width="10%">Project</th>
                                        <th scope="col" width="10%">Measure</th>
                                        <th scope="col" width="10%">Sub-Category</th>
                                        <th scope="col" width="5%">Province</th>
                                        <th scope="col" width="10%">PEA</th>
                                        <th scope="col" width="5%">City</th>
                                        <th scope="col" width="5%">Facility</th>
                                        <th scope="col" width="15%" style="color: white;background-color:  #17a2b8; vertical-align: middle">Amount<span class="contract_change_currency_from_text">({{$contract_currency}})</span>
                                        </th>
                                        <th scope="col" width="15%" style="color:white;background-color: #007bff; vertical-align: middle">
                                            Amount<span class="contract_change_currency">({{$currency_name}})</span>
                                        </th>

                                        <th scope="col" style="width: 10%; vertical-align: middle">Description</th>
                                </thead>
                                {{-- TABLE BODY --}}
                                <tbody>
                                    @foreach($payment->hasPaymentsHasSub as $pay)
                                        <tr class="table-active">
                                            <td style="vertical-align: middle;">
                                                <input type='hidden'  name='checkFlag' class='checkFlag' value='1' />
                                                <input name='record' id='check_all' type='checkbox' disabled>
                                            </td>
                                            <td>
                                                <select class='form-control payment_project_id' id='payment_project_id' disabled>
                                                     @if(!is_null($payment->contract_id))
                                                        @foreach(\App\Helpers\Helper::GetContractProjects($payment->contract_id) as $sub )
                                                            <option
                                                                    value="{{$sub['id']}}" {{$pay->project_id   ==$sub['id']?'selected':''}}>
                                                                {{$sub['title']}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </td>
                                            <td>
                                                <select class='form-control payment_project_measure' id='payment_project_measure' disabled>
                                                    <option value=''>Select</option>
                                                    @if(!is_null($pay->project_id)|| !is_null($pay->contract_id))
                                                        @foreach(\App\Helpers\Helper::GetContractProjectsMeasure($pay->contract_id,$pay->project_id) as $index=>$sub )
                                                            <option
                                                                    value="{{$sub['id']}}" {{$pay->measure_id==$sub['id']?'selected':''}}>
                                                                {{$sub['name']}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </td>
                                            <td>
                                                <select class='form-control payment_project_measure_sub_cate' id='payment_project_measure_sub_cate' disabled>
                                                    <option value=''>Select</option>
                                                    @if(!is_null($pay->project_id)|| !is_null($pay->contract_id) || !is_null($pay->measure_id))
                                                        @foreach(\App\Helpers\Helper::GetContractProjectsMeasureSubCategory($pay->contract_id,$pay->project_id,$pay->measure_id) as $index=>$sub )
                                                            <option
                                                                    value="{{$sub['id']}}" {{$pay->sub_category_id==$sub['id']?'selected':''}}>
                                                                {{$sub['name']}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </td>
                                            <td>
                                                <input type='text' class=' form-control payment_project_province_name' id='payment_project_province_name' value="{{$pay->hasProvince->name}}" disabled>
                                                <input type='hidden' class='payment_project_province_id' id='payment_project_province_id' value="{{$pay->hasProvince->id}}">
                                            </td>
                                            <td>
                                                <input disabled type='text' class='form-control payment_project_sub_cate_peas_name form-control' id='payment_project_sub_cate_peas_name' value="{{$pay->hasPeas->pea_name}}">
                                                <input disabled type='hidden' class='form-control payment_project_sub_cate_peas_id form-control' id='payment_project_sub_cate_peas_id' value="{{$pay->hasPeas->id}}">
                                            </td> 
                                            <td>
                                                <select id='payment_project_city_id' class='payment_project_city_id form-control' disabled>
                                                    @if(!is_null($pay->project_id)|| !is_null($pay->contract_id) || !is_null($pay->measure_id))
                                                        @foreach(\App\Helpers\Helper::GetContractProjectsMeasureSubCategoryCity($pay->contract_id,$pay->project_id,$pay->measure_id) as $city)
                                                            <option value="{{$city['id']}}" {{$pay->city_id==$city['id']?'selected':''}}>
                                                                {{$city['city_name']}}
                                                            </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </td> 
                                            <td>
                                                <select id='payment_project_facility_id' class='payment_project_facility_id form-control' disabled>
                                                    @if(!is_null($pay->facility))
                                                        <option value="{{ $pay->facility->id ?? null }}">{{ $pay->facility->facility_name ?? ''}}</option>
                                                    @else
                                                        <option value="" selected disabled>No Facility Added</option>
                                                    @endif
                                                </select>
                                            </td> 
                                            <td>
                                                <input type='text' class='payment_project_amount number_formated_two form-control' dir='rtl' style='width:100% !important;text-align:right;' value="{{$pay->budget}}" disabled>
                                            </td> 
                                                <td><input type='text' class='payment_project_amount_pkr number_formated_two form-control'  dir='rtl' style='width:100% !important;text-align:right;' value="{{$pay->budget_pkr}}" disabled>
                                            </td>
                                            <td>
                                                <input type='text' name='payment_project_description[]' class='form-control payment_project_description' value="{{$pay->description}}">
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>   
                                {{-- TABLE FOOTER --}}
                                <tfoot>
                                    <tr>
                                        <td colspan="7"></td>
                                        <td colspan="" style="text-align: right;"><b>Total</b></td>
                                        <td><input type="text"
                                                   class="form-control payment_total_amount number_formated_two"
                                                   style=" text-align:right"
                                                   readonly=""></td>
                                        <td><input type="text"
                                                   class="form-control payment_total_amount_pkr number_formated_two"
                                                   style=" text-align:right"
                                                   readonly=""></td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            {{--End Sub Category form--}}

            {{--Satrt Sub Category form--}}
            <div class="col-12">
                <div class="card card-outline card-primary shadow rounded-0 mb-4">
                    <!-- /.card-header -->
                    <div class="card-header">
                        <h3 class="card-title font-weight-bold">
                            <i class="fas fa-money-check-alt mr-2"></i>  Payment Details
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->

                    </div>
                    <div class="card-body" style="display: block;">

                        <div class="row">
                            <div class="col-5">
                                <div class="form-row">
                                    <div class="col-6 form-group">
                                        <label>Payment Amount(<span class="contract_change_currency_from_text">{{$contract_currency}}</span>):<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control numberJs text-right" name="payment_details_amount" value="{{$payment->payment_details_amount}}" required>
                                    </div>

                                    <div class="col-6 form-group">
                                        <label>Payment Date:<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control payment_details_date" name="payment_details_date" value="{{$payment->payment_details_date?\Carbon\Carbon::parse($payment->payment_details_date)->format('d-M-Y'):''}}" required>
                                    </div>

                                    <div class="col-6 form-group">
                                        <label>Payment Currency:<span class="text-danger">*</span></label>
                                        <select class="form-control" name="payment_details_currency" required>
                                            <option>Select Currency</option>
                                            @foreach($currency as $curr)
                                                <option value="{{$curr->id}}" {{$payment->payment_details_currency==$curr->id?'selected':''}}>{{$curr->currency_code}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-6 form-group">
                                        <label>Purpose Code:<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="payment_details_code" value="{{$payment->payment_details_code}}" required>
                                    </div>

                                    <div class="col-12 form-group">
                                        <label>Remarks:</label>
                                        <textarea class="form-control" name="payment_details_remarks" rows="5">{{$payment->payment_details_remarks}}</textarea>
                                    </div>
                                </div>  
                            </div>

                            <div class="col-7">
                                <div style="display: none" class="table_supporting_documents_message_detail">
                                    <div class="alert alert-success alert-block message">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>File Successfully Add</strong>
                                    </div>
                                </div>
                                <div class="progress table_supporting_documents_progress_detail" style="display: none">
                                    <div class="progress-bar table_supporting_documents_progress_bar_detail"></div>
                                </div>
                                <div class="text-right mb-2">
                                    <button type="button" class="no-print btn btn-primary btn-sm rounded-0 add_payment_supporting_documents">
                                        <li class="fa fa-plus mr-1"></li>
                                        Add New
                                    </button>

                                    <button type="button" class="no-print btn btn-default btn-sm rounded-0 delete_payment_supporting_documents">
                                        <i class="fas fa-trash-alt mr-1"></i> Delete Row
                                    </button>
                                </div>  

                                <div class="table table-sm table-wrapper-scroll-y my-custom-scrollbar">
                                    <table class="table_payment_supporting_documents table-bordered" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th colspan="5" class="p-2 text-white font-weight-light" style="background: #222222 !important;">
                                                    Payment Supporting Documents
                                                </th>
                                            </tr>
                                            <tr>
                                                <th style="width:5% !important;"></th>
                                                <th style="width:30% !important;">File</th>
                                                <th style="width:30% !important;">Category</th>
                                                <th style="width: 30% !important;">Description</th>
                                                <th style="width:5% !important;"></th>
                                            </tr>
                                        </thead>
                                        <tbody align="center">
                                            @foreach($payment->PaymentSupportingFileDetial as $file)
                                                <tr>
                                                    <td style="text-align: center">
                                                        <input data-id="{{$file->id}}" type='checkbox' name='record'>
                                                    </td>
                                                    <td>
                                                        @if( !empty($file->file))
                                                            <a href="{{ URL::to( '/files/payment/supporting/detail/' . $file->file)  }}"
                                                               target="_blank"
                                                               download="{{$file->file_name}}" 
                                                               title="{{$file->file_name}}">
                                                                <i class="fas fa-cloud-download-alt mr-1"></i> Download File
                                                            </a>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{ 
                                                            $file->category == 9 ? $file->other_category : config('constants.payment_supporting_documents')[$file->category] 
                                                        }}
                                                    </td>
                                                    <td style="text-align:left"><p>{{$file->description?$file->description:''}}</p></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            <!-- /.card-body -->
            </div>
            
            <!-- /.card -->
            <div class="col-12 clearfix form-group">
                <button id="cancelall" type="button" class="btn btn-light active rounded-0"><i class="fas fa-times text-danger mr-1"></i> Cancel</button>
                <button type="submit" class="btn btn-success rounded-0 check-for-total"><i class="fas fa-cloud-upload-alt mr-1"></i> | Update Changes</button>
            </div>
        </div>  
    </form>

    {{--End Buttons--}}
    @push('javascript')
    @endpush
@endsection