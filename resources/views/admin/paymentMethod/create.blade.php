<div class="modal fade" id="payment_method_modal">
    <form action="{{route('paymentMethod.store') }}" method="post">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style=" background-color: #65a3c6;
color: #2c2c2c;">
                    <h4 class="modal-title">Payment Method Form</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Sort Order&nbsp;</label>
                        <input type="text"

                               oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"
                               placeholder="Sort Order" name="sort_order" class="form-control sort_order @error('sort_order') is-invalid @enderror" value="{{ old('sort_order') }}">
                        @error('sort_order') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group">
                        <label>Payment Method Name&nbsp;</label>
                        <input type="text" placeholder="Payment Method Name" name="payment_method_name" class="form-control payment_method_name @error('payment_method_name') is-invalid @enderror" value="{{ old('payment_method_name') }}">
                        @error('payment_method_name') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group">
                        <label>Short Code&nbsp;</label>
                        <input type="text" placeholder="Short Code" name="short_code" class="form-control short_code @error('short_code') is-invalid @enderror" value="{{ old('short_code') }}">
                          @error('short_code') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group">
                        <label>Payment Method Currency &nbsp;</label>
                        <select name="currency_id" class="form-control @error('currency_id') is-invalid @enderror" id="currency_id" value="{{ old('currency_id') }}">
                            <option value="">Choose an Option</option>
                            @foreach(\App\Helpers\Helper::currency() as $currency)
                                <option value="{{$currency->id}}">{{$currency->currency_name}}</option>
                            @endforeach
                        </select>
                        @error('currency_id') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group">
                        <label>Exchange Rate &nbsp;</label>
                        <select name="exchange_rate" class="form-control @error('exchange_rate') is-invalid @enderror" id="exchange_rate" value="{{ old('exchange_rate') }}">
                            <option value="">Choose an Option</option>
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                        </select>
                        @error('exchange_rate') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">cancel</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </form>
    <!-- /.modal-dialog -->
</div>
