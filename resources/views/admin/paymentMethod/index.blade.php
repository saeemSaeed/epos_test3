@extends('admin.layout.index')
@section('content')
<section class="bg-primary content-header"
	style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;">
	<div class="row">
		<div class="col-8">
			<h4>Payment Method</h4>
		</div>
		<div class="col-4">
			<ol class="breadcrumb" style="color:#444;float: right">
				<li>
					<i class="fa fa-dashboard"></i> Dashboard &nbsp;
				</li>
				<li>
					<i class="fa fa-angle-right" style="color: #ccc;"></i> Payment Method &nbsp;
				</li>

			</ol>
		</div>
	</div>
</section>
<div class="container">
  <div class="row justify-content-between">
	<div style="margin-top: 10px; margin-left: 10px">
		<button type="button" class="btn btn-block btn-success btn-flat" data-toggle="modal" data-target="#payment_method_modal">
		<i class="fa fa-plus"></i> Add Payment Method
		</button>
	</div>
@can('payment-method-restore')
<div style="margin-top: 10px; margin-right: 10px">
        <a href="{{ route('paymentMethod.Deleted') }}" class="btn btn-block btn-danger btn-flat"><i class="fas fa-trash-alt"></i></a>
    </div>
    @endcan
</div>
</div>
<div class="container-fluid" style="margin-top: 10px">
	<div class="card">
		<div class="card-body">
			<div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<div class="col-sm-12 col-md-6"></div>
					<div class="col-sm-12 col-md-6"></div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<table id="paymentMethod" class="table table-bordered table-hover paymentMethod" role="grid" aria-describedby="example2_info">
							<thead style="    background-color: #65a3c6;
    color: #2c2c2c;">
								<tr role="row">
{{--									<th>ID</th>--}}
									<th style=";width: 10%">Sort Order</th>
									<th>Payment Method Name</th>
									<th>Short Code</th>
									<th>Payment Method Currency</th>
									<th>Exchange Rate Apply</th>
									<th>Action</th>

								</tr>
							</thead>
							<tbody>
							@foreach($paymentMethods as $index=>$payment)
								<tr>
{{--									<td style='text-align: center'>{{++$index}}</td>--}}
									<td style="text-align: center">{{$payment->sort_order}}</td>
									<td>{{$payment->payment_method_name}}</td>
									<td>{{isset($payment->short_code)? $payment->short_code:''}}</td>
									<td>{{isset($payment->hasCurrency->currency_name)? $payment->hasCurrency->currency_name:''}}</td>
									<td>{{$payment->exchange_rate ? 'Yes':'No'}}</td>

									<td data-id="{{$payment->id}}" style="text-align: center">
										<a data-id="{{$payment->id}}"
										   class="payment_method_modal_edit"
										   data-toggle="modal"
										   data-target="#payment_method_modal_edit"
										   title="Edit!"><i style="color: black;font-size: 14px!important"
															class="fas fa-pencil-alt"></i></a>&nbsp;&nbsp;
										<a


												onclick="return confirm('Are you sure?')"
												data-id="{{$payment->id}}"
												href="{{route('paymentMethod.delete',['id'=>$payment->id])}}"
												data-toggle="tooltip"
												title="Remove"><i style="color: black;font-size: 14px!important"
																  class=" fa fa-trash"></i></a></td>
								</tr>
							@endforeach


							</tbody>

						</table>
					</div>
				</div>

			</div>
		</div>
		<!-- /.card-body -->
	</div>
</div>
@include('admin.paymentMethod.create')
@include('admin.paymentMethod.edit')
@endsection
