<div class="modal fade" id="payment_method_modal_edit">
    <form method="post">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="    background-color: #65a3c6;
                    color: #2c2c2c;">
                    <h4 class="modal-title">Payment Method Form</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul>
                        </ul>
                    </div>
                    <div class="form-group">
                        <label>Sort Order&nbsp;</label>
                        <input type="text"
                               oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"
                               placeholder="Sort Order" name="sort_order" class="form-control sort_order" id="sort_order">

{{--                        <input type="hidden" placeholder="Payment Method Name" name="payment_method_id" class="form-control payment_method_id">--}}
                    </div>
                    <div class="form-group">
                        <label>Payment Method Name&nbsp;</label>
                        <input type="text" placeholder="Payment Method Name" name="payment_method_name" class="form-control payment_method_name" id="edit_payment_method_name">

                        <input type="hidden" placeholder="Payment Method Name" name="payment_method_id" class="form-control payment_method_id">
                    </div>
                    <div class="form-group">
                        <label>Short Code&nbsp;</label>
                        <input type="text" placeholder="Short Code" name="short_code" class="form-control short_code" id="edit_short_code">
                    </div>
                    <div class="form-group">
                        <label>Payment Method Currency &nbsp;</label>
                        <select name="currency_id" class="form-control currency_id"  id="edit_currency_id" >
                            <option value="">Choose an Option</option>
                            @foreach(\App\Helpers\Helper::currency() as $currency)
                                <option value="{{$currency->id}}">{{$currency->currency_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Exchange Rate &nbsp;</label>
                        <select name="exchange_rate" class="form-control exchange_rate" id="edit_exchange_rate" >
                            <option value="">Choose an Option</option>
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary edit_payment_method_save_modal">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">cancel</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </form>
    <!-- /.modal-dialog -->
</div>
