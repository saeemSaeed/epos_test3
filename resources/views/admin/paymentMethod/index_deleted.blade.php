,@extends('admin.layout.index')
@section('content')
<section class="bg-primary content-header"
	style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;">
	<div class="row">
		<div class="col-8">
			<h4>Payment Method</h4>
		</div>
		<div class="col-4">
			<ol class="breadcrumb" style="color:#444;float: right">
				<li>
					<i class="fa fa-dashboard"></i> Dashboard &nbsp;
				</li>
				<li>
					<i class="fa fa-angle-right" style="color: #ccc;"></i> Payment Method &nbsp;
				</li>

			</ol>
		</div>
	</div>
</section>
<div class="container-fluid" style="margin-top: 10px">
	<div class="card">
		<div class="card-body">
			<div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<div class="col-sm-12 col-md-6"></div>
					<div class="col-sm-12 col-md-6"></div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<table id="paymentMethod" class="table table-bordered table-hover paymentMethod" role="grid" aria-describedby="example2_info">
							<thead style="    background-color: #65a3c6;
								color: #2c2c2c;">
								<tr role="row">
{{--									<th>ID</th>--}}
									<th style=";width: 10%">Sort Order</th>
									<th>Payment Method Name</th>
									<th>Short Code</th>
									<th>Payment Method Currency</th>
									<th>Exchange Rate Apply</th>
									<th>Action</th>

								</tr>
							</thead>
							<tbody>
								@foreach($deletedPayment as $index=>$payment)
								<tr>
{{--									<td style='text-align: center'>{{++$index}}</td>--}}
									<td style="text-align: center">{{$payment->sort_order}}</td>
									<td>{{$payment->payment_method_name}}</td>
									<td>{{isset($payment->short_code)? $payment->short_code:''}}</td>
									<td>{{isset($payment->hasCurrency->currency_name)? $payment->hasCurrency->currency_name:''}}</td>
									<td>{{$payment->exchange_rate ? 'Yes':'No'}}</td>
									<td class="sorting_1 dtr-control" style="text-align: center!important;">
										<a type="submit" style="border: none;background-color: transparent" data-toggle="tooltip" title="" href="{{route('paymentMethod.restore',['id'=>$payment->id])}}" data-original-title="Restore"><i class="fas fa-trash-restore"></i></a>
									</td>
								</tr>
								@endforeach


							</tbody>

						</table>
					</div>
				</div>

			</div>
		</div>
		<!-- /.card-body -->
	</div>
</div>
@include('admin.paymentMethod.create')
@include('admin.paymentMethod.edit')
@endsection
