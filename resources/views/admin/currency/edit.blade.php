<div class="modal fade" id="currency_modal_edit">
  <form method="post">
    @csrf
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #65a3c6;color: #2c2c2c">
          <h4 class="modal-title">Currency Form</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger print-error-msg" style="display:none">
            <ul>
            </ul>
          </div>
          <div class="form-group">
              <div class="form-group">
                  <label>Sort Order&nbsp;</label>
                  <input

                      oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"
                      type="text" placeholder="Sort Order" name="sort_order" class="form-control sort_order

            @error('sort_order') is-invalid @enderror" id="sort_order">
                  @error('sort_order') <div class="invalid-feedback">{{ $message }}</div> @enderror
              </div>
            <label>Currency Name&nbsp;</label>
            <input type="text" placeholder="Currency Name" name="currency_name" class="form-control currency_name @error('currency_name') is-invalid @enderror">
            @error('currency_name') <div class="invalid-feedback">{{ $message }}</div> @enderror
            <input type="hidden" placeholder="Currency Name" name="currency_id" class="form-control currency_id">
          </div>
          <div class="form-group">
            <label>Currency Code&nbsp;</label>
            <input type="text" placeholder="Currency Code" name="currency_code" class="form-control currency_code @error('currency_code') is-invalid @enderror">
            @error('currency_code') <div class="invalid-feedback">{{ $message }}</div> @enderror
          </div>
          <div class="form-group">
            <label>Currency Symbols&nbsp;</label>
            <select class="form-control currency_symbol @error('currency_symbol') is-invalid @enderror" name="currency_symbol" id="currency_symbol">
              <option value="$">$</option>
              <option value="€">€</option>
              <option value="PKR">PKR</option>
              <option value="AED">AED</option>
              <option value="$">CAD</option>
              <option value="元">元</option>
            </select>
            @error('currency_symbol') <div class="invalid-feedback">{{ $message }}</div> @enderror
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary edit_currency_save_modal">Save</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">cancel</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
  </form>
  <!-- /.modal-dialog -->
</div>
