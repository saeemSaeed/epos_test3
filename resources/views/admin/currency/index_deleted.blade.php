@extends('admin.layout.index')
@section('content')
<section class="bg-primary content-header"
    style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
    <div class="row">
        <div class="col-8">
            <h1>Currency</h1>
        </div>
        <div class="col-4">
            <ol class="breadcrumb" style="color:#444;float: right">
                <li>
                    <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                </li>
                <li>
                    <i class="fa fa-angle-right" style="color: #ccc;"></i> Currency &nbsp;
                </li>
            </ol>
        </div>
    </div>
</section>

<div class="container" style="margin-top: 10px">
    <div class="card">
        <div class="card-body">
            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="currency" class="table table-bordered table-hover currency" role="grid"
                            aria-describedby="example2_info">
                            <thead style="background-color: #65a3c6;color: #2c2c2c">
                                <tr role="row">
{{--                                    <th style="width: 5%">ID</th>--}}
                                    <th style="width: 10%">Sort Order</th>
                                    <th>Currency Name</th>
                                    <th>Currency Code</th>
                                    <th>Symbol</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($deletedCurrency))
                                @foreach($deletedCurrency as $index=>$curr)
                                <tr>
                                    <td style="text-align: center">{{++$index}}</td>
                                    <td style="text-align: center">{{$curr->sort_order}}</td>

                                    <td>{{$curr->currency_name}}</td>
                                    <td>{{$curr->currency_code}}</td>
                                    <td>{{$curr->currency_symbol}}</td>
                                    <td class="sorting_1 dtr-control" style="text-align: center!important;">
                                        <a type="submit" style="border: none;background-color: transparent" data-toggle="tooltip" title="" href="{{route('currency.restore',['id'=>$curr->id])}}" data-original-title="Restore"><i class="fas fa-trash-restore"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</div>
@include('admin.currency.create')
@include('admin.currency.edit')
@endsection
