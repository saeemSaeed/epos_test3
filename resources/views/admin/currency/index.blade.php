@extends('admin.layout.index')
@section('content')
    <section class="bg-primary content-header"
             style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Currency</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li>
                        <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li>
                        <i class="fa fa-angle-right" style="color: #ccc;"></i> Currency &nbsp;
                    </li>

                </ol>
            </div>
        </div>
    </section>
    <div class="container">
  <div class="row justify-content-between">

    <div style="margin-top: 10px; margin-left: 10px">
        @can('currency-create')
        <button type="button" class="btn btn-block btn-success btn-flat" data-toggle="modal"
        data-target="#currency_modal">
        <i class="fa fa-plus"></i> Add Currency
        </button>
        @endcan
    </div>
    @can('currency-restore')
    <div style="margin-top: 10px; margin-right: 10px">
        <a href="{{ route('currency.Deleted') }}" class="btn btn-block btn-danger btn-flat"><i class="fas fa-trash-alt"></i></a>
    </div>
    @endcan
</div>
</div>
    <div class="container" style="margin-top: 10px">
        <div class="card">
            <div class="card-body">
                <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12 col-md-6"></div>
                        <div class="col-sm-12 col-md-6"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="currency" class="table table-bordered table-hover currency" role="grid"
                                   aria-describedby="example2_info">
                                <thead style="background-color: #65a3c6;color: #2c2c2c">
                                <tr role="row">
{{--                                    <th style="width: 5%">ID</th>--}}
                                    <th style="text-align: center;width: 10%">Sort Order</th>
                                    <th>Currency Name</th>
                                    <th>Currency Code</th>
                                    <th>Symbol</th>
                                    <th>Action</th>

                                </tr>
                                </thead>
                                <tbody>

                                @if(isset($currency))
                                    @foreach($currency as $index=>$curr)
                                        <tr>
{{--                                            <td style="text-align: center">{{++$index}}</td>--}}
                                            <td style="text-align: center">{{$curr->sort_order}}</td>
                                            <td>{{$curr->currency_name}}</td>
                                            <td>{{$curr->currency_code}}</td>
                                            <td>{{$curr->currency_symbol}}</td>

                                            <td data-id="{{$curr->id}}" style="text-align: center">
                                                @can('currency-edit')
                                                <a data-id="{{$curr->id}}"
                                                   class="getCurrency"
                                                   data-toggle="modal"
                                                   data-target="#currency_modal_edit"
                                                   title="edit!"><i style="color: black;font-size: 14px!important"
                                                                    class="fas fa-pencil-alt"></i></a>&nbsp;&nbsp;
                                                @endcan
                                                    @can('currency-delete')
                                                <a


                                                        onclick="return confirm('Are you sure?')"
                                                        data-id="{{$curr->id}}"
                                                        href="{{route('currency.delete',['id'=>$curr->id])}}"
                                                        data-toggle="tooltip"
                                                        title="Remove"><i style="color: black;font-size: 14px!important"
                                                                          class=" fa fa-trash"></i></a>
                                                    @endcan</td>
                                        </tr>

                                    @endforeach
                                @endif


                                </tbody>

                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
    @include('admin.currency.create')
    @include('admin.currency.edit')
@endsection
