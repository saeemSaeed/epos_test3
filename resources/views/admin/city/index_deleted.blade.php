@extends('admin.layout.index')
@section('content')
{{--<div class="row">--}}
    {{--<div class="col-12">--}}
        <section class="bg-primary content-header"
            style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
            <div class="row">
                <div class="col-8">
                    <h1 style="">City</h1>
                </div>
                <div class="col-4">
                    <ol class="breadcrumb" style="color:#444;float: right">
                        <li>
                            <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                        </li>
                        <li>
                            <i class="fa fa-angle-right" style="color: #ccc;"></i> City &nbsp;
                        </li>
                    </ol>
                </div>
            </div>
        </section>
    {{--</div>--}}
{{--</div>--}}

<div class="container-fluid" style="margin-top: 10px">
    <div class="card">
        <div class="card-body">
            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="city" class="table table-bordered data-table-city" role="grid"
                            aria-describedby="example2_info">
                            <thead style="background-color: #65a3c6;color: #2c2c2c">
                                <tr role="row">
{{--                                    <th>ID</th>--}}
                                    <th style=";width: 10%">Sort Order</th>
                                    <th>City Name</th>
                                    <th>City Location</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($deletedcity))
                                @foreach($deletedcity as $index=>$city)
                                <tr>
{{--                                    <td style="width:10%;text-align: center">{{++$index}}</td>--}}
                                    <td style="width:10%;text-align: center">{{$city->sort_order}}</td>
                                    <td>{{$city->city_name}}</td>
                                    <td>{{isset($city->hasProvince->name)?$city->hasProvince->name:''}}</td>
                                    <td class="sorting_1 dtr-control" style="text-align: center!important;">
                                        <a type="submit" style="border: none;background-color: transparent" data-toggle="tooltip" title="" href="{{route('city.restore',['id'=>$city->id])}}" data-original-title="Restore"><i class="fas fa-trash-restore"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</div>
@include('admin.city.create')
@include('admin.city.edit')
@endsection
