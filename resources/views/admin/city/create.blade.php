<div class="modal fade city_modal" id="city_modal">
  <form action="{{route('city.store') }}" method="post">
    @csrf

    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style=" background-color: #65a3c6;
color: #2c2c2c;">
          <h4 class="modal-title">City Form</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label>Sort Order&nbsp;</label>
                <input type="text"
                       oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"
                       placeholder="Sort Order" name="sort_order" class="form-control @error('sort_order') is-invalid @enderror" value="{{ old('sort_order')}}">
                @error('sort_order') <div class="invalid-feedback">{{ $message }}</div> @enderror
            </div>
          <div class="form-group">
            <label>City Name&nbsp;</label>
            <input type="text" placeholder="City Name" name="city_name" class="form-control @error('city_name') is-invalid @enderror" value="{{ old('city_name')}}">
            @error('city_name') <div class="invalid-feedback">{{ $message }}</div> @enderror
          </div>

          <div class="form-group">
            <label>City Code&nbsp;</label>
            <input type="text" placeholder="City Code" name="short_code" class="form-control @error('short_code') is-invalid @enderror" value="{{ old('short_code')}}">
            @error('short_code') <div class="invalid-feedback">{{ $message }}</div> @enderror
          </div>

          <div class="form-group">
            <label>City Province&nbsp;</label>
            <select name="province_id" class="form-control @error('province_id') is-invalid @enderror" id="province_id" value="{{ old('province_id') }}">
              <option value="">Choose an Option</option>
              @foreach(\App\Helpers\Helper::province() as $province)

                <option value="{{$province->id}}">{{$province->name}}</option>
              @endforeach

            </select>
            @error('province_id') <div class="invalid-feedback">{{ $message }}</div> @enderror
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">cancel</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
  </form>
  <!-- /.modal-dialog -->
</div>
