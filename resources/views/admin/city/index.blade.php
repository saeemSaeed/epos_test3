@extends('admin.layout.index')
@section('content')
{{--<div class="row">--}}
    {{--<div class="col-12">--}}
        <section class="bg-primary content-header"
            style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
            <div class="row">
                <div class="col-8">
                    <h1 style="">City</h1>
                </div>
                <div class="col-4">
                    <ol class="breadcrumb" style="color:#444;float: right">
                        <li>
                            <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                        </li>
                        <li>
                            <i class="fa fa-angle-right" style="color: #ccc;"></i> City &nbsp;
                        </li>
                    </ol>
                </div>
            </div>
        </section>
    {{--</div>--}}
{{--</div>--}}
<div class="container">
  <div class="row justify-content-between">
    <div style="margin-top: 10px; margin-left: 10px">
        @can('city-create')
        <button type="button" class="btn btn-block btn-success btn-flat" data-toggle="modal"
        data-target="#city_modal">
        <i class="fa fa-plus"></i> Add City
        </button>
        @endcan
    </div>
    @can('city-restore')
    <div style="margin-top: 10px; margin-right: 10px">
        <a href="{{ route('city.Deleted') }}" class="btn btn-block btn-danger btn-flat"><i class="fas fa-trash-alt"></i></a>
    </div>
    @endcan
</div>
</div>
<div class="container-fluid" style="margin-top: 10px">
    <div class="card">
        <div class="card-body">
            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="city" class="table table-bordered data-table-city" role="grid"
                            aria-describedby="example2_info">
                            <thead style="background-color: #65a3c6;color: #2c2c2c">
                                <tr role="row">
{{--                                    <th>ID</th>--}}
                                    <th style="width: 10%">Sort Order</th>
                                    <th>City Name</th>
                                    <th>City Code</th>
                                    <th>City Location</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($cities))
                                @foreach($cities as $index=>$city)
                                <tr>

{{--                                    <td style="width:10%;text-align: center">{{++$index}}   </td>--}}
                                    <td style="text-align: center">{{$city->sort_order}}</td>
                                    <td>{{$city->city_name}}</td>
                                    <td>{{$city->short_code}}</td>
                                    <td>{{isset($city->hasProvince->name)?$city->hasProvince->name:''}}</td>
                                    <td data-id="{{$city->id}}" style="text-align: center;width:15%">
                                        @can('city-edit')

                                        <a data-id="{{$city->id}}"
                                            class="getcity"
                                            data-toggle="modal"
                                            data-target="#default_edit_city"
                                            title="Edit!"><i style="color: black;font-size: 14px!important"
                                        class="fas fa-pencil-alt"></i></a>&nbsp;&nbsp;
                                        @endcan
                                        @can('city-delete')
                                        <a
                                            onclick="return confirm('Are you sure?')"
                                            data-id="{{$city->id}}"
                                            href="{{route('city.delete',['id'=>$city->id])}}"
                                            data-toggle="tooltip"
                                            title="Remove"><i
                                            style="color: black;font-size: 14px!important"
                                        class=" fa fa-trash"></i></a>
                                        @endcan
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</div>
@include('admin.city.create')
@include('admin.city.edit')
@endsection
