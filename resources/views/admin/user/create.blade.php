@extends('admin.layout.index')
@section('content')
    <section class="bg-primary content-header"
             style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Users</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float:right">
                    <li><i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li><i class="fa fa-angle-right" style="color: #ccc;"></i> Users &nbsp;</li>
                    <li class="active"><i class="fa fa-angle-right" style="color: #ccc;"></i> Create</li>
                </ol>
            </div>
        </div>
    </section>
    {{--Start create form--}}
    <form method="post" action="{{route('user.store')}}">
        @csrf
        <div class="col" style="margin-top: 10px">
            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title">Create New User</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row form-group">
                        <div class="col-2">
                            <label class="control-label">First Name</label>
                        </div>
                        <div class="col-10">
                            <input type="text" name="first_name" id="first_name" class="form-control " value="{{ old('first_name') }}">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-2">
                            <label class="control-label">Last name</label>
                        </div>
                        <div class="col-10"><input type="text" name="last_name" id="last_name" class="form-control" value="{{ old('last_name') }}"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-2">
                            <label class="control-label">Company</label>
                        </div>
                        <div class="col-10"><input type="text" name="company" id="company" class="form-control" value="{{ old('company') }}"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-2">
                            <label class="control-label">Email</label>
                        </div>
                        <div class="col-10"><input type="text" name="email" id="email" class="form-control" value="{{ old('email') }}"></div>
                    </div> <div class="row form-group">
                        <div class="col-2">
                            <label class="control-label">Phone</label>
                        </div>
                        <div class="col-10">
                            <input type="text" name="phone" id="phone" class="form-control phone_check" value="{{ old('phone') }}">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-2">
                            <label class="control-label">Role</label>
                        </div>
                        <div class="col-10">
                            <select name="roles" class="form-control" value="{{ old('roles') }}">
                                @foreach($roles as $role)
                                    <option value="{{$role}}">{{$role}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-2">
                            <label class="control-label">Password</label>
                        </div>
                        <div class="col-10"><input type="password" name="password" value="" id="password"
                                                   class="form-control"></div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <label class="control-label">Password confirm</label>
                        </div>
                        <div class="col-10"><input type="password" name="password_confirm" value="" id="password_confirm"
                                                   class="form-control"></div>
                    </div>
                    <br>
                    <div class="row form-group">
                        <div class="col-2"></div>
                        <div class="col-10">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-primary btn-flat">Submit</button>
                                    <button type="reset" class="btn btn-warning btn-flat">Reset</button>
                                    <a href="{{route('user.index')}}"
                                       class="btn btn-default btn-flat">Cancel</a></div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </form>
@endsection