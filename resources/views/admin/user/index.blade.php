@extends('admin.layout.index')
@section('content')
    <section class="bg-primary content-header" style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Users</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li>
                        <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li>
                        <i class="fa fa-angle-right" style="color: #ccc;"></i> Users
                    </li>
                    <li class="active">
                        <i class="fa fa-angle-right" style="color: #ccc;"></i> create
                    </li>
                </ol>
            </div>
        </div>
    </section>

    <div class="clearfix mt-3">
        @can('users-create')
            <a href="{{ route('user.create') }}" class="btn btn-success btn-flat float-left">
                <i class="fa fa-plus"></i> New User
            </a>
        @endcan
    </div>
    <div class="mt-3">
        <div class="card shadow">
            <div class="card-body">
                <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="user_table" class="table table-bordered table-hover dataTable dtr-inline"
                                   role="grid" aria-describedby="example2_info">
                                <thead>
                                <tr role="row" style="background: lightblue;color:black">
                                    <th>ID</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Groups</th>
                                    <th>Status</th>
                                    <th style="">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $index=>$user)
                                    <tr role="row" class="odd">
                                        <td style="text-align:center;">{{++$index}}</td>
                                        <td>{{$user->first_name}}</td>
                                        <td>{{$user->last_name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td align="right">{{$user->phone}}</td>

                                        <td align="center" valign="center">
                                            @if(($user->getRoleNames()->count() > 0))
                                                @foreach($user->getRoleNames() as $v)
                                                    <span class="badge badge-success badge-pill py-1 px-2">
                                                        <i class="fas fa-user-circle mr-1"></i> 
                                                        {{ ucfirst($v) }}
                                                    </span>
                                                @endforeach
                                            @else
                                                <span class="badge badge-danger badge-pill py-1 px-2">
                                                    <i class="fas fa-info-circle mr-1"></i> 
                                                    No Group Assigned
                                                </span>
                                            @endif
                                        </td>

                                        <td align="center" valign="center" data-toggle="tooltip" title="Click to update status">
                                            @if(!$user->status)
                                                <a href="{{route('user.status',['id'=>$user->id])}}"> 
                                                    <span class="badge badge-success badge-pill"><i class="fas fa-check-circle mr-1"></i> Active</span>
                                                </a>
                                            @else
                                                <a href="{{route('user.status',['id'=>$user->id])}}"> 
                                                    <span class="badge badge-danger badge-pill"><i class="fas fa-times-circle mr-1"></i> In-Active</span>
                                                </a>
                                            @endif
                                        </td>

                                        <td align="center" valign="center">
                                            @can('users-edit')
                                                <a href="{{ route('user.edit', $user->id) }}" data-toggle="tooltip" title="Edit">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                            @endcan

                                            @can('users-delete')
                                                <form style="display:inline;" method="DELETE"
                                                      action="{{route('user.delete',['id'=>$user->id])}}">
                                                    <a type="submit"
                                                       onclick="return confirm('Are you sure you want to delete?')"
                                                       data-toggle="tooltip" title=""
                                                       href="{{route('user.delete',['id'=>$user->id])}}"
                                                       data-original-title="Delete"><i
                                                                class="fas fa-trash-alt"></i></a></form>
                                            @endcan
                                            @can('user-show')
                                                <a class=" btn-mini" data-toggle="tooltip" title=""
                                                   href="{{route('user.profile',['id'=>$user->id])}}"
                                                   data-original-title="View"><i
                                                            class="fas fa-eye "></i></a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
@endsection