@extends('admin.layout.index')
@section('content')
    <style>
        .table td, .table th {
            padding: 0.35rem;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
        }
    </style>
    <section class="bg-primary content-header"
             style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Users</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li><i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li><i class="fa fa-angle-right" style="color: #ccc;"></i> Users &nbsp;</li>
                    <li class="active"><i class="fa fa-angle-right" style="color: #ccc;"></i> Profile</li>
                </ol>
            </div>
        </div>
    </section>

    <div class="col-6" style="margin-top: 10px">
        <div class="card card-outline card-primary">
            <div class="card-header">
                <h3 class="card-title">Profile</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
                <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-striped table-hover">
                    <tbody>
                    <tr>
                        <td><strong>IP address</strong></td>
                        <td>127.0.0.1</td>
                    </tr>
                    <tr>
                     <td><strong>First Name</strong></td>
                        <td>{{$user->first_name}}</td>
                    </tr>
                    <tr>
                        <td><strong>Last Name</strong></td>
                        <td>{{$user->last_name}}</td>
                    </tr>
                    <tr>
                        <td><strong>User name / Pseudo</strong></td>
                        <td>administrator</td>
                    </tr>
                    <tr>
                      <td><strong>Email</strong></td>
                        <td>{{$user->email}}</td>
                    </tr>
                    <tr>
                       <td><strong>Created Date</strong></td>
                        <td><strong>{{$user->created_at->diffForHumans()}}</strong></td>
                    </tr>
                    <tr>
                   <td><strong>Last Login</strong></td>
                         <td><strong>{{ \Carbon\Carbon::parse($user->last_login)->diffForHumans() }}</strong></td>

                    </tr>
                    <tr>
                        <td><strong>Status</strong></td>
                        <td><span class="label label-{{$user->status?'success':'danger'}}">{{$user->status?"Active":"inActive"}}
                                </span></td>
                    </tr>
                    <tr>
                    <td><strong>Company</strong></td>
                        <td>{{$user->company}}</td>
                    </tr>
                    <tr>
                       <td><strong>Phone</strong></td>
                        <td>{{$user->phone}}</td>
                    </tr>
                    <tr>
                        <td><strong>Groups</strong></td>
                        <td>
                            @if(!is_null($user->getRoleNames()))
                                @foreach($user->getRoleNames() as $v)
                                    <label class="badge badge-success">{{ucfirst( $v) }}</label>
                        @endforeach
                        @else
                            <td></td>

                        @endif

                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.card -->
        </div>
    </div>

@endsection