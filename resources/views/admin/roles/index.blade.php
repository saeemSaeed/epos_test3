@extends('admin.layout.index')
@section('content')
    <section class="bg-primary content-header"
             style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Roles</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li>
                        <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li>
                        <i class="fa fa-angle-right" style="color: #ccc;"></i> Roles &nbsp;
                    </li>

                </ol>
            </div>
        </div>
    </section>
    <div class="clearfix mt-3">
            @can('role-create')
                <a href="{{ route('roles.create') }}" class="btn btn-success btn-flat float-left">
                    <i class="fa fa-plus"></i> Create New Role
                </a>
            @endcan
        </div>
    </div>
    <div class="container-fluid" style="margin-top: 10px">
        <div class="card">

            <div class="card-body">


                    <div class="row">
                        <div class="col-sm-12">

                             <table id="role_table" class="table-bordered table-hover dataTable dtr-inline">
                                <thead>
                                <tr>
                                    <th style="width:10%;text-align: center">ID</th>
                                    <th>Name</th>
                                    <th width="20%" style="text-align: center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($roles as $key => $role)
                                    <tr>
                                        <td style="text-align: center">{{ ++$key}}</td>
                                        <td>{{ucfirst( $role->name) }}</td>
                                        <td style="text-align: center">
                                            <a class="btn btn-info" href="{{ route('roles.show',$role->id) }}">Show</a>
                                            @can('role-edit')
                                                <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Edit</a>
                                            @endcan
                                            @can('role-delete')
                                                <form method='Post' action="{{route('roles.destroy', $role->id)}}"
                                                      style="display:inline">
                                                    @method('DELETE')
                                                    @csrf
                                                    @if($role->name!=='Admin')
                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                    @endif
                                                </form>
                                            @endcan
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.card-body -->
        </div>

    @endsection