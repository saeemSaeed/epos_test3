@extends('admin.layout.index')
@section('content')
    <section class="bg-primary content-header"
             style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Roles</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li>
                        <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li>
                        <i class="fa fa-angle-right" style="color: #ccc;"></i> Role &nbsp;
                    </li>
                    <li>
                        <i class="fa fa-angle-right" style="color: #ccc;"></i> Edit &nbsp;
                    </li>
                </ol>
            </div>
        </div>
    </section>
    <div class="container-fluid" style="margin-top: 10px">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>Show Role</h2>
                        </div>
                        <div class="pull-right" style="float:right;">
                            <a class="btn btn-primary" href="{{ route('roles.index') }}"> Back</a>
                        </div>
                    </div>
                </div>
                <form action="{{route('roles.update',$role->id)}}" method="Post">
                    @method('PATCH')
                    @csrf
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Name:</strong>
                                <input name="name" class="form-control" placeholder='Name' value="{{$role->name}}">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                {{-- <strong>Permission:</strong> --}}
                                <br/>
                                <label><input type="checkbox" class="name check selectAll" id="selectAll"> Select All</label>
                                <div class="row">
                                    {{--     @foreach($permission as $index=>$value)
                                    <div class="col-4">
                                        <label> <input type="checkbox" class="name your_checkbox_class"
                                            name="permission[]"
                                            value="{{ 1}}">
                                        {{ $value->name }}</label>
                                    </div>
                                    @endforeach --}}
                                    <label><h4>Administration</h4></label>
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Menu Title</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class admin_create_select" id="admin_create_select" disabled> Create</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class admin_delete_select" id="admin_delete_select" disabled> Delete</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class admin_edit_select" id="admin_edit_select" disabled> Edit</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class admin_list_select" id="admin_list_select" disabled> List</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class admin_show_select" id="admin_show_select" disabled> Show</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>User</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_create" name="permission[]" value="54" {{in_array(54, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_delete" name="permission[]" value="56" {{in_array(56, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_edit" name="permission[]" value="55" {{in_array(55, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_list" name="permission[]" value="53" {{in_array(53, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_show" name="permission[]" value="57" {{in_array(57, $rolePermissions) ? "checked" : false}} disabled></td>
                                        </tr>
                                        <tr>
                                            <td>Role</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_create" name="permission[]" value="2" {{in_array(2, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_delete" name="permission[]" value="4" {{in_array(4, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_edit" name="permission[]" value="3" {{in_array(3, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_list" name="permission[]" value="1" {{in_array(1, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_show" name="permission[]" value="60" {{in_array(60, $rolePermissions) ? "checked" : false}} disabled></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <label><h4>EPOS Health</h4></label>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>Menu Title</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class epos_create_select" id="epos_create_select" disabled> Create</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class epos_delete_select" id="epos_delete_select" disabled> Delete</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class epos_edit_select" id="epos_edit_select" disabled> Edit</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class epos_list_select" id="epos_list_select" disabled> List</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class epos_show_select" id="epos_show_select" disabled> Show</th>
                                            <th style=" text-align:center"><input type="checkbox" class="name check your_checkbox_class epos_restore_select" id="epos_show_select" disabled> Restore</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Contracts</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_create" name="permission[]" value="50" {{in_array(50, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_delete" name="permission[]" value="52" {{in_array(52, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_edit" name="permission[]" value="51" {{in_array(51, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_list" name="permission[]" value="49" {{in_array(49, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_show" name="permission[]" value="61" {{in_array(61, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_restore" name="permission[]" value="79" {{in_array(79, $rolePermissions) ? "checked" : false}} disabled></td>

                                        </tr>
                                        <tr>
                                            <td>Projects</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_create" name="permission[]" value="46" {{in_array(46, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_delete" name="permission[]" value="48" {{in_array(48, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_edit" name="permission[]" value="47" {{in_array(47, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_list" name="permission[]" value="45" {{in_array(45, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_show" name="permission[]" value="62" {{in_array(62, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_restore" name="permission[]" value="78" {{in_array(78, $rolePermissions) ? "checked" : false}} disabled></td>

                                        </tr>
                                        <tr>
                                            <td>Payments</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_create" name="permission[]" value="42" {{in_array(42, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_delete" name="permission[]" value="44" {{in_array(44, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_edit" name="permission[]" value="43" {{in_array(43, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_list" name="permission[]" value="41" {{in_array(41, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_show" name="permission[]" value="63" {{in_array(63, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center" ><input type="checkbox" class="name your_checkbox_class epos_restore" name="permission[]" value="80" {{in_array(80, $rolePermissions) ? "checked" : false}} disabled></td>

                                        </tr>
                                        </tbody>
                                    </table>
                                    <label><h4>Helper</h4></label>
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Menu Title</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class helper_create_select" id="helper_create_select" disabled> Create</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class helper_delete_select" id="helper_delete_select" disabled> Delete</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class helper_edit_select" id="helper_edit_select" disabled> Edit</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class helper_list_select" id="helper_list_select" disabled> List</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class helper_restore_select" id="helper_restore_select" disabled> Restore</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Province/Region</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_create" name="permission[]" value="38" {{in_array(38, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_delete" name="permission[]" value="40" {{in_array(40, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_edit" name="permission[]" value="39" {{in_array(39, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_list" name="permission[]" value="37" {{in_array(37, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_restore" name="permission[]" value="81" {{in_array(81, $rolePermissions) ? "checked" : false}} disabled></td>

                                        </tr>
                                        <tr>
                                            <td>PEA's</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_create" name="permission[]" value="34" {{in_array(34, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_delete" name="permission[]" value="36" {{in_array(36, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_edit" name="permission[]" value="35" {{in_array(35, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_list" name="permission[]" value="33" {{in_array(33, $rolePermissions) ? "checked" : false}} disabled></td>

                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_restore" name="permission[]" value="82" {{in_array(82, $rolePermissions) ? "checked" : false}} disabled></td>
                                        </tr>
                                        <tr>
                                            <td>Contractors</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_create" name="permission[]" value="30" {{in_array(30, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_delete" name="permission[]" value="32" {{in_array(32, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_edit" name="permission[]" value="31" {{in_array(31, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_list" name="permission[]" value="29" {{in_array(29, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_restore" name="permission[]" value="83" {{in_array(83, $rolePermissions) ? "checked" : false}} disabled></td>

                                        </tr>
                                        <tr>
                                            <td>Measure's</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_create" name="permission[]" value="26" {{in_array(26, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_delete" name="permission[]" value="28" {{in_array(28, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_edit" name="permission[]" value="27" {{in_array(27, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_list" name="permission[]" value="25" {{in_array(25, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_restore" name="permission[]" value="84" {{in_array(84, $rolePermissions) ? "checked" : false}} disabled></td>

                                        </tr>
                                        <tr>
                                            <td>Sub Categories</td>
                                            <td  style=" text-align:center"> <input type="checkbox" class="name your_checkbox_class helper_create" name="permission[]" value="22" {{in_array(22, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"> <input type="checkbox" class="name your_checkbox_class helper_delete" name="permission[]" value="24" {{in_array(24, $rolePermissions) ? "checked" : false}} disabled> </td>
                                            <td  style=" text-align:center"> <input type="checkbox" class="name your_checkbox_class helper_edit" name="permission[]" value="23" {{in_array(23, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"> <input type="checkbox" class="name your_checkbox_class helper_list" name="permission[]" value="21" {{in_array(21, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_restore" name="permission[]" value="85" {{in_array(85, $rolePermissions) ? "checked" : false}} disabled></td>

                                        </tr>
                                        <tr>
                                            <td>Payment Method</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_create" name="permission[]" value="18" {{in_array(18, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_delete" name="permission[]" value="20" {{in_array(20, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_edit" name="permission[]" value="19" {{in_array(19, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_list" name="permission[]" value="17" {{in_array(17, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_restore" name="permission[]" value="86"  {{in_array(86, $rolePermissions) ? "checked" : false}} disabled></td>

                                        </tr>
                                        <tr>
                                            <td>Currency</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_create" name="permission[]" value="14" {{in_array(14, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_delete" name="permission[]" value="16" {{in_array(16, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_edit" name="permission[]" value="15" {{in_array(15, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_list" name="permission[]" value="13" {{in_array(13, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_restore" name="permission[]" value="87" {{in_array(87, $rolePermissions) ? "checked" : false}} disabled></td>

                                        </tr>
                                        <tr>
                                            <td>Exchange Rate</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_create" name="permission[]" value="10" {{in_array(10, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_delete" name="permission[]" value="12" {{in_array(12, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_edit" name="permission[]" value="11" {{in_array(11, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_list" name="permission[]" value="9" {{in_array(9, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_restore" name="permission[]" value="88" {{in_array(88, $rolePermissions) ? "checked" : false}} disabled></td>

                                        </tr>
                                        <tr>
                                            <td>City</td>
                                            <td style=" text-align:center" ><input type="checkbox" class="name your_checkbox_class helper_create" name="permission[]" value="6" {{in_array(6, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_delete" name="permission[]" value="8" {{in_array(8, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_edit" name="permission[]" value="7" {{in_array(7, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_list" name="permission[]" value="5" {{in_array(5, $rolePermissions) ? "checked" : false}} disabled></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_restore" name="permission[]" value="89" {{in_array(89, $rolePermissions) ? "checked" : false}} disabled></td>

                                        </tr>
                                        </tbody>
                                    </table>
                                    <label><h4>Dashboard</h4></label>
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th style="width: 36%">Menu Title</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class dashboard_show_select" id="dashboard_show_select" disabled> Show</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Total Users</td>
                                            <td style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="65" {{in_array(65, $rolePermissions) ? "checked" : false}} disabled></td>
                                        </tr>
                                        <tr>
                                            <td>Total Projects</td>
                                            <td style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="64" {{in_array(64, $rolePermissions) ? "checked" : false}} disabled></td>
                                        </tr>
                                        <tr>
                                            <td>Total Contracts</td>
                                            <td style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="66" {{in_array(66, $rolePermissions) ? "checked" : false}} disabled></td>
                                        </tr>
                                        <tr>
                                            <td>Total Payments</td>
                                            <td style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="67" {{in_array(67, $rolePermissions) ? "checked" : false}} disabled></td>
                                        </tr>
                                        <tr>
                                            <td>Project Budget Distribution Province Wise Graph</td>
                                            <td style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="68" {{in_array(68, $rolePermissions) ? "checked" : false}} disabled></td>
                                        </tr>
                                        <tr>
                                            <td>Overall Fund Dispositions EUR</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="69" {{in_array(69, $rolePermissions) ? "checked" : false}} disabled></td>
                                        </tr>
                                        <tr>
                                            <td>Fund Dispositions</td>
                                            <td style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="70" {{in_array(70, $rolePermissions) ? "checked" : false}} disabled></td>
                                        </tr>
                                        <tr>
                                            <td>Fund Dispositions AJK</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="71" {{in_array(71, $rolePermissions) ? "checked" : false}} disabled></td>
                                        </tr>
                                        <tr>
                                            <td>Fund Dispositions Balochistan</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="72" {{in_array(72, $rolePermissions) ? "checked" : false}} disabled></td>
                                        </tr>
                                        <tr>
                                            <td>Fund Dispositions Gilgit Baltistan</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="73" {{in_array(73, $rolePermissions) ? "checked" : false}} disabled></td>
                                        </tr>
                                        <tr>
                                            <td>Fund Dispositions Khyber Pakhtunkhwa</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="74" {{in_array(74, $rolePermissions) ? "checked" : false}} disabled></td>
                                        </tr>
                                        <tr>
                                            <td>Fund Dispositions Punjab</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="75" {{in_array(75, $rolePermissions) ? "checked" : false}} disabled></td>
                                        </tr>
                                        <tr>
                                            <td>Fund Dispositions Sindh</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="76" {{in_array(76, $rolePermissions) ? "checked" : false}} disabled></td>
                                        </tr>
                                        <tr>
                                            <td>Fund Dispositions ICT</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="77" {{in_array(77, $rolePermissions) ? "checked" : false}}    disabled></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
            </div>
            </form>
        </div>
    </div>
    </div>
@endsection