@extends('admin.layout.index')
@section('content')
<section class="bg-primary content-header"
    style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
    <div class="row">
        <div class="col-8">
            <h1>Roles</h1>
        </div>
        <div class="col-4">
            <ol class="breadcrumb" style="color:#444;float: right">
                <li>
                    <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                </li>
                <li>
                    <i class="fa fa-angle-right" style="color: #ccc;"></i> Roles &nbsp;
                </li>
                <li>
                    <i class="fa fa-angle-right" style="color: #ccc;"></i> Create &nbsp;
                </li>
            </ol>
        </div>
    </div>
</section>
<div class="row">
    <div class="col-2" style="margin-top: 10px; margin-left: 10px">
        Create Role
    </div>
</div>
<div class="container-fluid" style="margin-top: 10px">
    <div class="card">
        <div class="card-body">
            <form action="{{route('roles.store')}}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Name:</strong>
                            <input class="form-control" name="name" placeholder="Name">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            {{-- <strong>Permission:</strong> --}}
                            <br/>
                            <label><input type="checkbox" class="name check selectAll" id="selectAll"> Select All</label>
                            <div class="row">
                                {{--     @foreach($permission as $index=>$value)
                                <div class="col-4">
                                    <label> <input type="checkbox" class="name your_checkbox_class"
                                        name="permission[]"
                                        value="{{ $value->id}}">
                                    {{ $value->name }}</label>
                                </div>
                                @endforeach --}}
                                <label><h4>Administration</h4></label>
                                <table class="table table-striped ">
                                    <thead>
                                        <tr >
                                            <th>Menu Title</th>
                                            <th ><input type="checkbox" class="name check your_checkbox_class admin_create_select" id="admin_create_select"> Create</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class admin_delete_select" id="admin_delete_select"> Delete</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class admin_edit_select" id="admin_edit_select"> Edit</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class admin_list_select" id="admin_list_select"> List</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class admin_show_select" id="admin_show_select"> Show</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>User</td>
                                            <td   style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_create" name="permission[]" value="54"></td>
                                            <td   style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_delete" name="permission[]" value="56"></td>
                                            <td   style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_edit" name="permission[]" value="55"></td>
                                            <td   style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_list" name="permission[]" value="53"></td>
                                            <td   style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_show" name="permission[]" value="57"></td>
                                        </tr>
                                        <tr>
                                            <td>Role</td>
                                            <td   style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_create" name="permission[]" value="2"></td>
                                            <td   style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_delete" name="permission[]" value="4"></td>
                                            <td   style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_edit" name="permission[]" value="3"></td>
                                            <td   style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_list" name="permission[]" value="1"></td>
                                            <td   style=" text-align:center"><input type="checkbox" class="name your_checkbox_class admin_show" name="permission[]" value="60"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <label><h4>EPOS Health</h4></label>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Menu Title</th>
                                            <th  style=" text-align:center"><input type="checkbox" class="name check your_checkbox_class epos_create_select" id="epos_create_select"> Create</th>
                                            <th  style=" text-align:center"><input type="checkbox" class="name check your_checkbox_class epos_delete_select" id="epos_delete_select"> Delete</th>
                                            <th  style=" text-align:center"><input type="checkbox" class="name check your_checkbox_class epos_edit_select" id="epos_edit_select"> Edit</th>
                                            <th  style=" text-align:center"><input type="checkbox" class="name check your_checkbox_class epos_list_select" id="epos_list_select"> List</th>
                                            <th  style=" text-align:center"><input type="checkbox" class="name check your_checkbox_class epos_show_select" id="epos_show_select"> Show</th>
                                            <th  style=" text-align:center"><input type="checkbox" class="name check your_checkbox_class epos_restore_select" id="epos_show_select"> Restore</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Contracts</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_create" name="permission[]" value="50"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_delete" name="permission[]" value="52"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_edit" name="permission[]" value="51"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_list" name="permission[]" value="49"></td>
                                            <td style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_show" name="permission[]" value="61"></td>
                                            <td style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_restore" name="permission[]" value="79"></td>
                                        </tr>
                                        <tr>
                                            <td>Projects</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_create" name="permission[]" value="46"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_delete" name="permission[]" value="48"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_edit" name="permission[]" value="47"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_list" name="permission[]" value="45"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_show" name="permission[]" value="62"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_restore" name="permission[]" value="78"></td>
                                        </tr>
                                        <tr>
                                            <td>Payments</td>
                                            <td  style=" text-align:center" ><input type="checkbox" class="name your_checkbox_class epos_create" name="permission[]" value="42"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_delete" name="permission[]" value="44"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_edit" name="permission[]" value="43"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_list" name="permission[]" value="41"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_show" name="permission[]" value="63"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class epos_restore" name="permission[]" value="80"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <label><h4>Helper</h4></label>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Menu Title</th>
                                            <th ><input type="checkbox" class="name check your_checkbox_class helper_create_select" id="helper_create_select"> Create</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class helper_delete_select" id="helper_delete_select"> Delete</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class helper_edit_select" id="helper_edit_select"> Edit</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class helper_list_select" id="helper_list_select"> List</th>
                                            <th><input type="checkbox" class="name check your_checkbox_class helper_restore_select" id="helper_restore_select"> Restore</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Province/Region</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_create" name="permission[]" value="38"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_delete" name="permission[]" value="40"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_edit" name="permission[]" value="39"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_list" name="permission[]" value="37"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_restore" name="permission[]" value="81"></td>
                                        </tr>
                                        <tr>
                                            <td>PEA's</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_create" name="permission[]" value="34"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_delete" name="permission[]" value="36"></td>
                                            <td  style=" text-align:center"> <input type="checkbox" class="name your_checkbox_class helper_edit" name="permission[]" value="35"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_list" name="permission[]" value="33"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_restore" name="permission[]" value="82"></td>
                                        </tr>
                                        <tr>
                                            <td>Contractors</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_create" name="permission[]" value="30"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_delete" name="permission[]" value="32"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_edit" name="permission[]" value="31"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_list" name="permission[]" value="29"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_restore" name="permission[]" value="83"></td>
                                        </tr>
                                        <tr>
                                            <td>Measure's</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_create" name="permission[]" value="26"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_delete" name="permission[]" value="28"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_edit" name="permission[]" value="27"></td>
                                            <td  style=" text-align:center"> <input type="checkbox" class="name your_checkbox_class helper_list" name="permission[]" value="25"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_restore" name="permission[]" value="84"></td>
                                        </tr>
                                        <tr>
                                            <td>Sub Categories</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_create" name="permission[]" value="22"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_delete" name="permission[]" value="24"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_edit" name="permission[]" value="23"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_list" name="permission[]" value="21"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_restore" name="permission[]" value="85"></td>
                                        </tr>
                                        <tr>
                                            <td>Payment Method</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_create" name="permission[]" value="18"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_delete" name="permission[]" value="20"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_edit" name="permission[]" value="19"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_list" name="permission[]" value="17"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_restore" name="permission[]" value="86"></td>
                                        </tr>
                                        <tr>
                                            <td>Currency</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_create" name="permission[]" value="14"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_delete" name="permission[]" value="16"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_edit" name="permission[]" value="15"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_list" name="permission[]" value="13"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_restore" name="permission[]" value="87"></td>
                                        </tr>
                                        <tr>
                                            <td>Exchange Rate</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_create" name="permission[]" value="10"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_delete" name="permission[]" value="12"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_edit" name="permission[]" value="11"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_list" name="permission[]" value="9"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_restore" name="permission[]" value="88"></td>
                                        </tr>
                                        <tr>
                                            <td>City</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_create" name="permission[]" value="6"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_delete" name="permission[]" value="8"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_edit" name="permission[]" value="7"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_list" name="permission[]" value="5"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_restore" name="permission[]" value="89"></td>
                                        </tr>
                                        {{-- <tr>
                                            <td>File Category</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_create" name="permission[]" value="91"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_delete" name="permission[]" value="93"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_edit" name="permission[]" value="92"></td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_list" name="permission[]" value="90"></td>
                                             <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class helper_restore" name="permission[]" value="89"></td> 
                                        </tr> --}}
                                    </tbody>
                                </table>
                                <label><h4>Dashboard</h4></label>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 36%">Menu Title</th>
                                            <th ><input type="checkbox" class="name check your_checkbox_class dashboard_show_select" id="dashboard_show_select"> Show</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Total Users</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="65"></td>
                                        </tr>
                                        <tr>
                                            <td>Total Projects</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="64"></td>
                                        </tr>
                                        <tr>
                                            <td>Total Contracts</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="66"></td>
                                        </tr>
                                        <tr>
                                            <td>Total Payments</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="67"></td>
                                        </tr>
                                        <tr>
                                            <td>Project Budget Distribution Province Wise Graph</td>
                                            <td  style=" text-align:center"> <input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="68"></td>
                                        </tr>
                                        <tr>
                                            <td>Overall Fund Dispositions EUR</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="69"></td>
                                        </tr>
                                        <tr>
                                            <td>Fund Dispositions</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="70"></td>
                                        </tr>
                                        <tr>
                                            <td>Fund Dispositions AJK</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="71"></td>
                                        </tr>
                                        <tr>
                                            <td>Fund Dispositions Balochistan</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="72"></td>
                                        </tr>
                                        <tr>
                                            <td>Fund Dispositions Gilgit Baltistan</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="73"></td>
                                        </tr>
                                        <tr>
                                            <td>Fund Dispositions Khyber Pakhtunkhwa</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="74"></td>
                                        </tr>
                                        <tr>
                                            <td>Fund Dispositions Punjab</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="75"></td>
                                        </tr>
                                        <tr>
                                            <td>Fund Dispositions Sindh</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="76"></td>
                                        </tr>
                                        <tr>
                                            <td>Fund Dispositions ICT</td>
                                            <td  style=" text-align:center"><input type="checkbox" class="name your_checkbox_class dashboard_show" name="permission[]" value="77"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary" style="float: right"> Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
@endsection