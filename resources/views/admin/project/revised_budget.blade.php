@extends('admin.layout.index')
@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <style>
        label {
            font-size: 12px !important;
            color: #2a272a !important;
            margin-bottom: 0.1rem !important;
        }

        input {
            font-size: 14px !important;
            height: 30px !important;
        }

        select {
            font-size: 12px !important;
            height: 27px !important;
        }

        .form-group {
            margin-bottom: 0.5rem;
            padding-top: 5px !important;
        }
    </style>
    <section class="bg-primary content-header"
             style="background-color:#ffc533 !important; padding-bottom: 10px; padding-left: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Project</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li><i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li><i class="fa fa-angle-right" style="color: #ccc;"></i> Project &nbsp;</li>
                    <li class="active"><i class="fa fa-angle-right" style="color: #ccc;"></i> Revised Budget</li>
                </ol>
            </div>
        </div>
    </section>

    {{--Start create form--}}


    <div class="col">
        <div class="card card-outline card-primary">
            <div class="card-header" style="">
                <h3 class="card-title">Reference Budget Row</h3>
                <a href="{{route('project.show',[$project_id,'true'])}}" class=" btn btn-primary" style="float: right"> <h3 class="card-title" ><i class="fa fa-angle-left"></i>  Back</h3></a>

            </div>
            <div class="card-body">
                <table class="table table-sm">
                    <thead>
                    <tr>
                        {{--<th style="text-align:center;width: 2%!important;">ID</th>--}}
                        <th style="text-align:center;width: 20%!important;">Measures/Line Items</th>
                        <th style="text-align:center;width: 20%!important;">Sub-Category</th>
                        <th style="text-align:center;width: 150px!important;">Budget Amount</th>
                        <th style="text-align:center;width: 150px!important;">Revised Amount</th>
                        <th style="text-align:center;width: 35%!important;">Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>

                            <input type="text" class="form-control" readonly
                                   value="{{isset($getReferenceRow->hasOneMeasures->name)?$getReferenceRow->hasOneMeasures->name:''}}">
                        </td>
                        <td>
                            <input type="text" class="form-control" readonly
                                   value="{{isset($getReferenceRow->hasOneSubCategories->name)?$getReferenceRow->hasOneSubCategories->name:"unAllocate"}}
                                           ">
                        </td>
                        <td>
                            <input type="text"
                                   oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"
                                   class="form-control number_formated_two" dir="rtl" readonly
                                   value="{{$getReferenceRow->budget}}
                                           ">
                        </td>
                        <td>
                            <input type="text"
                                   oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"
                                   class="form-control number_formated_two" dir="rtl" readonly
                                   value="{{ isset(\App\Helpers\Helper::getLatestRow($getReferenceRow->project_id, $getReferenceRow->id)->revised_budget)?\App\Helpers\Helper::getLatestRow($getReferenceRow->project_id, $getReferenceRow->id)->revised_budget:'0.00'}}"
                            >
                        </td>
                        <td>
                            <input type="text" class="form-control" readonly
                                   value=" {{$getReferenceRow->description}}
                                           ">
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @if($checklastRowProvedOrNot)
        <form method="post" action="{{route('project.revisedBudgetStore')}}">
            @csrf
            <input value="{{$project_id}}" name="project_id" type="hidden">
            <input value="{{$line_reference_id}}" name="line_reference_id" type="hidden">
            <div class="col" style="margin-top: 10px">
                <div class="card card-outline card-primary">
                    <div class="card-header" style=" background-color: #65a3c6;
color: #2c2c2c;">
                        <h3 class="card-title">Request Revised Budget</h3>
                        <div class="card-tools">
                            <button type="button" style="float: right" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body" style="display: block;">
                        <div class="row">
                            <div class=" col-4">
                                <div class="form-group">
                                    <label>Revised Date</label>
                                    <div class="input-group date Pickerdate" id="revsed_date" data-target-input="nearest">
                                        <input placeholder="dd/mm/yy" type="text" class="form-control datetimepicker-input" data-target="#revsed_date"   name="date">
                                        <div class="input-group-append" data-target="#revsed_date" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>Revised Budget: &nbsp;</label>
                                    <input type="text"
                                           oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"
                                           class="form-control revised_budget_two_decimal  " name="revised_budget"
                                           dir="rtl"
                                           value="0.00" required>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>Description: &nbsp;</label>
                                    <input type="text" class="form-control " name="description">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success btn-lg">Save</button>
                    </div>
                </div>
            </div>
        </form>
    @endif
    <div class="col">
        <div class="card card-outline card-primary">
            <div class="card-header" style="">
                <h3 class="card-title">Previous Revised Budget</h3>
            </div>
            <div class="card-body">
                <table class="table table-sm" style="width:100% ;">
                    <thead>
                    <tr>
                        <th style="text-align:center;width: 2%!important;">ID</th>
                        <th style="text-align:center;width: 20%!important;">Date</th>
                        <th style="text-align:center;width: 15%!important;">Revised Budget</th>
                        <th style="text-align:center;width: 10%!important;">modified by</th>
                        <th style="text-align:center;width: 150px!important;">Status</th>
                        <th style="text-align:center;width: 35%!important;">Description</th>
                        <th style="text-align:center;width: 2%!important;"></th>
                        @if(\Illuminate\Support\Facades\Auth::user()->getRoleNames()[0]=='Admin')
                            <th style="text-align:center;">Action</th>
                        @endif
                        @if(\Illuminate\Support\Facades\Auth::user()->getRoleNames()[0]=='CEO')
                            <th style="text-align:center">Action</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($showProjectRevised as $index=>$revised_budget)

                        @if($revised_budget->status===0)
                            @if($revised_budget->user_id===\Illuminate\Support\Facades\Auth::id())
                                <tr>
                                    {{--<form method="post" action="{{route('reference.id.rowData')}}"></form>--}}
                                    <td style="text-align:center;width: 2%!important;">{{++$index}}</td>
                                    <td>
                                        <div class="input-group date Pickerdate" id="revsed_date" data-target-input="nearest">
                                            <input placeholder="dd/mm/yy" type="text" style="text-align: center"

                                                   value="{{\Carbon\Carbon::parse($revised_budget->date)->format('d/m/y H:i:s')}}}"

                                                   class="form-control datetimepicker-input" data-target="#revsed_date"   name="date">
                                            <div class="input-group-append" data-target="#revsed_date" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>



                                    </td>
                                    <td><input type="text"
                                               oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"
                                               class="form-control  number_formated_two" dir="rtl"
                                               value="{{$revised_budget->revised_budget}}"></td>
                                    <td><input type="text" class="form-control" readonly style="text-align:center"
                                               value="{{$revised_budget->hasUser->first_name.''.$revised_budget->hasUser->last}}">
                                    </td>
                                    <td style="text-align: center">
                                        @if($revised_budget->status===0)
                                            <span class="badge badge-warning "> Pending </span>
                                        @elseif($revised_budget->status===1)
                                            <span class="badge badge-success "> Approved </span>
                                        @elseif($revised_budget->status===2)
                                            <span class="badge badge-danger"> Rejected </span>
                                    @endif

                                    <td><input type="text" class="form-control"
                                               value="{{$revised_budget->description}}"></td>

                                    <td>
                                        <button type="submit" style="border: none;background: transparent"><i class="fa fa-save"
                                                                                                              style="font-size:15px !important;color: #0a0e14"
                                                                                                              aria-hidden="true"></i>
                                        </button>
                                    </td>
                                    @if(\Illuminate\Support\Facades\Auth::user()->getRoleNames()[0]=='Admin')
                                        <td class="" style="text-align: center!important;">
                                            <a class="green btn-revise-project" data-toggle="tooltip" title="" onclick="return confirm('Are you sure?')"  href="{{route('project.revisedBudgetStatus',['id'=>$revised_budget->id,'status'=>'pending'])}}" data-original-title="pending">
                                                <i class="ace-icon fa fa-clock"></i>
                                            </a>
                                            <a class="green btn-reject-project" data-toggle="tooltip" title="" href="{{route('project.revisedBudgetStatus',['id'=>$revised_budget->id,'status'=>'reject'])}}" data-original-title="Reject" onclick="return confirm('Are you sure?')" aria-describedby="tooltip136991">
                                                <i class="ace-icon fa fa-arrow-left"></i>
                                            </a>
                                            <a class="green btn-approve-project" data-toggle="tooltip" title="" href="{{route('project.revisedBudgetStatus',['id'=>$revised_budget->id,'status'=>'approve'])}}" onclick="return confirm('Are you sure?')" data-original-title="Approve">
                                                <i class="ace-icon fa fa-check-square"></i>
                                            </a>
                                        </td>
                                    @elseif(\Illuminate\Support\Facades\Auth::user()->getRoleNames()[0]=='CEO')
                                        <td class="" style="text-align: center!important;">
                                            <a class="green btn-revise-project" data-toggle="tooltip" title="" onclick="return confirm('Are you sure?')"  href="{{route('project.revisedBudgetStatus',['id'=>$revised_budget->id,'status'=>'pending'])}}" data-original-title="pending">
                                                <i class="ace-icon fa fa-clock"></i>
                                            </a>
                                            <a class="green btn-reject-project" data-toggle="tooltip" title="" href="{{route('project.revisedBudgetStatus',['id'=>$revised_budget->id,'status'=>'reject'])}}" data-original-title="Reject" onclick="return confirm('Are you sure?')" aria-describedby="tooltip136991">
                                                <i class="ace-icon fa fa-arrow-left"></i>
                                            </a>
                                            <a class="green btn-approve-project" data-toggle="tooltip" title="" href="{{route('project.revisedBudgetStatus',['id'=>$revised_budget->id,'status'=>'approve'])}}" onclick="return confirm('Are you sure?')" data-original-title="Approve">
                                                <i class="ace-icon fa fa-check-square"></i>
                                            </a>
                                        </td>
                                    @endif

                                </tr>
                            @endif
                        @else
                            <tr>
                                <td style="text-align:center;width: 2%!important;">{{++$index}}</td>
                                <td><input type="text" class="form-control" readonly style="text-align: center"
                                           value="{{\Carbon\Carbon::parse($revised_budget->date)->format('d/M/y H:i:s')}}"
                                    >



                                </td>
                                <td><input type="text"
                                           oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"
                                           class="form-control  number_formated_two" dir="rtl" readonly
                                           value="{{$revised_budget->revised_budget}}"></td>
                                <td><input type="text" class="form-control" readonly
                                           value="{{$revised_budget->hasUser->first_name.''.$revised_budget->hasUser->last}}">
                                </td>
                                <td style="text-align: center">
                                    @if($revised_budget->status===0)
                                        <span class="badge badge-warning "> Pending </span>
                                    @elseif($revised_budget->status===1)
                                        <span class="badge badge-success "> Approved </span>
                                    @elseif($revised_budget->status===2)
                                        <span class="badge badge-danger"> Rejected </span>
                                @endif

                                <td><input type="text" class="form-control" readonly
                                           value="{{$revised_budget->description}}"></td>
                                {{--<td></td>--}}


                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection