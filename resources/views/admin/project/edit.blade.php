@extends('admin.layout.index')
@section('content')

    <style>
        label {
            font-size: 12px !important;
            color: #2a272a !important;
            margin-bottom: 0.1rem !important;
        }

        input {
            font-size: 14px !important;
            height: 30px !important;
        }

        select {
            font-size: 12px !important;
            height: 27px !important;
        }

        .form-group {
            /*margin-bottom: 0.5rem;*/
            /*padding-top: 5px !important;*/
        }
    </style>
    <section class="bg-primary content-header"
             style="background-color:#ffc533 !important; padding-bottom: 10px; padding-left: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Project</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li><i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li><i class="fa fa-angle-right" style="color: #ccc;"></i> Project &nbsp;</li>
                    <li class="active"><i class="fa fa-angle-right" style="color: #ccc;"></i> Edit</li>
                </ol>
            </div>
        </div>
    </section>
    @if($project->status==2)
        <div class="alert alert-danger alert-block message">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{"Project staus is rejected"}}</strong>
        </div>
    @endif
    {{--    @if($project->is_approved==1)--}}
    {{--        @if($project->status==0)--}}
    {{--            <div class="alert alert-danger alert-block message">--}}
    {{--                <button type="button" class="close" data-dismiss="alert">×</button>--}}
    {{--                <strong>{{"Already Submitted for CEO approval"}}</strong>--}}
    {{--            </div>--}}
    {{--        @endif--}}
    {{--    @endif--}}
    <form method="post" action="{{route('project.update')}}" id="projectFormEdit" enctype="multipart/form-data"
          {{--          @if($project->is_approved==1)--}}
          {{--          @if($project->status==0)--}}
          {{--          onsubmit=" alert('Already Submitted for CEO approval');--}}
          {{--    return false;"--}}
          {{--            @endif--}}
          {{--            @endif--}}
          @if($project->status==2)
          onsubmit=" alert('Project staus is rejected');
    return false;"
            @endif
    >


        @csrf
        <input type="hidden" value="{{$project->id}}" name="project_id" class="project_id">
        <input type="hidden" value="{{$project->is_approved}}" name="is_approved" class="is_approved">
        <div class="col" style="margin-top: 10px">
            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title">Edit Project</h3>
                    @if($checkHistory>0)
                        <div class="card-tools">
                            <a href="{{route('project.history.list',
                            ['project_id'=>$project->id]
                            )}}">
                                Revision History
                            </a>
                        </div>
                    @endif
                </div>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-5">
                            <div class="form-group"><label>Project Title: &nbsp;</label>
                                <input type="text" style="background-color: #FFCCCB;" class="form-control"
                                       value="{{$project->title}}" name="title"
                                       @if($project->is_approved == 1)
                                       readonly
                                       @elseif($project->is_approved == 2)

                                       readonly @endif>
                            </div>
                            <div class="form-group"><label>Project Donor Number: &nbsp;</label>
                                <input type="text" class="form-control" name="donor_number"
                                       value="  {{$project->donor_number}}"
                                       @if($project->is_approved == 1)      readonly
                                       @elseif($project->is_approved == 2)

                                       readonly @endif>
                            </div>
                            <div class="form-group"><label>Budget (Per Agreement): &nbsp;</label>
                                <input type="text" style="background-color: #FFCCCB;"

                                       class="form-control project_budget number_formated_two" name="budget" dir="rtl"
                                       value="{{$project->budget}}"

                                        @if($project->status || $historyCount > 0)  
                                            readonly
                                        @endif>
                            </div>
                            @if($project->status || $historyCount > 0)
                                <div style="display:{{$project->is_approved == 1?'block':'none'}};" class="form-group">
                                    <label>Revised Budget: &nbsp; &nbsp;</label>
                                    <input type="text" style="background-color: rgba(162,166,166,0.87);"

                                           class="form-control bugdet_revised number_formated_two text-right" name="bugdet_revised" value="{{ $project->bugdet_revised }}"
                                           autocomplete="off" @if($project->is_approved == 1)

                                           @elseif($project->is_approved == 2)

                                           readonly
                                           @else readonly @endif>
                                </div>
                            @endif
                            <div class="form-group">
                                <label>Project Currency: &nbsp;</label>
                                <select class="form-control"
                                        style="background-color: #FFCCCB; @if($project->is_approved == 1) pointer-events: none; @endif"
                                        name="currency_id">
                                    @foreach(\App\Helpers\Helper::currency() as $cur)
                                        <option value="{{$cur->id}}" {{$project->currency_id==$cur->id?'selected':''}}>{{ucfirst($cur->currency_name)}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Project Donor: &nbsp;</label>
                                <select name="donor_name" id="donor_name" class="form-control"
                                        style="background-color: #FFCCCB !important; @if($project->is_approved == 1) pointer-events: none; @endif">
                                    <option value="{{$cur->id}}" {{$project->donor_name==$cur->id?'selected':''}}
                                    >KFW
                                    </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Province &nbsp;</label>
                                @if(!is_null($project->hasOneProvinceRegion->id))
                                    @if($provinceTransactionCount > 0 )
                                        <select name="province_id" id="province_id"
                                                    class="form-control project_province_select "
                                                style="background-color: #FFCCCB !important; pointer-events: none; @if($project->is_approved == 1) pointer-events: none; @endif">
                                            <option value="">Select</option>
                                            @foreach(\App\Helpers\Helper::province() as $pro)
                                                <option value="{{$pro->id}}" {{$project->hasOneProvinceRegion->id==$pro->id?'selected':''}}>{{$pro->name}}</option>
                                            @endforeach
                                        </select>
                                    @else
                                        <select name="province_id" id="province_id"
                                                class="form-control project_province_select "
                                                style="background-color: #FFCCCB !important; @if($project->is_approved == 1) pointer-events: none; @endif"
                                                required>
                                            <option value="">Select</option>
                                            @foreach(\App\Helpers\Helper::province() as $pro)
                                                <option value="{{$pro->id}}" {{$project->hasOneProvinceRegion->id==$pro->id?'selected':''}}>{{$pro->name}}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                @endif
                            </div>
                            <div class="form-group">
                                <label>PEA &nbsp;</label>
                                @if($provinceTransactionCount > 0 )
                                    <select name="pea_id" id="pea_id" class="form-control project_province_pea"
                                            style="background-color: #FFCCCB !important; pointer-events: none; @if($project->is_approved == 1) pointer-events: none; @endif">

                                        @if(!is_null($project->hasOneProvinceRegion))
                                            @if(!is_null($project->hasOnePea))
                                                @foreach(\App\Helpers\Helper::showPEAsWithinProvince($project->hasOneProvinceRegion->id) as $pro)
                                                    <option value="{{$pro->id}}" {{$project->hasOnePea->id==$pro->id?'selected':''}}>{{$pro->pea_name}}</option>
                                                @endforeach
                                            @else
                                                <option value="">unAllocated</option>
                                            @endif
                                        @else
                                            <option value="">unAllocated</option>
                                        @endif

                                    </select>
                                @else
                                    <select name="pea_id" id="pea_id" class="form-control project_province_pea"
                                            style="background-color: #FFCCCB !important; @if($project->is_approved == 1) pointer-events: none; @endif">

                                        @if(!is_null($project->hasOneProvinceRegion))
                                            @if(!is_null($project->hasOnePea))
                                                @foreach(\App\Helpers\Helper::showPEAsWithinProvince($project->hasOneProvinceRegion->id) as $pro)
                                                    <option value="{{$pro->id}}" {{$project->hasOnePea->id==$pro->id?'selected':''}}>{{$pro->pea_name}}</option>
                                                @endforeach
                                            @else
                                                <option value="">unAllocated</option>
                                            @endif
                                        @else
                                            <option value="">unAllocated</option>
                                        @endif

                                    </select>
                                @endif
                            </div>


                            {{--@if($project->is_approved != 1)--}}
                            {{--<div class="form-group">--}}

                            <input type="hidden" class="" name="status" value="{{$project->status}}">
                            {{--</div>--}}
                            {{--@endif--}}
                        </div>
                        <div class=" col-1"></div>

                        <div class="col-6">
                            <label>Project Duration:</label>
                            <div style="border: 1px solid red;padding: 15px">
                                <div class="row">
                                    <div class="col-6">
                                        <label>Planned Start Date:<span class="text-danger">*</span></label>

                                        <input placeholder="dd-mm-yyyy" name="planned_start_date"
                                               id="planned_start_date" class="form-control planned_start_date"
                                               value="{{$project->planned_start_date? \Carbon\Carbon::parse($project->planned_start_date)->format('d-M-Y'):''}}" required>

                                    </div>
                                    <div class="col-6">
                                        <label>Planned Completion Date:<span class="text-danger">*</span></label>
                                        <input placeholder="dd-mm-yyyy" name="planned_end_date"
                                               id="planned_end_date" class="form-control planned_end_date"
                                               value="{{$project->planned_end_date? \Carbon\Carbon::parse($project->planned_end_date)->format('d-M-Y'):''}}" required>

                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-6">
                                        <label>Actual Start Date:<span class="text-danger">*</span></label>

                                        <input placeholder="dd-mm-yyyy" name="project_start_date"
                                               id="project_start_date" class="form-control project_start_date"
                                               value="{{$project->project_start_date? \Carbon\Carbon::parse($project->project_start_date)->format('d-M-Y'):''}}" required>
                                    </div>
                                    <div class="col-6">
                                        <label>Current Expected Completion Date:<span class="text-danger">*</span></label>

                                        <input placeholder="dd-mm-yyyy" name="revised_expected_completion_date"
                                               id="revised_expected_completion_date"
                                               class="form-control revised_expected_completion_date"
                                               value="{{$project->revised_expected_completion_date? \Carbon\Carbon::parse($project->revised_expected_completion_date)->format('d-M-Y'):''}}" required>

                                        <br>
                                        <label>Actual Completion Date:<span class="text-danger">*</span></label>

                                        <input placeholder="dd-mm-yyyy" name="project_end_date"
                                               id="project_end_date" class="form-control project_end_date"
                                               value="{{$project->project_end_date? \Carbon\Carbon::parse($project->project_end_date)->format('d-M-Y'):''}}" required>


                                    </div>
                                </div>
                            </div>
                            <br>


                            <label>Project Details:</label>
                            <div style="border: 1px solid red;padding: 15px">
                                <div class="form-group">
                                    <label>Project :</label>
                                    <textarea type="text" name="project_description" id="project_description"
                                              class="form-control" placeholder="Description"
                                              style="font-size: 12px !important;"
                                              @if($project->is_approved == 1)
                                              readonly
                                              @elseif($project->is_approved == 2)

                                              readonly
                                            @endif>{{$project->project_description}}</textarea>
                                </div>
                                <div class="form-group">
                                    @if($project->status != 2)

                                        <button type="button" class="add-project-file_edit">
                                            Add File
                                            <li class="fa fa-file"></li>
                                        </button>
                                    @endif
                                    <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                        <table class="project_file_edit  table-bordered"
                                               style="width:100%;margin-bottom: 0px">
                                            <thead>
                                            <tr>
                                                <th style="width:5%"></th>
                                                <th style="width: 30%">File</th>
                                                <th style="width: 15%">Category</th>
                                                <th style="width: 55%">Description</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($project->hasProjectFile as $file)
                                                <tr>
                                                    {{--{{$file}}--}}
                                                    <td valign="top"><input data-id="{{$file->id}}" type='checkbox'
                                                                            name='record'>
                                                    </td>
                                                    <td valign="top">
                                                        @if( !empty($file->file))
                                                            <a style='float:left'
                                                               href="{{ URL::to( '/files/project/' . $file->file)  }}"
                                                               target="_blank" download> {{$file->file}}
                                                                <i class=" fa-lg fas fa-file-download"></i>
                                                            </a>
                                                        @endif
                                                    </td>
                                                    <td valign="top" style="text-align: center">
                                                        <span>{{$file->category}}</span></td>
                                                    <td valign="top" style="text-align:left">
                                                        <span>{{$file->description}}</span>
                                                    </td>
                                                    <td valign="top" style="text-align:center"></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    @if($project->status!=2)
                                        <button type="button" class="delete-project-file_edit"
                                                onclick="return confirm('Are you sure?');">
                                            Delete Row
                                        </button>
                                    @endif
                                </div>
                            </div>
                            <br>
                            <label style="display:none;">Total Budget:
                            </label>
                            <div class="input-group mb-3" style="display:none;">
                                <div class="input-group-prepend" style="display:none;">
                                    <button type="button" class="btn btn-default"
                                            style=" height: 30px;padding-top: 3px !important;">EUR
                                    </button>
                                </div>
                                <input type="text" style="text-align:right"
                                       class="form-control total_budget number_formated_two"
                                       value="{{($project->budget)}}"
                                       readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card card-outline card-primary">
                <div class="card-body">
                    <form>
                        @if($project->status!=2)
                            @if($project->is_approved == 1)
                                <button type="button" class="add-row-edit-revised" data-status="{{ $project->status || $historyCount > 0 ? 'true' : 'false' }}">
                                    <li class="fa fa-plus"></li>
                                </button>
                            @else
                                @if($project->status==0)
                                    <button type="button" class="add-row-edit">
                                        <li class="fa fa-plus"></li>
                                    </button>
                                @endif
                            @endif
                        @endif

                    </form>
                    <table class="project_province_edit table table-sm">
                        <thead>
                        <tr>
                            <th style="text-align:center;width: 2%!important;vertical-align: middle;"></th>
                            <th>Measures/Line Items</th>
                            <th>Sub-Category</th>
                            <th>Budget
                                <br>(Per Agreement)
                            </th>
                            @if($project->status || $historyCount > 0)
                                <th>
                                    Revised Budget
                                    @if($project->is_approved==1)
                                        As On
                                        <input placeholder="dd-mm-yyyy" type="text" name="revise_date" id="revise_date" class="revise_date form-control"
                                               value="{{$revisedDate? \Carbon\Carbon::parse($revisedDate)->format('d-M-Y'):null}}"
                                               required>
                                    @endif
                                </th>
                            @endif

                            <th style="text-align:center;width: 35%!important;vertical-align: middle;">Description</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($collection->sortBy('id') as $pro)
                            <tr>

                                <td><input type='checkbox' name='record' data-id="{{$pro->id}}">
                                    <input type='hidden' name='measure_id[]' value="{{$pro->id}}">
                                </td>
                                <td><select name="measure[]" id="measure" class="measureSelect form-control" style="background-color: #FFCCCB;">
                                        <option>Select</option>
                                        @if(!is_null($pro->hasOneMeasures))
                                            @foreach(\App\Helpers\Helper::Measures() as $measure)
                                                <option value="{{$measure->id}}" {{$pro->hasOneMeasures->id==$measure->id?'selected':''}}>{{$measure->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </td>
                                <td>
                                    <select class='sub_cate form-control' id='sub_cate' name='sub_cate[]'>
                                        <option value="">Select</option>
                                        @if(!is_null($pro->hasOneSubCategories))
                                            @foreach(\App\Helpers\Helper::findSubCategory($pro->hasOneMeasures->id) as $sub)
                                                <option value="{{$sub->id}}" {{$pro->hasOneSubCategories->id==$sub->id?'selected':''}}>{{$sub->name}}</option>
                                            @endforeach
                                        @else
                                            @if(!is_null($pro->hasOneMeasures))
                                                @foreach(\App\Helpers\Helper::findSubCategory($pro->hasOneMeasures->id) as $sub)
                                                    <option value="{{$sub->id}}">{{$sub->name}}</option>
                                                @endforeach
                                            @endif
                                        @endif
                                    </select>
                                </td>
                                <td><input type='text' name='provinceBudgetAmount[]'
                                           class='form-control provinceBudgetAmount number_formated_two text-right'
                                            value="{{$pro->budget}}"


                                           style=' background-color: #FFCCCB;'
                                           @if($project->status || $historyCount > 0) readonly @endif>
                                </td>
                                @if($project->status || $historyCount > 0)
                                    <td>

                                        <input type='text' name='provinceReviseAmount[]'
                                               class='form-control provinceReviseAmount  number_formated_two text-right'
                                               value="{{$pro->revise_amount}}"
                                               {{--                                           @if($project->is_approved != 1)--}}
                                               {{--                                           readonly--}}
                                               {{--                                           @if((double)$pro->revise_amount == 0)--}}
                                               {{--                                           value="{{$pro->revise_amount}}"--}}

                                               {{--                                           @else--}}
                                               {{--                                           value="{{$pro->revise_amount}}"--}}

                                               {{--                                           @endif--}}
                                               {{--                                           @else--}}
                                               {{--                                           --}}
                                               {{--                                           @endif--}}


                                               style=' background-color: #FFCCCB; '
                                               @if($project->is_approved != 1)
                                               readonly

                                                @endif>
                                    </td>
                                @endif
                                <td><input type='text' name='descriptionHasProvince[]' class='form-control '
                                           value="{{$pro->description}}"

                                           @if($project->status==1)

                                           @if($project->is_approved == 1)
                                           @else readonly
                                            @endif

                                    @else  @endif
                                    >
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th style="text-align:right !important;vertical-align:middle !important">
                                <label>Total</label></th>
                            <!--<th>Allocated Amount</th>-->
                            <th>
                                <input type="text" class="form-control province_total_amount font-weight-bold"
                                       name="province_total_amount" 
                                       dir="rtl"
                                       readonly />
                            </th>
                            @if($project->status || $historyCount > 0)
                                <th>
                                    <input type="text" class="form-control revised_total_amount font-weight-bold"
                                           name="revised_total_amount" 
                                           dir="rtl"
                                           readonly />
                                </th>
                            @endif
                            <th> 
                                @if($project->is_approved == 1)
                                   
                                    @if($project->status ||  $historyCount > 0) 
                                        @if($project->status==1)
                                            <label>Approval File: &nbsp;</label>
                                            <input type="file" name="ApprovelbudgetFile" class="float-right" required />
                                        @else
                                            @if($project->approval_file)
                                                <a style='text-align: center'
                                                   href="{{ URL::to( '/files/project/approval/' . $project->approval_file)  }}"
                                                   target="_blank"
                                                   download="{{ $project->approval_file_title }}"> {{$project->approval_file_title}} <i class=" fa-lg fas fa-file-download"></i>
                                                </a>
                                            @endif
                                            <br>
                                            <input type="file" name="ApprovelbudgetFile" class="float-right" required />
                                        @endif
                                   @endif

                                @elseif($project->is_approved==0)


                                @endif</th>
                        </tr>
                        <!--<tr>-->
                        <!--    <th></th>-->
                        <!--    <th></th>-->
                        <!--    <th>Unallocated Amount</th>-->
                        <!--    <th><input type="text" class="form-control province_unallocated_amount "-->
                        <!--               style="width: 150px;font-weight: bold;" readonly="" dir="rtl" value=""></th>-->
                        <!--    <th><input type="text" class="form-control province_revised_unallocated_amount"-->
                        <!--               style="width: 150px;font-weight: bold;" readonly="" dir="rtl" value=""></th>-->
                        <!--    <th></th>-->
                        <!--</tr>-->
                        <!--<tr>-->
                        <!--    <th></th>-->
                        <!--    <th></th>-->
                        <!--                                <th style="text-align:right !important;vertical-align:middle !important">Total</th>-->

                        <!--    <th><input type="text" class="form-control province_total_budget_amount "-->
                        <!--               style="width: 150px;font-weight: bold;" readonly="" dir="rtl" value=""></th>-->
                        <!--    <th><input type="text" class="form-control province_revised_total_budget_amount "-->
                        <!--               style="width: 150px;font-weight: bold;" readonly="" dir="rtl" value=""></th>-->
                        <!--    <th></th>-->
                        <!--</tr>-->
                        </tfoot>
                    </table>


                    @if($project->is_approved == 1)

                        <button type="button" class="delete-row_project_province_edit" hidden>Delete Row</button>

                    @else
                        <button type="button" class="delete-row_project_province" hidden>Delete Row</button>
                    @endif


                    <br>
                </div>
            </div>
            {{--End Sub Category form--}}
            {{--Start Buttons--}}
            <div class="col">

                @if($project->is_approved ==1)
                    <button id="projectSubmint" type="submit" class="btn btn-block btn-success btn-lg"
                            style="float:right;"
                            data-contractid="0">Save
                    </button>
                    <button id="cancelall" type="button" class="btn btn-block btn-primary btn-lg" style="float:right;">
                        Cancel
                    </button>
                @elseif($project->is_approved ==0)
                    <button id="projectSubmint" type="submit" class="btn btn-block btn-success btn-lg"
                            style="float:right;"
                            data-contractid="0">Save
                    </button>
                    <button id="cancelall" type="button" class="btn btn-block btn-primary btn-lg" style="float:right;">
                        Cancel
                    </button>
                @endif
            </div>
        </div>
    </form>
    {{--End Buttons--}}
@endsection
