@extends('admin.layout.index')
@section('content')
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>--}}
    <style>
        label {
            font-size: 12px !important;
            color: #2a272a !important;
            margin-bottom: 0.1rem !important;
        }

        input {
            font-size: 14px !important;
            height: 30px !important;
        }

        select {
            font-size: 12px !important;
            height: 27px !important;
        }

        .form-group {
            margin-bottom: 0.5rem;
            padding-top: 5px !important;
        }

    </style>
    <section class="bg-primary content-header"
             style="background-color:#ffc533 !important; padding-bottom: 10px; padding-left: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Project</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li><i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li><i class="fa fa-angle-right" style="color: #ccc;"></i> Project &nbsp;</li>
                    <li class="active"><i class="fa fa-angle-right" style="color: #ccc;"></i> create</li>
                </ol>
            </div>
        </div>
    </section>
    {{--Start create form--}}
    <form method="post" action="{{route('project.store')}}" id="projectForm" enctype="multipart/form-data">
        @csrf
        <div class="form-row">
            <div class="col-md-12">
                <div class="card card-outline card-primary shadow mt-2 rounded-0">
                    <div class="card-header rounded-0">
                        <h3 class="card-title font-weight-bold">Create New Project</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body" style="display: block;">
                        <div class="row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label>Project Title:<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="title" autocomplete="off" onchange="checkAlreadyExists()" required>
                                </div>

                                <div class="form-group">
                                    <label>Project Donor Number:<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="donor_number" autocomplete="off" required>
                                </div>

                                <div class="form-group">
                                    <label>Budget (Per Agreement):<span class="text-danger">*</span></label>
                                    <input type="text" style="background-color: #FFCCCB;" class="form-control project_budget text-right" placeholder="0.000000" name="budget" autocomplete="off" required>
                                </div>
                                <!--<div class="form-group"><label>Revised Budget: &nbsp;</label>-->
                                <!--    <input type="text" style="background-color: rgba(162,166,166,0.87);"-->
                                <!--           oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"-->
                                <!--           class="form-control bugdet_revised" name="bugdet_revised" dir="rtl" value="0.00"-->
                                <!--           autocomplete="off"   readonly>-->
                                <!--</div>-->

                                <div class="form-group">
                                    <label>Project Currency:<span class="text-danger">*</span></label>
                                    <select class="form-control" style="background-color: #FFCCCB;" name="currency_id"
                                            required>
                                        @foreach(\App\Helpers\Helper::currency() as $cur)
                                            <option value="{{$cur->id}}">{{ucfirst ($cur->currency_name)}}</option>
                                        @endforeach
                                    </select>
                                </div>


                                <div class="form-group">
                                    <label>Project Donor:<span class="text-danger">*</span></label>
                                    <select name="donor_name" id="donor_name" class="form-control"
                                            style="background-color: #FFCCCB !important;" required>
                                        <option value="KFW">KFW</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Province:<span class="text-danger">*</span></label>
                                    <select name="province_id" id="province_id" class="form-control project_province_select"
                                            style="background-color: #FFCCCB !important;" required>
                                        <option value="">Select</option>
                                        @foreach(\App\Helpers\Helper::province() as $pro)
                                            <option value="{{$pro->id}}">{{$pro->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>PEA:<span class="text-danger">*</span></label>
                                    <select name="pea_id" id="pea_id" class="form-control project_province_pea"
                                            style="background-color: #FFCCCB !important;" required>
                                        <option>Select</option>
                                    </select>
                                </div>

                            </div>
                            <div class=" col-1"></div>
                           <div class="col-6">

                                <label>Project Duration:</label>
                                <div style="border: 1px solid red;padding: 15px">
                                    <div class="row">
                                        <div class="col-6">
                                            <label>Planned Start Date:<span class="text-danger">*</span></label>
                                            <input placeholder="dd-mm-yyyy" type="text" class="form-control planned_start_date" id="planned_start_date" name="planned_start_date" required>
                                        </div>
                                        <div class="col-6">
                                            <label>Planned Completion Date:<span class="text-danger">*</span></label>
                                            <input placeholder="dd-mm-yyyy" type="text" class="form-control planned_end_date" id="planned_end_date"   name="planned_end_date" required>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-6">
                                            <label>Actual Start Date:<span class="text-danger">*</span></label>
                                            <input placeholder="dd-mm-yyyy" type="text" class="form-control project_start_date" id="project_start_date" name="project_start_date" required>
                                        </div>
                                        
                                        <div class="col-6">
                                            <label>Current Expected Completion Date:<span class="text-danger">*</span></label>
                                            <input placeholder="dd-mm-yyyy" type="text" class="form-control revised_expected_completion_date" id="revised_expected_completion_date"   name="revised_expected_completion_date" required>
                                            
                                            <br>
                                            
                                            <label>Actual Completion Date:<span class="text-danger">*</span></label>
                                            <input placeholder="dd-mm-yyyy" type="text" class="form-control project_end_date" id="project_end_date" name="project_end_date" required>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <label>Project Details:</label>
                                <div style="border: 1px solid red;padding: 15px">
                                    <div class="form-group">
                                        <label>Project Description:</label>
                                        <textarea type="text"
                                                  name="project_description"
                                                  id="project_description"
                                                  class="form-control"
                                                  placeholder="Description"
                                        ></textarea>
                                    </div>
                                               <div class="form-group">
                                        <button type="button" class="add-project-file-create">
                                            Add File
                                            <li class="fa fa-file"></li>
                                        </button>
                                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                            <table class="project_file_create table-bordered" style="margin-bottom: 0px">
                                                <thead>
                                                <tr>
                                                    <th style="width:5%"></th>
                                                    <th>File</th>
                                                    <th>Category</th>
                                                    <th>Description</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td style="
        text-align: center;
    "><input type='checkbox' name='record'></td>
                                                    <td>
                                                        <input type="file" style="font-size:12px !important;" name="file[]">
                                                    </td>
                                                    <td>
                                                        <select class="form-control" style="font-size: 14px !important;
                                        height: 35px !important;width: 100px!important; " name="category[]" required>
                                                            <option>Select</option>
                                                            <option value="Contract">Contract</option>
                                                            <option value="Quotation">Quotation</option>
                                                            <option value="PO">PO</option>
                                                        </select></td>
                                                    <td style="text-align:center"><input style="   font-size: 14px !important;
                                        height: 35px !important;
                                    " type="text" class='form-control' name="description[]"></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <button type="button" class="delete-project-file" style="display: none">
                                            Delete Row
                                        </button>
                                    </div>
                                </div>
                                <br>
                                <label style="display:none;">Total Budget:
                                </label>
                                <div class="input-group mb-3"  style="display:none;">
                                    <div class="input-group-prepend">
                                        <button type="button" class="btn btn-default"
                                                style=" height: 30px;padding-top: 3px !important;">EUR
                                        </button>
                                    </div>
                                    <!-- /btn-group -->
                                    <input type="text" style="text-align:right" class="form-control project_budget"
                                           readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <div class="col-md-12">
                <div class="card card-outline card-primary">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form>
                            <button type="button" class="add-row">
                                <li class="fa fa-plus"></li>
                            </button>
                        </form>
                        <table class="project_province table table-sm">
                            <thead>
                            <tr>
                                <th style="text-align:center;width: 2%!important;vertical-align: middle;"></th>
                                <th>Measures/Line Items</th>
                                <th>Sub-Category</th>
                                <th>Budget Amount Per Agreement</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th><input type="text" class="form-control province_total_amount province_total_amount_hide" style="display: none;font-weight: bold;"
                                       readonly=""
                                       name="province_total_amount" dir="rtl" value=""></th>
                            <th></th>
                            </tfoot>
                        </table>
                        <button type="button" class="delete-row_project_province" style="display: none">Delete Row</button>
                        <br>

                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                {{--End Sub Category form--}}
                {{--Start Buttons--}}
                <div class="col">
                    <button id="projectSubmint" type="submit" class="btn btn-block btn-success btn-lg"
                            style="float:right;"
                            data-contractid="0">Save
                    </button>
                    <button id="cancelall" type="button" class="btn btn-block btn-primary btn-lg" style="float:right;">
                        Cancel
                    </button>
                </div>
            </div>
        </div>  
    </form>
    {{--End Buttons--}}
@endsection
