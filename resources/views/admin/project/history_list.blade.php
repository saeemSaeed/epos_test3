@extends('admin.layout.index')
@section('content')
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>--}}
    <style>
        label {
            font-size: 12px !important;
            color: #2a272a !important;
            margin-bottom: 0.1rem !important;
        }

        input {
            font-size: 14px !important;
            height: 30px !important;
        }

        select {
            font-size: 12px !important;
            height: 27px !important;
        }

        .form-group {
            margin-bottom: 0.5rem;
            padding-top: 5px !important;
        }

    </style>
    <section class="bg-primary content-header"
             style="background-color:#ffc533 !important; padding-bottom: 10px; padding-left: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Project</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li><i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li><i class="fa fa-angle-right" style="color: #ccc;"></i> Project &nbsp;</li>
                    <li class="active"><i class="fa fa-angle-right" style="color: #ccc;"></i> History</li>
                </ol>
            </div>
        </div>
    </section>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Project History</h3>

                <a href="{{route('project.edit',[$project_id])}}" class=" btn btn-primary" style="float: right">
                    <h3 class="card-title" ><i class="fa fa-arrow-left"></i>  Back</h3></a>


        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                <tr  style="background: dimgrey;color: white">
                    <th style="width: 10px">ID</th>
                    <th>Title</th>
                    <th>Attachment</th>
                    <th>Revise Date</th>
                    <th>Created Date</th>
                    <th style="width: 40px">View</th>
                </tr>
                </thead>
                <tbody>

                <tr style="background: #a2a6a6;color: #000000; text-align:center;">
                    <td style="text-align: center">1</td>
                    <td style="text-align: center">{{$activeProject->title}}</td>
                    <td style="text-align: center">  @if($activeProject->approval_file)<a style='float:left'
                                   href="{{ URL::to( '/files/project/approval/' . $activeProject->approval_file)  }}"
                                   target="_blank" download="{{ $activeProject->approval_file_title }}"> {{$activeProject->approval_file_title}}
                                    <i class=" fa-lg fas fa-file-download"></i>
                                </a>
                            @endif</td>
                  <td style="text-align: center">
                            {{ (\App\Model\ProjectsHasProvince::
                            where('project_id',  $activeProject->id)->pluck('revise_date')
                            ->first())
                            ?
                            date('d-M-Y',strtotime(\App\Model\ProjectsHasProvince::
                            where('project_id',  $activeProject->id)->pluck('revise_date')
                            ->first())):''

                            }}
                    </td>
                    <td style="text-align: center">
                        {{date('d-M-Y a', strtotime($activeProject->updated_at))}}
                    </td>
                    <td><a class="badge bg-success"  href="{{route('project.edit',[
                        'project_id'=>$project_id
                        ])}}" >Show</a></td>
                </tr>
                @foreach($historyProject as $index=>$pro)
                    <tr>
                        <td style='text-align: center'>{{2+$index}}</td>
                        <td style="text-align: center">{{$pro->title}}</td>
                        <td>
                                @if($pro->approval_file)
                                <a style='float:left'
                                   href="{{ URL::to( '/files/project/approval/' . $pro->approval_file)  }}"
                                   target="_blank" download="{{$pro->approval_file_title }}"> {{$pro->approval_file_title}}
                                    <i class=" fa-lg fas fa-file-download"></i>
                                </a>
                                @endif
                            </td>
  <td  style="text-align: center">
                            {{ (\App\Model\projectHasProvinceHistory::
                            where('row_id',  $pro->id)->pluck('revise_date')
                            ->first())
                            ?
                            date('d-M-Y ',strtotime(\App\Model\projectHasProvinceHistory::
                            where('row_id',  $pro->id)->pluck('revise_date')
                            ->first())):''

                            }}

                        </td>

                        <td style="text-align: center">
                       {{date('d-M-Y h:i:s a ', strtotime($pro->updated_at))}}
                        </td>
                        <td><a class="badge bg-success" href="{{route('project.history.show',[

                        'id'=>$pro->id,
                        'project_id'=>$project_id
                        ])}}">Show</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
        {{--<div class="card-footer clearfix">--}}
            {{--<ul class="pagination pagination-sm m-0 float-right">--}}
                {{--<li class="page-item"><a class="page-link" href="#">«</a></li>--}}
                {{--<li class="page-item"><a class="page-link" href="#">1</a></li>--}}
                {{--<li class="page-item"><a class="page-link" href="#">2</a></li>--}}
                {{--<li class="page-item"><a class="page-link" href="#">3</a></li>--}}
                {{--<li class="page-item"><a class="page-link" href="#">»</a></li>--}}
            {{--</ul>--}}
        {{--</div>--}}
    </div>
@endsection