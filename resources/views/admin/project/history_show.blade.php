@extends('admin.layout.index')
@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <style>
        label {
            font-size: 12px !important;
            color: #2a272a !important;
            margin-bottom: 0.1rem !important;
        }

        input {
            font-size: 14px !important;
            height: 30px !important;
        }

        select {
            font-size: 12px !important;
            height: 27px !important;
        }

        .form-group {
            margin-bottom: 0.5rem;
            padding-top: 5px !important;
        }
    </style>
    <section class="bg-primary content-header"
             style="background-color:#ffc533 !important; padding-bottom: 10px; padding-left: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Project</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li><i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li><i class="fa fa-angle-right" style="color: #ccc;"></i> Project &nbsp;</li>
                    <li class="active"><i class="fa fa-angle-right" style="color: #ccc;"></i> View</li>
                </ol>
            </div>
        </div>
    </section>
    {{--Start create form--}}

    <input type="hidden" value="{{$project->id}}" name="id">

    <div class="col" style="margin-top: 10px">
        <div class="card card-outline card-primary">
            <div class="card-header">
                <h3 class="card-title">View Project</h3>

                <a href="{{route('project.history.list',[$project_id])}}" class=" btn btn-primary" style="float: right">
                    <h3 class="card-title"><i class="fa fa-arrow-left"></i> Back</h3></a>
            {{--<div class="card-tools">--}}
            {{--<button type="button" class="btn btn-tool" data-card-widget="collapse">--}}
            {{--<i class="fas fa-minus"></i>--}}
            {{--</button>--}}
            {{--</div>--}}
            <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->

            <div class="card-body" style="display: block;">
                <div class="row">
                    <div class="col-5">
                        <div class="form-group"><label>Project Title: &nbsp;</label>
                            <input type="text" style="background-color: #FFCCCB;" class="form-control"
                                   value="  {{$project->title}}" name="title" readonly>
                        </div>
                        <div class="form-group"><label>Project Donor Number: &nbsp;</label>
                            <input type="text" class="form-control" name="donor_number"
                                   value="  {{$project->donor_number}}" readOnly>
                        </div>

                        <div class="form-group"><label>Budget (Per Agreement): &nbsp;</label>
                            <input type="text" style="background-color: #FFCCCB;"
                                   oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"
                                   class="form-control project_budget number_formated_two" name="budget" dir="rtl"
                                   readonly value="{{($project->budget)}}">
                        </div>
                        <div class="form-group"><label>Revised Budget: &nbsp;</label>
                            <input type="text" style="background-color: rgba(162,166,166,0.87);"
                                   oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"
                                   class="form-control project_revised_budget" name="project_revised_budget" dir="rtl"
                                   value="0.00"
                                   readonly>
                        </div>
                        <div class="form-group">
                            <label>Project Currency: &nbsp;</label>
                            <select class="form-control" style="background-color: #FFCCCB;" name="currency_id" disabled>

                                @foreach(\App\Helpers\Helper::currency() as $cur)

                                    <option value="{{$cur->id}}" {{$project->currency_id==$cur->id?'selected':''}}>{{$cur->currency_name}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group">
                            <label>Project Donor: &nbsp;</label>
                            <select name="donor_name" id="donor_name" class="form-control"
                                    style="background-color: #FFCCCB !important;" disabled>
                                <option value="{{$cur->id}}" {{$project->donor_name==$cur->id?'selected':''}}
                                >KFW
                                </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Province &nbsp;</label>
                            <select name="province_id" id="province_id" class="form-control project_province_select "
                                    style="background-color: #FFCCCB !important;" disabled>
                                <option value="">Select</option>
                                @foreach(\App\Helpers\Helper::province() as $pro)
                                    <option value="{{$pro->id}}" {{$project->hasOneProvinceRegion->id==$pro->id?'selected':''}}>{{$pro->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>PEA &nbsp;</label>
                            <select name="pea_id" id="pea_id" class="form-control project_province_pea"
                                    style="background-color: #FFCCCB !important;" disabled>
                                {{--                                    {{$project->hasOnePea->name}}--}}
                                @if(!is_null($project->hasOneProvinceRegion))
                                    @if(!is_null($project->hasOnePea))
                                        @foreach(\App\Helpers\Helper::showPEAsWithinProvince($project->hasOneProvinceRegion->id) as $pro)
                                            <option {{$project->hasOnePea->id==$pro->id?'selected':''}}>{{$project->hasOnePea->pea_name}}</option>
                                        @endforeach
                                    @else
                                        <option>unAllocated</option>
                                    @endif
                                @else
                                    <option>unAllocated</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class=" col-1"></div>
                    <div class="col-6">
                        <label>Project Duration:</label>
                        <div style="border: 1px solid red;padding: 15px">
                            <div class="row">
                                <div class="col-6">
                                    <label>Planned Start Date</label>


                                    <input placeholder="dd-mm-yy"

                                           value="{{$project->planned_start_date? \Carbon\Carbon::parse($project->planned_start_date)->format('d-M-y'):''}}"
                                           type="text" class="form-control datetimepicker-input" data-target="#"
                                           name="planned_start_date" readonly>

                                </div>
                                <div class="col-6">
                                    <label>Planned Completion Date</label>

                                    <input placeholder="dd-mm-yy" type="text"
                                           class="form-control"
                                           value="{{$project->planned_end_date?\Carbon\Carbon::parse($project->planned_end_date)->format('d-M-y'):''}}"
                                           name="planned_end_date" readonly>


                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-6">
                                    <label>Actual Start Date</label>

                                    <input placeholder="dd-mm-yy" type="text"
                                           class="form-control "

                                           value="{{$project->project_start_date?\Carbon\Carbon::parse($project->project_start_date)->format('d-M-y'):''}}"
                                           readonly
                                           data-target="#project_start_date" name="project_start_date">

                                </div>
                                <div class="col-6">
                                    <label>Current Expected Completion Date</label>

                                    <input placeholder="dd-mm-yy" type="text"
                                           class="form-control datetimepicker-input"
                                           value="{{$project->revised_expected_completion_date?\Carbon\Carbon::parse($project->revised_expected_completion_date)->format('d-M-y'):''}}"

                                           data-target="#revised_expected_completion_date"
                                           name="revised_expected_completion_date" readonly>


                                    <br>
                                    <label>Actual Completion Date</label>

                                    <input placeholder="dd-mm-yy" type="text"
                                           value="{{$project->project_end_date?\Carbon\Carbon::parse($project->project_end_date)->format('d-M-y'):''}}"

                                           readonly
                                           class="form-control " data-target="#project_end_date"
                                           name="project_end_date">

                                </div>

                            </div>
                        </div>

                    <br>
                    <div style="border: 1px solid red;padding: 15px">
                        <div class="form-group">
                            <label>Project Details:
                            </label><br>
                            <label>Project Description:</label>
                            <textarea readonly type="text"
                                      name="project_description"
                                      id="project_description"
                                      class="form-control"
                                      placeholder="Description" style="font-size: 12px !important;"
                            >{{$project->project_description }}</textarea>
                        </div>

                        <div class="form-group">
                            <div class="table-wrapper-scroll-y my-custom-scrollbar">

                                <table class="project_file_edit table-bordered" style="width:100%;margin-bottom: 0px">
                                    <thead>
                                    <tr>
                                        <th style="width:5%"></th>
                                        <th style="width: 30%">File</th>
                                        <th style="width: 15%">Category</th>
                                        <th style="width: 55%">Description</th>
                                        <th></th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($project->hasProjectFile as $file)
                                        <tr>
                                            {{--{{$file}}--}}
                                            <td valign="top"><input data-id="{{$file->id}}" type='checkbox'
                                                                    name='record'>
                                            </td>
                                            <td valign="top">
                                                @if( !empty($file->file))
                                                    <a style='float:left'
                                                       href="{{ URL::to( '/files/project/' . $file->file)  }}"
                                                       target="_blank" download> {{$file->file}}
                                                        <i class=" fa-lg fas fa-file-download"></i>
                                                    </a>
                                                @endif

                                            </td>
                                            <td valign="top" style="text-align: center"><span>{{$file->category}}</span>
                                            </td>

                                            <td valign="top" style="text-align:left"><span>{{$file->description}}</span>
                                            </td>
                                            <td style="text-align:center"></td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                    <br>
                    <label>Total Budget:
                    </label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <button type="button" class="btn btn-default"
                                    style=" height: 30px;padding-top: 3px !important;">EUR
                            </button>
                        </div>
                        <!-- /btn-group -->
                        <input type="text" class="form-control total_budget number_formated_two"
                               style="text-align:right" value="{{($project->budget)}}"
                               readonly>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>

    <!-- /.card -->
    </div>

    <div class="col">
        <div class="card card-outline card-primary">
            <!-- /.card-header -->
            <div class="card-body">
                <form>


                </form>
                <table class="project_province table table-sm" id="content">
                    <thead>
                    <tr>
                        <th style="text-align:center;width: 2%!important;vertical-align: middle;"></th>

                        <th style="text-align:center;width: 20%!important;vertical-align: middle;">Measures/Line Items
                        </th>
                        <th style="text-align:center;width: 20%!important;vertical-align: middle;">Sub-Category</th>
                        <th style="text-align:center;width: 150px!important;vertical-align:middle !important">Budget
                            (Per Agreement)
                        </th>
                        <th style="text-align:center;width: 150px!important;vertical-align:middle !important">Revised
                            Budget

                            As On
                            <input placeholder="dd-mm-yy"
                                   value="{{$revisedDate? \Carbon\Carbon::parse($revisedDate)->format('d-M-y'):null}}"
                                   type="text" class="form-control revise_date"
                                   id="revise_date" name="revise_date" readonly
                                    {{--@if($project->status==1)--}}

                                    {{--@else--}}
                                    {{--readonly--}}
                                    {{--@endif--}}
                            >

                        </th>

                        <th style="text-align:center;width: 35%!important;vertical-align: middle;">Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($collection->sortBy('measure_code') as $pro)


                        <tr>
                            <td><input type='checkbox' name='record'></td>


                            <td><select name="measure[]" id="measure" class="measureSelect form-control" disabled
                                        style="background-color: #FFCCCB;">
                                    @if(!is_null($pro->hasOneMeasures))
                                        @foreach(\App\Helpers\Helper::Measures() as $measure)
                                            <option value="{{$measure->id}}" {{$pro->hasOneMeasures->id==$measure->id?'selected':''}}>{{$measure->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </td>
                            <td>
                                <select class='sub_cate form-control' id='sub_cate' name='sub_cate[]' disabled
                                        style=''>


                                    @if(!is_null($pro->hasOneSubCategories))

                                        @foreach(\App\Helpers\Helper::findSubCategory($pro->hasOneMeasures->id) as $sub)
                                            <option value="{{$sub->id}}" {{$pro->hasOneSubCategories->id==$sub->id?'selected':''}}>{{$sub->name}}</option>
                                        @endforeach

                                    @else
                                        @if(!is_null($pro->hasOneMeasures))
                                            <option value="">unallocated name</option>
                                            @foreach(\App\Helpers\Helper::findSubCategory($pro->hasOneMeasures->id) as $sub)
                                                <option value="{{$sub->id}}">{{$sub->name}}</option>
                                            @endforeach
                                        @endif
                                    @endif

                                </select>
                            </td>
                            <td><input type='text' name='provinceBudgetAmount[]'
                                       class='form-control provinceBudgetAmount number_formated_two' disabled
                                       dir='rtl' value="{{($pro->budget)}}"

                                       style=' background-color: #FFCCCB;'>
                            </td>
                            <td><input type='text' name='provinceBudgetAmount[]'
                                       class='form-control provinceReviseAmount  number_formated_two'
                                       dir='rtl'
                                       @if((double)$pro->revise_amount == 0)
                                       value="{{$pro->budget}}"

                                       @else
                                       value="{{$pro->revise_amount}}"

                                       @endif
                                       readonly

                                       oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"
                                       style=' background-color: #FFCCCB;'>
                            </td>

                            <td><input type='text' name='description[]' class='form-control' readonly
                                       value="{{$pro->description}}">
                            </td>

                        </tr>

                    @endforeach

                    </tbody>

                    <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th style="text-align:right !important;vertical-align:middle !important">Total</th>
                        <th><input type="text" class="form-control province_total_amount "
                                   style="width: 150px;font-weight: bold;"
                                   readonly=""
                                   name="province_total_amount" dir="rtl" value=""></th>
                        <th><input type="text" class="form-control revised_total_amount "
                                   style="width: 150px;font-weight: bold;"
                                   readonly=""
                                   name="revised_total_amount" dir="rtl" value=""></th>
                        <th></th>
                    </tr>
                    <!--<tr>-->
                    <!--    <th></th>-->
                    <!--    <th></th>-->
                    <!--    <th>Unallocated Amount</th>-->
                    <!--    <th><input type="text" class="form-control province_unallocated_amount " style="width: 150px;font-weight: bold;" readonly="" dir="rtl" value=""></th>-->
                    <!--    <th><input type="text" class="form-control province_revised_unallocated_amount" style="width: 150px;font-weight: bold;" readonly="" dir="rtl" value=""></th>-->
                    <!--    <th></th>-->
                    <!--</tr>-->
                    <!--<tr>-->
                    <!--    <th></th>-->
                    <!--    <th></th>-->
                    <!--                            <th style="text-align:right !important;vertical-align:middle !important">Total</th>-->

                    <!--    <th><input type="text" class="form-control province_total_budget_amount " style="width: 150px;font-weight: bold;" readonly="" dir="rtl" value=""></th>-->
                    <!--    <th><input type="text" class="form-control province_revised_total_budget_amount " style="width: 150px;font-weight: bold;" readonly="" dir="rtl" value=""></th>-->
                    <!--    <th></th>-->
                    <!--</tr>-->
                    </tfoot>
                </table>

                <br>

                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    {{--End Sub Category form--}}
    {{--Start Buttons--}}


    <script>

    </script>

    {{--End Buttons--}}
@endsection
