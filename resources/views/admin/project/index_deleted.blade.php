@extends('admin.layout.index')
@section('content')
    <section class="bg-primary content-header"
             style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Projects</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li>
                        <i class="fa fa-dashboard"></i> Dashboard  
                    </li>
                    <li>
                        <i class="fa fa-angle-right" style="color: #ccc;"></i> Projects  
                    </li>

                </ol>
            </div>
        </div>
    </section>
   
    <div class="container-fluid" style="margin-top: 10px">
        <div class="card">

            <div class="card-body">
                <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12 col-md-6"></div>
                        <div class="col-sm-12 col-md-6"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="project_table"
                                   class="table table-bordered table-hover dataTable dtr-inline project_table"
                                   role="grid" aria-describedby="example2_info">
                                <thead style="background-color: #65a3c6;color: #2c2c2c">
                                <tr role="row" style="    background-color: #65a3c6;
                                       color: #2c2c2c;">
                                    <th>ID</th>
                                    <th>Status</th>
                                    <th>Project Name</th>
                                    <th>Project Budget</th>
                                    <th>Project Currency</th>
                                    <th>Project Start Date</th>
                                    <th>Project End Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($deletedProjects as $index=>$project)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1 dtr-control" style='text-align: center'>{{++$index}}</td>
                                        <td class="sorting_1 dtr-control">
                                            @if($project->status===0)
                                                <span class="badge badge-warning "> Pending </span>
                                            @elseif($project->status===1)
                                                <span class="badge badge-success "> Approved </span>
                                            @elseif($project->status===2)
                                                <span class="badge badge-danger"> Rejected </span>

                                            @endif
                                        </td>
                                        <td class="sorting_1 dtr-control">{{$project->title}}</td>
                                        <td class="sorting_1 dtr-control number_formated_two"
                                            style="text-align:right;">{{($project->budget)}}</td>
                                   <td class="sorting_1 dtr-control" style="text-align: center!important;">{{isset($project->hasCurrency->currency_name)?$project->hasCurrency->currency_name:''}} </td>
                                 <td class="sorting_1 dtr-control" style="text-align: center!important;">{{$project->project_start_date?date('d-M-Y', strtotime($project->project_start_date)):''}}</td>
                                    <td class="sorting_1 dtr-control" style="text-align: center!important;">{{$project->project_end_date?date('d-M-Y', strtotime($project->project_end_date)):''}}</td>
                                        <td class="sorting_1 dtr-control" style="text-align: center!important;">
                                             <a type="submit" style="border: none;background-color: transparent" data-toggle="tooltip" title="" href="{{route('project.restore',['id'=>$project->id])}}" data-original-title="Restore"><i class="fas fa-trash-restore"></i></a>

                                        </td>
                                      
                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
@endsection