<script>
    console.log(1);
    $(document).ready(function () {
        console.log(1);

        $('.project_budget').on('change', function () {

            $('.project_budget').val($.number($(this).val(), 2));
            $('.total_budget').val($.number($(this).val(), 2));


        });
        $('#projectFormEdit').submit(function () {

                project_budget = parseFloat($('.project_budget').val().split(",").join(""));
                province_total_amount = parseFloat($('.province_total_amount').val().split(",").join(""));
                if (project_budget < province_total_amount) {
                    swal("Project total budget cannot exceed from project budget, please correct calculations!");
                    return false;
                }
                // else if (project_budget !== province_total_amount) {
                //     swal("Project total budget and project budget is not matching, please correct calculations!");
                //     return false;
                // }
                else {
                    return true;
                }


            }
        );
        $('#projectForm').on('submit', function () {

            project_budget = parseFloat($('.project_budget').val().split(",").join(""));
            province_total_amount = parseFloat($('.province_total_amount').val().split(",").join(""));

            if (project_budget < province_total_amount) {
                swal("Project total budget and project budget is not matching, please correct calculations!");
                return false;
            }
            // else if (project_budget !== province_total_amount) {
            //     swal("Project total budget and project budget is not matching, please correct calculations!");
            //     return false;
            // }
            else {
                return true;
            }


        });

        $(document).on('change', '.project_province_select', function (event) {


            var provinceSelect = $(this).val();
//            $('.project_province_select_hidden').val($(this).val());
            $.ajax({
                url: "{{ route('constant.pea.find') }}",
                method: 'get',
                data: {
                    id: provinceSelect,
                },

                success: function (response) {
                    console.log(response);


                    $html_pea = "<option value=''>Select</option>";
                    $.each(response.provinceRegion, function (index, val) {

                        $html_pea += "<option value='" + val['id'] + "'>" + val['pea_name'] + "</option>";

                    });

                    $('.project_province_pea').html($html_pea);
                }
            });


        });
        $(document).on('change', '.measureSelect', function (event) {

            var row = $(this).closest('tr');
            var pay = row.find('.measureSelect').children("option:selected").val();

            $.ajax({
                url: "{{ route('constant.sub-cat.option') }}",
                method: 'get',
                data: {
                    id: pay,
                },

                success: function (response) {
                    $html = '';
                    if (response.showAllSubCategoriesOfmeasure.length === 0) {
                        $html += "<option value=''>Not Applicable</option>";
                    } else {
                        $html = '';
                        $html += "<option value=''>Select</option>";

                        $.each(response.showAllSubCategoriesOfmeasure, function (index, val) {

                            $html += "<option value='" + val['id'] + "'>" + val['name'] + "</option>";

                        });
                    }
                    row.find('.sub_cate').html($html);

//
//                        $html_pea = "";
//                        $.each(response.provinceRegion, function (index, val) {
//
//                            $html_pea += "<option value='" + val['id'] + "'>" + val['pea_name'] + "</option>";
//
//                        });
//
//                        row.find('.peas_select').html($html_pea);


//                        $('.measure').html($html);
                }
            });


        });


        var measure = {!! \App\Helpers\Helper::MeasureSelect()!!}
        $(document).on('change', '.provinceBudgetAmount', function () {
            $(this).val($.number($(this).val(), 2));
            calc_total(); // calculate total amount
            calc_revised_total(); // calculate total revised amount

            $get_table_row_col = $(this).closest('tr').find('.provinceBudgetAmount');

            if (checkBudgeEdit() === true) {
                // checkBudgeEdit();
                calc_total();
                calc_revised_total();
//                checkBudgeEdit();
            } else {
                // checkBudgeEdit();
                $get_table_row_col.val(0.00);
                console.log($get_table_row_col.val(0.00));
                calc_total();
                calc_revised_total();
//                checkBudgeEdit();

            }
//            console.log($get_table_row_col.val());


        });
        $(document).on('change', '.revised_budget', function () {
            $(this).val($.number($(this).val(), 2));

        });

        $(".add-row").click(function () {
            $('.delete-row_project_province').css('display', 'block');
            $('.province_total_amount_hide').css('display', 'block');
            $('.revised_total_amount_hide').css('display', 'block');
            var name = $("#name").val();
            var email = $("#email").val();
            var markup = "<tr><td><input name='row_count[]' type='hidden'  ><input type='checkbox' name='record' class='form-control'></td><td>" + measure + "</td><td><select class='sub_cate form-control'  name='sub_cate[]' style='width:150px !important;'><option>Select </option></select></td><td><input type='text' value='0.00' name='amount[]' class='form-control provinceBudgetAmount number_formated_two'  dir='rtl'  oninput='this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');'   style='background-color: #FFCCCB;'></td><td><input type='text' name='revise_amount[]' value='0.00' class='form-control provinceReviseAmount number_formated_two'  dir='rtl'  oninput='this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');'   style='background-color: #e9ecef;' readonly></td><td><input type='text' name='descriptionHasProvince[]' class='form-control'  ></td></tr>";
            $(".project_province").append(markup);
        });

        function checkBudgeEdit() {
            project_budget = parseFloat($('.project_budget').val().split(",").join(""));
            province_total_amount = parseFloat($('.province_total_amount').val().split(",").join(""));
            if (project_budget < province_total_amount) {
                swal("Warning", "Project total budget and project budget is not matching, please correct calculations!", "warning");
                return false;
            }
            return true;
        }

        function checkBudgeCreate() {
            project_budget = parseFloat($('.project_budget').val().split(",").join(""));
            province_total_amount = parseFloat($('.province_total_amount').val().split(",").join(""));
            if (project_budget < province_total_amount) {
                swal("Warning", `Project measures amount doesn't match the Project budget of ${project_budget}.`, "warning");
                return false;
            }
            return true;
        }

        calc_total();
        calc_revised_total();
//        $(".project_province").on('change', function () {
//            calc_total();
//            calc_revised_total();
//            checkBudgeCreate();
//        });
//        $(".project_province_edit").on('change', function () {

//            calc_total();
//            calc_revised_total();
//            checkBudgeEdit();
//            if (!checkBudgeEdit()) {
////                $(this).val(0);
//                console.log("$get_table_row_col.val()");
//                console.log($get_table_row_col);
//                console.log($get_table_row_col.val("0.00"));
//                $get_table_row_col.val("0.00");
//                $get_table_row_col=null;
//
////                console.log( $(this).val());
////                console.log( $(this).val("0"));
//                calc_total();
//                calc_revised_total();
//            }

//        });


        function calc_total() {
            var sum2 = 0;
            $(".provinceBudgetAmount").each(function () {
                //add only if the value is number

                if (this.value.indexOf(',') > -1) {

                    sum2 += parseFloat(this.value.split(",").join(""));

                } else if (!isNaN(this.value) && this.value.length != 0) {
                    sum2 += parseFloat(this.value);

                }
            });

            $(".province_total_amount").val($.number(sum2, 2));

        }


        function calc_revised_total() {
            var sum2 = 0;
            $(".provinceReviseAmount ").each(function () {
                //add only if the value is number

                if (this.value.indexOf(',') > -1) {

                    sum2 += parseFloat(this.value.split(",").join(""));

                } else if (!isNaN(this.value) && this.value.length != 0) {
                    sum2 += parseFloat(this.value);

                }
            });

            $(".revised_total_amount").val($.number(sum2, 2));

        }

//        function calc_total() {
//            var sum = 0;
//            $(".provinceBudgetAmount").each(function () {
//                swal(parseFloat($.number($(this).val(),2)));
//                sum += parseFloat($(this).val() ? $(this).val(1) : 0);
//            });
//
//            $('.province_total_amount').val($.number(parseFloat(sum),2));
//        }

        //            province_total_amount

        // Find and remove selected table rows
        $(".delete-row_project_province").click(function () {
            $(".project_province").find('input[name="record"]').each(function () {
                if ($(this).is(":checked")) {
                    $(this).parents("tr").remove();
                }

            });
        });


        $(".add-row-edit").click(function () {

            var name = $("#name").val();
            var email = $("#email").val();
            var markup = "<tr><td><input type='checkbox' class='form-control' name='record'></td><td>" + measure + "</td><td><select class='sub_cate form-control' id='sub_cate' name='sub_cate[]' style=''><option value=''>Select </option></select></td><td><input type='text' value='0.00' name='provinceBudgetAmount[]' class='form-control provinceBudgetAmount number_formated_two'  dir='rtl'  oninput='this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');'   style='background-color: #FFCCCB;' ></td><td><input readonly type='text' name='revise_amount[]' value='0.00' class='form-control provinceReviseAmount number_formated_two'  dir='rtl'  oninput='this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');'   style='background-color: #e9ecef;'></td><td><input type='text' name='descriptionHasProvince[]' class='form-control'  ></td></tr>";
            $(".project_province_edit").append(markup);


            var countRow = $('.project_province_edit >tbody >tr').length;
            if(countRow > 0) {
                $(".project_province_select").attr("style", "pointer-events: none;").css('background-color', '#FFCCCB');
                $(".project_province_pea").attr("style", "pointer-events: none;").css('background-color', '#FFCCCB');
            } else {
                $(".project_province_select").attr("style", "pointer-events: ;").css('background-color', '#FFCCCB');
                $(".project_province_pea").attr("style", "pointer-events: ;").css('background-color', '#FFCCCB');
            }
        });

        $(".delete-row_project_province").click(function () {
            $(".project_province_edit").find('input[name="record"]').each(function () {
                if ($(this).is(":checked")) {
                    $(this).parents("tr").remove();
                }

            });
            var countRow = $('.project_province_edit >tbody >tr').length;
            if(countRow > 0) {
                $(".project_province_select").attr("style", "pointer-events: none;").css('background-color', '#FFCCCB');
                $(".project_province_pea").attr("style", "pointer-events: none;").css('background-color', '#FFCCCB');
            } else {
                $(".project_province_select").attr("style", "pointer-events: ;").css('background-color', '#FFCCCB');
                $(".project_province_pea").attr("style", "pointer-events: ;").css('background-color', '#FFCCCB');
            }
        });

    //file


    $(".add-project-file-create").click(function () {
//        swal(1);
        $('.delete-project-file').css('display', 'block');
        var markup = "<tr><th><input type='checkbox' name='record'></th><th ><input   type='file' style='font-size:12px !important;' name='file[]' ></th><th><select class='form-control category_project'  style='font-size: 14px !important;height: 35px !important;width: 100px!important;' name='category[]' required><option value='select'>Select</option><option value='Contract'>Contract</option><option value='Quotation'>Quotation</option><option value='PO'>PO</option></select></th><th style='text-align:center'><input style='font-size: 14px !important;height: 35px !important;' type='text' class='form-control description_project' name='description[]'></th>    <th style='text-align:center'></th></tr>";
        $(".project_file_create").append(markup);
    });
    $(".delete-project-file").click(function () {
        $(".project_file_create").find('input[name="record"]').each(function () {
            console.log($(this).attr('data-id'));
            if ($(this).is(":checked")) {

                $(this).parents("tr").remove();
            }
        });
    });


    $(".add-project-file_edit").click(function () {
        $('.delete-project-file_edit').css('display', 'block');
        var markup = "<tr><th><input type='checkbox' name='record'></th><th ><input   type='file' style='font-size:12px !important;' name='file' ></th><th><select class='form-control category_project'  style='font-size: 14px !important;height: 35px !important;width: 100px!important;' name='category[]' required><option value='select'>Select</option><option value='Contract'>Contract</option><option value='Quotation'>Quotation</option><option value='PO'>PO</option></select></th><th style='text-align:center'><input style='font-size: 14px !important;height: 35px !important;' type='text' class='form-control description_project' name='description[]'></th>    <th style='text-align:center'><button type='button' class='project_add_singe_file' id='project_add_singe_file' style='background: rgba(105,126,255,0.8)'>+</button></th></tr>";
        $(".project_file_edit").append(markup);
    });
    //    $(".delete-project-file").click(function () {
    //
    //        $(".project_file").find('input[name="record"]').each(function () {
    //            console.log($(this).is(":checked"));
    //            if ($(this).is(":checked")) {
    //                swal($(this).attr('data-id'));
    //                console.log($(this).attr('data-id'));
    //                deleteProjectFile($(this).attr('data-id'));
    //                $(this).parents("tr").remove();
    //            }
    //
    //        });
    //    });
    $(".delete-project-file_edit").click(function () {
        $(".project_file_edit").find('input[name="record"]').each(function () {
            console.log($(this).attr('data-id'));
            if ($(this).is(":checked")) {

                deleteProjectFile($(this).attr('data-id'));
                $(this).parents("tr").remove();
            }
        });
    });
    function deleteProjectFile($id) {
        $.ajax({
            url: "{{ route('project.delete.file') }}",
            method: 'get',
            data: {
                id: $id
            },
            success: function (response) {
                console.log(response);
                console.log("hello");
            }, error: function () {
                console.log('error');
            }
        });
    }

    $(document).on('click', '.project_add_singe_file', function (event) {


        var row = $(this).closest('tr');
        var files = row.find("input[type='file']");
        var category = row.find('.category_project').val();
        console.log(category);
        var description = row.find('.description_project').val();

        var formData = new FormData();
        formData.append('file', files[0].files[0]);
        formData.append('category', category);
        formData.append('project_id', $('.project_id').val());
        formData.append('description', description);
        formData.append("_token", "{{ csrf_token() }}");

        $.ajax({
            url: "{{route('project.file.single')}}",

            // Form data
            data: formData,
            type: 'POST',
            //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false,

            success: function (data) {
                console.log('project');
                console.log(data);
                console.log(data['success']['file']);
                console.log(data['success']['category']);
                console.log(data['path']);
                $html = "<tr><td valign='top'><input data-id='" + data['success']['id'] + "' type='checkbox' name='record'></td><td valign='top'><a style='float:left' href='" + data['path'] + "' target='_blank' download>" + data['success']['file'] + "    <i class=' fa-lg fas fa-file-download'></i> </a></td><td valign='top' style='text-align: center'><span>" + data['success']['category'] + "</span></td><td valign='top' style='text-align:left'><span>" + data['success']['description'] + "</span></td>    <td valign='top' style='text-align:center'></td></tr>";

                $('.project_file_edit').append($html);
                row.remove();
                console.log('Upload completed');
            },
            error: function (e) {
                console.log(e);
            },
        });
        return false;
    });


    $(".custom-file-input").on("change", function () {

        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    });
</script>
