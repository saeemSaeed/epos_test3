@extends('admin.layout.index')
@section('content')
<section class="bg-primary content-header" style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
	<div class="row">
		<div class="col-8">
			<h1>Projects</h1>
		</div>
		<div class="col-4">
			<ol class="breadcrumb" style="color:#444;float: right">
				<li>
					<i class="fa fa-dashboard"></i> Dashboard
				</li>
				<li>
					<i class="fa fa-angle-right" style="color: #ccc;"></i> Projects
				</li>
			</ol>
		</div>
	</div>
</section>
<div class="clearfix mt-3">
	<a href="{{ route('project.create') }}" class="btn btn-success btn-flat float-left"><i class="fa fa-plus"></i> New Project</a>

	@can('projects-restore')
		<a href="{{ route('project.projectDeleted') }}" class="btn btn-danger btn-flat float-right"><i class="fas fa-trash-alt"></i></a>
	@endcan
</div>
<div class="mt-3">
	<div class="card shadow">
		<div class="card-body">
			<div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<div class="col-sm-12">
						<table id="project_table"
							class="table table-bordered table-hover dataTable dtr-inline project_table"
							role="grid" aria-describedby="example2_info">
							<thead style="background-color: #65a3c6;color: #2c2c2c">
								<tr role="row" style="background-color: #65a3c6;
									color: #2c2c2c;">
									<th>ID</th>
									<th>Status</th>
									<th>Revised History</th>
									<th>Project Name</th>
									<th>Project Budget</th>
									<th>Revised Budget</th>
									<th>Project Currency</th>
									<th>Project Start Date</th>
									<th>Project End Date</th>
									<th style="width: 5%">Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($projects as $index=>$project)
									<tr role="row" class="odd">
										<td class="sorting_1 dtr-control text-center">{{++$index}}</td>
										<td class="sorting_1 dtr-control">
											@if($project->status===0)
												<span class="badge badge-warning badge-pill"><i class="fas fa-exclamation-circle mr-1"></i> Pending </span>
											@elseif($project->status===1)
												<span class="badge badge-success badge-pill"><i class="fas fa-check-circle mr-1"></i> Approved </span>
											@elseif($project->status===2)
												<span class="badge badge-danger badge-pill"><i class="fas fa-times-circle mr-1"></i> Rejected </span>
											@endif
										</td>
										<td class="sorting_1 dtr-control text-center">
											@if(\App\Helpers\Helper::historyList($project->id)['historyProjectCount'])
												<a href="{{route('project.history.list', ['project_id'=>$project->id])}}">
													History
													<span class="badge badge-dark">{{ \App\Helpers\Helper::historyList($project->id)['historyProjectCount'] ?? '—' }}</span>
												</a>
											@else
												<small class="font-italic">(No History)</small>
											@endif
										</td>
										<td class="sorting_1 dtr-control">{{$project->title}}</td>
										<td class="sorting_1 dtr-control number_formated_two text-right">{{ $project->budget ?? '—' }}</td>
										<td class="sorting_1 dtr-control number_formated_two text-right">{{ $project->bugdet_revised ?? '—' }}</td>
										<td class="sorting_1 dtr-control text-center">{{ $project->hasCurrency->currency_name ?? '—'}} </td>
									 	<td class="sorting_1 dtr-control text-right">{{$project->project_start_date?date('d-M-Y', strtotime($project->project_start_date)):'—'}}</td>
										<td class="sorting_1 dtr-control text-right">{{$project->project_end_date?date('d-M-Y', strtotime($project->project_end_date)):'—'}}</td>
										<td align="left">
											<div class="dropdown">
											  	<button type="button" class="btn btn-link text-muted" data-toggle="dropdown" data-boundary="window">
											   	<i class="fas fa-cog fa-lg"></i>
											  	</button>
											  <div class="dropdown-menu">


											  		{{-- PENDING STATUS --}}
											  		@if(auth()->user()->hasRole(['Admin', 'CEO']))

												  		<div class="dropdown-header">Project Status</div>
												  		
                                       	<a class="dropdown-item" href="{{route('project.status',['id'=>$project->id,'status'=>'pending'])}}">
                                       		<i class="fas fa-info-circle mr-1 text-warning"></i> Pending 
                                       	</a>

                                       	<a class="dropdown-item" href="{{route('project.status',['id'=>$project->id,'status'=>'approve'])}}">
                                       		<i class="fas fa-check-circle mr-1 text-success"></i> Approve 
                                       	</a>

                                       	<a class="dropdown-item" href="{{route('project.status',['id'=>$project->id,'status'=>'reject'])}}">
                                       		<i class="fas fa-times-circle mr-1 text-danger"></i> Reject 
                                       	</a>
                                        @endif

                                       <div class="dropdown-header">Project Options</div>

											  		{{-- SHOW --}}
											  		@can('projects-edit')
												  		<a class="dropdown-item" href="{{route('project.show',['id'=>$project->id])}}">
												  			<i class="far fa-eye mr-1"></i> View
												  		</a>
											  		@endcan

											  		{{-- EDIT --}}
											  		@can('projects-edit')
												  		<a class="dropdown-item" href="{{route('project.edit', $project->id)}}">
												  			<i class="fas fa-edit mr-1"></i> Edit
	                                     	</a>
											  		@endcan
											  		{{-- DELETE --}}
											  		@can('projects-delete')
                                          <form style="display:inline;" method="Post" action="{{route('project.delete',['id'=>$project->id])}}">
                                             @csrf
                                             <button class="dropdown-item" type="submit" onclick="return confirm('Are you sure you want to delete?')">
                                                <i class="fas fa-trash-alt text-danger mr-1"></i> Delete
                                             </button>
                                          </form>
                                       @endcan
											  </div>
											</div>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- /.card-body -->
	</div>
</div>
@endsection