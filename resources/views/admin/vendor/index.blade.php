@extends('admin.layout.index')
@section('content')
	<section class="bg-primary content-header"
			 style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
	<div class="row">
		<div class="col-8">
			<h1>Vendors</h1>
		</div>
		<div class="col-4">
			<ol class="breadcrumb" style="color:#444;float: right">
				<li>
					<i class="fa fa-dashboard"></i> Dashboard &nbsp;
				</li>
				<li>
					<i class="fa fa-angle-right" style="color: #ccc;"></i> Vendors &nbsp;
				</li>
				
			</ol>
		</div>
	</div>
</section>
<div class="row">
	<div class="col-2" style="margin-top: 10px; margin-left: 10px">
		<a href="{{ route('vendor.create') }}" type="button" class="btn btn-block btn-success btn-flat">
		<i class="fa fa-plus"></i> Vendor
		</a>
	</div>
</div>
<div class="container" style="margin-top: 10px">
	<div class="card">
		<div class="card-body">
			<div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<div class="col-sm-12 col-md-6"></div>
					<div class="col-sm-12 col-md-6"></div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<table id="vendors" class="table table-bordered table-hover dataTable dtr-inline vendors"  role="grid" aria-describedby="example2_info">
							<thead>
								<tr role="row">
									<th>ID</th>
									<th>Status</th>
									<th>Name</th>
									<th>Address</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr role="row" class="odd">
									<td>ID</td>
									<td>Status</td>
									<td>Name</td>
									<td>Address</td>
									<td>Action</td>
								</tr>
								
								
							</tbody>
							
						</table>
					</div>
				</div>
				
			</div>
		</div>
		<!-- /.card-body -->
	</div>
</div>
@endsection