@extends('admin.layout.index')
@section('content')
<section class="bg-primary content-header"
	style="background-color:#ffc533 !important; padding-bottom: 20px; margin-top: 20px;">
	<div class="row">
		<div class="col-8">
			<h1>Contracts</h1>
		</div>
		<div class="col">
			<ol class="breadcrumb" style="color:#444">
				<li>
					<i class="fa fa-dashboard"></i> Dashboard &nbsp;
					
				</li>
				<li>
					<i class="fa fa-angle-right" style="color: #ccc;"></i> Contracts &nbsp;
				</li>
				<li class="active">
					<i class="fa fa-angle-right" style="color: #ccc;"></i> newcontract
				</li>
			</ol>
		</div>
	</div>
</section>
{{--Start create form--}}
<div class="col" style="margin-top: 10px">
	<div class="card card-outline card-primary">
		<div class="card-header">
			<h3 class="card-title">Create New Vendor</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse">
				<i class="fas fa-minus"></i>
				</button>
			</div>
			<!-- /.card-tools -->
		</div>
		<!-- /.card-header -->
		<div class="card-body" style="display: block;">
			{{--start create form--}}
			<div class="row">
				<div class="col-5">
					<div class="form-group">
						<label>Name</label>
						<input type="text" class="form-control">
					</div>
					<div class="form-group">
						<label>First Name</label>
						<input type="text" class="form-control">
					</div>
					<div class="form-group">
						<label>Middle Name</label>
						<input type="text" class="form-control">
					</div>
					<div class="form-group">
						<label>Last Name</label>
						<input type="text" class="form-control">
					</div>
					<div class="form-group">
						<label>Address</label>
						<textarea class="form-control" ></textarea>
					</div>
					<div class="form-group">
						<label>Financial</label>
					</div>
					<div class="form-group">
						<label>A/c No</label>
						<input type="text" class="form-control">
					</div>
					<div class="form-group">
						<label>TAX No</label>
						<input type="text" class="form-control">
					</div>

				</div>
				
				<div class="col-2"></div>
				<div class="col-3">
					
					<div class="form-group">
						<label>Title</label>
						<input type="text" class="form-control">
					</div>
					
					<div class="form-group">
						<label>Gender</label>
						<input type="text" class="form-control">
					</div>
					<div class="form-group">
						<label>Phone</label>
						<input type="text" class="form-control">
					</div>
					<div class="form-group">
						<label for="">Status</label>
						<select id="" class="form-control">
							<option value="">Active</option>
							<option value="">Pending</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<button id="addupdateContract" type="button" class="btn btn-block btn-success btn-lg" style="float:right;"
				data-contractid="0">Create
				</button>
				<button id="cancelall" type="button" class="btn btn-block btn-primary btn-lg" style="float:right;">Cancel
				</button>
			</div>
			{{--End create form--}}
		</div>
	</div>
	@endsection