<div class="modal fade edit_fileCategory" id="edit_fileCategory">
    <form id="get_city_save">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style=" background-color: #65a3c6;
                    color: #2c2c2c;">
                    <h4 class="modal-title">File Category Form</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul>
                        </ul>
                    </div>
                    <div class="form-group">
                        <label>Title&nbsp;</label>
                        <input type="text" placeholder="City Name " name="title" class="form-control fileCategory_title">
                        <input type="hidden" placeholder="City Name " name="fileCategory_id" class="form-control fileCategory_id ">
                        @error('city_name') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary save_fileCategory">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">cancel</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </form>
    <!-- /.modal-dialog -->
</div>