<div class="modal fade file_category_modal" id="file_category_modal">
  <form action="{{route('fileCategory.store') }}" method="post">
    @csrf
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style=" background-color: #65a3c6;
          color: #2c2c2c;">
          <h4 class="modal-title">File Category Form</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Title&nbsp;</label>
            <input type="text" placeholder="File Category Name" name="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title')}}" required>
            @error('title') <div class="invalid-feedback">{{ $message }}</div> @enderror
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">cancel</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
  </form>
  <!-- /.modal-dialog -->
</div>