@extends('admin.layout.index')
@section('content')
<section class="bg-primary content-header"
    style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
    <div class="row">
        <div class="col-8">
            <h1>Sub category</h1>
        </div>
        <div class="col-4">
            <ol class="breadcrumb" style="color:#444;float: right">
                <li>
                    <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                </li>
                <li>
                    <i class="fa fa-angle-right" style="color: #ccc;"></i> Sub category &nbsp;
                </li>
            </ol>
        </div>
    </div>
</section>
<div class="container">
    <div class="row justify-content-between">
        <div style="margin-top: 10px; margin-left: 10px">
            @can('sub-categories-create')
            <button type="button" class="btn btn-block btn-success btn-flat" data-toggle="modal"
            data-target="#sub_category_modal">
            <i class="fa fa-plus"></i> Add Sub category
            </button>
            @endcan
        </div>
        @can('sub-categories-restore')
        <div  style="margin-top: 10px; margin-right: 10px">
            <a href="{{ route('subCategories.Deleted') }}" class="btn btn-block btn-danger btn-flat"><i class="fas fa-trash-alt"></i></a>
        </div>
        @endcan
    </div>
</div>
<div class="container-fluid" style="margin-top: 10px">
    <div class="card">
        <div class="card-body">
            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="subCategory" class="table table-bordered table-hover subCategory" role="grid"
                            aria-describedby="example2_info">
                            <thead style="    background-color: #65a3c6;
                                color: #2c2c2c;">
                                <tr role="row">
{{--                                    <th>ID</th>--}}
                                    <th style=";width: 10%">Sort Order</th>
                                    <th>Sub-Category Name</th>
                                    <th>Category</th>
                                    <th  style=";width: 10%">Category Code</th>
                                    <th style="text-align: center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($subCategories as $index=>$sub)
                                <tr role="row" class="odd">
                                    <td style="text-align: center">{{$sub->sequence_number}}</td>
{{--                                    <td style='text-align: center'>{{++$index}}</td>--}}

                                    <td>{{$sub->name}}</td>
                                    <td>{{isset($sub->hasMeasure->name) ? $sub->hasMeasure->name:''}}</td>
                                    <td style="text-align: center"> {{isset($sub->hasMeasure->code) ? $sub->hasMeasure->code:''}}</td>
                                    <td data-id="{{$sub->id}}"  style="text-align: center ">
                                        @can('sub-categories-edit')
                                        <a data-id="{{$sub->id}}"
                                            class="sub_category_edit"
                                            data-toggle="modal"
                                            data-target="#sub_category_edit"
                                            title="edit!"><i style="color: black;font-size: 14px!important"
                                        class="fas fa-pencil-alt"></i></a>&nbsp;&nbsp;
                                        @endcan   @can('sub-categories-delete')
                                        <a
                                            onclick="return confirm('Are you sure?')"
                                            data-id="{{$sub->id}}"
                                            href="{{route('sub-categories.delete',['id'=>$sub->id])}}"
                                            data-toggle="tooltip"
                                            title="Remove"><i style="color: black;font-size: 14px!important"
                                        class=" fa fa-trash"></i></a> @endcan </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
    @include('admin.subCategories.create')
    @include('admin.subCategories.edit')
    @endsection
