<div class="modal fade" id="sub_category_modal">
    <form action="{{route('sub-categories.store') }}" method="post">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style=" background-color: #65a3c6;
color: #2c2c2c;">
                    <h4 class="modal-title">Sub category</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="form-group">
                            <label>Sort Order&nbsp;</label>
                            <input type="text" placeholder="Add Sort Order" name="sequence_number"
                                   oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"
                                   class="form-control @error('sequence_number') is-invalid @enderror" value="{{ old('sequence_number') }}">
                            @error('name')
                            <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        <label>Category &nbsp;</label>
                        <select name="measure_id" class="form-control @error('measure_id') is-invalid @enderror" id="measure_id" value="{{ old('measure_id') }}">
                            <option value=''>Choose an Option</option>
                            @foreach(\App\Helpers\Helper::Measures() as $measure)
                                <option value="{{$measure->id}}">{{$measure->name}}</option>
                            @endforeach
                        </select>
                        @error('measure_id') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group">
                        <label>Sub category&nbsp;</label>
                        <input type="text" placeholder="Sub Chart of Account Name" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}">
                        @error('name') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">cancel</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </form>
    <!-- /.modal-dialog -->
</div>
