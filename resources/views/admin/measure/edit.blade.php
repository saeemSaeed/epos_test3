<div class="modal fade" id="measure_edit">
    <form method="post">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style=" background-color: #65a3c6;
color: #2c2c2c;">
                    <h4 class="modal-title">Measures/Line Items</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <input type="hidden" placeholder="Location Name" class="form-control measure_id" name="id">
                <div class="modal-body">
                    <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul>
                        </ul>
                    </div>
                    <div class="form-group">
                        <label>Sort Order &nbsp;</label>
                        <input type="text" placeholder="Sort Order"

                               oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"
                               name="sort_order" class="form-control sort_order @error('sort_order') is-invalid @enderror" >
                        @error('sort_order') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group">
                        <label>Item Name &nbsp;</label>
                        <input type="text" placeholder="Chart of Account Name" name="name" class="form-control measure_name @error('name') is-invalid @enderror" >
                        @error('name') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                    <div class="form-group">
                        <label>Item Code &nbsp;</label>
                        <input type="text" placeholder="Chart of Account Code" name="code" class="form-control measure_code @error('code') is-invalid @enderror">
                        @error('code') <div class="invalid-feedback">{{ $message }}</div> @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary edit_measure_save_modal">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">cancel</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </form>
    <!-- /.modal-dialog -->
</div>
