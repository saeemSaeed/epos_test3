@extends('admin.layout.index')
@section('content')
    <section class="bg-primary content-header"
             style="background-color:#ffc533 !important; padding-bottom: 10px; margin-top: 10px;padding-left: 10px;">
        <div class="row">
            <div class="col-8">
                <h1>Measures/Line Items</h1>
            </div>
            <div class="col-4">
                <ol class="breadcrumb" style="color:#444;float: right">
                    <li>
                        <i class="fa fa-dashboard"></i> Dashboard &nbsp;
                    </li>
                    <li>
                        <i class="fa fa-angle-right" style="color: #ccc;"></i> Measures/Line Items &nbsp;
                    </li>

                </ol>
            </div>
        </div>
    </section>
   <div class="container">
  <div class="row justify-content-between">

    <div style="margin-top: 10px; margin-left: 10px">
        @can('measures-list')
        <button type="button" class="btn btn-block btn-success btn-flat" data-toggle="modal"
        data-target="#measure">
        <i class="fa fa-plus"></i> Measures/Line Items
        </button>
        @endcan
    </div>
    @can('measures-restore')
    <div  style="margin-top: 10px; margin-right: 10px">
        <a href="{{ route('measure.Deleted') }}" class="btn btn-block btn-danger btn-flat"><i class="fas fa-trash-alt"></i></a>
    </div>
    @endcan
</div>
</div>
    <div class="container-fluid" style="margin-top: 10px">
        <div class="card">
            <div class="card-body">
                <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12 col-md-6"></div>
                        <div class="col-sm-12 col-md-6"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="measures" class="table table-bordered table-hover measures" role="grid"
                                   aria-describedby="example2_info">
                                <thead style="    background-color: #65a3c6;
                                       color: #2c2c2c;">
                                <tr role="row">
{{--                                    <th>ID</th>--}}
                                    <th style=";width: 10%">Sort Order</th>
                                    <th>Category Name</th>
                                    <th style=";width: 10%">Category Code</th>
                                    <th style="text-align: center">Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                @if($measures)
                                    @foreach($measures as $index=> $measure)
                                        <tr>
{{--                                            <td style='text-align: center'>{{++$index}}</td>--}}
                                            <td style='text-align: center'>{{$measure->sort_order}}</td>
                                            <td >{{$measure->name}}</td>
                                            <td style='text-align: center'>{{$measure->code}}</td>

                                            <td data-id="{{$measure->id}}" style="text-align: center">
                                                @can('measures-edit')
                                                    <a data-id="{{$measure->id}}"
                                                       class="getProvinceRegion"
                                                       data-toggle="modal"
                                                       data-target="#measure_edit"
                                                       title="edit!"><i style="color: black;font-size: 14px!important"
                                                                          class="fas fa-pencil-alt"></i></a>&nbsp;&nbsp;
                                                @endcan

                                                @can('measures-delete') <a


                                                        onclick="return confirm('Are you sure?')"
                                                        data-id="{{$measure->id}}"
                                                        href="{{route('measure.delete',['id'=>$measure->id])}}"
                                                        data-toggle="tooltip"
                                                        title="Remove"><i style="color: black;font-size: 14px!important"
                                                                          class=" fa fa-trash"></i></a>
                                                @endcan</td>

                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>

                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
    @include('admin.measure.create')
    @include('admin.measure.edit')
@endsection
