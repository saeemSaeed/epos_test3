<div class="modal fade" id="measure">
  <form action="{{route('measure.store') }}" method="post">
    @csrf
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style=" background-color: #65a3c6;
color: #2c2c2c;">
          <h4 class="modal-title">Measures/Line Items</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>



        <div class="modal-body">
          <div class="form-group">
            <label>Sort Order &nbsp;</label>
            <input type="text" placeholder="Sort Order"

                   oninput="this.value = this.value.replace(/[^0-9\.]/g, '').replace(/(\..*)\./g, '$1');"
                   name="sort_order" class="form-control @error('sort_order') is-invalid @enderror" value="{{ old('sort_order') }}">
            @error('sort_order') <div class="invalid-feedback">{{ $message }}</div> @enderror
          </div>

            <div class="form-group">
            <label>Item Name &nbsp;</label>
            <input type="text" placeholder="Chart of Account Name" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}">
            @error('name') <div class="invalid-feedback">{{ $message }}</div> @enderror
          </div>

          <div class="form-group">
            <label>Item Code &nbsp;</label>
            <input type="text" placeholder="Chart of Account Code" name="code" class="form-control @error('code') is-invalid @enderror" value="{{ old('code') }}">
            @error('code') <div class="invalid-feedback">{{ $message }}</div> @enderror
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">cancel</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
  </form>
  <!-- /.modal-dialog -->
</div>
