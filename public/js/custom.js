$(document).ready( function () {
    $('#contract_table').DataTable();
    $('#payment_table').DataTable();
    $('#province-region').DataTable();
    $('#peas').DataTable();
    $('#contractors').DataTable();
    $('#vendors').DataTable();
    $('#measures').DataTable();
    $('#subCategory').DataTable();
    $('#paymentMethod').DataTable();
    $('#exchangeRate').DataTable();
    $('#city').DataTable();
});