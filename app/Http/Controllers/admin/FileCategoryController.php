<?php

namespace App\Http\Controllers\Admin;

use App\Model\FileCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FileCategoryController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:fileCategory-list|fileCategory-create|fileCategory-edit|fileCategory-delete', ['only' => ['index','store']]);
        $this->middleware('permission:fileCategory-create', ['only' => ['create','store']]);
        $this->middleware('permission:fileCategory-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:fileCategory-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $fileCategory = FileCategory::get();
        // return $fileCategory;
        return view('admin.fileCategory.index',compact('fileCategory'));
    }


    public function store(Request $request)
    {
    		$file = $request->validate([
            'title' => 'required|max:255|alpha',
        ]);
    		
    		FileCategory::create($file);

        return redirect()->route('fileCategory.index')->with('success', 'File Category Create Successfully');
    }

    public function edit(Request $request)
    {
        $file = FileCategory::find($request->id);
        return $file;
    }

    public function update(Request $request)
    {
        // return $request;
        $validator = $request->validate([
            'title' => 'required',
        ]);
        $file = FileCategory::where('id', $request->id)->update(
                [
                    'title' => $request->title,
                ]
            );
        // return $file;
        if ($file) {
            return response('Update Successfully', 200);
        }
        return response('Something went wrong.', 200);
    }

    public function destroy($id)
    {
    		   $fileCategory = FileCategory::find($id)->delete();

        return redirect()->route('fileCategory.index')->with('success', 'File Category Deleted Successfully');

    }
}
