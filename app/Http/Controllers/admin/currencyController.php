<?php

namespace App\Http\Controllers\admin;

use App\Model\currency;
use App\Model\exchangeRates;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class currencyController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:currency-list|currency-create|currency-edit|currency-delete|currency-restore', ['only' => ['index', 'store']]);
        $this->middleware('permission:currency-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:currency-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:currency-delete', ['only' => ['destroy']]);
        $this->middleware('permission:currency-restore', ['only' => ['Restore']]);
    }

    public function index()
    {
        $currency = new currency();

        return view('admin.currency.index', [
            'currency' => $currency->getAllCurrency()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $currency = new currency();
        $currency->validateCurrency($request);

        $currency->storeCurrency($request->input());
        return redirect()->route('currency.index')->with('success', 'Currency Create Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function showExchangeRate(Request $request)
    {
        $currency = new currency();

        return [
            "currency" => $currency->showCurrency($request->id),
            "id" => $request->id
        ];


    }
    public function showExchangeRateFrom(Request $request)
    {
        $currency = new currency();

        return [
            "currency" => $currency->showCurrency($request->id),
            "id" => $request->id
        ];


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $currency = new currency();
        return $currency->showCurrency($request->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
//return $request;
        $currency = new currency();
        $validator = $this->validate($request, [
            'currency_name' => 'required|max:255|unique:currencies,currency_name,' . $request->currency_id,
            'currency_code' => 'required',
            'currency_symbol' => 'required',
            'sort_order' => 'required|unique:currencies,sort_order,'.$request->currency_id,
        ]);
//        return $request->except('_token');
        if ($currency->updateCurrency($request->currency_id, $request->except('_token'))) {
            return response('Update Successfully', 200);
        }
        return response('Something went wrong.', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currency = new currency();

        if ($currency->deleteCurrency($id)) {
            return redirect()->route('currency.index')->with('success', 'Currency Delete Successfully');

        }
        return redirect()->route('currency.index')->with('error', 'You Cannot Delete Currency');
    }

    public function Deleted()
    {
        $deletedCurrency = currency::onlyTrashed()->get();
        return view('admin.currency.index_deleted', compact('deletedCurrency'));
    }

    public function Restore($id)
    {

        $currency = currency::onlyTrashed()->find($id);
        $currency->restore();
        return redirect()->route('currency.index');
    }
}
