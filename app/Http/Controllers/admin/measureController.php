<?php

namespace App\Http\Controllers\admin;

use App\Model\Measures;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class measureController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:measures-list|measures-create|measures-edit|measures-delete|measures-restore', ['only' => ['index','store']]);
        $this->middleware('permission:measures-create', ['only' => ['create','store']]);
        $this->middleware('permission:measures-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:measures-delete', ['only' => ['destroy']]);
        $this->middleware('permission:measures-restore', ['only' => ['measureRestore']]);
    }
    public function index()
    {
        $measures = new Measures();

        return view('admin.measure.index', [
            'measures' => $measures->getAllMeasures()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $measures = new Measures();
        $measures->validateMeasures($request);

        $measures->storeMeasures($request->input());
        return redirect()->route('measure.index')->with('success', 'Measure Create Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $measures = new Measures();

        return $measures->showMeasures($request->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $measures = new Measures();
        $validator = $this->validate($request,[
             'name' => 'required|max:255|unique:measures,id,'.$request->measure_id,
             'sort_order' => 'required|max:255|unique:measures,sort_order,'.$request->measure_id,
             'code' => 'required',
        ]);
        if ($measures->updateMeasures($request->measure_id, $request->except('_token'))) {
            return response('Update Successfully', 200);
        }
        return response('Something went wrong.', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $measures = new Measures();

        if ($measures->deleteMeasures($id)) {
            return redirect()->route('measure.index')->with('success', 'Measure Delete Successfully');
        }
        return redirect()->route('measure.index')->with('error', 'Measure is in use and cannot be deleted.');
    }

    public function measureDeleted() {
      $deletedmeasure = Measures::onlyTrashed()->get();
      return view('admin.measure.index_deleted',compact('deletedmeasure'));
    }

    public function measureRestore($id) {
       $measure = Measures::onlyTrashed()->find($id);
       $measure->restore();
       return redirect()->route('measure.index');
    }

}
