<?php

namespace App\Http\Controllers\admin;

use App\Model\subCategories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class subCategoriesController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:sub-categories-list|sub-categories-create|sub-categories-edit|sub-categories-delete|sub-categories-restore', ['only' => ['index', 'store']]);
        $this->middleware('permission:sub-categories-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:sub-categories-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:sub-categories-delete', ['only' => ['destroy']]);
        $this->middleware('permission:sub-categories-restore', ['only' => ['subCategoriesRestore']]);
    }

    public function index()
    {
        $subCategories = new subCategories();

        return view('admin.subCategories.index', [
            'subCategories' => $subCategories->getAllSubCategories()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $provinceRegion = new subCategories();
        $provinceRegion->validatesubCategories($request);
        $provinceRegion->storeSubCategories($request->input());
        return redirect()->route('sub-categories.index')->with('success', 'SubCategories Create Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $provinceRegion = new subCategories();
        return $provinceRegion->showSubCategories($request->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // return $request->all();
        $provinceRegion = new subCategories();
        $validator = $this->validate($request, [
            'name' => 'required|max:255',
            'measure_id' => 'required|nullable',
            'sequence_number' => 'required|unique:sub_categories,sequence_number,'.$request->sub_category_id,
        ]);
        if ($provinceRegion->updateSubCategories($request->sub_category_id, $request->except('_token'))) {
            return response('Update Successfully', 200);
        }
        return response('Something went wrong.', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $provinceRegion = new subCategories();

        if ($provinceRegion->deleteSubCategories($id)) {
            return redirect()->route('sub-categories.index')->with('success', 'SubCategories Delete Successfully');

        }
        return redirect()->route('sub-categories.index')->with('error', 'SubCategories Cannot Delete Successfully');
    }

    public function subCategoriesDeleted() {
      $deletedsubCategories = subCategories::onlyTrashed()->get();
      return view('admin.subCategories.index_deleted',compact('deletedsubCategories'));
    }

    public function subCategoriesRestore($id) {
       $subCategories = subCategories::onlyTrashed()->find($id);
       $subCategories->restore();
       return redirect()->route('sub-categories.index');
    }
}
