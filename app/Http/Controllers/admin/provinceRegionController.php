<?php

namespace App\Http\Controllers\admin;

use App\Model\provinceRegion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class provinceRegionController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:province-region-list|province-region-create|province-region-edit|province-region-delete|province-region-restore', ['only' => ['index', 'store']]);
        $this->middleware('permission:province-region-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:province-region-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:province-region-delete', ['only' => ['destroy']]);
        $this->middleware('permission:province-region-restore', ['only' => ['provinceRegionRestore']]);
    }

    public function index()
    {
        $provinceRegion = new provinceRegion();

        return view('admin.provinceRegion.index', [
            'getAllProvinceRegion' => $provinceRegion->getAllProvinceRegion()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $provinceRegion = new provinceRegion();
        $provinceRegion->validateProvinceRegion($request);

        $provinceRegion->storeProvinceRegion($request->input());
        return redirect()->route('province-region.index')->with('success', 'Province Create Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $provinceRegion = new provinceRegion();
        return $provinceRegion->FindProvinceRegion($request->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {


        $provinceRegion = new provinceRegion();
        $validator = $this->validate($request, [
            'sort_order'=> 'required|unique:province_regions,sort_order,'.$request->province_region_id,
            'name' => 'required|max:255|unique:province_regions,name,'.$request->province_region_id,
            'short_code' => 'nullable',
            'detail' => 'nullable|max:255',

        ]);

        if ($provinceRegion->updateProvinceRegion($request->province_region_id, $request->except('_token'))) {
            return response('Update Successfully', 200);
        }
        return response('Something went wrong.', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $provinceRegion = new provinceRegion();
        if ($provinceRegion->deleteProvinceRegion($id)) {
            return redirect()->route('province-region.index')->with('success', 'Province Delete Successfully');

        }
        return redirect()->route('province-region.index')->with('error', 'You Cannot Delete Province');
    }

    public function provinceRegionDeleted() {
        $deletedprovinceRegion = provinceRegion::onlyTrashed()->get();
        return view('admin.provinceRegion.index_deleted',compact('deletedprovinceRegion'));
    }

    public function provinceRegionRestore($id) {
        $provinceRegion = provinceRegion::onlyTrashed()->find($id);
        $provinceRegion->restore();

       return redirect()->route('province-region.index');

    }
}
