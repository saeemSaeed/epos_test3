<?php

namespace App\Http\Controllers\admin;

use App\Model\currency;
use App\Model\exchangeRates;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;

class exchangeRateController extends Controller
{
//    function __construct()
//    {
//        $this->middleware('permission:exchange-rate-list|exchange-rate-create|exchange-rate-edit|exchange-rate-delete|exchange-rate-restore', ['only' => ['index', 'store']]);
//        $this->middleware('permission:exchange-rate-create', ['only' => ['create', 'store']]);
//        $this->middleware('permission:exchange-rate-edit', ['only' => ['edit', 'update']]);
//        $this->middleware('permission:exchange-rate-delete', ['only' => ['destroy']]);
//        $this->middleware('permission:exchange-rate-restore', ['only' => ['Restore']]);
//    }

    public function index()
    {
        $exchangeRates = new exchangeRates();

        return view('admin.exchangeRate.index', [
            'exchangeRates' => $exchangeRates->getAllExchangeRates()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function GetSortOrder()
    {
//        $currency = new currency();
        $order = exchangeRates::max('sort_order');
        return response()->json([


            'data' => ++$order,


        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function exchangeRatesContractStore(Request $request)
    {

        $exchangeRates = new exchangeRates();

        $request->request->add(['exchange_rate' => str_replace(",", "", $request->exchange_rate)]);
        $request->request->add(['start_date' =>
            Carbon::createFromFormat('d-M-Y', $request->start_date)->format('Y/m/d')]);
        $request->request->add(['end_date' =>
            Carbon::createFromFormat('d-M-Y', $request->end_date)->format('Y/m/d')]);
        $validator = $request->validate([
            'exchange_rate' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'date|after_or_equal:start_date',
            'currency_to' => 'required',
            'currency_from' => 'required',
            'sort_order' => 'required|unique:exchange_rates,sort_order',


        ]);
        if ($check = $exchangeRates->storeExchangeRates($request->except('_token'))) {

            return response()->json([
                'message' => 'Update Successfully',
                'error' => null,
                'data' => $check,
                'start_date' => Carbon::createFromFormat('Y/m/d', $check->start_date)->format('d-M-Y'),
                'currency_code' => $check->currencyTo->currency_code,
                'currency_code_from' => $check->currencyFrom->currency_code,
            ], 200);

        }
        return response()->json([
            'message' => 'Not Update Successfully',
            'error' => $validator->errors(),
            'data' => $check,
        ], 422);


    }

    public function store(Request $request)
    {

        $exchangeRates = new exchangeRates();
        $request->request->add(['start_date' => Carbon::createFromFormat('d-M-Y', $request->start_date)->format('Y/m/d')]);
        $request->request->add(['end_date' => Carbon::createFromFormat('d-M-Y', $request->end_date)->format('Y/m/d')]);
        $request->request->add(['exchange_rate' => str_replace(",", "", $request->exchange_rate)]);


        $exchangeRates->validateExchangeRates($request);

        $exchangeRates->storeExchangeRates($request->input());
        return redirect()->route('exchangeRate.index')->with('success', 'ExchangeRates Create Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function GetExchangeRate(Request $request)
    {

        $date = isset($request->exchange_rate_date) ? Carbon::createFromFormat('d-M-yy', $request->exchange_rate_date)->format('Y-m-d') : null;

        if (($date) AND ($request->to) AND ($request->from)) {

            $exchangeRates = exchangeRates::
            where('currency_to', $request->to)
                ->where('currency_from', $request->from)
                ->where('start_date', '<=', $date)
                ->where('end_date', '>=', $date)
                ->select('exchange_rate', 'id')->latest()->first();

            return [
                'exchange_rates' => $exchangeRates ? $exchangeRates->exchange_rate : null,
                'exchange_rate_id' => $exchangeRates ? $exchangeRates->id : null,
                'exchangeRates' => $exchangeRates,

            ];
        } else {
            return [
                'exchange_rates' => null,
                'exchange_rates_id' => null,

            ];
        }
    }

    public function edit(Request $request)
    {
        $exchangeRates = new exchangeRates();
        $data = $exchangeRates->showExchangeRates($request->id);
//        Carbon::createFromFormat('d-M-Y', $request->end_date)->format('Y/m/d');
        return (['data' => $data,
            'start_date' => Carbon::createFromFormat('Y-m-d', $data->start_date)->format('d-M-Y'),
            'end_date' => Carbon::createFromFormat('Y-m-d', $data->end_date)->format('d-M-Y'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $exchangeRates = new exchangeRates();

        $request->request->add(['exchange_rate' => str_replace(",", "", $request->exchange_rate)]);
        $request->request->add(['start_date' =>Carbon::createFromFormat('d-M-Y', $request->start_date)->format('Y-m-d')]);
        $request->request->add(['end_date' => Carbon::createFromFormat('d-M-Y', $request->end_date)->format('Y-m-d')]);

        $validator = $this->validate($request, [
            'exchange_rate' => 'required',
            'start_date' => 'required|date|date_format:Y-m-d',
            'end_date' => 'required|date|after_or_equal:start_date|date_format:Y-m-d',
            'currency_to' => 'required',
            'currency_from' => 'required',
            'sort_order' => 'required|unique:exchange_rates,sort_order,' . $request->exchange_rate_id,
        ]);
// return $request->all();

        if ($exchangeRates->updateExchangeRates($request->exchange_rate_id, $request->except('_token'))) {
            return response('Update Successfully', 200);

        }
        return response('Something went wrong.', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $exchangeRates = new exchangeRates();

        if ($exchangeRates->deleteExchangeRates($id)) {
            return redirect()->route('exchangeRate.index')->with('success', 'ExchangeRates Delete Successfully');

        }
        return redirect()->route('exchangeRate.index')->with('error', 'ExchangeRates not Delete Successfully');
    }

    public function Deleted()
    {
        $deletedexchangeRates = exchangeRates::onlyTrashed()->get();
        return view('admin.exchangeRate.index_deleted', compact('deletedexchangeRates'));
    }

    public function Restore($id)
    {

        $exchangeRates = exchangeRates::onlyTrashed()->find($id);
        $exchangeRates->restore();
        return redirect()->route('exchangeRate');
    }
}
