<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Measures;
use App\Model\Projects;
use App\Model\ProjectsHasProvince;
use App\Model\contractHasProvincDivision;
use App\Model\contracts;
use App\Model\paymentMethod;
use App\Model\payments;
use App\Model\provinceRegion;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{

    public function index() {


        $userCount = User::where('status',0)->count();
        $projectCount = Projects::count();
        $contractCount = contracts::count();
        $payementsCount = payments::count();
        $paymentMethod = paymentMethod::get();

        $provinceRegion = new provinceRegion();
        $allProvinces = $provinceRegion->getAllProvinceRegion();
        $province = [];
        $provinceSum = [];


        foreach($provinceRegion->getAllProvinceRegion() as $index => $pro) {
            $province[] = $pro->name;
            $obj=Projects::where('province_id',$pro->id)->sum('budget');
            $provinceSum[$pro->name]=$obj;
        }
        $measures = new Measures();
        $measuresAll = $measures->getAllMeasures();

        $projects = new Projects();
        $projects = $projects->getAllProject();

        return view('admin.dashboard.index', compact('userCount','projectCount','contractCount','payementsCount','provinceSum','measuresAll','allProvinces','paymentMethod', 'projects'));
    }


}
