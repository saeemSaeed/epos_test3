<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\ContractsHasRetainFile;
use App\Model\Facility;
use App\Model\Projects;
use App\Model\ProjectsHasProvince;
use App\Model\city;
use App\Model\contractHasProvincDivision;
use App\Model\contractPayment;
use App\Model\contractors;
use App\Model\contracts;
use App\Model\contractsHasFile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class contractsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:contracts-list|contracts-create|contracts-edit|contracts-delete|contracts-restore', ['only' => ['index', 'store']]);
        $this->middleware('permission:contracts-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:contracts-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:contracts-delete', ['only' => ['destroy']]);
        $this->middleware('permission:contracts-restore', ['only' => ['contractRestore']]);
    }

    public function index()
    {
        $contracts = new contracts();
        return view('admin.contracts.index', ['contracts' => $contracts->getAllContracts()]);
    }

    public function create()
    {
        $contractors = new contractors();
        return view('admin.contracts.create', ['contractors' => $contractors->getAllContractors()]);
    }

    public function createProjectBudget(Request $request)
    {
        $project = Projects::find($request->project_id);
        $contractSum = contracts::where('project_id', $request->project_id)->sum('amount');
        $total_budget_left = $project->budget - $contractSum;
        return response()->json(['total_budget_left' => $total_budget_left], 201);
    }

    public function store(Request $request)
    {
        isset($request->contract_date) ? $request->request->add(['contract_date' =>
            Carbon::createFromFormat('d-M-Y', $request->contract_date)->format('Y/m/d')]) : null;
        isset($request->planned_start_date) ? $request->request->add(['planned_start_date' =>
            Carbon::createFromFormat('d-M-Y', $request->planned_start_date)->format('Y/m/d')]) : null;
        isset($request->planned_end_date) ? $request->request->add(['planned_end_date' =>
            Carbon::createFromFormat('d-M-Y', $request->planned_end_date)->format('Y/m/d')]) : null;
        isset($request->contract_start_date) ? $request->request->add(['contract_start_date' =>
            Carbon::createFromFormat('d-M-Y', $request->contract_start_date)->format('Y/m/d')]) : null;
        isset($request->contract_end_date) ? $request->request->add(['contract_end_date' =>
            Carbon::createFromFormat('d-M-Y', $request->contract_end_date)->format('Y/m/d')]) : null;
        isset($request->ps_validity) ?
            $request->request->add(['ps_validity' =>
                Carbon::createFromFormat('d-M-Y', $request->ps_validity)->format('Y/m/d')]) : null;


        $contracts = new contracts();
        $contractPayment = new contractPayment();

        $contracts->validateContracts($request);


        $request->request->add(['amount' => (double)str_replace(",", "", $request->amount)]);
        $request->request->add(['amount_pkr' => (double)str_replace(",", "", $request->amount_pkr)]);
        $request->request->add(['orginal_amount' => (double)str_replace(",", "", $request->amount)]);
        $request->request->add(['orginal_amount_pkr' => (double)str_replace(",", "", $request->amount_pkr)]);

        $request->request->add(['ps_amount' => (double)str_replace(",", "", $request->ps_amount)]);

        if ($request->hasfile('performance_security_file')) {

            $file = $request->file('performance_security_file');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $approval_file = $file->getClientOriginalName();
            $approval_file_title = $approval_file;
            $hash_file_name = time() . '.' . $extension;

            $file->move('files/contract/performance_security_file/', $hash_file_name);


            $request->request->add(['performance_security_file' => $hash_file_name]);
            $request->request->add(['performance_security_file_name' => $approval_file_title]);
        } else {
            $request->request->add(['performance_security_file' => null]);
            $request->request->add(['performance_security_file_name' => null]);
        }
        $object = $contracts->storeContracts($request->input());
        $storeProjectsHasProvince = new contractHasProvincDivision();


        if (($request->record_contract) && count($request->record_contract) > 0) {

            foreach ($request->record_contract as $index => $file) {

                $projectFile = contractsHasFile::where('id', $request->record_contract[$index])->
                    update(['contract_id' => $object->id,
                ]);
            }
        }
        if (($request->record_retain) && count($request->record_retain) > 0) {


            foreach ($request->record_retain as $index => $file) {

                $projectFile = ContractsHasRetainFile::where('id', $request->record_retain[$index])->
                update(['contract_id' => $object->id,
                ]);
            }
        }
        foreach ($request['project_id'] as $index => $obj) {
            $dummy = $storeProjectsHasProvince->storeContractHasProvinceDivision([
                'contract_id' => $object->id,
                'project_id' => isset($request->project_id[$index]) ? $request->project_id[$index] : '',
                'province_id' => isset($request->province_contracts_id[$index]) ? $request->province_contracts_id[$index] : '',
                'city_id' => isset($request->sub_cate_city_id[$index]) ? $request->sub_cate_city_id[$index] : null,
                'facility_id' => isset($request->sub_cate_facility_id[$index]) ? $request->sub_cate_facility_id[$index] : null,
                'measure_id' => isset($request->measures_contracts[$index]) ? $request->measures_contracts[$index] : null,
                'sub_category_id' => isset($request->sub_cate_contracts[$index]) ? $request->sub_cate_contracts[$index] : null,
                'pea_id' => isset($request->sub_cate_peas_id[$index]) ? $request->sub_cate_peas_id[$index] : '',
                'description' => isset($request->contract_child_description[$index]) ? $request->contract_child_description[$index] : ''
            ]);


            if (Projects::find($request->project_id[$index])->currency_id == $request->currency_id_from) {
                $dummy->budget = isset($request->province_amount[$index]) ? (double)str_replace(",", "", $request->province_amount[$index]) : 0;
                $dummy->budget_pkr = isset($request->province_amount_pkr[$index]) ? (double)str_replace(",", "", $request->province_amount_pkr[$index]) : 0;

            } else {
                $dummy->budget_pkr = isset($request->province_amount[$index]) ? (double)str_replace(",", "", $request->province_amount[$index]) : 0;
                $dummy->budget = isset($request->province_amount_pkr[$index]) ? (double)str_replace(",", "", $request->province_amount_pkr[$index]) : 0;

            }
            $dummy->save();
        }


        if ($request->province_contracts) {
            if ($request->province_contracts) {
                if ($request['installment_no'] > 0) {
                    foreach ($request['installment_no'] as $index => $obj) {


                        $contractPayment->storeContractPayment([
                            'contract_id' => $object->id,
//                        'schedule_date' => isset($request->contract_payment_date[$index]) ? $request->contract_payment_date[$index] : null,
                            'schedule_date' => isset($request->contract_payment_date[$index]) ?
                                Carbon::createFromFormat('d-M-Y', $request->contract_payment_date[$index])->format('Y-m-d') : null,
                            'percentage' => isset($request->percentage[$index]) ? str_replace("%", "", $request->percentage[$index]) : null,
                            'amount' => isset($request->paymentAmount[$index]) ? (double)str_replace(",", "", $request->paymentAmount[$index]) : null,
                            'amount_pkr' => isset($request->paymentAmountpkr[$index]) ? (double)str_replace(",", "", $request->paymentAmountpkr[$index]) : null,
                        ]);
                    }
                }
            }
        }

        return redirect()->route('contract.index')->with('success', 'Contract Create Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contract = new contracts();
        $contractors = new contractors();
        $projects = new Projects();
        if ($contract->showContracts($id)) {
            return view('admin.contracts.show', [

                'contract' => $contract->showContracts($id),
                'activeProject' => $projects->getActiveProject(),
                'contractors' => $contractors->getAllContractors()]);
        } else {
            return redirect()->route('contract.index')->with('error', 'Not Found Contract');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contract = new contracts();
        $contractors = new contractors();
        $projects = new Projects();
        if ($contract->showContracts($id)) {
            return view('admin.contracts.edit', [

                'contract' => $contract->showContracts($id),
                'activeProject' => $projects->getActiveProject(),
                'contractors' => $contractors->getAllContractors()]);
        } else {
            return redirect()->route('contract.index')->with('success', 'Not Found Contract');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->request->add(['amount_edit' => (double)str_replace(",", "", $request->amount_edit)]);
        $request->request->add(['amount_pkr' => (double)str_replace(",", "", $request->amount_pkr)]);
        $request->request->add(['ps_amount' => (double)str_replace(",", "", $request->ps_amount)]);
        $contracts = new contracts();

        isset($request->contract_date_edit) ? $request->request->add(['contract_date_edit' =>
            Carbon::createFromFormat('d-M-Y', $request->contract_date_edit)->format('Y/m/d')]) : null;

        isset($request->contract_planned_start_date_edit) ? $request->request->add(['contract_planned_start_date_edit' =>
            Carbon::createFromFormat('d-M-Y', $request->contract_planned_start_date_edit)->format('Y/m/d')]) : null;
        isset($request->contract_planned_end_date_edit) ? $request->request->add(['contract_planned_end_date_edit' =>
            Carbon::createFromFormat('d-M-Y', $request->contract_planned_end_date_edit)->format('Y/m/d')]) : null;
        isset($request->contract_start_date_edit) ? $request->request->add(['contract_start_date_edit' =>
            Carbon::createFromFormat('d-M-Y', $request->contract_start_date_edit)->format('Y/m/d')]) : null;
        isset($request->contract_end_date_edit) ? $request->request->add(['contract_end_date_edit' =>
            Carbon::createFromFormat('d-M-Y', $request->contract_end_date_edit)->format('Y/m/d')]) : null;

        isset($request->ps_validity) ?
            $request->request->add(['ps_validity' =>
                Carbon::createFromFormat('d-M-Y', $request->ps_validity)->format('Y/m/d')]) : null;
        if ($request->hasfile('performance_security_file')) {

            $file = $request->file('performance_security_file');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $approval_file = $file->getClientOriginalName();
            $approval_file_title = $approval_file;
            $hash_file_name = time() . '.' . $extension;

            $file->move('files/contract/performance_security_file/', $hash_file_name);


            $request->request->add(['performance_security_file_file' => $hash_file_name]);
            $request->request->add(['performance_security_file_name' => $approval_file_title]);
        } else {
            $request->request->add(['performance_security_file' => null]);
            $request->request->add(['performance_security_file_name' => null]);
        }

        $contracts->validateUpdateContracts($request);


        $check = $contracts->updateContracts($request->id, $request->except('_token'));

        $storeProjectsHasProvince = new contractHasProvincDivision();
        if ($check) {
            $storeProjectsHasProvince->deleteContractHasProvince($request->id);
            foreach ($request['project_id_edit'] as $index => $obj) {
                $dummy=$storeProjectsHasProvince->storeContractHasProvinceDivision([
                    'contract_id' => $request->id,
                    'project_id' => isset($request->project_id_edit[$index]) ? $request->project_id_edit[$index] : '',
                    'measure_id' => isset($request->measure_id_edit[$index]) ? $request->measure_id_edit[$index] : null,
                    'sub_category_id' => isset($request->subcategory_id_edit[$index]) ? $request->subcategory_id_edit[$index] : null,
                    'province_id' => isset($request->province_id_contracts_edit[$index]) ? $request->province_id_contracts_edit[$index] : '',
                    'budget' => isset($request->contract_province_budget_edit[$index]) ?
                        (double)str_replace(",", "", $request->contract_province_budget_edit[$index]) : 0,
                    'budget_pkr' => isset($request->contract_province_budget_edit_pkr[$index]) ?
                        (double)str_replace(",", "", $request->contract_province_budget_edit_pkr[$index]) : 0,
                    'pea_id' => isset($request->pea_id_contracts_edit[$index]) ? $request->pea_id_contracts_edit[$index] : '',
                    'city_id' => isset($request->contract_city_edit[$index]) ? $request->contract_city_edit[$index] : '',
                    'facility_id' => isset($request->sub_cate_facility_id[$index]) ? $request->sub_cate_facility_id[$index] : null,
                    'description' => isset($request->description_edit[$index]) ? $request->description_edit[$index] : ''
                ]);

                if (Projects::find($request->project_id_edit[$index])->currency_id == $request->currency_id_from_edit) {
                    $dummy->budget =isset($request->contract_province_budget_edit[$index]) ?
                        (double)str_replace(",", "", $request->contract_province_budget_edit[$index]) : 0;
                    $dummy->budget_pkr =isset($request->contract_province_budget_edit_pkr[$index]) ?
                        (double)str_replace(",", "", $request->contract_province_budget_edit_pkr[$index]) : 0;
                } else {
                    $dummy->budget_pkr = isset($request->contract_province_budget_edit[$index]) ? (double)str_replace(",", "", $request->contract_province_budget_edit[$index]) : 0;
                    $dummy->budget = isset($request->contract_province_budget_edit_pkr[$index]) ? (double)str_replace(",", "", $request->contract_province_budget_edit_pkr[$index]) : 0;

                }
                $dummy->save();
            }

            if ($check) {
                $contractPayment = new contractPayment();
                $contractPayment->deleteContractPayment($request->id);
//                return $request;

                if ($request['installment_no'] > 0) {
                    foreach ($request['installment_no'] as $index => $obj) {


                        $contractPayment->storeContractPayment([
                            'contract_id' => $request->id,
//                        'schedule_date' => isset($request->contract_payment_date[$index]) ? $request->contract_payment_date[$index] : null,
                            'schedule_date' => isset($request->contract_payment_date[$index]) ?


                                Carbon::createFromFormat('d-M-Y', $request->contract_payment_date[$index])->format('Y-m-d') : null,
                            'percentage' => isset($request->percentage[$index]) ? str_replace("%", "", $request->percentage[$index]) : null,
                            'amount' => isset($request->paymentAmount_edit[$index]) ? (double)str_replace(",", "", $request->paymentAmount_edit[$index]) : null,
                            'amount_pkr' => isset($request->paymentAmountPkr[$index]) ? (double)str_replace(",", "", $request->paymentAmountPkr[$index]) : null,
                        ]);
                    }
                }
            }
            return redirect()->route('contract.index')->with('success', 'Contract Update Successfully');

        }
        return redirect()->route('contract.index')->with('error', 'Contract Not Update Successfully');
    }


    public function destroy($id)
    {
        $project = new contracts();

        if ($project->deleteContracts($id)) {
            $storeProjectsHasProvince = new contractHasProvincDivision();
            $storeProjectsHasProvince->deleteContractHasProvince($id);
            return redirect()->route('contract.index')->with('success', 'Project Delete Successfully');

        }
        return redirect()->route('contract.index')->with('error', 'Project Cannot Delete Successfully');
    }



//    status 0 for revise
//             1 for approve
//             -1 reject
    public function status($id, $status)
    {
        //    status 0 for revise
//             1 for approve
//             -1 reject
        $checkValue = null;
        $contract = new contracts();
        if ($status == 'revise') {
            $checkValue = 0;
        }
        if ($status == 'approve') {
            $checkValue = 1;
        }
        if ($status == 'reject') {
            $checkValue = 2;
        }

        $obj = $contract->status($id, $checkValue);

        return redirect()->route('contract.index')->with('success', 'Contract Status Successfully');
    }


    public function contractFileCreate(Request $request)
    {


        $approval_file = '';
        $approval_file_title = '';

        if (isset($request->file)) {
            if ($request->hasfile('file')) {

                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $approval_file = $file->getClientOriginalName();
                $approval_file_title = $approval_file;
                $hash_file_name = time() . '.' . $extension;
//                $file->move('files/project/approval/', $hash_file_name);

                $file->move('files/contract/', $hash_file_name);
//                return $request;
                $contractFile = contractsHasFile::create([
                    'file' => $hash_file_name,
                    'file_name' => $approval_file_title,
                    'category' => $request->category,
                    'contract_file_amount' => (double)str_replace(",", "", $request->contract_file_amount) ??null,
                    'description' => ($request->description) ?? "",
                ]);

                if ($contractFile) {
                    return response()->json([
                        'flag' => true,
                        'success' => $contractFile,
                        'path' => URL::to('/files/contract/' . $contractFile->file)
                    ]);


                }
            }
            return response()->json([
                'flag' => false,
                'success' => false
            ]);
        }
        return response()->json([
            'flag' => false,
            'success' => false
        ]);
    }

    public function contractFile(Request $request)
    {


        $approval_file = '';
        $approval_file_title = '';

        if (isset($request->file)) {
            if ($request->hasfile('file')) {

                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $approval_file = $file->getClientOriginalName();
                $approval_file_title = $approval_file;
                $hash_file_name = time() . '.' . $extension;
//                $file->move('files/project/approval/', $hash_file_name);

                $file->move('files/contract/', $hash_file_name);
                $contractFile = contractsHasFile::create([
                    'contract_id' => $request->contract_id,
                    'file' => $hash_file_name,
                    'file_name' => $approval_file_title,
                    'category' => $request->category,
                    'contract_file_amount' => (double)str_replace(",", "", $request->contract_file_amount) ??null,
                    'description' => ($request->description) ?? "",
                ]);

                if ($contractFile) {
                    return response()->json([
                        'flag' => true,
                        'success' => $contractFile,
                        'path' => URL::to('/files/contract/' . $contractFile->file)
                    ]);


                }
            }
            return response()->json([
                'flag' => false,
                'success' => false
            ]);
        }
        return response()->json([
            'flag' => false,
            'success' => false
        ]);
    }

    public function contractFileRetainCreate(Request $request)
    {


        $approval_file = '';
        $approval_file_title = '';


        if ($request->hasfile('file_retain')) {

            $file = $request->file('file_retain');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $approval_file = $file->getClientOriginalName();
            $approval_file_title = $approval_file;

            $hash_file_name = time() . '.' . $extension;
//                $file->move('files/project/approval/', $hash_file_name);

            $file->move('files/contract/retain/', $hash_file_name);
            $contractFile = ContractsHasRetainFile::create([
                'file' => $hash_file_name,
                'file_name' => $approval_file_title,
                'retain_file_date' =>
                    isset($request->retain_file_date) ?
                        Carbon::createFromFormat('d-M-Y', $request->retain_file_date)->format('Y-m-d') : '',
                'retain_file_amount' => (double)str_replace(",", "", $request->retain_file_amount) ??null,
                'description' => ($request->description_retain) ??'',
            ]);
            if ($contractFile) {
                return response()->json([
                    'flag' => true,
                    'success' => $contractFile,
                    'path' => URL::to('/files/contract/retain' . $contractFile->file)
                ]);


            }
            return response()->json([
                'flag' => false,
                'success' => false
            ]);


        }
        return response()->json([
            'flag' => false,
            'success' => false
        ]);
    }

    public function contractFileRetain(Request $request)
    {


        $approval_file = '';
        $approval_file_title = '';


        if ($request->hasfile('file_retain')) {

            $file = $request->file('file_retain');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $approval_file = $file->getClientOriginalName();
            $approval_file_title = $approval_file;

            $hash_file_name = time() . '.' . $extension;
//                $file->move('files/project/approval/', $hash_file_name);

            $file->move('files/contract/retain/', $hash_file_name);
            $contractFile = ContractsHasRetainFile::create([
                'contract_id' => $request->contract_id,
                'file' => $hash_file_name,
                'file_name' => $approval_file_title,
                'retain_file_date' =>
                    isset($request->retain_file_date_edit) ?
                        Carbon::createFromFormat('d-M-Y', $request->retain_file_date_edit)->format('Y-m-d') : '',
                'retain_file_amount' => (double)str_replace(",", "", $request->retain_file_amount) ??null,
                'description' => ($request->description_retain) ??'',
            ]);
            if ($contractFile) {
                return response()->json([
                    'flag' => true,
                    'success' => $contractFile,
                    'path' => URL::to('/files/contract/retain' . $contractFile->file)
                ]);


            }
            return response()->json([
                'flag' => false,
                'success' => false
            ]);


        }
        return response()->json([
            'flag' => false,
            'success' => false
        ]);
    }

    public function DeleteFile(Request $request)
    {
        $ProjectsFiles = contractsHasFile::find($request->id);
        //$ProjectsFiles->file
        if ($ProjectsFiles) {
            $checkDeleleteOrNot = \File::delete('files/contract/' . $ProjectsFiles->file);
            $ProjectsFiles->delete();
        }

        return response()->json([
            'id' => $request->id,
            'contractFiles' => $ProjectsFiles->file
        ]);
    }

    public function DeleteFileRetain(Request $request)
    {
        $ProjectsFiles = ContractsHasRetainFile::find($request->id);
        //$ProjectsFiles->file
        if ($ProjectsFiles) {
            $checkDeleleteOrNot = \File::delete('files/contract/retain/' . $ProjectsFiles->file);
            $ProjectsFiles->delete();
        }

        return response()->json([
            'id' => $request->id,
            'contractFiles' => $ProjectsFiles
        ]);
    }

    public function contractDeleted()
    {
        $deletedContracts = contracts::onlyTrashed()->get();
        return view('admin.contracts.index_deleted', compact('deletedContracts'));
    }

    public function contractRestore($id)
    {
        $contracts = contracts::onlyTrashed()->find($id);
        $contracts->restore();
        return redirect()->route('contract.index');
    }


    public static function getActiveProjectMeasures(Request $request)
    {

        $ProjectsHasProvince = new ProjectsHasProvince();
        $Projects = new Projects();
        $getProvincePeaCity = $Projects->getProvincePeaCity($request->project_id);

        $measure = [];
        $getActiveProjectMeasure = $ProjectsHasProvince->getActiveProjectMeasure($request->project_id);

        if (count($getActiveProjectMeasure) > 0) {

            foreach ($getActiveProjectMeasure as $index => $proItem) {

                $measure[$index]['id'] = $proItem->hasOneMeasures->id;
                $measure[$index]['name'] = $proItem->hasOneMeasures->name;

            }

        }

//        row.find('.province_contracts').value(response.province_contracts);
//        row.find('.province_contracts_id').value(response.province_contracts_id);
//        row.find('.sub_cate_peas').value(response.sub_cate_peas);
//        row.find('.sub_cate_peas_id').value(response.sub_cate_peas_id);
//        row.find('.sub_cate_city').value(response.sub_cate_city);
//        row.find('.sub_cate_city_id').value(response.sub_cate_city_id);
        $cities = new city();

        $html = '';

        $html .= "<option value=''>Select City</option>";
        foreach ($cities->showCityAgainstProvince($getProvincePeaCity[0]->hasOneProvinceRegion->id) as $index => $pro) {
            // $html[$index]=array('title'=> $pro->city_name,'label'=> $pro->city_name, 'value'=> $pro->id);
            $html .= "<option value='" . $pro->id . "'>" . $pro->city_name . "</option>";
        }


        return ([
            'getActiveProjectMeasure' => $measure,
            'province_contracts' => $getProvincePeaCity[0]->hasOneProvinceRegion->name,
            'province_contracts_id' => $getProvincePeaCity[0]->hasOneProvinceRegion->id,
            'sub_cate_peas_id' => isset($getProvincePeaCity[0]->hasOnePea->id) ? $getProvincePeaCity[0]->hasOnePea->id : null,
            'sub_cate_peas' => isset($getProvincePeaCity[0]->hasOnePea->pea_name) ? $getProvincePeaCity[0]->hasOnePea->pea_name : null,
            'cities' => ($html)
        ]
        );
    }

    public static function getActiveProjectMeasuresSelectedSubcategory(Request $request)
    {
        $project = new contracts();
        $subcategory = [];
        $getProjectSelectedMeasureSubcategory = $project->getProjectSelectedMeasureSubcategory($request->project_id, $request->measures_id);
//       dd($getProjectSelectedMeasureSubcategory);
        if (count($getProjectSelectedMeasureSubcategory) > 0) {
            foreach ($getProjectSelectedMeasureSubcategory as $index => $proItem) {

                $subcategory[$index]['id'] = $proItem->hasOneSubCategories->id;
                $subcategory[$index]['name'] = $proItem->hasOneSubCategories->name;

            }

        }


        return ([
            'ProjectSelectedMeasureSubcategory' => $subcategory
        ]
        );
    }


    public static function QueryBudgetProjectMeasureSubcategory($request)
    {
        $CheckProvinceBudget = ProjectsHasProvince::where('project_id', $request->contracts_project_id)
                                                    ->where('measure_id', $request->measures_contracts)
                                                    ->where(function ($query) use ($request) {
                                                        $query->whereNull('sub_category_id')
                                                              ->orWhere('sub_category_id', $request->sub_cate_contracts);
                                                    })
                                                    ->select('budget', 'revise_amount', 'project_id')
                                                    ->first();

        if($request->has('contracts_project_id')) {
            $project = Projects::find($CheckProvinceBudget->project_id);
            return $project->bugdet_revised > 0 ? $CheckProvinceBudget['revise_amount'] : $CheckProvinceBudget['budget'];
        }

    }

    public static function QueryCheckBudget($request)
    {

        $contractHasProvincDivision = contractHasProvincDivision::
        where('project_id', $request->contracts_project_id)
            ->where('measure_id', $request->measures_contracts)
            ->Where('sub_category_id', $request->sub_cate_contracts)
            ->sum('budget');

        return !is_null($contractHasProvincDivision) ? $contractHasProvincDivision : 0;
    }

    public static function QueryCheckBudgetPkr($request)
    {

        $contractHasProvincDivision = contractHasProvincDivision::
        where('project_id', $request->contracts_project_id)
            ->where('measure_id', $request->measures_contracts)
            ->Where('sub_category_id', $request->sub_cate_contracts)
            ->sum('budget_pkr');

        return !is_null($contractHasProvincDivision) ? $contractHasProvincDivision : 0;
    }


    public static function contractCheckedBudgetProjectMeasureSubcategory(Request $request)
    {

        $ProjectsApproval = new Projects();
        $project_currency_id = $ProjectsApproval->where('id', $request->contracts_project_id)->pluck('currency_id')->first();

        if (($request->contract_currency_id_to == $project_currency_id) || ($request->contract_currency_id_from == $project_currency_id)) {
            $CheckProjectApprovedOrNot = $ProjectsApproval->CheckProjectApprovedOrNot($request->contracts_project_id);


            $amount_edit = (double)str_replace(",", "", $request->dymanic_contract_amount);

            $getProjectMeasureBudget = (double)self::QueryBudgetProjectMeasureSubcategory($request);


            $project_currency_id = $ProjectsApproval->where('id', $request->contracts_project_id)->pluck('currency_id')->first();
            $dymanic_contract_amount_pkr = (double)str_replace(",", "", $request->dymanic_contract_amount_pkr);
            $calcaulateBudget = 0;
            if ($project_currency_id == $request->contract_currency_id_from) {
                $QueryCheckBudget = (double)self::QueryCheckBudget($request);
            } else {
                $QueryCheckBudget = (double)self::QueryCheckBudget($request);
            }
            if ($project_currency_id == $request->contract_currency_id_from) {

                $calcaulateBudget = $amount_edit + $QueryCheckBudget;
            } else {
                $calcaulateBudget = $dymanic_contract_amount_pkr + $QueryCheckBudget;
            }

            if ($CheckProjectApprovedOrNot) {
                if ($getProjectMeasureBudget < $calcaulateBudget) {
                    return (
                    [
                        'CheckProjectApprovedOrNot' => $CheckProjectApprovedOrNot,
                        'project_currency_id' => $project_currency_id,
                        'calcaulateBudget' => $calcaulateBudget,
                        'dymanic_contract_amount_pkr' => (double)str_replace(",", "", $request->dymanic_contract_amount_pkr),
                        'flag' => true,
                        'getProjectMeasureBudget' => $getProjectMeasureBudget,

                        'amount_edit + QueryCheckBudget' => $amount_edit + $QueryCheckBudget,
                        'currencyNotMatch' => true
                    ]);
                }


                return [
                    'CheckProjectApprovedOrNot' => $CheckProjectApprovedOrNot,
                    'project_currency_id' => $project_currency_id,
                    'calcaulateBudget' => $calcaulateBudget,
                    'dymanic_contract_amount_pkr' => (double)str_replace(",", "", $request->dymanic_contract_amount_pkr),
                    'flag' => false, 
                    'getProjectMeasureBudget' => $getProjectMeasureBudget,
                    'amount_edit + QueryCheckBudget' => $amount_edit + $QueryCheckBudget,
                    'currencyNotMatch' => true,
                ];

            }
            return [
                'CheckProjectApprovedOrNot' => $CheckProjectApprovedOrNot, 'currencyNotMatch' => true];
        } else {
            return [
                'currencyNotMatch' => false


            ];
        }
    }


    public static function contractCheckedBudgetProjectMeasureSubcategoryUpdate(Request $request)
    {


        $ProjectsApproval = new Projects();
        $savededBudgetbudgetQueryCheckBudget1 = 0;
        $calcaulateBudget = 0;
        $contract_province_budget_edit_pkr = 0;

        $CheckProjectApprovedOrNot = $ProjectsApproval->CheckProjectApprovedOrNot($request->contracts_project_id);
        $contract_province_budget_edit_pkr = (double)str_replace(",", "", $request->contract_province_budget_edit_pkr);
        $project_currency_id = $ProjectsApproval->where('id', $request->contracts_project_id)->pluck('currency_id')->first();

        if (($request->contract_currency_id_to == $project_currency_id) || ($request->contract_currency_id_from == $project_currency_id)) {
            $amount_edit = (double)str_replace(",", "", $request->dymanic_contract_amount);

            $getProjectMeasureBudget = self::QueryBudgetProjectMeasureSubcategory($request);
            $savededBudget = contractHasProvincDivision::find($request->contract_id);
            
            if ($project_currency_id == $request->contract_currency_id_from) {

                $QueryCheckBudget = self::QueryCheckBudget($request);

                $savededBudgetbudgetQueryCheckBudget;

                if ($savededBudget) {
                    $savededBudgetbudgetQueryCheckBudget = (double)$QueryCheckBudget - (double)$savededBudget->budget;
                } else {
                    $savededBudgetbudgetQueryCheckBudget = (double)$QueryCheckBudget - 0;
                }


                $savededBudgetbudgetQueryCheckBudget1 = $savededBudgetbudgetQueryCheckBudget < 0 ? 0 : $savededBudgetbudgetQueryCheckBudget;
                $calcaulateBudget = $amount_edit + $savededBudgetbudgetQueryCheckBudget1;


            } else {
                $QueryCheckBudgetPkr = self::QueryCheckBudget($request);

                if (($savededBudget)) {

                    $savededBudgetbudgetQueryCheckBudget = (double)$QueryCheckBudgetPkr - (double)$savededBudget->budget_pkr;
//return [
//    $request->contract_id
//];
                } else {
                    $savededBudgetbudgetQueryCheckBudget = (double)$QueryCheckBudgetPkr - 0;
                }

                $savededBudgetbudgetQueryCheckBudget1 = $savededBudgetbudgetQueryCheckBudget < 0 ? 0 : $savededBudgetbudgetQueryCheckBudget;

                $calcaulateBudget = $contract_province_budget_edit_pkr + $savededBudgetbudgetQueryCheckBudget1;


            }


            if ($CheckProjectApprovedOrNot) {


                if ($getProjectMeasureBudget < $calcaulateBudget) {
                    return (
                    [
                        'flag' => true,
                        'CheckProjectApprovedOrNot' => $CheckProjectApprovedOrNot,
                        'contract_province_budget_edit_pkr' => $contract_province_budget_edit_pkr,
                        'calcaulateBudget' => $calcaulateBudget,
                        'getProjectMeasureBudget' => $getProjectMeasureBudget,
                        'amount_edit' => $amount_edit,
                        'QueryCheckBudget' => $savededBudgetbudgetQueryCheckBudget1,

                        'contract_province_budget_edit_pkr + QueryCheckBudget' => $amount_edit + $savededBudgetbudgetQueryCheckBudget,
                        'currencyNotMatch' => true
                    ]);
                }

                return [
                    'flag' => false,
                    'CheckProjectApprovedOrNot' => $CheckProjectApprovedOrNot,
                    'contract_province_budget_edit_pkr' => $contract_province_budget_edit_pkr,
                    'calcaulateBudget' => $calcaulateBudget,
                    'getProjectMeasureBudget' => $getProjectMeasureBudget,
                    'amount_edit' => $amount_edit,
                    'QueryCheckBudget' => $savededBudgetbudgetQueryCheckBudget1,

                    'amount_edit + QueryCheckBudget' => $contract_province_budget_edit_pkr + $savededBudgetbudgetQueryCheckBudget,
                    'currencyNotMatch' => true];


            }
            return [
                'CheckProjectApprovedOrNot' => $CheckProjectApprovedOrNot, 'currencyNotMatch' => true];


        }
        return [
            'currencyNotMatch' => false];
    }

    function deleteContractChildRow(Request $request)
    {
        contractHasProvincDivision::find($request->id)->delete();
        return [
            'delete' => 1, 'delete' => true];
    }



    public function getFacilities(Request $request)
    {
        return Facility::where('city_id', $request->city)->pluck('facility_name', 'id');
    }
}
