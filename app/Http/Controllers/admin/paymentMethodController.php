<?php

namespace App\Http\Controllers\admin;

use App\Model\paymentMethod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class paymentMethodController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:payment-method-list|payment-method-create|payment-method-edit|payment-method-delete|payment-method-restore', ['only' => ['index', 'store']]);
        $this->middleware('permission:payment-method-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:payment-method-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:payment-method-delete', ['only' => ['destroy']]);
        $this->middleware('permission:payment-method-restore', ['only' => ['Restore']]);
    }

    public function index()
    {
        $paymentMethod = new paymentMethod();

        return view('admin.paymentMethod.index', [
            'paymentMethods' => $paymentMethod->getAllPaymentMethod()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $paymentMethod = new paymentMethod();
        $paymentMethod->validatePaymentMethod($request);

        $paymentMethod->storePaymentMethod($request->input());
        return redirect()->route('paymentMethod.index')->with('success', 'paymentMethod Create Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $paymentMethod = new paymentMethod();
        return $paymentMethod->showPaymentMethod($request->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
//        return $request;
        $paymentMethod = new paymentMethod();
        $validator = $this->validate($request, [
            'currency_id' => 'required|max:255',
            'payment_method_name' => 'required',
            'exchange_rate' => 'required',
            'short_code' => 'required',
            'sort_order' => 'required|unique:payment_methods,sort_order,'.$request->payment_method_id,
        ]);
//        return $request;
        if ($paymentMethod->updatePaymentMethod($request->payment_method_id, $request->except('_token'))) {
            return response('Update Successfully', 200);
        }
        return response('Something went wrong.', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $paymentMethod = new paymentMethod();

        if ($paymentMethod->deletePaymentMethod($id)) {
            return redirect()->route('paymentMethod.index')->with('success', 'PaymentMethod Delete Successfully');

        }
        return redirect()->route('paymentMethod.index')->with('error', 'PaymentMethod Cannot Delete Successfully');
    }
    public function Deleted() {
      $deletedPayment = paymentMethod::onlyTrashed()->get();
      return view('admin.paymentMethod.index_deleted',compact('deletedPayment'));
    }

    public function Restore($id) {

       $paymentMethod = paymentMethod::onlyTrashed()->find($id);
       $paymentMethod->restore();
       return redirect()->route('paymentMethod.index');
       }
}
