<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\ContractorFinancialDetails;
use App\Model\Facility;
use App\Model\PaymentSupportingDocuments;
use App\Model\PaymentSupportingFileDetial;
use App\Model\contractHasProvincDivision;
use App\Model\contractors;
use App\Model\contracts;
use App\Model\currency;
use App\Model\payments;
use App\Model\paymentsHasSub;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;


class paymentcontroller extends Controller
{
    function __construct()
    {
        $this->middleware('permission:payments-list|payments-create|payments-edit|payments-delete|payments-restore', ['only' => ['index', 'store']]);
        $this->middleware('permission:payments-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:payments-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:payments-delete', ['only' => ['destroy']]);
        $this->middleware('permission:payments-restore', ['only' => ['paymentRestore']]);
    }

    public function index()
    {
        $payments = new payments;
        return view('admin.payment.index', ['payments' => $payments->getAllPayment()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $contractors = contractors::whereHas('contracts', function ($query) {
        //                     return $query->where('status', true)->whereHas('payments', function ($q) {
        //                         return $q->select(DB::raw('SUM(invoice_amount) as total'))
        //                                  ->havingRaw('total < contracts.amount');
        //                     });
        //                })->dd();

        // return $contractors;
        $contractor_id = contracts::groupBy('contractor_id')->select('contractor_id')->get();
        $currency = currency::all();
        return view('admin.payment.create', [
            'contractor_id' => $contractor_id,
            'currency' => $currency
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getContract(Request $request)
    {
        $contracts = new contracts();
        $obj = $contracts->showContracts($request->contract_id);

        return response()->json([
            'contract_title' => $obj->hasContractor->contractor_name,
            'currency_name' => $obj->hasCurrency->currency_name,
            'currency_id' => $obj->hasCurrency->id,
            'contractExchange_rate' => $obj->hasPaymentMethod->exchange_rate,
            'contractPaymentMethod' => $obj->hasPaymentMethod->payment_method_name,
            'contractPaymentMethodId' => $obj->hasPaymentMethod->id,
            'contractMeasuresId' => $obj->hasMeasures->id,
            'contractMeasures' => $obj->hasMeasures->name,
            'contractAmount' => $obj->amount,
            'province_id' => $obj->province_id,
            'obj' => $obj,

        ]);
    }


    public function GetAccountData(Request $request)
    {

        $ContractorFinancialDetails = ContractorFinancialDetails::where('id', $request->account_id)->first();
        if (($ContractorFinancialDetails)) {
            return response()->json([
                'bank_name' => $ContractorFinancialDetails->bank_name,
                'account_no' => $ContractorFinancialDetails->account_no,
                'iban' => $ContractorFinancialDetails->iban,
                'branch_name' => $ContractorFinancialDetails->branch_name,
                'branch_code' => $ContractorFinancialDetails->branch_code,
                'city' => $ContractorFinancialDetails->hasCity->city_name,
                'swift_code' => $ContractorFinancialDetails->swift_code,
                'branch_address' => $ContractorFinancialDetails->branch_address,
                'success' => true
            ]);
        } else {
        }
    }

    public function GetContractorContracts(Request $request)
    {
        $contracts = contracts::whereStatus(true)->where('contractor_id', $request->contractor_id)->select('id', 'contract_title')->get();
        $ContractorFinancialDetails = ContractorFinancialDetails::where('contractor_id', $request->contractor_id)->pluck('bank_name', 'id');
        $html = '<option value="">Select Contract</option>';
        $html_account = '<option value="">Select Account No</option>';
        if (($contracts) && (count($contracts)) > 0) {
            foreach ($contracts as $index => $obj) {
                $html .= '<option value="' . $obj->id . '">' . $obj->contract_title . '</option>';
            }

            foreach ($ContractorFinancialDetails as $id => $name) {
                $html_account .= "<option value='$id'>$name</option>";
            }

            return response()->json([
                'vendor_contracts' => $html,
                'contractor_financial_details' => $html_account,
                'contractor' => contractors::find($request->contractor_id) ?? null,
                'success' => true
            ]);
        }
        return response()->json([
            'vendor_contracts' => null,
            'contractor_financial_details' => null,
            'success' => true
        ]);
    }

    //get contract basic detial
    public function GetLastPayment($contract_id)
    {
        $previous_payment = payments::where('contract_id', $contract_id)->select('created_at', 'invoice_amount')->latest()->first();
        return [
            'last_date' => $previous_payment ? Carbon::parse($previous_payment->created_at)->format('d-M-Y') : "",
            'last_amount' => $previous_payment ? $previous_payment->invoice_amount : 0
        ];
    }

    public function GetPreviousPayment($contract_id)
    {
        $previous_payment = paymentsHasSub::where('contract_id', $contract_id)->get(array(
                DB::raw('SUM(budget)'),
                DB::raw('SUM(budget_pkr)')
            ));

        return [
            'previous_payment_budget' => $previous_payment[0]['SUM(budget)'] ? $previous_payment[0]['SUM(budget)'] : 0,
            'previous_payment_budget_pkr' => $previous_payment[0]['SUM(budget_pkr)'] ? $previous_payment[0]['SUM(budget_pkr)'] : 0
        ];
    }

    public function GetContractsDetails(Request $request)
    {

        $contracts = contracts::where('id', $request->contract_id)
            ->select('id', 'payment_id', 'exchange_rate_id', 'amount', 'amount_pkr', 'less_retention', 'ps_validity')
            ->first();

        if ($contracts) {
            $contract_amount = $contracts->amount ? $contracts->amount : 0;
            $contract_revised_amount = $contracts->amount_pkr ? $contracts->amount_pkr : 0;
            $previous_payment_budget = self::GetPreviousPayment($request->contract_id)['previous_payment_budget'];
            $previous_payment_budget_pkr = self::GetPreviousPayment($request->contract_id)['previous_payment_budget_pkr'];
            $balance_payable_amount = $contract_amount - $previous_payment_budget;
            $balance_payable_amount_pkr = $contract_revised_amount - $previous_payment_budget_pkr;

            return response()->json([
                'contract_exchange_rate_currency_code' => $contracts->hasExChangeRate->currencyTo->currency_code,
                'contract_exchange_rate_currency_code_id' => $contracts->hasExChangeRate->currencyTo->id,
                'contract_exchange_rate_currency_code_from' => $contracts->hasExChangeRate->currencyFrom->currency_code,
                'contract_exchange_rate_currency_code_from_id' => $contracts->hasExChangeRate->currencyFrom->id,
                'contract_amount' => $contract_amount,
                'contract_revised_amount' => $contract_revised_amount,

                'previous_payment_amount' => self::GetPreviousPayment($request->contract_id)['previous_payment_budget'],
                'previous_payment_amount_pkr' => self::GetPreviousPayment($request->contract_id)['previous_payment_budget_pkr'],
                'balance_payable_amount' => $balance_payable_amount,
                'balance_payable_amount_pkr' => $balance_payable_amount_pkr,
                'contract_projects' => self::getContractProjects($request->contract_id),

                'payment_origin' => $contracts->hasPaymentMethod->short_code ?? null,
                'contract_currency' => $contracts->hasExChangeRate->currencyFrom->currency_code ?? null,
                'last_payment_date' => !empty(self::GetLastPayment($request->contract_id)['last_date']) ? self::GetLastPayment($request->contract_id)['last_date'] : 'Nill',
                'last_payment_amount' => self::GetLastPayment($request->contract_id)['last_amount'],
                'less_retention' => $contracts->less_retention,
                'ps_validity'    => $contracts->ps_validity,
                'payment_request_no' => $contracts->payments()->count() == 0 ? 1 : $contracts->payments()->count() + 1,


                'success' => true
            ]);
        } else {
            return response()->json([
                'contract_exchange_rate_currency_code' => null,
                'contract_amount' => null,
                'contract_revised_amount' => null,
                'contract_projects' => self::getContractProjects($request->contract_id),
                'success' => true
            ]);
        }
    }

    //get all project against single contract
    public function getContractProjects($id)
    {

        $contractHasProvincDivision = contractHasProvincDivision::with('hasProjects:id,title')
            ->where('contract_id', $id)
            ->select('project_id')
            ->groupBy('project_id')
            ->get();
        $projectArray = [];
        $html = '<option value="">Select a Project</option>';
        foreach ($contractHasProvincDivision as $index => $arr) {
            $html .= '<option value="' . $arr->hasProjects->id . '">' . $arr->hasProjects->title . '</option>';
        }
        return $html;
    }

    //get all project measure against selected contract
    public function GetProjectMeasureSubcategory(Request $request)
    {
        $contractHasProvincDivision = contractHasProvincDivision::where('contract_id', $request->contract_id)
            ->where('project_id', $request->project_id)
            ->where('measure_id', $request->payment_project_measure)
            ->select('sub_category_id')
            ->groupBy('sub_category_id')
            ->get();


        $html = '<option value="">Select Subcategory</option>';

        if (count($contractHasProvincDivision) > 0) {
            foreach ($contractHasProvincDivision as $index => $arr) {
                $html .= '<option value="' . $arr->hasSubCategories->id . '">' . $arr->hasSubCategories->name . '</option>';
            }
        }

        return response()->json([
            'contractHasProvincDivision' => $contractHasProvincDivision,
            'sub_category_id' => $html,
            'success' => true
        ]);
    }

    public function GetProjectMeasure(Request $request)
    {

        $getcontractData = contractHasProvincDivision::where('contract_id', $request->contract_id)->where('project_id', $request->project_id)->first();

        $contractHasProvincDivision = contractHasProvincDivision::with('hasMeasure')
            ->where('contract_id', $request->contract_id)
            ->where('project_id', $request->project_id)
            ->select('measure_id')
            ->groupBy('measure_id')
            ->get();

        $contracts_cities = contractHasProvincDivision::with('hasMeasure')
            ->where('contract_id', $request->contract_id)
            ->where('project_id', $request->project_id)
            ->select('city_id')
            ->groupBy('city_id')
            ->get();
        $html = '<option value="">Select Measure</option>';
        if ($contracts_cities) {
            foreach ($contractHasProvincDivision as $index => $arr) {
                $html .= '<option value="' . $arr->hasMeasure->id . '">' . $arr->hasMeasure->name . '</option>';
            }
        }


        $cities = '<option value="">Select City</option>';

        if ($contracts_cities[0]->city_id) {
            foreach ($contracts_cities as $index => $arr) {
                $cities .= '<option value="' . ($arr->hasCity->id ?? '') . '">' . ($arr->hasCity->city_name ?? '') . '</option>';
            }
        }
        return response()->json([
            'project_measure' => $html,
            'cities' => $cities,

            'peas_id' => $getcontractData->hasPeas->id ?? null,
            'peas_name' => $getcontractData->hasPeas->pea_name ?? null,
            'province_id' => $getcontractData->hasProvince->id ?? null,
            'province_name' => $getcontractData->hasProvince->name ?? null,

            'success' => true
        ]);
    }

    public function PreviousPaymentAmount()
    {
    }

    public function store(Request $request)
    {
        $payments = new payments();
        $request->request->add(['invoice_amount' =>
            isset($request->invoice_amount) ? (float)str_replace(",", "", $request->invoice_amount) : 0]);
        $request->request->add(['invoice_less_retention_amount' => isset($request->invoice_less_retention_amount) ? (float)str_replace(",", "", $request->invoice_less_retention_amount) : 0]);
        $request->request->add(['payment_net_payable_amount' => isset($request->payment_net_payable_amount) ? (float)str_replace(",", "", $request->payment_net_payable_amount) : 0]);
        $request->request->add(['payment_details_amount' => isset($request->payment_details_amount) ? (float)str_replace(",", "", $request->payment_details_amount) : 0]);
        $request->request->add(['invoice_net_payable' => isset($request->invoice_net_payable) ? (float)str_replace(",", "", $request->invoice_net_payable) : 0]);

        isset($request->invoice_date) ? $request->request->add(['invoice_date' => Carbon::parse($request->invoice_date)->format('Y/m/d')]) : null;
        isset($request->invoice_performance_security_validity) ? $request->request->add(['invoice_performance_security_validity' => Carbon::parse($request->invoice_performance_security_validity)->format('Y/m/d')]) : null;
        isset($request->payment_details_date) ? $request->request->add(['payment_details_date' => Carbon::parse($request->payment_details_date)->format('Y/m/d')]) : null;

        $payments->validatePayment($request);

        $get_payment_id = $payments->storePayment($request->all());
        if ($get_payment_id->id) {
            foreach ($request['project_id'] as $index => $arr) {
                paymentsHasSub::create([
                    'payment_id' => $get_payment_id->id,
                    'contract_id' => $request['contract_id'],
                    'project_id' => $request['project_id'][$index],
                    'measure_id' => $request['measure_id'][$index],
                    'sub_category_id' => $request['sub_cate_id'][$index],
                    'province_id' => $request['province_id'][$index],
                    'pea_id' => $request['sub_cate_peas_id'][$index],
                    'city_id' => $request['city_id'][$index],
                    'facility_id' => $request['facility_id'][$index],
                    'budget' => isset($request['payment_amount'][$index]) ? (float)str_replace(",", "", $request['payment_amount'][$index]) : 0,
                    'budget_pkr' => isset($request['payment_amount_pkr'][$index]) ? (float)str_replace(",", "", $request['payment_amount_pkr'][$index]) : 0,
                    'description' => $request['payment_project_description'][$index]

                ]);
            }

            if (($request->supporting_documents_progress) > 0 && count($request->supporting_documents_progress)) {
                foreach ($request->supporting_documents_progress as $index => $file)
                    PaymentSupportingDocuments::where('id', $request->supporting_documents_progress[$index])->update(['payment_id' => $get_payment_id->id]);
            }

            if (($request->payment_supporting_documents) > 0 && count($request->payment_supporting_documents)) {
                foreach ($request->payment_supporting_documents as $index => $file)
                    PaymentSupportingFileDetial::where('id', $request->payment_supporting_documents[$index])->update(['payment_id' => $get_payment_id->id]);
            }
        }
        //        paymentsHasSub::

        return redirect()->route('payment.index')->with('success', 'Payment Create Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Payments = new payments;
        $currency = currency::all();
        $payment = $Payments->showPayment($id);
        $contractor_id = contracts::groupBy('contractor_id')->select('contractor_id')->get();


        $contracts = new contracts();
        $obj = $contracts->showContracts($payment->contract_id);


        $getAllVendorContract = contracts::where('contractor_id', $payment->contractor_id)->select('id', 'contract_title')->get();
        $ContractorFinancialDetails = ContractorFinancialDetails::where('contractor_id', $payment->contractor_id)->pluck('bank_name', 'id');

        $contract_amount = $obj->amount ? $obj->amount : 0;
        $contract_revised_amount = $obj->amount_pkr ? $obj->amount_pkr : 0;
        $previous_payment_budget = self::GetPreviousPayment($payment->contract_id)['previous_payment_budget'];
        $previous_payment_budget_pkr = self::GetPreviousPayment($payment->contract_id)['previous_payment_budget_pkr'];
        $balance_payable_amount = $contract_amount - $previous_payment_budget;
        $balance_payable_amount_pkr = $contract_revised_amount - $previous_payment_budget_pkr;

        return view('admin.payment.show', [
            'payment' => $payment,
            'contractor_id' => $contractor_id,
            'contract_title' => $obj->hasContractor->contractor_name,
            'currency_name' => $obj->hasCurrency->currency_code,
            'currency_id' => $obj->hasCurrency->id,
            'contractExchange_rate' => $obj->hasPaymentMethod->exchange_rate ?? null,
            'contractPaymentMethod' => $obj->hasPaymentMethod->payment_method_name ?? null,
            'contractPaymentMethodId' => $obj->hasPaymentMethod->id ?? null,
            //            'contractMeasuresId' => $obj->hasMeasures->id,0
            //            'contractMeasures' => $obj->hasMeasures->name,
            'contractAmount' => $obj->amount,
            'province_id' => $obj->province_id,
            'get_all_vendor_contract' => $getAllVendorContract,
            'contractor_financial_details' => $ContractorFinancialDetails,
            'currency' => $currency,

            'contract_amount' => $contract_amount,
            'contract_revised_amount' => $contract_revised_amount,
            'previous_payment_amount' => self::GetPreviousPayment($payment->contract_id)['previous_payment_budget'],
            'previous_payment_amount_pkr' => self::GetPreviousPayment($payment->contract_id)['previous_payment_budget_pkr'],
            'balance_payable_amount' => $balance_payable_amount,
            'balance_payable_amount_pkr' => $balance_payable_amount_pkr,
            'contract_projects' => self::getContractProjects($obj->contract_id),

            'payment_origin' => $obj->hasPaymentMethod ? $obj->hasPaymentMethod->short_code : '',
            'contract_currency' => $obj->hasExChangeRate->currencyFrom->currency_code,
            'last_payment_date' => self::GetLastPayment($payment->contract_id)['last_date'],
            'last_payment_amount' => self::GetLastPayment($payment->contract_id)['last_amount'],

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Payments = new payments;
        $currency = currency::all();
        $payment = $Payments->showPayment($id);
        $contractor_id = contracts::groupBy('contractor_id')->select('contractor_id')->get();


        $contracts = new contracts();
        $obj = $contracts->showContracts($payment->contract_id);


        $getAllVendorContract = contracts::where('contractor_id', $payment->contractor_id)->select('id', 'contract_title')->get();
        $ContractorFinancialDetails = ContractorFinancialDetails::where('contractor_id', $payment->contractor_id)->pluck('bank_name', 'id');

        $contract_amount = $obj->amount ? $obj->amount : 0;
        $contract_revised_amount = $obj->amount_pkr ? $obj->amount_pkr : 0;
        $previous_payment_budget = self::GetPreviousPayment($payment->contract_id)['previous_payment_budget'];
        $previous_payment_budget_pkr = self::GetPreviousPayment($payment->contract_id)['previous_payment_budget_pkr'];
        $balance_payable_amount = $contract_amount - $previous_payment_budget;
        $balance_payable_amount_pkr = $contract_revised_amount - $previous_payment_budget_pkr;

        return view('admin.payment.edit', [
            'payment' => $payment,
            'contractor_id' => $contractor_id,
            'contract_title' => $obj->hasContractor->contractor_name,
            'currency_name' => $obj->hasCurrency->currency_code,
            'currency_id' => $obj->hasCurrency->id,
            'contractExchange_rate' => $obj->hasPaymentMethod->exchange_rate ?? null,
            'contractPaymentMethod' => $obj->hasPaymentMethod->payment_method_name ?? null,
            'contractPaymentMethodId' => $obj->hasPaymentMethod->id ?? null,
            //            'contractMeasuresId' => $obj->hasMeasures->id,0
            //            'contractMeasures' => $obj->hasMeasures->name,
            'contractAmount' => $obj->amount,
            'province_id' => $obj->province_id,
            'get_all_vendor_contract' => $getAllVendorContract,
            'contractor_financial_details' => $ContractorFinancialDetails,
            'currency' => $currency,

            'contract_amount' => $contract_amount,
            'contract_revised_amount' => $contract_revised_amount,
            'previous_payment_amount' => self::GetPreviousPayment($payment->contract_id)['previous_payment_budget'],
            'previous_payment_amount_pkr' => self::GetPreviousPayment($payment->contract_id)['previous_payment_budget_pkr'],
            'balance_payable_amount' => $balance_payable_amount,
            'balance_payable_amount_pkr' => $balance_payable_amount_pkr,
            'contract_projects' => self::getContractProjects($obj->contract_id),

            'payment_origin' => $obj->hasPaymentMethod ? $obj->hasPaymentMethod->short_code : '',
            'contract_currency' => $obj->hasExChangeRate->currencyFrom->currency_code,
            'last_payment_date' => self::GetLastPayment($payment->contract_id)['last_date'],
            'last_payment_amount' => self::GetLastPayment($payment->contract_id)['last_amount'],

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            \DB::beginTransaction();

            $payments = new payments();
            $paymentsHasSub = new paymentsHasSub();
            $request->request->add(['invoice_amount' =>
                isset($request->invoice_amount) ? (float)str_replace(",", "", $request->invoice_amount) : 0]);
            $request->request->add(['invoice_less_retention_amount' => isset($request->invoice_less_retention_amount) ? (float)str_replace(",", "", $request->invoice_less_retention_amount) : 0]);
            $request->request->add(['payment_net_payable_amount' => isset($request->payment_net_payable_amount) ? (float)str_replace(",", "", $request->payment_net_payable_amount) : 0]);
            $request->request->add(['payment_details_amount' => isset($request->payment_details_amount) ? (float)str_replace(",", "", $request->payment_details_amount) : 0]);
            $request->request->add(['invoice_net_payable' => isset($request->invoice_net_payable) ? (float)str_replace(",", "", $request->invoice_net_payable) : 0]);

            isset($request->invoice_date) ? $request->request->add(['invoice_date' => Carbon::parse($request->invoice_date)->format('Y/m/d')]) : null;
            isset($request->invoice_performance_security_validity) ? $request->request->add(['invoice_performance_security_validity' => Carbon::parse($request->invoice_performance_security_validity)->format('Y/m/d')]) : null;
            isset($request->payment_details_date) ? $request->request->add(['payment_details_date' => Carbon::parse($request->payment_details_date)->format('Y/m/d')]) : null;

            $payments->validatePayment($request);
            $check = $payments->updatePayment($request->id, $request->except('_token'));


            if ($check) {
                if (isset($request['project_id'])) {
                    $payment = payments::find($id);
                    foreach ($request['project_id'] as $index => $arr) {
                        $paymentsHasSub->create([
                            'payment_id' => $id,
                            'contract_id' => $payment->contract_id,
                            'project_id' => $request['project_id'][$index],
                            'measure_id' => $request['measure_id'][$index],
                            'sub_category_id' => $request['sub_cate_id'][$index] ?? null,
                            'province_id' => $request['province_id'][$index],
                            'pea_id' => $request['sub_cate_peas_id'][$index],
                            'city_id' => $request['city_id'][$index],
                            'facility_id' => $request['facility_id'][$index],
                            'budget' => isset($request['payment_amount'][$index]) ? (float)str_replace(",", "", $request['payment_amount'][$index]) : 0,
                            'budget_pkr' => isset($request['payment_amount_pkr'][$index]) ? (float)str_replace(",", "", $request['payment_amount_pkr'][$index]) : 0,
                            'description' => $request['payment_project_description'][$index]

                        ]);
                    }
                }
            }



            if (($request->supporting_documents_progress) > 0 && count($request->supporting_documents_progress)) {
                foreach ($request->supporting_documents_progress as $index => $file)
                    PaymentSupportingDocuments::where('id', $request->supporting_documents_progress[$index])
                        ->update(['payment_id' => $id]);
            }

            if (($request->payment_supporting_documents) > 0 && count($request->payment_supporting_documents)) {
                foreach ($request->payment_supporting_documents as $index => $file)
                    PaymentSupportingFileDetial::where('id', $request->payment_supporting_documents[$index])
                        ->update(['payment_id' => $id]);
            }

            // return $check;
            return redirect()->route('payment.index')->with('success', 'Payment updated successfully!');
            \DB::commit();
        } catch (\Exception $e) {
            dd($e);
            return redirect()->back()->with('error', "Something went wrong." . $e->getCode());
        }
    }

    public function status($id, $status)
    {
        //    status 0 for revise
        //             1 for approve
        //             -1 reject
        $checkValue = null;
        $project = new payments();

        if ($status == 'approve') {
            $checkValue = 1;
        }
        if ($status == 'pending') {
            $checkValue = 0;
        }
        if ($status == 'reject') {
            $checkValue = 2;
        }

        $obj = $project->status($id, $checkValue);

        return redirect()->route('payment.index')->with('success', 'Payment Status Successfully');
    }

    public function destroy($id)
    {
        $project = new payments();
        $project->deletePayment($id);
        return redirect()->route('payment.index')->with('success', 'Payment Delete     Successfully');
    }


    public function paymentDeleted()
    {
        $deletedPayments = payments::onlyTrashed()->get();
        return view('admin.payment.index_deleted', compact('deletedPayments'));
    }

    public function paymentRestore($id)
    {
        $payments = payments::onlyTrashed()->find($id);
        $payments->restore();
        return redirect()->route('payment.index');
    }


    public function supportingDocumentsFileSave(Request $request)
    {
        //return !isset($request->payment_id) ? $request->payment_id : NULL;
        $approval_file = '';
        $approval_file_title = '';


        if ($request->hasfile('file')) {

            $file = $request->file('file');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $approval_file = $file->getClientOriginalName();
            $approval_file_title = $approval_file;

            $hash_file_name = time() . '.' . $extension;
            //                $file->move('files/project/approval/', $hash_file_name);

            $file->move('files/payment/supporting/', $hash_file_name);

            $contractFile = PaymentSupportingDocuments::create([
                'payment_id' => !isset($request->payment_id) ? $request->payment_id : NULL,
                'file' => $hash_file_name,
                'file_name' => $approval_file_title,
                'category' => $request->category,
                'description' => $request->description,
                'other_category' => $request->category == 9 ? $request->other : null
            ]);

            $contractFile['category_name'] = $request->category == 9 ? $request->other : config('constants.supporting_documents')[$request->category];

            if ($contractFile) {
                return response()->json([
                    'flag' => true,
                    'success' => $contractFile,
                    'path' => URL::to('/files/payment/supporting/' . $contractFile->file)
                ]);
            }
            return response()->json([
                'flag' => false,
                'success' => false
            ]);
        }
        return response()->json([
            'flag' => false,
            'success' => false
        ]);
    }

    public function supportingDocumentsFileDelete(Request $request)
    {
        $ProjectsFiles = PaymentSupportingDocuments::find($request->id);
        //$ProjectsFiles->file
        if ($ProjectsFiles) {
            $checkDeleleteOrNot = \File::delete('/files/payment/supporting/' . $ProjectsFiles->file);
            $ProjectsFiles->delete();
        }

        return response()->json([
            'id' => $request->id,
            'file' => $ProjectsFiles->file
        ]);
    }


    public function paymentSupportingDocumentsFileSave(Request $request)
    {
        $approval_file = '';
        $approval_file_title = '';


        if ($request->hasfile('file')) {

            $file = $request->file('file');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $approval_file = $file->getClientOriginalName();
            $approval_file_title = $approval_file;

            $hash_file_name = time() . '.' . $extension;
            //                $file->move('files/project/approval/', $hash_file_name);

            $file->move('files/payment/supporting/detail', $hash_file_name);

            $contractFile = PaymentSupportingFileDetial::create([
                'payment_id' => !isset($request->payment_id) ? $request->payment_id : NULL,
                'file' => $hash_file_name,
                'file_name' => $approval_file_title,
                'category' => $request->category,
                'description' => $request->description,
                'other_category' => $request->category == 9 ? $request->other : null
            ]);

            $contractFile['category_name'] = $request->category == 9 ? $request->other : config('constants.supporting_documents')[$request->category];

            if ($contractFile) {
                return response()->json([
                    'flag' => true,
                    'success' => $contractFile,
                    'path' => URL::to('/files/payment/supporting/detail/' . $contractFile->file)
                ]);
            }
            return response()->json([
                'flag' => false,
                'success' => false
            ]);
        }
        return response()->json([
            'flag' => false,
            'success' => false
        ]);
    }

    public function paymentSupportingDocumentsFileDelete(Request $request)
    {
        $ProjectsFiles = PaymentSupportingFileDetial::find($request->id);
        //$ProjectsFiles->file
        if ($ProjectsFiles) {
            $checkDeleleteOrNot = \File::delete('/files/payment/supporting/detail/' . $ProjectsFiles->file);
            $ProjectsFiles->delete();
        }

        return response()->json([
            'id' => $request->id,
            'contractFiles' => $ProjectsFiles->file
        ]);
    }


    public function calculateBudget(Request $request)
    {
        $contract_budget = contractHasProvincDivision::where('contract_id', $request->contract_id)
            ->where('project_id', $request->project_id)
            ->where('measure_id', $request->measure_id)
            ->where(function ($query) use ($request) {
                $query->whereNull('sub_category_id')
                    ->orWhere('sub_category_id', $request->sub_cate_id);
            })
            ->where('city_id', $request->city_id)
            ->where('facility_id', $request->facility_id)
            ->first();


        if ($contract_budget) { // if contract found
            $calculateBudgetAmount = $contract_budget->contract->hasExChangeRate->currency_from == 1 ? $contract_budget->budget :  $contract_budget->budget_pkr;
            // $contract_budget = ;
            // if($contract->hasExChangeRate->currency_from == App\Model\Projects::find($con->project_id)->currency_id)
            // check if valuet is less then contract budget
            if ($request->budget <= $calculateBudgetAmount) {
                $payment_previous_amount = paymentsHasSub::where('contract_id', $request->contract_id)
                    ->where('project_id', $request->project_id)
                    ->where('measure_id', $request->measure_id)
                    ->where(function ($query) use ($request) {
                        $query->whereNull('sub_category_id')
                            ->orWhere('sub_category_id', $request->sub_cate_id);
                    })
                    ->where('city_id', $request->city_id)
                    ->sum('budget');

                $check = $calculateBudgetAmount - ($payment_previous_amount + $request->budget);

                if (round($check) >= 0) { // if remaing amount is less then zero
                    // if(round($request->payment_total_amount) == round($request->invoice_amount)) {
                    return [
                        'success' => true,
                        'message' => 'Ammount added successfully.'
                    ];
                    // }  else {
                    //     return [
                    //         'success' => false,
                    //         'message' => "Total Project(s) amount of ($request->payment_total_amount) not equal to total invoice amount of ($request->invoice_amount)." ,
                    //     ];  
                    // }
                } else {
                    $remaining = number_format(round($calculateBudgetAmount - $payment_previous_amount), 6);
                    return [
                        'success' => false,
                        'message' => "Your value is invalid: Contract Budget ($calculateBudgetAmount) - Previous Payment ($payment_previous_amount) = Remaining Amount ($remaining)",
                    ];
                }
            } else {
                return [
                    'success' => false,
                    'message' => "Requested amount of ($request->budget) exceeds contract budget ($calculateBudgetAmount).",
                ];
            }
        } else { // no contract found
            return [
                'success' => false,
                'message' => 'No contract found against these requirements.'
            ];
        }
    }

    public function getFacilities(Request $request)
    {
        $facilities = Facility::whereHas('contractHasProvincDivisions', function ($query) use ($request) {
            return $query->where([
                'contract_id' => $request->contract ?? null,
                'measure_id' => $request->measure ?? null,
                'project_id' => $request->project ?? null,
                'sub_category_id' => $request->subcategory ?? null,
                'city_id' => $request->city,
            ]);
        })
            ->pluck('facility_name', 'id');
        return $facilities;
    }
}
