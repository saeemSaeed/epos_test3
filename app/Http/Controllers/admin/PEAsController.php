<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\PEASContactInfo;
use App\Model\PEAs;
use Illuminate\Http\Request;

class PEAsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:project-execution-agency-list|project-execution-agency-create|project-execution-agency-edit|project-execution-agency-delete|project-execution-agency-restore', ['only' => ['index','store']]);
        $this->middleware('permission:project-execution-agency-create', ['only' => ['create','store']]);
        $this->middleware('permission:project-execution-agency-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:project-execution-agency-delete', ['only' => ['destroy']]);
        $this->middleware('permission:project-execution-agency-restore', ['only' => ['PEAsRestore']]);
    }
    public function index()
    {
        $city = new PEAs();

        return view('admin.PEAs.index', [
            'peas' => $city->getPEAs()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $provinceRegion = new PEAs();
        $provinceRegion->validatePEAs($request);
        $peaId = $provinceRegion->storePEAs($request->input());

        if (!is_null($request['contact_name'])) {
            foreach ($request['contact_name'] as $index => $obj) {
                PEASContactInfo::create([
                    'pea_id' => $peaId->id,
                    'contact_name' => isset($request->contact_name[$index]) ? $request->contact_name[$index]:'' ,
                    'contact_designation' => isset($request->contact_designation[$index]) ? $request->contact_designation[$index]:'' ,
                    'contact_role' => isset($request->contact_role[$index]) ? $request->contact_role[$index]:'' ,
                    'contact_email' => isset($request->contact_email[$index]) ? $request->contact_email[$index]:'' ,
                    'contact_mobile_no' => isset($request->contact_mobile_no[$index]) ? $request->contact_mobile_no[$index]:'' ,
                    'contact_landline' => isset($request->contact_landline[$index]) ? $request->contact_landline[$index]:'' ,
                ]);
            }
        }
        return redirect()->route('PEAs.index')->with('success', 'PEA Create Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $provinceRegion = new PEAs();
        return $provinceRegion->showPEAs($request->id);
    }

     public function editContactInfoDAta(Request $request)
    {
        $data = PEASContactInfo::where('pea_id',$request->id)->get();
        return  $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $provinceRegion = new PEAs();
        $validator = $this->validate($request,[
            'pea_name' => 'required|max:255',
            'province_id' => 'required',
            'sort_order' => 'required|unique:p_e_as,sort_order,'.$request->pea_id,
        ]);
        if ($provinceRegion->updatePEAs($request->pea_id, $request->except('_token'))) {
//
            $contactinformation = PEASContactInfo::where('pea_id',$request->pea_id)->get();
            foreach($contactinformation as $contact) {
                $contact->delete();
            }
             if (!is_null($request['contact_name'])) {
                foreach ($request['contact_name'] as $index => $obj) {
                    PEASContactInfo::create([
                        'pea_id' => $request->pea_id,
                        'contact_name' => isset($request->contact_name[$index]) ? $request->contact_name[$index]:'' ,
                        'contact_designation' => isset($request->contact_designation[$index]) ? $request->contact_designation[$index]:'' ,
                        'contact_role' => isset($request->contact_role[$index]) ? $request->contact_role[$index]:'' ,
                        'contact_email' => isset($request->contact_email[$index]) ? $request->contact_email[$index]:'' ,
                        'contact_mobile_no' => isset($request->contact_mobile_no[$index]) ? $request->contact_mobile_no[$index]:'' ,
                        'contact_landline' => isset($request->contact_landline[$index]) ? $request->contact_landline[$index]:'' ,
                    ]);
                }
            }


//
            return response('Update Successfully', 200);
        }
        return response('Something went wrong.', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        dd('$id');
        $provinceRegion = new PEAs();
         if ($provinceRegion->deletePEAs($id)) {
            $contactinformation = PEASContactInfo::where('pea_id',$id)->get();
            foreach($contactinformation as $contact) {
                $contact->delete();
            }
          return redirect()->route('PEAs.index')->with('success', 'PEA Delete Successfully');
        }
    }

    public function PEAsDeleted() {

      $deletedPEAs = PEAs::onlyTrashed()->get();
      return view('admin.PEAs.index_deleted',compact('deletedPEAs'));
    }

    public function PEAsRestore($id) {

       $PEAsRestore = PEAs::onlyTrashed()->find($id);
       $PEAsRestore->restore();
       return redirect()->route('PEAs.index');
    }
}
