<?php

namespace App\Http\Controllers\admin;

use App\Model\city;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class cityController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:city-list|city-create|city-edit|city-delete|city-restore', ['only' => ['index','store']]);
        $this->middleware('permission:city-create', ['only' => ['create','store']]);
        $this->middleware('permission:city-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:city-delete', ['only' => ['destroy']]);
        $this->middleware('permission:city-restore', ['only' => ['Restore']]);
    }
    public function index()
    {
        $city = new city();

        return view('admin.city.index', [
            'cities' => $city->getAllCity()
        ]);
    }


    public function getAllCity()
    {
        $city = new city();

        return  [
            'cities' => $city->getAllCity()
        ];
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $provinceRegion = new city();
        $provinceRegion->validateCity($request);

        $provinceRegion->storeCity($request->input());
        return redirect()->route('city.index')->with('success', 'city Create Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $provinceRegion = new city();
        return $provinceRegion->showCity($request->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $provinceRegion = new city();
        return $provinceRegion->showCity($request->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $provinceRegion = new city();

        $request->validate([
            'city_name' => 'required|max:255|unique:cities,city_name,'.$request->city_id,
            'sort_order' => 'required|max:255|unique:cities,sort_order,'.$request->city_id,
            'province_id' => 'required',
        ], [
            'city_name.unique' => 'City name already exists. Please change city name.',
            'sort_order.unique' => 'Sort order already exists. Please change sort order.'
        ]);
        if ($provinceRegion->updateCity($request->city_id, $request->except('_token'))) {
            return response('Update Successfully', 200);
        }
        return response('Something went wrong.', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $provinceRegion = new city();

        if ($provinceRegion->deleteCity($id)) {
            return redirect()->route('city.index')->with('success', 'city Delete Successfully');

        }
        return redirect()->route('city.index')->with('error', 'City is in use and cannot be deleted.');
    }

    public function Deleted() {
      $deletedcity = city::onlyTrashed()->get();
      return view('admin.city.index_deleted',compact('deletedcity'));
    }

    public function Restore($id) {

       $city = city::onlyTrashed()->find($id);
       $city->restore();
       return redirect()->route('city.index');
       }

}
