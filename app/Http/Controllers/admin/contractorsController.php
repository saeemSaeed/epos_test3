<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\ContractorContactInformation;
use App\Model\ContractorFinancialDetails;
use App\Model\contractors;
use Illuminate\Http\Request;

class contractorsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:contractors-list|contractors-create|contractors-edit|contractors-delete|contractors-restore', ['only' => ['index','store']]);
        $this->middleware('permission:contractors-create', ['only' => ['create','store']]);
        $this->middleware('permission:contractors-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:contractors-delete', ['only' => ['destroy']]);
        $this->middleware('permission:contractors-restore', ['only' => ['contractorsRestore']]);

    }
    public function index()
    {
        $contractors = new contractors();

        return view('admin.contractors.index', [
            'contractors' => $contractors->getAllContractors()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.contractors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $contractors = new contractors();
        $contractors->validateContractors($request);
        $contractorId = $contractors->storeContractors($request->input());

        if (!is_null($request['contact_name'])) {
            foreach ($request['contact_name'] as $index => $obj) {
                ContractorContactInformation::create([
                    'contractor_id' => $contractorId->id,
                    'contact_name' => isset($request->contact_name[$index]) ? $request->contact_name[$index]:'' ,
                    'contact_designation' => isset($request->contact_designation[$index]) ? $request->contact_designation[$index]:'' ,
                    'contact_role' => isset($request->contact_role[$index]) ? $request->contact_role[$index]:'' ,
                    'contact_email' => isset($request->contact_email[$index]) ? $request->contact_email[$index]:'' ,
                    'contact_mobile_no' => isset($request->contact_mobile_no[$index]) ? $request->contact_mobile_no[$index]:'' ,
                    'contact_landline' => isset($request->contact_landline[$index]) ? $request->contact_landline[$index]:'' ,
                ]);
            }
        }

        if (!is_null($request['bank_name'])) {
            foreach ($request['bank_name'] as $index => $obj) {
                ContractorFinancialDetails::create([
                    'contractor_id' => $contractorId->id,
                    'bank_name' => isset($request->bank_name[$index]) ? $request->bank_name[$index]:'' ,
                    'account_no' => isset($request->account_no[$index]) ? $request->account_no[$index]:'' ,
                    'iban' => isset($request->iban[$index]) ? $request->iban[$index]:'' ,
                    'branch_name' => isset($request->branch_name[$index]) ? $request->branch_name[$index]:'' ,
                    'branch_code' => isset($request->branch_code[$index]) ? $request->branch_code[$index]:'' ,
                    'city_id' => isset($request->city_id[$index]) ? $request->city_id[$index]:'' ,
                    'swift_code' => isset($request->swift_code[$index]) ? $request->swift_code[$index]:'' ,
                    'branch_address' => isset($request->branch_address[$index]) ? $request->branch_address[$index]:'' ,

                ]);

            }
        }
        return redirect()->route('contractors.index')->with('success', 'Contractor Create Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $contractors = new contractors();

        return view('admin.contractors.edit', [
            'contractor' => $contractors->showContractors($request->id)]);
    }
    public function profile(Request $request)
    {
// return $request->id;
        $contractors = new contractors();
        return view('admin.contractors.profile', [
            'contractor' => $contractors->showContractors($request->id)]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    { 
        $contractors = new contractors();
        $request->validate([
            'contractor_name' => 'required|max:255',
            'contractor_city' => 'required',
            'contractor_address' => 'required',
            'contractor_landline' => 'required',
            'sort_order' => 'required|unique:contractors,sort_order,'.$request->contractor_id,
        ]);

        if ($contractors->updateContractors($request->contractor_id, $request->except('_token'))) {

            $contactinformation = ContractorContactInformation::where('contractor_id',$request->contractor_id)->get();
            foreach($contactinformation as $contact) {
                $contact->delete();
            }

            if (!is_null($request['contact_name'])) {
                foreach ($request['contact_name'] as $index => $obj) {
                    ContractorContactInformation::create([
                        'contractor_id' => $request->contractor_id,
                        'contact_name' => isset($request->contact_name[$index]) ? $request->contact_name[$index]:'' ,
                        'contact_designation' => isset($request->contact_designation[$index]) ? $request->contact_designation[$index]:'' ,
                        'contact_role' => isset($request->contact_role[$index]) ? $request->contact_role[$index]:'' ,
                        'contact_email' => isset($request->contact_email[$index]) ? $request->contact_email[$index]:'' ,
                        'contact_mobile_no' => isset($request->contact_mobile_no[$index]) ? $request->contact_mobile_no[$index]:'' ,
                        'contact_landline' => isset($request->contact_landline[$index]) ? $request->contact_landline[$index]:'' ,
                    ]);
                }
            }

            if (!is_null($request['bank_name'])) {
                foreach ($request['bank_name'] as $index => $obj) {
                    if ($request->status[$index] == "false") {
                        ContractorFinancialDetails::updateOrCreate(['id' => $request->id[$index]], [
                            'contractor_id'  => $request->contractor_id,
                            'bank_name'      => isset($request->bank_name[$index]) ? $request->bank_name[$index] : null,
                            'account_no'     => isset($request->account_no[$index]) ? $request->account_no[$index] : null,
                            'iban'           => isset($request->iban[$index]) ? $request->iban[$index] : null,
                            'branch_name'    => isset($request->branch_name[$index]) ? $request->branch_name[$index] : null,
                            'branch_code'    => isset($request->branch_code[$index]) ? $request->branch_code[$index] : null,
                            'branch_address' => isset($request->branch_address[$index]) ? $request->branch_address[$index] : null,
                            'city_id'        => isset($request->city_id[$index]) ? $request->city_id[$index] : null,
                            'swift_code'     => isset($request->swift_code[$index]) ? $request->swift_code[$index] : null
                        ]);
                    } else { // row has been deleted
                        if (!is_null($request->id[$index])) {
                            ContractorFinancialDetails::find($request->id[$index])->delete(); // remove deleted row from db if already stored
                        }
                    }
                }
            }

            return redirect()->route('contractors.index')->with('success', 'Contractor Update Successfully');
        }

        return redirect()->route('contractors.index')->with('error', 'Contractor Not Update Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $provinceRegion = new contractors();
        if ($provinceRegion->deleteContractors($id)) {
            $contactinformation = ContractorContactInformation::where('contractor_id',$id)->get();
            foreach($contactinformation as $contact) {
                $contact->delete();
            }
            $financialDetails = ContractorFinancialDetails::where('contractor_id',$id)->get();
            foreach($financialDetails as $financial) {
                $financial->delete();
            }
            return redirect()->route('contractors.index')->with('success', 'Contractor Delete Successfully');
        }
        return redirect()->route('contractors.index')->with('error', 'Contractor is in use and cannot be deleted.');
    }

    public function contractorsDeleted() {
      $deletedContractors = contractors::onlyTrashed()->get();
      return view('admin.contractors.index_deleted',compact('deletedContractors'));
    }

    public function contractorsRestore($id) {
       $contractors = contractors::onlyTrashed()->find($id);
       $contractors->restore();
       return redirect()->route('contractors.index');
    }
}
