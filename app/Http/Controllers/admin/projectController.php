<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\projectHasProvinceHistory;
use App\Model\projectHistory;
use App\Model\projectRevised;
use App\Model\Projects;
use App\Model\ProjectsFiles;
use App\Model\ProjectsHasProvince;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use App\Model\contractHasProvincDivision;


class projectController extends Controller
{


    function __construct()
    {
        $this->middleware('permission:projects-list|projects-create|projects-edit|projects-delete|projects-restore', ['only' => ['index', 'store']]);
        $this->middleware('permission:projects-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:projects-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:projects-delete', ['only' => ['destroy']]);
        $this->middleware('permission:projects-restore', ['only' => ['projectRestore']]);
    }

    public function index()
    {
        $projects = new Projects();
        //dd($projects);
        return view('admin.project.index', ['projects' => $projects->getAllProject()]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.project.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (
            (double)str_replace(",", "", $request->province_total_amount)

            >
            (double)str_replace(",", "", $request->budget)


        ) {
            return redirect()->back()->with('warning', 'Total Budget amount exceed the Project Budget');
        }
        $project = new Projects();
        $storeProjectsHasProvince = new ProjectsHasProvince();

        $project->validateProject($request);
        isset($request->planned_start_date) ? $request->request->add(['planned_start_date' => Carbon::createFromFormat('d-M-Y', $request->planned_start_date)->format('Y/m/d')]) : null;
        isset($request->planned_end_date) ? $request->request->add(['planned_end_date' => Carbon::createFromFormat('d-M-Y', $request->planned_end_date)->format('Y/m/d')]) : null;
        isset($request->project_start_date) ? $request->request->add(['project_start_date' => Carbon::createFromFormat('d-M-Y', $request->project_start_date)->format('Y/m/d')]) : null;
        isset($request->project_end_date) ? $request->request->add(['project_end_date' => Carbon::createFromFormat('d-M-Y', $request->project_end_date)->format('Y/m/d')]) : null;
        isset($request->revised_expected_completion_date) ? $request->request->add(['revised_expected_completion_date' => Carbon::createFromFormat('d-M-Y', $request->revised_expected_completion_date)->format('Y/m/d')]) : null;

        // dd($request->input());
        // $request->request->add(['planned_end_date' => Carbon::createFromFormat('d/M/Y', $request->planned_end_date)->format('Y/m/d')]):'';
        // $request->request->add(['project_start_date' => Carbon::createFromFormat('d/M/Y', $request->project_start_date)->format('Y/m/d')]):'';
        // $request->request->add(['project_end_date' => Carbon::createFromFormat('d/M/Y', $request->project_end_date)->format('Y/m/d')]):'';
        $request->request->add(['status', 0]);
        $request->request->add(['budget' => (double)str_replace(",", "", $request->budget)]);
        $request->request->add(['bugdet_revised' => (double)str_replace(",", "", $request->bugdet_revised)]);
//dd($request->input());
        $object = $project->storeProject($request->input());
        if ($request->hasfile('file')) {

            $files = $request->file('file');
            foreach ($files as $index => $file) {

                $extension = $file->getClientOriginalExtension(); // getting image extension
                $filename = $file->getClientOriginalName();

                $file->move('files/project/', $filename);
                $projectFile = ProjectsFiles::create([
                    'project_id' => $object->id,
                    'file' => $filename,
                    'category' => $request->category[$index],
                    'description' => $request->description[$index],
                ]);
            }
        }


        if (!is_null($request['measure'])) {
            foreach ($request['measure'] as $index => $obj) {
                $storeProjectsHasProvince->storeProjectsHasProvince([
                    'project_id' => $object->id,
                    'measure_id' => isset($request->measure[$index]) ? $request->measure[$index] : null,
                    'sub_category_id' => isset($request->sub_cate[$index]) ? $request->sub_cate[$index] :null,
                    'budget' => isset($request->amount[$index]) ? (double)str_replace(",", "", $request->amount[$index]) : 0,
                    'revise_amount' => isset($request->revise_amount[$index]) ? (double)str_replace(",", "", $request->revise_amount[$index]) : 0,
                    'description' => isset($request->descriptionHasProvince[$index]) ? $request->descriptionHasProvince[$index] : '',

                ]);
            }
        }

        return redirect()->route('project.index')->with('success', 'Project Create Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $flagScroll = null)
    {

        $forStoring = [];
        $project = new Projects();
        if (!is_null($project->showProject($id))) {
            foreach ($project->showProject($id)->belongsToProvince as $index => $pro) {
                $pro->measure_code = $pro->hasOneMeasures->code;
                $forStoring[] = $pro;

            }
        } else {
            return redirect()->route('project.index')->with('error', 'Cannot find Project');
        }

        return view('admin.project.show', [
            'collection' => collect($forStoring),
            'project' => $project->showProject($id),
            'flagScroll' => ($flagScroll) ? true : false
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

        $project = new Projects();
        $projectHistory = new projectHistory();
        $forStoring = [];
        $countProjectProvince = ProjectsHasProvince::where('project_id', $request->id)->count();
        if (!is_null($project->showProject($request->id))) {
            foreach ($project->showProject($request->id)->belongsToProvince as $index => $pro) {
                $pro->measure_code = $pro->hasOneMeasures->code;
                $forStoring[] = $pro;
            }

            $revisedDate = ProjectsHasProvince::where('project_id', $request->id)->latest()->pluck('revise_date')->first();

        } else {
            return redirect()->route('project.index')->with('error', 'Cannot find Project');
        }

        // dd($project->showProject($request->id));

        return view('admin.project.edit',
            [
                'collection' => collect($forStoring),
                'project' => $project->showProject($request->id),
                'checkHistory' => $projectHistory->showProject($request->id) ? count($projectHistory->showProject($request->id)) : 0,
                'provinceTransactionCount' => $countProjectProvince,
                'revisedDate' => $revisedDate,
                'historyCount' => count($projectHistory->showProject($request->id)),
            ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $approval_file = null;
        $approval_file_title = null;
        $hash_file_name = null;
        if (
            (double)str_replace(",", "", $request->province_total_amount)
            >
            (double)str_replace(",", "", $request->budget)


        ) {
            return redirect()->back()->with('warning', 'Total Budget amount exceed the Project Budget');
        }


//return $request->status;
        if (($request->is_approved) == 1) {
            if (($request->status) == 2) {
                return redirect()->back()->with('error', 'Project Status is Rejected');
            }

            if (($request->status) == 0) {
//                return (double) str_replace(",", "", $request->bugdet_revised);


                if ($request->hasfile('ApprovelbudgetFile')) {
                    $file = $request->file('ApprovelbudgetFile');
                    $extension = $file->getClientOriginalExtension(); // getting image extension
                    $approval_file = $file->getClientOriginalName();
                    $approval_file_title = $approval_file;
                    $hash_file_name = time(). '.' . $extension;
                    $file->move('files/project/approval/', $hash_file_name);
                    $request->request->
                    add(['approval_file' =>
                        ($hash_file_name !== null)
                            ? $hash_file_name
                            : null]);
                    $request->request->
                    add(['approval_file_title' =>
                        ($approval_file_title !== null)
                            ? $approval_file_title
                            : null]);
                }

                $projectHasProvince = new ProjectsHasProvince();
                $project = new Projects();
                isset($request->planned_start_date) ? $request->request->add(['planned_start_date' => Carbon::createFromFormat('d-M-Y', $request->planned_start_date)->format('Y/m/d')]) : null;
                isset($request->planned_end_date) ? $request->request->add(['planned_end_date' => Carbon::createFromFormat('d-M-Y', $request->planned_end_date)->format('Y/m/d')]) : null;
                isset($request->project_start_date) ? $request->request->add(['project_start_date' => Carbon::createFromFormat('d-M-Y', $request->project_start_date)->format('Y/m/d')]) : null;
                isset($request->project_end_date) ? $request->request->add(['project_end_date' => Carbon::createFromFormat('d-M-Y', $request->project_end_date)->format('Y/m/d')]) : null;
                isset($request->revised_expected_completion_date) ? $request->request->add(['revised_expected_completion_date' => Carbon::createFromFormat('d-M-Y', $request->revised_expected_completion_date)->format('Y/m/d')]) : null;

                $request->request->add(['budget' => str_replace(",", "", $request->budget)]);
                $request->request->add(['bugdet_revised' => (double) str_replace(",", "", $request->bugdet_revised)]);

  $project->validateUpdateProject($request);
                if ($project->updateProject($request->project_id, $request->except('_token'))) {
                    $checkDeleteOrNot = $projectHasProvince->deleteProjectsHasProvince($request->project_id);

                    if (!is_null($request['measure'])) {
                        foreach ($request['measure'] as $index => $obj) {

                            $projectHasProvince->storeProjectsHasProvince([
                                'project_id' => $request->project_id,
                                'measure_id' => isset($request->measure[$index]) ? $request->measure[$index] : null,
                                'sub_category_id' => isset($request->sub_cate[$index]) ? $request->sub_cate[$index] : null,
                                'budget' => isset($request->provinceBudgetAmount[$index]) ? (double)str_replace(",", "", $request->provinceBudgetAmount[$index]) : 0,
                                'revise_amount' => isset($request->provinceReviseAmount[$index]) ? (double)str_replace(",", "", $request->provinceReviseAmount[$index]) : 0,
                                'description' => isset($request->descriptionHasProvince[$index]) ? $request->descriptionHasProvince[$index] : '',
                          'revise_date' => $request->revise_date != null ?

                                Carbon::createFromFormat('d-M-Y', $request->revise_date)->format('Y-m-d') : null
                            ]);
                        }


                    }
                    return redirect()->route('project.index')->with('success', 'Project Update Successfully');

                }
            }


            $projectHasProvince = new ProjectsHasProvince();
            $projectHasProvinceHistory = new projectHasProvinceHistory();
            $projectHistory = new projectHistory();
            $project = new Projects();
            isset($request->planned_start_date) ? $request->request->add(['planned_start_date' => Carbon::createFromFormat('d-M-Y', $request->planned_start_date)->format('Y/m/d')]) : null;
            isset($request->planned_end_date) ? $request->request->add(['planned_end_date' => Carbon::createFromFormat('d-M-Y', $request->planned_end_date)->format('Y/m/d')]) : null;
            isset($request->project_start_date) ? $request->request->add(['project_start_date' => Carbon::createFromFormat('d-M-Y', $request->project_start_date)->format('Y/m/d')]) : null;
            isset($request->project_end_date) ? $request->request->add(['project_end_date' => Carbon::createFromFormat('d-M-Y', $request->project_end_date)->format('Y/m/d')]) : null;
            isset($request->revised_expected_completion_date) ? $request->request->add(['revised_expected_completion_date' => Carbon::createFromFormat('d-M-Y', $request->revised_expected_completion_date)->format('Y/m/d')]) : null;

            $request->request->add(['budget' => str_replace(",", "", $request->budget)]);
            $request->request->add(['status' => 0]);
            $request->request->add(['is_approved' => 1]);
            $request->request->add(['bugdet_revised' => (double)str_replace(",", "", $request->bugdet_revised)]);
            $getdataFromPerntantTable = $project->find($request->project_id);
            if ($request->hasfile('ApprovelbudgetFile')) {
                $file = $request->file('ApprovelbudgetFile');
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $approval_file = $file->getClientOriginalName();
                $approval_file_title = $approval_file;
                $hash_file_name = time() . '.' . $extension;
                $file->move('files/project/approval/', $hash_file_name);

            }
            $request->request->
            add(['approval_file' =>
                ($hash_file_name !== null)
                    ? $hash_file_name
                    : null]);
            $request->request->
            add(['approval_file_title' =>
                ($approval_file_title !== null)
                    ? $approval_file_title
                    : null]);
  $project->validateUpdateProject($request);
            if ($project->updateProject($request->project_id, $request->except('_token'))) {
                $checkProjectHistorySaveOrNot = $projectHistory->storeProject([
                    'project_id' => $getdataFromPerntantTable->id,
                    'title' => $getdataFromPerntantTable->title,
                    'donor_number' => $getdataFromPerntantTable->donor_number,
                    'donor_name' => $getdataFromPerntantTable->donor_name,
                    'budget' => $getdataFromPerntantTable->budget,
                    'bugdet_revised' => $getdataFromPerntantTable->bugdet_revised,
                    'currency_id' => $getdataFromPerntantTable->currency_id,
                    'province_id' => $getdataFromPerntantTable->province_id,
                    'pea_id' => $getdataFromPerntantTable->pea_id,
                    'planned_start_date' => $getdataFromPerntantTable->planned_start_date,
                    'planned_end_date' => $getdataFromPerntantTable->planned_end_date,
                    'project_start_date' => $getdataFromPerntantTable->project_start_date,
                    'project_end_date' => $getdataFromPerntantTable->project_end_date,
                    'project_description' => $getdataFromPerntantTable->project_description,
                    'status' => 1,
                    'is_approved' => 1,

                    'approval_file' => $getdataFromPerntantTable->approval_file,
                    'approval_file_title' => $getdataFromPerntantTable->approval_file_title,

                ]);

                if ($checkProjectHistorySaveOrNot) {
//                    dd($projectHasProvince->showProjectProvinceBy($getdataFromPerntantTable->id));


                    foreach ($projectHasProvince->showProjectProvinceBy($getdataFromPerntantTable->id) as $index => $his) {
//dd($his->project_id);
                        $projectHasProvinceHistoryCheckorNOt = $projectHasProvinceHistory->storeProjectsHasProvince([
                            'row_id' => $checkProjectHistorySaveOrNot->id,
                            'project_id' => $his->project_id,
                            'measure_id' => isset($his->measure_id) ? $his->measure_id : '',
                            'sub_category_id' => isset($his->sub_category_id) ? $his->sub_category_id : null,
                            'budget' => isset($his->budget) ? (double)str_replace(",", "", $his->budget) : 0,
                            'revise_amount' => isset($his->revise_amount) ? (double)str_replace(",", "", $his->revise_amount) : 0,
                            'description' => isset($his->description) ? $his->description : '',
                            'revise_date' => $his->revise_date

                        ]);
//                        dd($projectHasProvinceHistoryCheckorNOt);
                    }

                    $checkDeleteOrNot = $projectHasProvince->deleteProjectsHasProvince($request->project_id);
                    if (!is_null($request['measure'])) {
                        foreach ($request['measure'] as $index => $obj) {

                            $projectHasProvince->storeProjectsHasProvince([
                                'project_id' => $request->project_id,
                                'measure_id' => isset($request->measure[$index]) ? $request->measure[$index] : null,
                                'sub_category_id' => isset($request->sub_cate[$index]) ? $request->sub_cate[$index] : null,
                                'budget' => isset($request->provinceBudgetAmount[$index]) ? (double)str_replace(",", "", $request->provinceBudgetAmount[$index]) : 0,
                                'revise_amount' => isset($request->provinceReviseAmount[$index]) ? (double)str_replace(",", "", $request->provinceReviseAmount[$index]) : 0,
                                'description' => isset($request->descriptionHasProvince[$index]) ? $request->descriptionHasProvince[$index] : '',
                                'revise_date' => $request->revise_date != null ?

                                    Carbon::createFromFormat('d-M-Y', $request->revise_date)->format('Y-m-d') : null
                            ]);
                        }


                    }

                }

            }

            return redirect()->route('project.index')->with('success', 'Project Update Successfully');


        } else {
            if ($request->status == 2) {
                return redirect()->back()->with('error', 'Project Status is Rejected');

            }
            $request->request->
            add(['approval_file' =>
                ($hash_file_name !== null)
                    ? $hash_file_name
                    : null]);
            $request->request->
            add(['approval_file_title' =>
                ($approval_file_title !== null)
                    ? $approval_file_title
                    : null]);
            $projectHasProvince = new ProjectsHasProvince();
            $project = new Projects();
            isset($request->planned_start_date) ? $request->request->add(['planned_start_date' => Carbon::createFromFormat('d-M-Y', $request->planned_start_date)->format('Y/m/d')]) : null;
            isset($request->planned_end_date) ? $request->request->add(['planned_end_date' => Carbon::createFromFormat('d-M-Y', $request->planned_end_date)->format('Y/m/d')]) : null;
            isset($request->project_start_date) ? $request->request->add(['project_start_date' => Carbon::createFromFormat('d-M-Y', $request->project_start_date)->format('Y/m/d')]) : null;
            isset($request->project_end_date) ? $request->request->add(['project_end_date' => Carbon::createFromFormat('d-M-Y', $request->project_end_date)->format('Y/m/d')]) : null;
            isset($request->revised_expected_completion_date) ? $request->request->add(['revised_expected_completion_date' => Carbon::createFromFormat('d-M-Y', $request->revised_expected_completion_date)->format('Y/m/d')]) : null;

            $request->request->add(['budget' => str_replace(",", "", $request->budget)]);
            $request->request->add(['revised_budget' => (double)str_replace(",", "", $request->revised_budget)]);
            $request->request->add(['is_approved' => 0]);
            $request->request->add(['status' => 0]);
  $project->validateUpdateProject($request);
            if ($project->updateProject($request->project_id, $request->except('_token'))) {
                $checkDeleteOrNot = $projectHasProvince->deleteProjectsHasProvince($request->project_id);

                if (!is_null($request['measure'])) {
                    foreach ($request['measure'] as $index => $obj) {

                        $projectHasProvince->storeProjectsHasProvince([
                            'project_id' => $request->project_id,
                            'measure_id' => isset($request->measure[$index]) ? $request->measure[$index] : '',
                            'sub_category_id' => isset($request->sub_cate[$index]) ? $request->sub_cate[$index] :null,
                            'budget' => isset($request->provinceBudgetAmount[$index]) ? (double)str_replace(",", "", $request->provinceBudgetAmount[$index]) : 0,
                            'revise_amount' => isset($request->provinceReviseAmount[$index]) ? (double)str_replace(",", "", $request->provinceReviseAmount[$index]) : 0,
                            'description' => isset($request->descriptionHasProvince[$index]) ? $request->descriptionHasProvince[$index] : ''
                        ]);
                    }


                }
                return redirect()->route('project.index')->with('success', 'Project Update Successfully');

            }
        }
        return redirect()->route('project.index')->with('error', 'Project Not Update Successfully');
    }

    public function status($id, $status)
    {
        //    status 0 for pending
//             1 for approve
//            2 reject
        $checkValue = null;
        $project = new Projects();
        if ($status == 'pending') {
            $checkValue = 0;
        }
        if ($status == 'approve') {
            $checkValue = 1;
        }
        if ($status == 'reject') {
            $checkValue = 2;
        }

        $obj = $project->status($id, $checkValue);

        return redirect()->route('project.index')->with('success', 'Project Status Successfully');
    }

    public function destroy($id)
    {

        $project = new Projects();

        if ($project->deleteProject($id)) {
            return redirect()->route('project.index')->with('success', 'Project Delete Successfully');

        }
        return redirect()->route('project.index')->with('error', 'You Cannot Delete Project');
    }

    public function selectedProvince(Request $request)
    {
        $getAllProvinceOfProject = new ProjectsHasProvince();
        foreach ($getAllProvinceOfProject->getAllProvinceOfProject($request->project_id) as $item) {
            $province[] = ($item->hasOneProvinceRegion);
        }
        return (['province' => $province]);
    }

    public function projectDeleted()
    {
        $deletedProjects = Projects::onlyTrashed()->get();
        return view('admin.project.index_deleted', compact('deletedProjects'));
    }

    public function projectRestore($id)
    {
        $projects = new Projects();
        $project = Projects::onlyTrashed()->find($id);
        $project->restore();
        return view('admin.project.index', ['projects' => $projects->getAllProject()]);
    }

    public function projectFile(Request $request)
    {


        if ($request->ajax()) {
            if ($request->hasfile('file')) {

                $files = $request->file('file');


                $extension = $files->getClientOriginalExtension(); // getting image extension
                $filename = $files->getClientOriginalName();
                $files->move('files/project/', $filename);
                $projectFile = ProjectsFiles::create([
                    'project_id' => $request->project_id,
                    'file' => $filename,
                    'category' => $request->category,
                    'description' => $request->description,
                ]);

                if ($projectFile) {
                    return response()->json([
                        'flag' => true,
                        'success' => $projectFile,
                        'path' => URL::to('/files/project/' . $projectFile->file)
                    ]);


                }


            }

            return response()->json([
                'flag' => false
            ]);


        }
    }

    public function destroyFile(Request $request)
    {
        $ProjectsFiles = ProjectsFiles::find($request->id);
        //$ProjectsFiles->file
        if ($ProjectsFiles) {
            $checkDeleleteOrNot = \File::delete('files/project/' . $ProjectsFiles->file);
            $ProjectsFiles->delete();
        }

        return response()->json([
            'id' => $request->id,
            'ProjectsFiles' => $ProjectsFiles->file
        ]);
    }

    public function revisedBudget(Request $request, $project_id = null, $line_reference_id = null)
    {
        $ProjectsHasProvince = new ProjectsHasProvince();
        $getReferenceRow = $ProjectsHasProvince->getReferenceRow(
            isset($request->line_reference_id) ? $request->line_reference_id : $line_reference_id,
            isset($request->project_id) ? $request->project_id : $project_id);

        $projectRevised = new projectRevised();
        $showProjectRevised = $projectRevised->showProjectRevised(
            isset($request->project_id) ? $request->project_id : $project_id
            , isset($request->line_reference_id) ? $request->line_reference_id : $line_reference_id);
        $checklastRowProvedOrNot = $projectRevised->getLatestRow(
            isset($request->project_id) ? $request->project_id : $project_id,
            isset($request->line_reference_id) ? $request->line_reference_id : $line_reference_id);

//        dd(isset($checklastRowProvedOrNot) ? $checklastRowProvedOrNot->status==1 ? true : False : False);

        return view('admin.project.revised_budget',
            [
                'project_id' => isset($request->project_id) ? $request->project_id : $project_id,
                'line_reference_id' => isset($request->line_reference_id) ? $request->line_reference_id : $line_reference_id,
                'getReferenceRow' => isset($getReferenceRow) ? $getReferenceRow : null,
                'showProjectRevised' => isset($showProjectRevised) ? $showProjectRevised : null,
                'checklastRowProvedOrNot' => !is_null($checklastRowProvedOrNot) ? $checklastRowProvedOrNot->status == 1 ? true : False : true
            ]);
    }
    //    status 0 for pending
//             1 for approve
//            2 reject
    public function revisedBudgetStore(Request $request)
    {
        $request->request->add(['user_id' => Auth::user()->getId()]);
        $request->request->add(['revised_budget' => str_replace(",", "", $request->revised_budget)]);

        $request->request->add(['status' => 0]);

        $RevisedTotalSum = 0;
        $checkTotalBudet = Projects::find($request->project_id);

        $revised = new projectRevised();
        if (count($revised->getProjectReferenceRow($request->project_id)) > 0) {
            foreach ($revised->getProjectReferenceRow($request->project_id) as $index => $obj) {
                $get = projectRevised::where('line_reference_id', $obj->line_reference_id)->select('revised_budget')
                    ->where('status', 1)->latest()->first();

                $RevisedTotalSum += isset($get->revised_budget) ? $get->revised_budget : 0;

            }
        } else {
            $RevisedTotalSum = (double)$request->revised_budget;
        }
//        dd($get);
//            dd([$RevisedTotalSum >= $checkTotalBudet->budget, $RevisedTotalSum, $checkTotalBudet->budget]);
        if ($RevisedTotalSum >= $checkTotalBudet->budget) {
            return redirect()->route('project.revisedBudget', ['project_id' => $request->project_id, 'line_reference_id' => $request->line_reference_id])
                ->with('error', 'Revised Budget Exceed from total Project Budget');

        }


        $revised->validateProjectRevised($request);
//        return ($request);
        $request->request->add(['date' => Carbon::createFromFormat('d/M/Y H:i:s', $request->date)->format('Y/m/d H:i:s')]);
//return ($request);
        $revised->storeProjectRevised($request->input());


        return redirect()->route('project.revisedBudget', ['project_id' => $request->project_id, 'line_reference_id' => $request->line_reference_id]);
    }

    public
    function revisedBudgetStatus($id, $status)
    {
        //     0 for pending
        //     1 for approve
        //     2 for reject
        $checkValue = null;
        $project = new Projects();
        if ($status == 'pending') {
            $checkValue = 0;
        }
        if ($status == 'approve') {
            $checkValue = 1;
        }
        if ($status == 'reject') {
            $checkValue = 2;

        }
        projectRevised::where('id', $id)->update(['status' => $checkValue]);

        return redirect()->back()->with('success', 'Revised Budget Status Changed Successfully');
    }

    public function historyList($project_id)
    {
        $activeProject = Projects::find($project_id);
        $projectHistory = new projectHistory();

        return view('admin.project.history_list', [
            'project_id' => $project_id,
            'activeProject' => $activeProject,
            'historyProject' => $projectHistory->getHistoryOfProject($project_id)
        ]);
    }

    public function historyShow($id, $project_id)
    {
        $forStoring = [];
        $projectHistory = new projectHistory();
        $projectHasProvinceHistory = new projectHasProvinceHistory();
        foreach ($projectHistory->showProjectByRowId($id)->belongsToProvince as $index => $pro) {
            $pro->measure_code = $pro->hasOneMeasures->code;
            $forStoring[] = $pro;
        }
        $revisedDate = ProjectsHasProvince::where('project_id', $project_id)->latest()->pluck('revise_date')->first();
        return view('admin.project.history_show', [
            'collection' => collect($forStoring),
            'project_id' => $project_id,
            'project' => $projectHistory->showProjectByRowId($id),
            'revisedDate' => $revisedDate,

        ]);
    }
        public function CheckDefiningBudgetContract(Request $request){

        $contractHasProvincDivision = contractHasProvincDivision::where('project_id', $request->project_id)
                                        ->where('measure_id', $request->measureSelect)
                                        ->Where('sub_category_id', $request->sub_cate)
                                        ->sum('budget');



        return response()->json([
            'contracts_project_id' => $request->project_id,
            'measures_contracts' => $request->measureSelect,
            'sub_cate_contracts' => $request->sub_cate,
            'provinceReviseAmount' => $request->provinceReviseAmount,
            'flag' => $request->provinceReviseAmount > $contractHasProvincDivision ? true : false,
            'message' => "Revision value cannot decrease from sum of total contract(s) amount $contractHasProvincDivision"

        ]);
    }
    
      public function deleteProjectChildRow($id, Request $request)
    {
        $projecthasprovince = ProjectsHasProvince::find($id);

        if ($id !== 'undefined') {
            $data = contractHasProvincDivision::where('project_id', $request->project_id)
                                                ->where('measure_id', $request->measure_id)
                                                ->where('sub_category_id', $request->subcategory_id)
                                                ->count();
            if ($data == 0) {
                $projecthasprovince->delete();
                return response()->json(['message' => 'Deleted Successfully', 'flag' => true], 201);
            } else {
                return response()->json(['message' => 'Cannot delete its using in Contract', 'flag' => false], 201);
            }
        } else {
            return response()->json(['message' => 'Delete Successfully', 'flag' => true], 201);
        }


    }


    public function checkProjectTitle(Request $request)
    {
        $title = strtoupper($request->title);

        $project = Projects::whereRaw('UPPER(title) = ? ', [$title])->get()->isNotEmpty();

        if($project) {
            return response()->json(['message' => 'Project title already exists!'], 409);
        }
    }

}
