<?php

namespace App\Http\Controllers;

use App\Model\city;
use App\Model\PEAs;
use App\Model\Projects;
use App\Model\provinceRegion;
use App\Model\subCategories;
use Illuminate\Http\Request;

class constantController extends Controller
{
    public static function GetCityPeas(Request $request)
    {

        $city= new city();

        $provinceRegion = new provinceRegion();
        $cities = $city->showCityAgainstProvince($request->id);
        $provinceRegions = $provinceRegion->showAllPEASOfProvinceRegion($request->id);
        return ([
            'cities' => $cities,
            'provinceRegion' => $provinceRegions
]
        );
    }

    public static function getActiveProjects(Request $request)
    {
        $project = new Projects();

        return ([
            'ActiveProjects' => $project->getActiveProject()
        ]
        );
    }

    public static function PeaFind(Request $request)
    {


        $pea = new PEAs();
        $provinceRegionsPea = $pea->showPEAsWithinProvince($request->id);
        return ([
            'provinceRegion' => $provinceRegionsPea]
        );
    }
    public static function MeasuresSelect(Request $request)
    {

        $showAllSubCategoriesOfmeasure = new subCategories();

        $provinceRegion = new provinceRegion();
        $showAllSubCategoriesOfmeasures = $showAllSubCategoriesOfmeasure->showAllSubCategoriesOfmeasure($request->id);
        $provinceRegions = $provinceRegion->showAllPEASOfProvinceRegion($request->id);
        return ([
            'showAllSubCategoriesOfmeasure' => $showAllSubCategoriesOfmeasures,
            'provinceRegion' => $provinceRegions]
        );
    }

    public static function contractSelectProvince(Request $request)
    {
        $provinceRegion = new provinceRegion();
        $provinceRegions = $provinceRegion->FindAllProvinceRegion($request->ids[1]);
        return ([
            'provinceRegion' => $provinceRegions]
        );
    }


    public static function SubCategories(Request $request)
    {
        $showAllSubCategoriesOfmeasure = new subCategories();
        $showAllSubCategoriesOfmeasures = $showAllSubCategoriesOfmeasure->showSubCategoriesWithOutHasMeasuree($request->id);

        return ([
            'showAllSubCategoriesOfmeasure' => $showAllSubCategoriesOfmeasures
        ]);
    }

    public static function allProvince()
    {
        $provinceRegion= new provinceRegion();
            $showAll =$provinceRegion->getAllProvinceRegion();
        return ([
            'showAllProvince' => $showAll
        ]);
    }
    public static function Province(Request $request)
    {
        $provinceRegion= new provinceRegion();
            $showAll =$provinceRegion->showProvinceRegion($request->id);
        return ([
            'showAllProvince' => $showAll
        ]);
    }
}
