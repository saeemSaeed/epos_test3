<?php
namespace App\Listeners;
use Illuminate\Auth\Events\Login;
use Carbon\Carbon;

class LogSuccessfulLogin {

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(){


    }

    /**
     * Handle the event.
     *
     * @param  Login $event
     * @throws \Exception
     */

    public function handle(Login $event){
        $user = $event->user;
        $user->last_login = Carbon::now();

        $user->save();

    }
}