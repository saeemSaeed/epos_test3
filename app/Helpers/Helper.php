<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;

use App\Model\city;
use App\Model\contractHasProvincDivision;
use App\Model\contracts;
use App\Model\currency;
use App\Model\Measures;
use App\Model\paymentMethod;
use App\Model\paymentsHasSub;
use App\Model\PEAs;
use App\Model\projectHistory;
use App\Model\projectRevised;
use App\Model\Projects;
use App\Model\ProjectsHasProvince;
use App\Model\provinceRegion;
use App\Model\subCategories;


class Helper
{

    public static function projects()
    {
        $projects = new Projects();

        return $projects->getAllProject();
    }


    public static function paymentMethod()
    {
        $paymentMethods = new paymentMethod();

        return $paymentMethods->getAllPaymentMethod();
    }

    public static function peas()
    {
        $peas = new PEAs();

        return $peas->getPEAs();
    }

    public static function showPEAsWithinProvince($id)
    {
        $peas = new PEAs();

        return $peas->showPEAsWithinProvince($id);
    }

    public static function Measures()
    {
        $measures = new Measures();

        return $measures->getAllMeasures();
    }

    public static function subCategory()
    {
        $measures = new subCategories();

        return $measures->getAllSubCategories();
    }

    public static function findSubCategory($id)
    {
        $measures = new subCategories();

        return $measures->showAllSubCategoriesOfmeasure($id);
    }

    public static function cities()
    {
        $city = new city();

        return $city->getAllCity();
    }


    public static function citiesOptions()
    {

        $city = new city();
        $html = '';
        foreach ($city->getAllCity() as $city) {
            $html .= '<option value="' . $city->id . '">' . $city->city_name . '</option>';
        }
        return $html;
    }

    public static function province()
    {
        $provinceRegion = new provinceRegion();

        return $provinceRegion->getAllProvinceRegion();
    }

    public static function currency()
    {
        $currency = new currency();

        return $currency->getAllCurrency();
    }

    public static function provinceSelect()
    {
        $html = '';
        $html .= '<select name="province[]"  required id="province" class="provinceSelect control - form" style="width:120px !important;    background - color: #FFCCCB;" required>';
        $html .= '<option value="">Select</option>';
        foreach (self::province() as $pro) {
            $html .= '<option value="' . $pro->id . '">' . $pro->name . '</option>';
        }


        $html .= '</select>';


        return json_encode($html);
    }

    public
    static function MeasureSelect()
    {
        $html = '';
        $html .= '<select name="measure[]" id="measure" class="measureSelect form-control" style="background-color: #FFCCCB;" required>';
        $html .= '<option value="">Select</option>';
        foreach (self::Measures() as $pro) {
            $html .= '<option value="' . $pro->id . '">' . $pro->name . '</option>';
        }


        $html .= '</select>';


        return json_encode($html);
    }

    public
    static function GetProvinceCity($id)
    {
        $city = new city();
        $cities = $city->showCityAgainstProvince($id);

        return ([
            'cities' => $cities,
        ]
        );
    }

    public
    static function GetCityPeas($id)
    {

        $city = new city();

        $provinceRegion = new provinceRegion();
        $cities = $city->showCityAgainstProvince($id);
        $provinceRegions = $provinceRegion->showAllPEASOfProvinceRegion($id);
        return ([
            'cities' => $cities,
            'provinceRegion' => $provinceRegions
        ]
        );
    }

    public
    static function getLatestRow($project_id, $line_reference_id)
    {
        return projectRevised::
        where('project_id', $project_id)
            ->where('line_reference_id', $line_reference_id)
            ->where('status', 1)
            ->latest()->first();

    }

    public
    static function historyList($project_id)
    {
        $activeProject = Projects::find($project_id);
        $projectHistory = new projectHistory();
        $getHistoryOfProject = $projectHistory->getHistoryOfProject($project_id);

        return ([
            'project_id' => $project_id,

            'historyProjectCount' => count($projectHistory->getHistoryOfProject($project_id)),
        ]);
    }

    public
    static function getProjectMeasuresSelectedSubcategory($project_id, $measures_id)
    {
        $project = new contracts();
        $subcategory = [];
        $getProjectSelectedMeasureSubcategory = $project->getProjectSelectedMeasureSubcategory($project_id, $measures_id);

        if (count($getProjectSelectedMeasureSubcategory) > 0) {
            foreach ($getProjectSelectedMeasureSubcategory as $index => $proItem) {

                $subcategory[$index]['id'] = $proItem->hasOneSubCategories->id;
                $subcategory[$index]['name'] = $proItem->hasOneSubCategories->name;

            }

        }


        return ($subcategory
        );
    }


    public
    static function getProjectMeasures($project_id)
    {

        $ProjectsHasProvince = new ProjectsHasProvince();


        $measure = [];
        $getActiveProjectMeasure = $ProjectsHasProvince->getActiveProjectMeasure($project_id);

        if (count($getActiveProjectMeasure) > 0) {
            foreach ($getActiveProjectMeasure as $index => $proItem) {
                $measure[$index]['id'] = $proItem->hasOneMeasures->id;
                $measure[$index]['name'] = $proItem->hasOneMeasures->name;
            }
        }
        return $measure;

    }





    public
    static function GetContractProjects($contract_id)
    {

        $contract = new contractHasProvincDivision();

       $projects=$contract->where('contract_id',$contract_id)->select('project_id')->groupBy('project_id')->get();

        $getProjects = [];


        if (count($projects) > 0) {
            foreach ($projects as $index => $proItem) {
                $getProjects[$index]['id'] = $proItem->hasProjects->id;
                $getProjects[$index]['title'] = $proItem->hasProjects->title;
            }
        }
        return $getProjects;

    }




    public static function GetContractProjectsMeasure($contract_id,$project_id)
    {

        $contract = new contractHasProvincDivision();

        $projects=$contract
            ->where('contract_id',$contract_id)
            ->where('project_id',$project_id)
            ->select('measure_id')->groupBy('measure_id')->get();

        $getProjects = [];


        if (count($projects) > 0) {
            foreach ($projects as $index => $proItem) {
                $getProjects[$index]['id'] = $proItem->belongsToMeasure->id;
                $getProjects[$index]['name'] = $proItem->belongsToMeasure->name;
            }
        }
        return $getProjects;

    }


    public static function GetContractProjectsMeasureSubCategory($contract_id,$project_id,$measure_id)
    {

        $contract = new contractHasProvincDivision();

        $projects=$contract
            ->where('contract_id',$contract_id)
            ->where('project_id',$project_id)
            ->where('measure_id',$measure_id)
            ->select('sub_category_id')->groupBy('sub_category_id')->get();

        $getProjects = [];


        if (count($projects) > 0) {
            foreach ($projects as $index => $proItem) {
                $getProjects[$index]['id'] = $proItem->hasSubCategories->id??null;
                $getProjects[$index]['name'] = $proItem->hasSubCategories->name??null;
            }
        }
        return $getProjects;

    }



    public static function GetContractProjectsMeasureSubCategoryCity($contract_id,$project_id,$measure_id)
    {

        $contract = new contractHasProvincDivision();

        $projects=$contract
            ->where('contract_id',$contract_id)
            ->where('project_id',$project_id)
            ->where('measure_id',$measure_id)
            ->select('city_id')->groupBy('city_id')->get();

        $getProjects = [];


        if (count($projects) > 0) {
            foreach ($projects as $index => $proItem) {
                $getProjects[$index]['id'] = $proItem->hasCity->id;
                $getProjects[$index]['city_name'] = $proItem->hasCity->city_name;
            }
        }
        return $getProjects;

    }

    public static function addOrdinalNumberSuffix($num) {
        if (!in_array(($num % 100),array(11,12,13))){
            switch ($num % 10) {
                // Handle 1st, 2nd, 3rd
                case 1:  return $num.'st';
                case 2:  return $num.'nd';
                case 3:  return $num.'rd';
            }
        }
        return $num.'th';
    }

}




