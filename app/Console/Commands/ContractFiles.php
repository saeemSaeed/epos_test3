<?php

namespace App\Console\Commands;

use App\Model\contractsHasFile;
use App\Model\ContractsHasRetainFile;
use Illuminate\Console\Command;

class ContractFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'contract:file';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ContractsHasRetainFile::where('contract_id',null)->delete();
        contractsHasFile::where('contract_id',null)->delete();
        $this->info('Delete All unusual files');

    }

}
