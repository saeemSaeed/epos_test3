<?php

namespace App\Model;

use App\Model\Facility;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class city extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['city_name', 'province_id','sort_order','short_code'];

    public function validateCity($request)
    {
        $request->validate([
            'city_name' => 'unique:cities|required|max:255|alpha',
            'sort_order' => 'unique:cities|required',
            'province_id' => 'required',

        ]);
    }

    public function getAllCity()
    {
              return city::orderBy('sort_order')->get();

    }

    public function storeCity($object)
    {
        return city::create($object);
    }

    public function showCity($id)
    {
        return city::with('hasProvince')->find($id);
    }

    public function showCityAgainstProvince($id)
    {
        return city::where('province_id',$id)->get();
    }

    public function updateCity($id, $object)
    {


        return city::where('id', $id)
            ->update(
                [
                    'city_name' => $object['city_name'],
                    'province_id' => $object['province_id'],
                    'sort_order' => $object['sort_order'],
                    'short_code' => $object['short_code'],

                ]
            );

    }

   public function deleteCity($id)
    {
        $provinceRegion = new city;
            // return $provinceRegion->find($id)->delete();
        if(count($provinceRegion->find($id)->contractHasProvincDivision()->get()) > 0 || count($provinceRegion->find($id)->paymentsHasSub()->get()) > 0|| count($provinceRegion->find($id)->contractors()->get()) > 0 ) {
            return false;
        }

        else {
            return $provinceRegion->find($id)->delete();
        }
        return $provinceRegion->find($id)->delete();
    }
    //for deleting function
    public function contractHasProvincDivision()
    {
        return $this->hasMany(contractHasProvincDivision::class , 'city_id', 'id');
    }

    public function paymentsHasSub()
    {
        return $this->hasMany(paymentsHasSub::class , 'city_id', 'id');
    }

    public function contractors()
    {
        return $this->hasMany(contractors::class , 'contractor_city', 'id');
    }

    public function hasProvince()
    {
        return $this->belongsTo('App\Model\provinceRegion', 'province_id', 'id');
    }
    public function facilities()
    {
        return $this->hasMany(Facility::class);
    }
}
