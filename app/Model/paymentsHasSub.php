<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class paymentsHasSub extends Model
{
    use SoftDeletes;
    
    protected $table = 'payment_has_project_measure';

    protected $fillable = [
        'payment_id',
        'contract_id',
        'project_id',
        'measure_id',
        'sub_category_id',
        'province_id',
        'pea_id',
        'city_id',
        'budget',
        'budget_pkr',
        'description',
        'facility_id',
    ];


    public function getAllPaymentsHasSub()
    {
        return paymentsHasSub::get();
    }

    public function storePaymentsHasSub($object)
    {
        return paymentsHasSub::create($object);
    }

    public function showPaymentsHasSub($id)
    {
        return paymentsHasSub::find($id);
    }

    public function updatePaymentsHasSub($id, $object)
    {


        return paymentsHasSub::where('id', $id)
            ->update(
                [
                    'payment_id' => $object['payment_id'],
                    'contract_id' => $object['contract_id'],
                    'measure_id' => $object['measure_id'],
                    'sub_category_id' => $object['sub_cate_id'],
                    'province_id' => $object['province_id'],
                    'pea_id' => $object['sub_cate_peas_id'],
                    'city_id' => $object['city_id'],
                    'budget' => $object['payment_amount'],
                    'budget_pkr' => $object['payment_amount_pkr'],
                    'description' => $object['payment_project_description'],

                ]
            );
    }

    public function deletePaymentsHasSub($id)
    {
        $provinceRegion = new paymentsHasSub;
        return $provinceRegion->where('payment_id', $id)->delete();
    } //

    public function hasCurrency()
    {
        return $this->belongsTo('App\Model\currency', 'currency_id', 'id');
    }
    public function hasProvince()
    {
        return $this->hasOne('App\Model\provinceRegion', 'id', 'province_id');
    }


    public function hasPeas()
    {
        return $this->hasOne('App\Model\PEAs', 'id', 'pea_id');
    }
    public function hasCity()
    {
        return $this->hasOne('App\Model\city', 'id', 'city_id');
    }

    public function facility()
    {
        return $this->hasOne('App\Model\Facility', 'id', 'facility_id');
    }
}
