<?php

namespace App\model;

use App\Model\contracts;
use App\Model\payments;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class paymentMethod extends Model
{
    use SoftDeletes;
    protected $fillable = ['currency_id', 'payment_method_name', 'exchange_rate','short_code','sort_order'];

    public function validatePaymentMethod($request)
    {
        $request->validate([
            'currency_id' => 'required|max:255',
            'payment_method_name' => 'required',
            'exchange_rate' => 'required',
            'short_code' => 'required',
            'sort_order' => 'required|unique:payment_methods',

        ]);
    }

    public function getAllPaymentMethod()
    {
        return paymentMethod::orderBy('sort_order')->get();
    }

    public function storePaymentMethod($object)
    {
        return paymentMethod::create($object);
    }

    public function showPaymentMethod($id)
    {
        return paymentMethod::find($id);
    }

    public function updatePaymentMethod($id, $object)
    {

        return paymentMethod::where('id', $id)
            ->update(
                [
                    'currency_id' => $object['currency_id'],
                    'payment_method_name' => $object['payment_method_name'],
                    'exchange_rate' => $object['exchange_rate'],
                    'short_code' => $object['short_code'],
                    'sort_order' => $object['sort_order'],

                ]
            );
    }

    public function deletePaymentMethod($id)
    {
        $provinceRegion = new paymentMethod;
            // return $provinceRegion->find($id)->delete();
        if (count($provinceRegion->find($id)->contracts()->get()) > 0) {
            return false;
        } else {
            return $provinceRegion->find($id)->delete();
        }
    }

    //
    public function contracts()
    {
        return $this->hasMany(contracts::class, 'payment_id', 'id');
    }

    public function hasCurrency()
    {
        return $this->belongsTo('App\Model\currency', 'currency_id', 'id');
    }
}
