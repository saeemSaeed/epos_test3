<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class contracts extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'contract_title',
        'contractor_id',
        'amount',
        'amount_pkr',
        'orginal_amount',
        'orginal_amount_pkr',
        'payment_id',
        'short_name',
        'currency_id',
        'measure_id',
        'planned_start_date',
        'planned_end_date',
        'contract_description',
        'contract_date',
        'contract_start_date',
        'contract_end_date',
        'contract_file',
        'performance_security_file',
        'performance_security_file_name',
        'ps_amount',
        'ps_validity',
        'instrument_type',
        'ps_currency',
        'exchange_rate_id',
        'less_retention',
    ];

    public function validateContracts($request)
    {

        $request->validate([

            'contract_title' => 'required|max:255|unique:contracts,contract_title',
            'contractor_id' => 'required',
            'amount' => 'required',
            'payment_id' => 'required',

            'planned_start_date' => 'nullable|date',
            'planned_end_date' => 'nullable|date|after:planned_start_date',


        ]);
    }

    public function validateUpdateContracts($request)
    {
        $request->validate([


            'contract_title_edit' => 'required|max:255|unique:contracts,contract_title,' . $request->id,
            'contractor_id_edit' => 'required',
            'amount_edit' => 'required',
            'payment_id_edit' => 'required',

            'contract_planned_start_date_edit' => 'nullable|date',
            'contract_planned_end_date_edit' => 'nullable|date|after:contract_planned_start_date_edit',

        ]);
    }

    public function getAllContracts()
    {
        return contracts::orderby('contract_title')->get();
    }

    public function storeContracts($object)
    {
        return contracts::create($object);
    }

    public function showContracts($id)
    {
        return contracts::find($id);
    }

    public function updateContracts($id, $object)
    {


        return contracts::where('id', $id)->update(
            [
//                'project_id' => $object['project_id'],
                'contract_title' => $object['contract_title_edit'],
                'contractor_id' => $object['contractor_id_edit'],
                'amount' => $object['amount_edit'],
                'amount_pkr' => $object['amount_pkr'],
                'payment_id' => $object['payment_id_edit'],

                'planned_start_date' => $object['contract_planned_start_date_edit'],
                'planned_end_date' => $object['contract_planned_end_date_edit'],

                'contract_description' => $object['contract_description_edit'],
                'contract_start_date' => $object['contract_start_date_edit'],
                'contract_date' => $object['contract_date_edit'],
                'contract_end_date' => $object['contract_end_date_edit'],
                'performance_security_file' => $object['performance_security_file'],
                'performance_security_file_name' => $object['performance_security_file_name'],
                'ps_amount' => $object['ps_amount'],
                'ps_currency' => $object['ps_currency'],
                'ps_validity' => $object['ps_validity'],
                'instrument_type' => $object['instrument_type'],
                'exchange_rate_id' => $object['exchange_rate_id'],
                'less_retention' => $object['less_retention'],
            ]
        );
    }


    public function deleteContracts($id)
    {
        $provinceRegion = new contracts;
        // return $provinceRegion->find($id)->delete();
        if ($provinceRegion->find($id)) {
            if (count($provinceRegion->find($id)->payments()->get()) > 0) {
                return false;
            } else {
                return $provinceRegion->find($id)->delete();
            }
        }
    }




    public function payments()
    {
        return $this->hasMany(payments::class, 'contract_id', 'id');
    }

    public function contractBelongsToProvinceDivision()
    {
        return $this->hasMany('App\Model\contractHasProvincDivision', 'contract_id', 'id');
    }

    public function hasCity()
    {
        return $this->belongsTo('App\Model\city', 'contract_currency', 'id');
    }

    public function hasContract()
    {
        return $this->belongsTo('App\Model\contracts', 'Contractor_id', 'id');
    }

    public function hasContractor()
    {
        return $this->belongsTo('App\Model\contractors', 'contractor_id', 'id');
    }

    public function hasOneContractor()
    {
        return $this->hasOne('App\Model\contractors', 'id', 'contractor_id');
    }

    public function hasCurrency()
    {
        return $this->belongsTo('App\Model\currency', 'currency_id', 'id');
    }

    public function hasPaymentMethod()
    {
        return $this->belongsTo('App\Model\paymentMethod', 'payment_id', 'id');
    }

    public function hasMeasures()
    {
        return $this->belongsTo('App\Model\Measures', 'measure_id', 'id');
    }

    public function status($id, $status)
    {


        return contracts::where('id', $id)->update(['status' => $status]);
    }

    public function hasContractFile()
    {
        return $this->hasMany('App\Model\contractsHasFile', 'contract_id', 'id');
    }

    public function ContractsHasRetainFile()
    {
        return $this->hasMany('App\Model\ContractsHasRetainFile', 'contract_id', 'id');
    }

    public function hasContractPayment()
    {
        return $this->hasMany('App\Model\contractPayment', 'contract_id', 'id');
    }

    public function getProjectSelectedMeasureSubcategory($project_id, $measure_id)
    {
        return ProjectsHasProvince::
        where('project_id', $project_id)->
        where('measure_id', $measure_id)->
        where('sub_category_id', '<>', 'select')->
        where('sub_category_id', '<>', '')
            ->groupBy('sub_category_id')->select('sub_category_id')->get();
    }

    public function hasExChangeRate()
    {
        return $this->belongsTo('App\Model\exchangeRates', 'exchange_rate_id', 'id');
    }

}
