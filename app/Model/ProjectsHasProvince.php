<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProjectsHasProvince extends Model
{
    protected $fillable = [
        'project_id',
//        'province_id',
        'measure_id',
        'sub_category_id',
        'budget',
        'revise_amount',
//        'pea_id',
        'description',
        'revise_date',

    ];

    public function validateProjectsHasProvince($request)
    {
        $request->validate([
            'project_id' => 'required|max:255',
            'province_id' => 'required',
            'budget' => 'required',

        ]);
    }

    public function getAllProjectsHasProvince()
    {
        return ProjectsHasProvince::get();
    }

    public function storeProjectsHasProvince($object)
    {
        return ProjectsHasProvince::create($object);
    }

    public function showProjectsHasProvince($id)
    {
        return ProjectsHasProvince::find($id);
    }


    public function showProjectProvinceBy($id)
    {
        return ProjectsHasProvince::where('project_id',$id)->get();
    }


    public function updateProjectsHasProvince($id, $object)
    {


        return ProjectsHasProvince::where('id', $id)
            ->update(
                [
                    'project_id' => $object['project_id'],
                    'province_id' => $object['province_id'],
                    'measure_id' => $object['measure_id'],
                    'sub_category_id' => $object['sub_category_id'],
                    'budget' => $object['budget'],
                    'revise_amount' => $object['revise_amount'],
                    'pea_id' => $object['pea_id'],

                    'description' => $object['description'],
                    'revise_date' => $object['revise_date'],

                ]
            );
    }

    public function deleteProjectsHasProvince($id)
    {
        $provinceRegion = new ProjectsHasProvince;
        return $provinceRegion->where('project_id', $id)->delete();
    }


    public function hasOneMeasures()
    {
        return $this->hasOne('App\Model\Measures', 'id', 'measure_id');
    }

    public function hasOneSubCategories()
    {
        return $this->hasOne('App\Model\subCategories', 'id', 'sub_category_id');
    }

    public function hasOneProvinceRegion()
    {
        return $this->hasOne('App\Model\provinceRegion', 'id', 'province_id');
    }

    public function hasOnePea()
    {
        return $this->hasOne('App\Model\PEAs', 'id', 'pea_id');
    }

    public function getAllProvinceOfProject($id)
    {
        return $this->where('project_id', $id)->groupBy('province_id')->select('province_id')->get();
    }
       public function getReferenceRow($id, $project_id)
    {
        $provinceRegion = new ProjectsHasProvince;
      return  $provinceRegion->where(
            'id' , $id)
        ->where('project_id', $project_id)->first();
    }
    public function hasUser()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function getActiveProjectMeasure($id)
    {

        return $this->where('project_id', $id)->groupBy('measure_id')->select('measure_id')->get();
    }
}
