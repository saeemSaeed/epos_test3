<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Projects;
use App\Model\contracts;
use App\Model\payments;
use App\Model\paymentMethod;
use App\Model\exchangeRates;
class currency extends Model
{
use SoftDeletes;
    protected $fillable = ['currency_name', 'currency_code','currency_symbol','sort_order'];

    public function validateCurrency($request)
    {
        $request->validate([
            'currency_name' => 'unique:currencies|required|max:255',
            'sort_order' => 'unique:currencies|required',
            'currency_code' => 'required',
            'currency_symbol' => 'required',

        ]);
    }

    public function getAllCurrency()
    {
        return currency::orderBy('sort_order')->get();
    }

    public function storeCurrency($object)
    {
        return currency::create($object);
    }

    public function showCurrency($id)
    {
        return currency::find($id);
    }
    public function CurrencyExchangeRate($id)
    {
        return exchangeRates::where('currency_to',$id)->select('id','start_date','end_date','currency_to')->get();
    }

    public function updateCurrency($id, $object)
    {

        return currency::where('id', $id)
            ->update(
                [
                    'currency_name' => $object['currency_name'],
                    'currency_code' => $object['currency_code'],
                    'sort_order' => $object['sort_order'],
                    'currency_symbol' => $object['currency_symbol'],

                ]
            );
    }

     public function deleteCurrency($id)
    {
        $provinceRegion = new currency;
            // return $provinceRegion->find($id)->delete();

        if(count($provinceRegion->find($id)->Projects()->get()) > 0 || count($provinceRegion->find($id)->contracts()->get()) > 0|| count($provinceRegion->find($id)->payments()->get())  > 0 || count($provinceRegion->find($id)->paymentMethod()->get()) > 0 || count($provinceRegion->find($id)->exchangeRatesfrom()->get()) > 0 || count($provinceRegion->find($id)->exchangeRatesto()->get()) > 0) {
            return false;
        }

        else {
            return $provinceRegion->find($id)->delete();
        }

    }

    //for delete currency
    public function Projects()
    {
        return $this->hasMany(Projects::class , 'currency_id', 'id');
    }

    public function contracts()
    {
        return $this->hasMany(contracts::class , 'currency_id', 'id');
    }

    public function payments()
    {
        return $this->hasMany(payments::class , 'currency_id', 'id');
    }

    public function paymentMethod()
    {
        return $this->hasMany(paymentMethod::class , 'currency_id', 'id');
    }

    public function exchangeRatesfrom()
    {
        return $this->hasMany(exchangeRates::class , 'currency_from', 'id');
    }
    public function exchangeRatesto()
    {
        return $this->hasMany(exchangeRates::class , 'currency_to', 'id');
    }
}
