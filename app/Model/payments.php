<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class payments extends Model
{
    use SoftDeletes;
    protected $fillable = [

        'contractor_id',
        'contract_id',
        'invoice_amount',
        'invoice_currency',
        'invoice_date',
        'invoice_no',
        'invoice_less_retention',
        'invoice_less_retention_amount',
        'invoice_withdraw_application',
        'invoice_performance_security_validity',
        'invoice_net_payable',
        'invoice_description',
        'beneficiary_account_id',
        'payment_details_amount',
        'payment_details_date',
        'payment_details_currency',
        'payment_details_code',
        'payment_details_remarks',
        'payment_count_no',

    ];


    public function validatePayment($request)
    {
        $request->validate([
			'invoice_amount'                         => 'required',
			'invoice_currency'                       => 'required',
			'invoice_date'                           => 'required',
			'invoice_no'                             => 'required',
			'invoice_less_retention'                 => 'required',
			'invoice_less_retention_amount'          => 'required',
			'invoice_withdraw_application'           => 'required',
			'invoice_performance_security_validity'  => 'required',
			'invoice_net_payable'                    => 'required',
			'beneficiary_account_id'                 => 'required',
			'payment_details_amount'                 => 'required',
			'payment_details_date'                   => 'required',
			'payment_details_currency'               => 'required',
        ]);
    }

    public function getAllPayment()
    {
        return payments::orderBy('contractor_id', 'ASC')
                        ->orderBy('contract_id', 'ASC')
                        ->orderBy('payment_count_no', 'ASC')
                        ->latest()
                        ->get();
    }

    public function storePayment($object)
    {

        return payments::create($object);
    }

    public function showPayment($id)
    {
        return payments::find($id);
    }

    public function updatePayment($id, $object)
    {

        return payments::where('id', $id)
            ->update(
                [
                    'invoice_amount' => $object['invoice_amount'] ?? null,
                    'invoice_currency' => $object['invoice_currency'] ?? null,
                    'invoice_date' => $object['invoice_date'] ?? null,
                    'invoice_no' => $object['invoice_no'] ?? null,
                    'invoice_less_retention' => $object['invoice_less_retention'] ?? null,
                    'invoice_less_retention_amount' => $object['invoice_less_retention_amount'] ?? null,
                    'invoice_withdraw_application' => $object['invoice_withdraw_application'] ?? null,
                    'invoice_performance_security_validity' => $object['invoice_performance_security_validity'] ?? null,
                    'invoice_net_payable' => $object['invoice_net_payable'] ?? null,
                    'invoice_description' => $object['invoice_description'] ?? null,
                    'beneficiary_account_id' => $object['beneficiary_account_id'] ?? null,
                    'payment_details_amount' => $object['payment_details_amount'] ?? null,
                    'payment_details_date' => $object['payment_details_date'] ?? null,
                    'payment_details_currency' => $object['payment_details_currency'] ?? null,
                    'payment_details_code' => $object['payment_details_code'] ?? null,
                    'payment_details_remarks' => $object['payment_details_remarks'] ?? null,
                ]
            );
    }

    public function deletePayment($id)
    {
        $provinceRegion = new payments;
        $provinceRegion->find($id)->hasPaymentsHasSub()->delete();
        return $provinceRegion->find($id)->delete();
    } //

    public function hasCurrency()
    {
        return $this->belongsTo('App\Model\currency', 'currency_id', 'id');
    }

    public function invoiceCurrency()
    {
        return $this->belongsTo('App\Model\currency', 'invoice_currency', 'id');
    }

    public function Has()
    {
        return $this->belongsTo('App\Model\currency', 'invoice_currency', 'id');
    }

    public function hasMeasure()
    {
        return $this->hasMany('App\Model\Measures', 'id', 'measure_id');
    }


    public function hasPaymentMethod()
    {
        return $this->belongsTo('App\Model\paymentMethod', 'contractPaymentMethod', 'id');
    }

    public function hasPaymentsHasSub()
    {
        return $this->hasMany('App\Model\paymentsHasSub', 'payment_id', 'id');
    }

    public function status($id, $status)
    {


        return payments::where('id', $id)->update(['status' => $status]);
    }

    public function ContractorFinancialDetails()
    {

        return $this->hasOne('App\Model\ContractorFinancialDetails', 'id', 'beneficiary_account_id');
    }

    public function PaymentSupportingFileDetial()
    {
        return $this->hasMany('App\Model\PaymentSupportingFileDetial', 'payment_id', 'id');
    }

    public function PaymentSupportingDocuments()
    {
        return $this->hasMany('App\Model\PaymentSupportingDocuments', 'payment_id', 'id');
    }


    public function PaymentHasContractor()
    {

        return $this->hasOne('App\Model\contractors', 'id', 'contractor_id');
    }
    public function PaymentHasContract()
    {

        return $this->hasOne('App\Model\contracts', 'id', 'contract_id');
    }
}
