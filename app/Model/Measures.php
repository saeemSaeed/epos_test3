<?php

namespace App\model;


use App\Model\ProjectsHasProvince;
use App\Model\contractHasProvincDivision;
use App\Model\contracts;
use App\Model\payments;
use App\Model\paymentsHasSub;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Measures extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'code','status','sort_order'];

    public function validateMeasures($request)
    {
        $request->validate([
            'name' => 'unique:measures|required|max:255',
           'code' => 'required',
           'sort_order' => 'required|unique:measures',
        ]);
    }

    public function getAllMeasures()
    {
        return Measures::orderby('sort_order')->get();
    }

    public function storeMeasures($object)
    {
        return Measures::create($object);
    }

    public function showMeasures($id)
    {
        return Measures::find($id);
    }

    public function updateMeasures($id, $object)
    {

        return Measures::where('id', $id)
            ->update(
                [
                    'name' => $object['name'],
                    'code' => $object['code'],
                    'sort_order' => $object['sort_order'],
                ]
            );
    }

    public function deleteMeasures($id)
    {
        $provinceRegion = new Measures;
        if (count($provinceRegion->find($id)->ProjectsHasProvince()->get()) > 0 || count($provinceRegion->find($id)->contractHasProvincDivision()->get()) > 0 || count($provinceRegion->find($id)->paymentsHasSub()->get()) > 0) {
            return false;
        } else {
            return $provinceRegion->find($id)->delete();
        }

    }
        //delete
    public function ProjectsHasProvince()
    {
        return $this->hasMany(ProjectsHasProvince::class , 'measure_id', 'id');
    }

    public function contracts()
    {
        return $this->hasMany(contracts::class , 'measure_id', 'id');
    }

    public function contractHasProvincDivision()
    {
        return $this->hasMany(contractHasProvincDivision::class , 'measure_id', 'id');
    }

    public function payments()
    {
        return $this->hasMany(payments::class , 'contractMeasures', 'id');
    }

    public function paymentsHasSub()
    {
        return $this->hasMany(paymentsHasSub::class , 'measure_id', 'id');
    }
}
