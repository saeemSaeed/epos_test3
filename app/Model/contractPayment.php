<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class contractPayment extends Model
{
    protected $table = 'contracts_payment';
    protected $fillable = [
        'contract_id',
        'schedule_date',
        'percentage',
        'amount',
        'amount_pkr',

    ];

    public function geContractPayment()
{
    return contractPayment::get();
}

    public function storeContractPayment($object)
    {
        return contractPayment::create($object);
    }

    public function showContractPayment($id)
    {
        return contractPayment::find($id);
    }

    public function updateContractPayment($id, $object)
    {


        return contractPayment::where('id', $id)
            ->update(
                [
                    'contract_id' => $object['contract_id'],
                    'schedule_date' => $object['schedule_date'],
                    'percentage' => $object['percentage'],
                    'amount' => $object['amount']


                ]
            );
    }
    public function deleteContractPayment($id)
    {
        $provinceRegion = new contractPayment;
        return $provinceRegion->where('contract_id', $id)->delete();
    }
}
