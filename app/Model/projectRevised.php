<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class projectRevised extends Model
{
//    use SoftDeletes;
    protected $fillable = ['project_id', 'line_reference_id', 'user_id', 'revised_budget', 'date', 'description', 'status'];

    public function validateProjectRevised($request)
    {
        $request->validate([
            'line_reference_id' => 'required',
            'user_id' => 'required',
            'project_id' => 'required',
            'revised_budget' => 'required',
            'date' => 'required',
        ]);
    }

    public function getProjectRevised()
    {
        return projectRevised::get();
    }

    public function storeProjectRevised($object)
    {
        return projectRevised::create($object);
    }

    public function showProjectRevised($project_id, $line_reference_id)
    {
        return projectRevised::where('project_id', $project_id)
            ->where('line_reference_id', $line_reference_id)
            ->            orderBy('id','desc')->get();

    }

    public function getLatestRow($project_id, $line_reference_id)
    {
//       dd);=

        return projectRevised::
        where('user_id',Auth::id())
            ->where('project_id', $project_id)
            ->where('line_reference_id', $line_reference_id)
            ->latest()->first();
    }

    public function hasUser()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function getProjectReferenceRow($project_id)
    {
        return projectRevised::where('project_id',$project_id)->select('line_reference_id')->groupBy('line_reference_id')->get();
    }

}

