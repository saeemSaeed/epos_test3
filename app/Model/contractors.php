<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class contractors extends Model
{
use SoftDeletes;
    protected $fillable = [
        'sort_order',
        'contractor_name',
        'contractor_ntn',
        'contractor_city',
        'contractor_address',
        'contractor_landline',
        'contractor_details',
    ];

    public function validateContractors($request)
    {
        $request->validate([
            'contractor_name' => 'required|max:255',
            'contractor_city' => 'required',
            'contractor_address' => 'required',
            'contractor_landline' => 'required',
            'sort_order' => 'required|unique:contractors',
        ]);
    }

    public function getAllContractors()
    {
        return contractors::orderby('sort_order')->get();
    }

    public function storeContractors($object)
    {

        return contractors::create($object);

    }

    public function showContractors($id)
    {
        return contractors::find($id);
    }

    public function updateContractors($id, $object)
    {

        return contractors::where('id', $id)->update(
            [
                'contractor_name' => $object['contractor_name'],
                'contractor_ntn' => $object['contractor_ntn'],
                'contractor_city' => $object['contractor_city'],
                'contractor_landline' => $object['contractor_landline'],
                'contractor_details' => $object['contractor_details'],
                'contractor_address' => $object['contractor_address'],
                'sort_order' => $object['sort_order'],
            ]
        );
    }

    public function deleteContractors($id)
    {
        $provinceRegion = new contractors;
            // return $provinceRegion->find($id)->delete();
        if (count($provinceRegion->find($id)->contracts()->get()) > 0) {
            return false;
        } else {
            return $provinceRegion->find($id)->delete();
        }
    }

    public function contracts()
    {
        return $this->hasMany(contracts::class, 'Contractor_id', 'id');
    }

    public function hasCity()
    {
        return $this->belongsTo('App\Model\city', 'contractor_city', 'id');
    }

    public function contractorContactInformation()
    {
        return $this->hasMany('App\Model\ContractorContactInformation', 'contractor_id', 'id');
    }

    public function ContractorFinancialDetails()
    {
        return $this->hasMany('App\Model\ContractorFinancialDetails', 'contractor_id', 'id');
    }

}
