<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Projects extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
        'donor_number',
        'donor_name',
        'budget',
        'currency_id',
        'province_id',
        'pea_id',
        'currency_id',
        'planned_start_date',
        'planned_end_date',
        'project_start_date',
        'project_end_date',
        'project_description',
        'bugdet_revised',
        'revised_expected_completion_date',
        'status',
        'approval_file',
        'approval_file_title',
        'is_approved',
    ];

    public function validateProject($request)
    {
        $request->validate([

            'title' => 'required|unique:projects||max:255',
            'budget' => 'required',
            'donor_name' => 'required',
            'province_id' => 'required',
            'currency_id' => 'required',
               'planned_start_date' => 'nullable|date',
            'planned_end_date' =>'nullable|date|after:planned_start_date',

        ]);
    }
        public function CheckProjectApprovedOrNot($id)
    {
        $CheckProjectApprovedOrNot=Projects::where('id',$id)->where('is_approved',1)->Where('status',1)->first();
        return  !is_null($CheckProjectApprovedOrNot) ? true : false;;
    }

    public function validateUpdateProject($request)
    {
        $request->validate([

            'title' => 'required|max:255|unique:projects,id,' . $request->id,
            'budget' => 'required',
            'donor_name' => 'required',
            'province_id' => 'required',
            'currency_id' => 'required',
               'planned_start_date' => 'nullable|date',
            'planned_end_date' =>'nullable|date|after:planned_start_date',

        ]);
    }

    public function getAllProject()
    {
        return Projects::get();
    }

    public function storeProject($object)
    {
        return Projects::create($object);
    }
    public function getActiveProject()
    {
        return Projects::where('is_approved',1)->orWhere('status',1)->select('id','title')->orderby('title')->get();
    }

    public function showProject($id)
    {
        return Projects::find($id);
    }

    public function updateProject($id, $object)
    {


        return Projects::where('id', $id)
            ->update(
                [
                    'title' => $object['title'],
                    'donor_number' => $object['donor_number'],
                    'donor_name' => $object['donor_name'],
                    'budget' => $object['budget'],
                    'pea_id' => $object['pea_id'],
                    'province_id' => $object['province_id'],
                    'currency_id' => $object['currency_id'],
                    'planned_start_date' => $object['planned_start_date'],
                    'planned_end_date' => $object['planned_end_date'],
                    'project_start_date' => $object['project_start_date'],
                    'project_end_date' => $object['project_end_date'],
                    'project_description' => $object['project_description'],
                    'bugdet_revised' => $object['bugdet_revised'] ?? null,
                    'revised_expected_completion_date' => $object['revised_expected_completion_date'],
                    'status' => $object['status'],
                    'approval_file' => $object['approval_file'] ?? null,
                    'approval_file_title' => $object['approval_file_title'] ?? null,

                ]
            );
    }

    public function deleteProject($id)
    {
        $provinceRegion = new Projects;
        return $provinceRegion->find($id)->delete();
    }

    public function contracts()
    {
        return $this->hasMany(contracts::class, 'project_id', 'id');
    }

    public function status($id, $status)
    {

        if((int)$status ===1 ) {
            $approval = 1;
            return Projects::where('id', $id)->update(
                [
                    'status' => $status,
                    'is_approved' =>$approval
                ]);
        }

        return Projects::where('id', $id)->update(
            [
                'status' => $status,

            ]);
    }


    public function belongsToProvince()
    {
        return $this->hasMany('App\Model\ProjectsHasProvince', 'project_id', 'id');
    }

    public function hasOneProvinceRegion()
    {
        return $this->hasOne('App\Model\provinceRegion', 'id', 'province_id');
    }

    public function hasOnePea()
    {
        return $this->hasOne('App\Model\PEAs', 'id', 'pea_id');
    }

    public function hasCurrency()
    {
        return $this->hasOne('App\Model\currency', 'id', 'currency_id');
    }

    public function hasProjectFile()
    {
        return $this->hasMany('App\Model\ProjectsFiles', 'project_id', 'id');
    }

    public function getProvincePeaCity($id)
    {

        return $this->where('id', $id)->select('province_id','pea_id')->get();
    }
}
