<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class contractsHasFile extends Model
{
       protected $fillable = ['contract_id',
           'file','file_name',
           'file_icon',
           'category',
           'contract_file_amount',
           'description'];
}
