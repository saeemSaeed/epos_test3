<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FileCategory extends Model
{
	protected $table = 'file_categories';
   protected $guarded = [];
}
