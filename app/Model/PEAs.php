<?php


namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class PEAs extends Model
{
    use SoftDeletes;
    protected $fillable = ['pea_name', 'province_id', 'pea_description','sort_order'];

    public function validatePEAs($request)
    {
        $request->validate([
            'pea_name' => 'required|max:255',
            'province_id' => 'required',
            'sort_order' => 'required|unique:p_e_as',


        ]);
    }

    public function getPEAs()
    {
        return PEAs::orderby('sort_order')->get();
    }


    public function storePEAs($object)
    {
        return PEAs::create($object);
    }

    public function showPEAs($id)
    {
        return PEAs::find($id);
    }

    public function showPEAsWithinProvince($id)
    {
        return PEAs::where('province_id', $id)->orderBy('sort_order')->get();
    }

    public function updatePEAs($id, $object)
    {


        return PEAs::where('id', $id)
            ->update(
                [
                    'pea_name' => $object['pea_name'],
                    'province_id' => $object['province_id'],
                    'pea_description' => $object['pea_description'],
                    'sort_order' => $object['sort_order'],

                ]
            );

    }

    public function deletePEAs($id)
    {
        $provinceRegion = new PEAs;
        // return $provinceRegion->find($id)->delete();
        if ( count($provinceRegion->find($id)->contractHasProvincDivision()->get()) > 0 || count($provinceRegion->find($id)->paymentsHasSub()->get()) > 0) {
            return false;
        } else {
            return $provinceRegion->find($id)->delete();
        }

    }

    //delete functions
    public function ProjectsHasProvince()
    {
        return $this->hasMany(ProjectsHasProvince::class, 'pea_id', 'id');
    }

    public function contractHasProvincDivision()
    {
        return $this->hasMany(contractHasProvincDivision::class, 'pea_id', 'id');
    }

    public function paymentsHasSub()
    {
        return $this->hasMany(paymentsHasSub::class, 'pea_id', 'id');
    }

    public function hasProvince()
    {
        return $this->belongsTo('App\Model\provinceRegion', 'province_id', 'id');
    }
}
