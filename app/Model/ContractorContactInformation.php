<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContractorContactInformation extends Model
{
	protected $table = 'contractor_contact_information';
	protected $guarded = [];
}
