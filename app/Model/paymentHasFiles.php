<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class paymentHasFiles extends Model
{
    protected $table = 'payment_has_files';
    protected $fillable = ['payment_id', 'file','category','description'];
}
