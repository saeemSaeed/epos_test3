<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    protected $guarded = [];



    public function contractHasProvincDivisions()
    {
    	return $this->hasMany('App\Model\contractHasProvincDivision', 'facility_id', 'id');
    }

}
