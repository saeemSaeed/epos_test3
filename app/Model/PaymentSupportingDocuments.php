<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PaymentSupportingDocuments extends Model
{
    protected $table = 'payment_supporting_file';
    protected $guarded = [];
}
