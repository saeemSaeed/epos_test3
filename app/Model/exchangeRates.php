<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class exchangeRates extends Model
{
    use SoftDeletes;
    protected $fillable = ['exchange_rate', 'start_date', 'end_date', 'currency_to', 'currency_from', 'sort_order'];

    public function validateExchangeRates($request)
    {
        $request->validate([
            'exchange_rate' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'date|after_or_equal:start_date',
            'currency_to' => 'required',
            'currency_from' => 'required',
            'sort_order' => 'required|unique:exchange_rates',


        ]);
    }

    public function getAllExchangeRates()
    {
        return exchangeRates::orderBy('sort_order')->get();
    }

    public function storeExchangeRates($object)
    {
        return exchangeRates::create($object);
    }

    public function showExchangeRates($id)
    {
        return exchangeRates::find($id);
    }

    public function updateExchangeRates($id, $object)
    {

        return exchangeRates::where('id', $id)
            ->update(
                [
                    'exchange_rate' => $object['exchange_rate'],
                    'start_date' => $object['start_date'],
                    'end_date' => $object['end_date'],
                    'sort_order' => $object['sort_order'],

                ]
            );
    }

    public function deleteExchangeRates($id)
    {
        $provinceRegion = new exchangeRates;
        return $provinceRegion->find($id)->delete();
    }

    public function currencyFrom()
    {
        return $this->belongsTo('App\Model\currency', 'currency_from', 'id');
    }

    public function currencyTo()
    {
        return $this->belongsTo('App\Model\currency', 'currency_to', 'id');
    }
}
