<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class contractHasProvincDivision extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'project_id',
        'measure_id',
        'contract_id',
        'sub_category_id',
        'province_id',
        'pea_id',
        'city_id',
        'facility_id',
        'budget_pkr',
        'budget',
        'description',
        'facility_id'
    ];

    public function validateContractHasProvinceDivision($request)
    {
        $request->validate([
            'contract_id' => 'required|max:255',
            'sub_category_id' => 'required',
            'province_id' => 'required',
            'budget' => 'required',

        ]);
    }

    public function getAllContractHasProvinceDivision()
    {
        return contractHasProvincDivision::get();
    }

    public function storeContractHasProvinceDivision($object)
    {
        return contractHasProvincDivision::create($object);
    }

    public function showContractHasProvinceDivision($id)
    {
        return ProjectsHasProvince::find($id);
    }

    public function updateContractHasProvinceDivision($id, $object)
    {


        return contractHasProvincDivision::where('id', $id)
            ->update(
                [
                    'sub_category_id' => $object['sub_category_id'],
                    'contract_id' => $object['contract_id'],
                    'province_id' => $object['province_id'],
                    'pea_id' => $object['pea_id'],
                    'city_id' => $object['city_id'],
                    'budget' => $object['budget'],
                    'budget_pkr' => $object['budget_pkr'],
                    'description' => $object['description']



                ]
            );
    }
    public function deleteContractHasProvince($id)
    {
        $provinceRegion = new contractHasProvincDivision;
        return $provinceRegion->where('contract_id', $id)->delete();
    }
    public function deleteContractHasProvinceDivision($id)
    {
        $provinceRegion = new contractHasProvincDivision;
        return $provinceRegion->find($id)->delete();
    }

    public function hasProjects()
    {
        return $this->belongsTo('App\Model\Projects', 'project_id', 'id');
    }
    public function hasSubCategories()
    {
        return $this->belongsTo('App\Model\subCategories', 'sub_category_id', 'id');
    }
    public function hasProvince()
    {
        return $this->hasOne('App\Model\provinceRegion', 'id', 'province_id');
    }


    public function hasPeas()
    {
        return $this->hasOne('App\Model\PEAs', 'id', 'pea_id');
    }
    public function hasCity()
    {
        return $this->hasOne('App\Model\city', 'id', 'city_id');
    }
    public function hasMeasure()
    {
        return $this->hasOne    ('App\Model\Measures', 'id', 'measure_id');
    }

    public function belongsToMeasure()
    {
        return $this->belongsTo('App\Model\Measures', 'measure_id', 'id');
    }


    public function contract()
    {
        return $this->belongsTo('App\Model\contracts', 'contract_id', 'id');
    }
}
