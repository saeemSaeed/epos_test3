<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PEASContactInfo extends Model
{
   protected $table = 'peas_contact_information';
   protected $guarded = [];
}
