<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


   class ProjectsFiles extends Model
{
    protected $table = 'projects_files';
    protected $fillable = ['project_id','file','category','description'];

}
