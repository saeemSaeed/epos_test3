<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class projectHistory extends Model
{
    use SoftDeletes;

    protected $table = 'projects_history';
    protected $fillable = [
        'project_id',
        'title',
        'donor_number',
        'donor_name',
        'budget',
        'currency_id',
        'province_id',
        'pea_id',
        'currency_id',
        'planned_start_date',
        'planned_end_date',
        'project_start_date',
        'project_end_date',
         'revised_expected_completion_date',
        'bugdet_revised',
        'project_description',
        'approval_file',
        'approval_file_title',
        'status',
    ];

    public function validateProject($request)
    {
        $request->validate([

            'title' => 'required|unique:projects||max:255',
            'budget' => 'required',
            'donor_name' => 'required',
            'province_id' => 'required',
            'currency_id' => 'required',

        ]);
    }
    public function validateUpdateProject($request)
    {
        $request->validate([

            'title' => 'required|max:255|unique:projects,id,' . $request->id,
            'budget' => 'required',
            'donor_name' => 'required',
            'province_id' => 'required',
            'currency_id' => 'required',

        ]);
    }

    public function getAllProject()
    {
        return projectHistory::get();
    }

    public function getHistoryOfProject($id)
    {
        return projectHistory::where('project_id',$id)->orderBy('created_at','desc')->get();
    }

    public function storeProject($object)
    {
//        dd($object);
        return projectHistory::create($object);
    }

    public function showProject($id)
    {
        return projectHistory::where('project_id',$id)->get();
    }
    public function showProjectByRowId($id)
    {
        return projectHistory::where('id',$id)->first();
    }

    public function updateProject($id, $object)
    {


        return projectHistory::where('id', $id)
            ->update(
                [
                    'title' => $object['title'],
                    'donor_number' => $object['donor_number'],
                    'donor_name' => $object['donor_name'],
                    'budget' => $object['budget'],
                    'pea_id' => $object['pea_id'],
                    'province_id' => $object['province_id'],
                    'currency_id' => $object['currency_id'],
                    'planned_start_date' => $object['planned_start_date'],
                    'planned_end_date' => $object['planned_end_date'],
                    'project_start_date' => $object['project_start_date'],
                    'project_end_date' => $object['project_end_date'],
                    'project_description' => $object['project_description'],
                    'bugdet_revised' => $object['bugdet_revised'],
                    'revised_expected_completion_date' => $object['revised_expected_completion_date'],
                    'approval_file' => $object['approval_file'],
                    'approval_file_title' => $object['approval_file_title']

                ]
            );
    }

    public function deleteProject($id)
    {
        $provinceRegion = new projectHistory;
        return $provinceRegion->find($id)->delete();
    }

    public function contracts()
    {
        return $this->hasMany(contracts::class, 'project_id', 'id');
    }

    public function status($id, $status)
    {


        return projectHistory::where('id', $id)->update(['status' => $status]);
    }

    public function belongsToProvince()
    {
        return $this->hasMany('App\Model\projectHasProvinceHistory', 'row_id', 'id');
    }
    public function hasOneProvinceRegion()
    {
        return $this->hasOne('App\Model\provinceRegion', 'id', 'province_id');
    }

    public function hasOnePea()
    {
        return $this->hasOne('App\Model\PEAs', 'id', 'pea_id');
    }

    public function hasCurrency()
    {
        return $this->hasOne('App\Model\currency', 'id', 'currency_id');
    }
    public function hasProjectFile()
    {
        return $this->hasMany('App\Model\ProjectsFiles', 'project_id', 'id');
    }


}
