<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PaymentSupportingFileDetial extends Model
{
    protected $table = 'payment_supporting_file_detial';
    protected $guarded = [];
}
