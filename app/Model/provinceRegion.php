<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use App\Model\PEAs;
use App\Model\ProjectsHasProvince;
use App\Model\city;
use App\Model\contractHasProvincDivision;
use App\Model\contracts;
use App\Model\paymentsHasSub;
use Illuminate\Database\Eloquent\SoftDeletes;
class provinceRegion extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'short_code', 'detail','sort_order'];

    public function validateProvinceRegion($request)
    {
        $request->validate([
            'name' => 'required|max:255|unique:province_regions',
            'sort_order' => 'required|unique:province_regions',
            'short_code' => 'nullable',
            'detail' => 'nullable|max:255',
        ]);
    }

    public function getAllProvinceRegion()
    {
        return provinceRegion::orderby('sort_order')->get();
    }

    public function storeProvinceRegion($object)
    {
        return provinceRegion::create($object);
    }

    public function showProvinceRegion($id)
    {
        return provinceRegion::where('id', $id)->get();
    }

    public function FindProvinceRegion($id)
    {
        return provinceRegion::find($id);
    }

    public function updateProvinceRegion($id, $object)
    {

        return provinceRegion::where('id', $id)
            ->update(
                [
                    'name' => $object['name'],
                    'short_code' => $object['short_code'],
                    'detail' => $object['detail'],
                    'sort_order' => $object['sort_order'],
                ]
            );
    }

    public function deleteProvinceRegion($id)
    {
        $provinceRegion = new provinceRegion;
            // return $provinceRegion->find($id)->delete();
        if (count($provinceRegion->find($id)->pea()->get()) > 0 || count($provinceRegion->find($id)->city()->get()) > 0 || count($provinceRegion->find($id)->paymentsHasSub()->get()) > 0  || count($provinceRegion->find($id)->contractHasProvincDivision()->get()) > 0) {
            return false;
        } else {
            return $provinceRegion->find($id)->delete();
        }
    }
// Only to check this model id present in other /models tables
    public function PEA()
    {
        return $this->hasMany(PEAs::class , 'province_id', 'id');
    }

    public function city()
    {
        return $this->hasMany(city::class , 'province_id', 'id');
    }

    public function projectHasProvince()
    {
        return $this->hasMany(ProjectsHasProvince::class , 'province_id', 'id');
    }

    public function paymentsHasSub()
    {
        return $this->hasMany(paymentsHasSub::class , 'province_id', 'id');
    }

    public function contracts()
    {
        return $this->hasMany(contracts::class , 'province_id', 'id');
    }

    public function contractHasProvincDivision()
    {
        return $this->hasMany(contractHasProvincDivision::class , 'province_id', 'id');
    }
    public function FindAllProvinceRegion($id)
    {
        return provinceRegion::whereIn('id', $id)->get();
    }

    public function showAllPEASOfProvinceRegion($id)
    {
        return PEAs::where('province_id', $id)->get();
    }

}
