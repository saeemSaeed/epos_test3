<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContractorFinancialDetails extends Model
{
    protected $table = 'contractor_financial_details';
    protected $guarded = [];
    public function hasCity()
    {
        return $this->belongsTo('App\Model\city', 'city_id', 'id');
    }
}
