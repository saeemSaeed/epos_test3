<?php

namespace App\model;

use App\Model\ProjectsHasProvince;
use App\Model\contractHasProvincDivision;
use App\Model\paymentsHasSub;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class subCategories extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'measure_id','sequence_number'];

    public function validateSubCategories($request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'measure_id' => 'required|nullable',
            'sequence_number' => 'required|int|unique:sub_categories',
        ]);
    }

    public function getAllSubCategories()
    {
        return subCategories::orderby('sequence_number')->get();
    }

    public function storeSubCategories($object)
    {

        return subCategories::create($object);

    }

    public function showAllSubCategoriesOfmeasure($id)
    {
        return subCategories::where('measure_id',$id)->orderby('sequence_number')->get();
    }
    public function showSubCategories($id)
    {
            return subCategories::with('hasMeasure')->find($id);
    }

    public function showSubCategoriesWithOutHasMeasuree($id)
    {
            return subCategories::where('measure_id',$id)->orderby('sequence_number')->get();
    }


    public function updateSubCategories($id, $object)
    {

        return subCategories::where('id', $id)->update(
            [
                'name' => $object['name'],
                'measure_id' => $object['measure_id'],
                'sequence_number' => $object['sequence_number'],
            ]
        );
    }

    public function deleteSubCategories($id)
    {
        $provinceRegion = new subCategories;
            // return $provinceRegion->find($id)->delete();
        if(count($provinceRegion->find($id)->ProjectsHasProvince()->get()) > 0 || count($provinceRegion->find($id)->contractHasProvincDivision()->get()) > 0|| count($provinceRegion->find($id)->paymentsHasSub()->get()) > 0 ) {
            return false;
        }
        else {
            return $provinceRegion->find($id)->delete();
        }
    }
    //delete
    public function ProjectsHasProvince()


    {
        return $this->hasMany(ProjectsHasProvince::class , 'sub_category_id', 'id');
    }

    public function contractHasProvincDivision()
    {
        return $this->hasMany(contractHasProvincDivision::class , 'sub_category_id', 'id');
    }

    public function paymentsHasSub()
    {
        return $this->hasMany(paymentsHasSub::class , 'sub_cate_contracts', 'id');
    }

    public function hasMeasure()
    {
        return $this->belongsTo('App\Model\Measures', 'measure_id', 'id');
    }

}
