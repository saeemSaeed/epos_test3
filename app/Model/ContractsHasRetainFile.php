<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContractsHasRetainFile extends Model
{
    protected $table='contracts_has_retain_files';
    protected $fillable = ['contract_id','file','file_name','retain_file_date'


		,'retain_file_amount'
		,'description'

	];
}
