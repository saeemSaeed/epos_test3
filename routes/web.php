<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

route::get('test',function (){
    return view('admin.project.history_list');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'admin\adminController@index')->name('home');
    // laravel logs
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
//contants routes
    Route::prefix('constant')->group(function () {
        Route::get('get/active/project', 'constantController@getActiveProjects')->name('constant.active.project');
        Route::get('get/sub-cat/', 'constantController@SubCategories')->name('constant.sub-cat');
        Route::get('get/all/province', 'constantController@allProvince')->name('constant.get.province');
        Route::get('get/all/province/one', 'constantController@Province')->name('constant.get.province.one');
        Route::get('get/sub-cat/option', 'constantController@MeasuresSelect')->name('constant.sub-cat.option');
        Route::get('get/pea/find', 'constantController@PeaFind')->name('constant.pea.find');

        Route::get('get/contract/sub-cat/option', 'constantController@contractSelectProvince')->name('constant.contract.sub-cat.option');
        //get city and pea against province
        Route::get('get/contract/cityPeas', 'constantController@GetCityPeas')->name('constant.contract.cityPeas');
    });


    Route::prefix('admin')->group(function () {

        route::get('dashboard','admin\DashboardController@index')->name('dashboard.index');
        Route::get('home', 'admin\adminController@index')->name('home');


        Route::prefix('vendor')->group(function () {
            Route::get('index', 'admin\vendorController@index')->name('vendor.index');
            Route::get('create', 'admin\vendorController@create')->name('vendor.create');
        });


        Route::prefix('report')->group(function () {
            Route::get('index', 'admin\reportController@index')->name('report.index');
        });

        Route::prefix('province-region')->group(function () {
            Route::get('/', 'admin\provinceRegionController@index')->name('province-region.index');
            Route::post('store', 'admin\provinceRegionController@store')->name('province-region.store');
            Route::post('update', 'admin\provinceRegionController@update')->name('province-region.update');
            Route::get('show/{id}', 'admin\provinceRegionController@show')->name('province-region.show');
            Route::get('edit', 'admin\provinceRegionController@edit')->name('province-region.edit');
            Route::get('delete/{id}', 'admin\provinceRegionController@destroy')->name('province-region.delete');

            Route::get('/deleted', 'admin\provinceRegionController@provinceRegionDeleted')->name('provinceRegion.Deleted');
            Route::get('restore/{id}', 'admin\provinceRegionController@provinceRegionRestore')->name('provinceRegion.Restore');
        });


        Route::prefix('measure')->group(function () {
            Route::get('/', 'admin\measureController@index')->name('measure.index');
            Route::post('store', 'admin\measureController@store')->name('measure.store');
            Route::post('update', 'admin\measureController@update')->name('measure.update');
            Route::get('show/{id}', 'admin\measureController@show')->name('measure.show');
            Route::get('edit', 'admin\measureController@edit')->name('measure.edit');
            Route::get('delete/{id}', 'admin\measureController@destroy')->name('measure.delete');
            Route::get('/deleted', 'admin\measureController@measureDeleted')->name('measure.Deleted');
            Route::get('restore/{id}', 'admin\measureController@measureRestore')->name('measure.restore');
        });
        Route::prefix('sub-categories')->group(function () {
            Route::get('index', 'admin\subCategoriesController@index')->name('sub-categories.index');
            Route::get('/', 'admin\subCategoriesController@index')->name('sub-categories.index');
            Route::post('store', 'admin\subCategoriesController@store')->name('sub-categories.store');
            Route::post('update', 'admin\subCategoriesController@update')->name('sub-categories.update');
            Route::get('show/{id}', 'admin\subCategoriesController@show')->name('sub-categories.show');
            Route::get('edit', 'admin\subCategoriesController@edit')->name('sub-categories.edit');
            Route::get('delete/{id}', 'admin\subCategoriesController@destroy')->name('sub-categories.delete');

            Route::get('/deleted', 'admin\subCategoriesController@subCategoriesDeleted')->name('subCategories.Deleted');
            Route::get('restore/{id}', 'admin\subCategoriesController@subCategoriesRestore')->name('subCategories.restore');
        });
        Route::prefix('paymentMethod')->group(function () {
            Route::get('/', 'admin\paymentMethodController@index')->name('paymentMethod.index');
            Route::post('store', 'admin\paymentMethodController@store')->name('paymentMethod.store');
            Route::post('update', 'admin\paymentMethodController@update')->name('paymentMethod.update');
            Route::get('show/{id}', 'admin\paymentMethodController@show')->name('paymentMethod.show');
            Route::get('edit', 'admin\paymentMethodController@edit')->name('paymentMethod.edit');
            Route::get('delete/{id}', 'admin\paymentMethodController@destroy')->name('paymentMethod.delete');

            Route::get('/deleted', 'admin\paymentMethodController@Deleted')->name('paymentMethod.Deleted');
            Route::get('restore/{id}', 'admin\paymentMethodController@Restore')->name('paymentMethod.restore');
        });
        Route::prefix('currency')->group(function () {
            Route::get('index', 'admin\currencyController@index')->name('currency.index');
            Route::get('/', 'admin\currencyController@index')->name('currency.index');

            Route::post('store', 'admin\currencyController@store')->name('currency.store');
            Route::post('update', 'admin\currencyController@update')->name('currency.update');
            Route::get('show', 'admin\currencyController@show')->name('currency.show');
            Route::get('show\exchangerate', 'admin\currencyController@showExchangeRate')->name('currency.exchangerate.show');
            Route::get('show\exchangerate\from', 'admin\currencyController@showExchangeRateFrom')->name('currency.exchangerate.show.from');
            Route::get('edit', 'admin\currencyController@edit')->name('currency.edit');
            Route::get('delete/{id}', 'admin\currencyController@destroy')->name('currency.delete');

            Route::get('/deleted', 'admin\currencyController@Deleted')->name('currency.Deleted');
            Route::get('restore/{id}', 'admin\currencyController@Restore')->name('currency.restore');
        });
        Route::prefix('exchange_rate')->group(function () {
            Route::get('index', 'admin\exchangeRateController@index')->name('exchangeRate');
            Route::get('/', 'admin\exchangeRateController@index')->name('exchangeRate.index');
            Route::post('store', 'admin\exchangeRateController@store')->name('exchangeRate.store');
            Route::post('contract/store', 'admin\exchangeRateController@exchangeRatesContractStore')->name('exchangeRate.contract.store');
            Route::post('update', 'admin\exchangeRateController@update')->name('exchangeRate.update');
            Route::get('show/{id}', 'admin\exchangeRateController@show')->name('exchangeRate.show');
            Route::get('edit', 'admin\exchangeRateController@edit')->name('exchangeRate.edit');
            Route::get('exchangerate/from/to', 'admin\exchangeRateController@GetExchangeRate')->name('exchangerate.from.to');

            Route::get('delete/{id}', 'admin\exchangeRateController@destroy')->name('exchangeRate.delete');

            Route::get('/deleted', 'admin\exchangeRateController@Deleted')->name('exchangeRate.Deleted');
            Route::get('restore/{id}', 'admin\exchangeRateController@Restore')->name('exchangeRate.restore');

            Route::get('sort/order', 'admin\exchangeRateController@GetSortOrder')->name('exchangeRate.sort.order');
        });

        Route::prefix('city')->group(function () {
            Route::get('index', 'admin\cityController@index')->name('city.index');


            Route::get('/', 'admin\cityController@index')->name('city.index');
            Route::post('store', 'admin\cityController@store')->name('city.store');
            Route::post('update', 'admin\cityController@update')->name('city.update');
            Route::get('show/{id}', 'admin\cityController@show')->name('city.show');
            Route::get('edit', 'admin\cityController@edit')->name('city.edit');
            Route::get('delete/{id}', 'admin\cityController@destroy')->name('city.delete');

            Route::get('/deleted', 'admin\cityController@Deleted')->name('city.Deleted');
            Route::get('restore/{id}', 'admin\cityController@Restore')->name('city.restore');
        });
        Route::prefix('fileCategory')->group(function () {
            Route::get('index', 'admin\filecategorycontroller@index')->name('filecategory.index');
            Route::post('store', 'admin\filecategorycontroller@store')->name('filecategory.store');
            Route::post('update', 'admin\filecategorycontroller@update')->name('filecategory.update');
            Route::get('show/{id}', 'admin\filecategorycontroller@show')->name('filecategory.show');
            Route::get('edit', 'admin\filecategorycontroller@edit')->name('filecategory.edit');
            Route::get('delete/{id}', 'admin\filecategorycontroller@destroy')->name('filecategory.delete');
        });

        Route::prefix('contractors')->group(function () {
            Route::get('index', 'admin\contractorsController@index')->name('contractors.index');
            Route::get('create', 'admin\contractorsController@create')->name('contractors.create');
            Route::get('/', 'admin\contractorsController@index')->name('contractors.index');
            Route::post('store', 'admin\contractorsController@store')->name('contractors.store');
            Route::post('update', 'admin\contractorsController@update')->name('contractors.update');
            Route::get('show/{id}', 'admin\contractorsController@show')->name('contractors.show');
            Route::get('edit', 'admin\contractorsController@edit')->name('contractors.edit');
            Route::get('profile', 'admin\contractorsController@profile')->name('contractors.profile');
            Route::get('delete/{id}', 'admin\contractorsController@destroy')->name('contractors.delete');

            Route::get('/deleted', 'admin\contractorsController@contractorsDeleted')->name('contractors.Deleted');
            Route::get('restore/{id}', 'admin\contractorsController@contractorsRestore')->name('contractors.restore');
        });


        Route::prefix("PEAs")->group(function () {
            Route::get('index', 'admin\PEAsController@index')->name("PEAs.index");
            Route::get('create', 'admin\PEAsController@create')->name('PEAs.create');
            Route::get('/', 'admin\PEAsController@index')->name('PEAs.index');
            Route::post('store', 'admin\PEAsController@store')->name('PEAs.store');
            Route::post('update', 'admin\PEAsController@update')->name('PEAs.update');
            Route::get('show/{id}', 'admin\PEAsController@show')->name('PEAs.show');
            Route::get('edit', 'admin\PEAsController@edit')->name('PEAs.edit');
            Route::get('editContactInfo', 'admin\PEAsController@editContactInfoDAta')->name('PEAs.editContactInfo');
            Route::get('delete/{id}', 'admin\PEAsController@destroy')->name('PEAs.delete');

            Route::get('/deleted', 'admin\PEAsController@PEAsDeleted')->name('PEAs.Deleted');
            Route::get('restore/{id}', 'admin\PEAsController@PEAsRestore')->name('PEAs.Restore');
        });


        Route::prefix('contract')->group(function () {
            Route::get('index', 'admin\contractsController@index')->name('contract.index');
            Route::get('create', 'admin\contractsController@create')->name('contract.create');

            Route::get('/', 'admin\contractsController@index')->name('contract.index');
            Route::get('/getActiveProjectMeasures', 'admin\contractsController@getActiveProjectMeasures')->name('contract.getActiveProjectMeasures');
            Route::post('store', 'admin\contractsController@store')->name('contract.store');
            Route::post('update', 'admin\contractsController@update')->name('contract.update');
            Route::get('show/{id}', 'admin\contractsController@show')->name('contract.show');
            Route::get('edit/{id}', 'admin\contractsController@edit')->name('contract.edit');
            Route::get('delete/{id}', 'admin\contractsController@destroy')->name('contract.delete');
            Route::get('contract/status/{id}/{status}', 'admin\contractsController@status')->name('contract.status');
            Route::get('create-project-budget', 'admin\contractsController@createProjectBudget')->name('contract.create.project.budget');
            Route::get('getActiveProjectMeasuresSelectedSubcategory', 'admin\contractsController@getActiveProjectMeasuresSelectedSubcategory')->name('contract.getActiveProjectMeasuresSelectedSubcategory');
            Route::get('/deleted', 'admin\contractsController@contractDeleted')->name('contract.contractDeleted');
            Route::get('restore/{id}', 'admin\contractsController@contractRestore')->name('contract.restore');
            Route::post('/contract/file/', 'admin\contractsController@contractFile')->name('contract.file.single');
            Route::post('/contract/file/create', 'admin\contractsController@contractFileCreate')->name('contract.file.single.create');
            Route::post('/contract/file/retain', 'admin\contractsController@contractFileRetain')->name('contract.file.retain.single');
            Route::post('/contract/file/retain/create', 'admin\contractsController@contractFileRetainCreate')->name('contract.file.retain.single.create');
            Route::get('file/delete', 'admin\contractsController@DeleteFile')->name('contract.file.delete');
            Route::get('file/delete/retain', 'admin\contractsController@DeleteFileRetain')->name('contract.file.retain.delete');

            Route::get('contract/checked/budget/project/measure/subcategory',
                'admin\contractsController@contractCheckedBudgetProjectMeasureSubcategory')
                ->name('contract.checked.budget.project.measure.subcategory');
            Route::get('contract/checked/budget/project/measure/subcategory/update',
                'admin\contractsController@ ')
                ->name('contract.checked.budget.project.measure.subcategory.update');
            Route::get('delete/contract/child/row', 'admin\contractsController@deleteContractChildRow')->name('delete.contract.child.row');

            Route::get('/contract/getFacilities', 'admin\contractsController@getFacilities')->name('contract.getFacilities');
        });

        Route::prefix('payment')->group(function () {
            Route::get('index', 'admin\paymentcontroller@index')->name('payment.index');
            Route::get('create', 'admin\paymentcontroller@create')->name('payment.create');
            Route::post('store', 'admin\paymentcontroller@store')->name('payment.store');


            Route::post('update/{id}', 'admin\paymentcontroller@update')->name('payment.update');
            Route::get('show/{id}', 'admin\paymentcontroller@show')->name('payment.show');
            Route::get('edit/{id}', 'admin\paymentcontroller@edit')->name('payment.edit');
            Route::get('delete/{id}', 'admin\paymentcontroller@destroy')->name('payment.delete');
            Route::get('get/contract', 'admin\paymentcontroller@getContract')->name('payment.get.contract');
            Route::get('status/{id}/{status}', 'admin\paymentcontroller@status')->name('payment.status');


            Route::get('/deleted', 'admin\paymentcontroller@paymentDeleted')->name('payment.paymentDeleted');
            Route::get('restore/{id}', 'admin\paymentcontroller@paymentRestore')->name('payment.restore');
            Route::get('contractors/contract', 'admin\paymentcontroller@GetContractorContracts')->name('payment.GetContractorContracts');
            Route::get('contract/details', 'admin\paymentcontroller@GetContractsDetails')->name('payment.GetContractsDetails');
            Route::get('contract/account/data', 'admin\paymentcontroller@GetAccountData')->name('payment.GetAccountData');
            Route::get('contract/projects', 'admin\paymentcontroller@getContractProjects')->name('payment.getContractProjects');
            Route::get('contract/projects/measure', 'admin\paymentcontroller@GetProjectMeasure')->name('payment.GetProjectMeasure');

            Route::get('contract/projects/measure/subcategory', 'admin\paymentcontroller@GetProjectMeasureSubcategory')->name('payment.GetProjectMeasureSubcategory');
            Route::get('calculate/budget', 'admin\paymentcontroller@calculateBudget')->name('payment.calculate.budget');

//files/
            Route::post('/supporting/documents/file', 'admin\paymentcontroller@supportingDocumentsFileSave')->name('supporting.documents.file.save');
            Route::get('supporting/documents/file', 'admin\paymentcontroller@supportingDocumentsFileDelete')->name('supporting.documents.file.delete');

            Route::post('payment/supporting/documents/file', 'admin\paymentcontroller@paymentSupportingDocumentsFileSave')->name('payment.supporting.documents.file.save');
            Route::get('payment/supporting/documents/file', 'admin\paymentcontroller@paymentSupportingDocumentsFileDelete')->name('payment.supporting.documents.file.delete');

            Route::post('payment/getFacilities', 'admin\paymentcontroller@getFacilities')->name('payment.getFacilities');

        });


        Route::prefix('user')->group(function () {
            Route::get('/', 'admin\userController@index')->name('user.index');
            Route::get('create', 'admin\userController@create')->name('user.create');
            Route::post('store', 'admin\userController@store')->name('user.store');
            Route::get('profile/{id}', 'admin\userController@profile')->name('user.profile');
            Route::get('edit/{id}', 'admin\userController@edit')->name('user.edit');
            Route::get('status/{id}', 'admin\userController@status')->name('user.status');
            Route::get('delete/{id}', 'admin\userController@destroy')->name('user.delete');
            Route::PATCH('update/{id}', 'admin\userController@update')->name('user.update');
        });


    });


    Route::get('/home', 'admin\adminController@index')->name('home');
    Route::resource('roles', 'RoleController');

//    Route::resource('users', 'UserController');


    Route::prefix('project')->group(function () {
        Route::get('/', 'admin\projectController@index')->name('project.index');
        Route::get('create', 'admin\projectController@create')->name('project.create');
        Route::get('/', 'admin\projectController@index')->name('project.index');
        Route::post('store', 'admin\projectController@store')->name('project.store');
        Route::post('update', 'admin\projectController@update')->name('project.update');
              Route::get('show/{id}/{flagScroll?}', 'admin\projectController@show')->name('project.show');

        Route::get('edit/{id}', 'admin\projectController@edit')->name('project.edit');
        Route::Post('delete/{id}', 'admin\projectController@destroy')->name('project.delete');

       Route::get('/deleted', 'admin\projectController@projectDeleted')->name('project.projectDeleted');
        Route::get('restore/{id}', 'admin\projectController@projectRestore')->name('project.restore');

        Route::get('project/status/{id}/{status}', 'admin\projectController@status')->name('project.status');
                Route::get('selected/province', 'admin\projectController@selectedProvince')->name('project.selected.province');
                        Route::post('/project/file/', 'admin\projectController@projectFile')->name('project.file.single');
        Route::get('delete/file/', 'admin\projectController@destroyFile')->name('project.delete.file');


        Route::get('delete/child/{projecthasprovince}', 'admin\projectController@deleteProjectChildRow');

         ///revised project route
        Route::get('revised-budget/{project_id?}/{line_reference_id?}', 'admin\projectController@revisedBudget')->name('project.revisedBudget');
        Route::Post('revised-budget/store', 'admin\projectController@revisedBudgetStore')->name('project.revisedBudgetStore');
        Route::get('revised-budget/status/{id}/{status}', 'admin\projectController@revisedbudgetstatus')->name('project.revisedBudgetStatus');

//project history
        Route::get('history/list/{historyList}', 'admin\projectController@historyList')->name('project.history.list');
        Route::get('history/show/{id}/{project_id}', 'admin\projectController@historyShow')->name('project.history.show');


//        project check bugdet value
        Route::get('history/list/{historyList}', 'admin\projectController@historyList')->name('project.history.list');
        Route::get('check/defining/budget/contract', 'admin\projectController@CheckDefiningBudgetContract')->name('project.check.defining.budget.contract');


        Route::get('check/project-title-exists', 'admin\projectController@checkProjectTitle')->name('project.check.projectTitle');



    });

});
