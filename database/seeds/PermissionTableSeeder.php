<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;


class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',


            'city-list',
            'city-create',
            'city-edit',
            'city-delete',

            'exchange-rate-list',
            'exchange-rate-create',
            'exchange-rate-edit',
            'exchange-rate-delete',

            'currency-list',
            'currency-create',
            'currency-edit',
            'currency-delete',

            'payment-method-list',
            'payment-method-create',
            'payment-method-edit',
            'payment-method-delete',


            'sub-categories-list',
            'sub-categories-create',
            'sub-categories-edit',
            'sub-categories-delete',

            'measures-list',
            'measures-create',
            'measures-edit',
            'measures-delete',

            'contractors-list',
            'contractors-create',
            'contractors-edit',
            'contractors-delete',
            'contractors-profile',

            'project-execution-agency-list',
            'project-execution-agency-create',
            'project-execution-agency-edit',
            'project-execution-agency-delete',


            'province-region-list',
            'province-region-create',
            'province-region-edit',
            'province-region-delete',

            'payments-list',
            'payments-create',
            'payments-edit',
            'payments-delete',

            'projects-list',
            'projects-create',
            'projects-edit',
            'projects-delete',
            'projects-status',

            'contracts-list',
            'contracts-create',
            'contracts-edit',
            'contracts-delete',

            'users-list',
            'users-create',
            'users-edit',
            'users-delete',
            'users-status',

        ];


        foreach ($permissions as $permission) {

            Permission::create(['name' => $permission]);
        }
    }
}