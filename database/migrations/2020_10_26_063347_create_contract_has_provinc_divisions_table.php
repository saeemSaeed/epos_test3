<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractHasProvincDivisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_has_provinc_divisions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('contract_id');
            $table->string('sub_category_id')->nullable();
            $table->string('province_id');
            $table->string('pea_id')->nullable();
            $table->string('city_id')->nullable();
            $table->double('budget');
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_has_provinc_divisions');
    }
}
