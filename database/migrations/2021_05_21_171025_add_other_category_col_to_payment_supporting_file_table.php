<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOtherCategoryColToPaymentSupportingFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_supporting_file', function (Blueprint $table) {
            $table->string('other_category')->after('description')->nullable();
        });

        Schema::table('payment_supporting_file_detial', function (Blueprint $table) {
            $table->string('other_category')->after('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_supporting_file', function (Blueprint $table) {
            $table->dropColumn('other_category');
        });

        Schema::table('payment_supporting_file_detial', function (Blueprint $table) {
            $table->dropColumn('other_category');
        });
    }
}
