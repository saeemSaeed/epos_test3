<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFacilityIdToContractHasProvincDivisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contract_has_provinc_divisions', function (Blueprint $table) {

            $table->unsignedBigInteger('facility_id')
                  ->after('city_id')
                  ->nullable();

            $table->foreign('facility_id')
                  ->references('facilities')
                  ->on('id')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract_has_provinc_divisions', function (Blueprint $table) {
            $table->dropColumn('facility_id');
        });
    }
}
