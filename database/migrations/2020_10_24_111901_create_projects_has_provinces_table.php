<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsHasProvincesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects_has_provinces', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('project_id')->index();
            $table->string('province_id')->nullable();
            $table->string('measure_id');
            $table->string('sub_category_id')->nullable();
            $table->double('budget');
            $table->string('pea_id')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects_has_provinces');
    }
}
