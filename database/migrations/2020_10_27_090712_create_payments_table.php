<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('contract_id');
            $table->string('contract_title');
            $table->double('contract_amount');
            $table->double('payment_exchange_rate');
            $table->string('currency_id')->nullable();
            $table->string('contractPaymentMethod')->nullable();
            $table->string('contractMeasures')->nullable();
            $table->date('payment_date')->nullable();
            $table->date('payment_description')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
