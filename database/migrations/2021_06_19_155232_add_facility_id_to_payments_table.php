<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFacilityIdToPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_has_project_measure', function (Blueprint $table) {
            $table->unsignedBigInteger('facility_id')
                  ->after('city_id')
                  ->nullable();

            $table->foreign('facility_id')
                  ->references('facilities')
                  ->on('id')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_has_project_measure', function (Blueprint $table) {
            $table->dropColumn('facility_id');
        });
    }
}
