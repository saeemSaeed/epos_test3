-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 13, 2021 at 05:56 AM
-- Server version: 8.0.21
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `epos_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `sort_order` int NOT NULL,
  `city_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_code` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=222 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `sort_order`, `city_name`, `short_code`, `province_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Islamabad', 'ISB', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 2, 'Lahore', 'LHE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 3, 'Karachi', 'KHI', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(5, 5, 'Peshawar', 'PSC', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(6, 6, 'Muzafarabad', 'MFG', '12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(7, 7, 'Quetta', 'UET', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(8, 8, 'Gilgit', 'GIL', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(11, 9, 'Faisalabad', 'FSD', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 10, 'Abbotabad', 'ABT', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(13, 11, 'DIK', 'DIK', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(14, 4, 'Skardu', 'KDU', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(15, 12, 'Mirpur', 'MPR', '12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(16, 13, 'kpk', 'LRC', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(17, 99, 'Patokee', 'PTO', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 14, 'Bahawalpur', 'BHV', '1', NULL, NULL, NULL),
(20, 15, 'Bhalwal', 'BWL', '1', NULL, NULL, NULL),
(21, 16, 'DeraGhaziKhan', 'DEA', '1', NULL, NULL, NULL),
(22, 17, 'Faisalabad', 'FSD', '1', NULL, NULL, NULL),
(23, 18, 'Gadoon', 'GAD', '1', NULL, NULL, NULL),
(24, 19, 'GujarKhan', 'GKN', '1', NULL, NULL, NULL),
(25, 20, 'Gujranwala', 'GRW', '1', NULL, NULL, NULL),
(26, 21, 'Gujrat', 'GRT', '1', NULL, NULL, NULL),
(27, 22, 'Hasilpur', 'HAS', '1', NULL, NULL, NULL),
(28, 23, 'Hassanabdal', 'HSN', '1', NULL, NULL, NULL),
(29, 24, 'Havelian', 'HVN', '1', NULL, NULL, NULL),
(30, 25, 'Islamabad', 'ISB', '1', NULL, NULL, NULL),
(31, 26, 'Jhelum', 'JMR', '1', NULL, NULL, NULL),
(32, 27, 'LahoreCantt', 'LHE', '1', NULL, NULL, NULL),
(33, 28, 'LakkiMarwat', 'LKM', '1', NULL, NULL, NULL),
(34, 29, 'LalaMosa', 'LLM', '1', NULL, NULL, NULL),
(35, 30, 'Landhi', 'LND', '1', NULL, NULL, NULL),
(36, 31, 'Larkana', 'LRK', '1', NULL, NULL, NULL),
(37, 32, 'Lasbela', 'LSB', '1', NULL, NULL, NULL),
(38, 33, 'Lodhran', 'LON', '1', NULL, NULL, NULL),
(39, 34, 'LoraLai', 'LRG', '1', NULL, NULL, NULL),
(40, 35, 'Malakand', 'MKD', '1', NULL, NULL, NULL),
(41, 36, 'MandiBahauddin', 'MBD', '1', NULL, NULL, NULL),
(42, 37, 'MandiBurewala', 'MBW', '1', NULL, NULL, NULL),
(43, 38, 'Mangla', 'MAN', '1', NULL, NULL, NULL),
(44, 39, 'Mansehra', 'HRA', '1', NULL, NULL, NULL),
(45, 40, 'Mardan', 'MDX', '1', NULL, NULL, NULL),
(46, 41, 'Matli', 'MLI', '1', NULL, NULL, NULL),
(47, 42, 'Mehrabpur', 'MHR', '1', NULL, NULL, NULL),
(48, 43, 'MianChannu', 'MYH', '1', NULL, NULL, NULL),
(49, 44, 'Mianwali', 'MWD', '1', NULL, NULL, NULL),
(50, 45, 'Mingaora', 'MNG', '1', NULL, NULL, NULL),
(51, 46, 'Mogulpura/Lahore', 'MGP', '1', NULL, NULL, NULL),
(52, 47, 'MultanCantt', 'MXC', '1', NULL, NULL, NULL),
(53, 48, 'MultanCity', 'MUX', '1', NULL, NULL, NULL),
(54, 49, 'Multan', 'MUL', '1', NULL, NULL, NULL),
(55, 50, 'Muridke', 'MDK', '1', NULL, NULL, NULL),
(56, 51, 'Muzaffargarh', 'NNS', '1', NULL, NULL, NULL),
(57, 52, 'NankanaSahib', 'NRW', '1', NULL, NULL, NULL),
(58, 53, 'Narowal', 'WNS', '1', NULL, NULL, NULL),
(59, 54, 'Qasimabad', 'QSM', '1', NULL, NULL, NULL),
(60, 55, 'QilaSheikhupura', 'QSP', '1', NULL, NULL, NULL),
(61, 56, 'RahimYarKhan', 'RYK', '1', NULL, NULL, NULL),
(62, 57, 'Raiwind\r\n', 'RND', '1', NULL, NULL, NULL),
(63, 58, 'Rajanpur', 'RJP', '1', NULL, NULL, NULL),
(64, 59, 'Rawalpindi', 'RWP', '1', NULL, NULL, NULL),
(65, 60, 'Sialkot', 'SKT', '1', NULL, NULL, NULL),
(66, 61, 'Sibi', 'SBQ', '1', NULL, NULL, NULL),
(67, 62, 'Talagang', 'TLG', '1', NULL, NULL, NULL),
(68, 63, 'TandoAdam', 'TNA', '1', NULL, NULL, NULL),
(69, 64, 'TandoAllahyar', 'TDA', '1', NULL, NULL, NULL),
(70, 65, 'Wagah', 'WGH', '1', NULL, NULL, NULL),
(71, 66, 'Wah', 'WAH', '1', NULL, NULL, NULL),
(72, 67, 'Wana', 'WAF', '1', NULL, NULL, NULL),
(73, 68, 'Wazirabad', 'WZA', '1', NULL, NULL, NULL),
(74, 69, 'Chaman', 'CMN', '2', NULL, NULL, NULL),
(75, 70, 'ChangaManga', 'CGM', '2', NULL, NULL, NULL),
(76, 71, 'Charsadda', 'CSD', '2', NULL, NULL, NULL),
(77, 72, 'Chichawatni', 'CCE', '2', NULL, NULL, NULL),
(78, 73, 'ChichokiMallian', 'CCM', '2', NULL, NULL, NULL),
(79, 74, 'Chilas\r\n', 'CHB', '2', NULL, NULL, NULL),
(80, 75, 'Chishtian', 'CSI', '2', NULL, NULL, NULL),
(81, 76, 'Dalbandin', 'DBA', '2', NULL, NULL, NULL),
(82, 77, 'Daska', 'DKA', '2', NULL, NULL, NULL),
(83, 78, 'Gwadar', 'GWD', '2', NULL, NULL, NULL),
(84, 79, 'Hafizabad', 'HFD', '2', NULL, NULL, NULL),
(85, 80, 'HaripurHazara', 'HRU', '2', NULL, NULL, NULL),
(86, 81, 'Haripur', 'HPR', '2', NULL, NULL, NULL),
(87, 82, 'Jiwani', 'JIW', '2', NULL, NULL, NULL),
(88, 83, 'Jungshahi', 'JGS', '2', NULL, NULL, NULL),
(89, 84, 'Kabirwala', 'KBW', '2', NULL, NULL, NULL),
(90, 85, 'Kadanwari', 'KCF', '2', NULL, NULL, NULL),
(91, 86, 'Kalat', 'KBH', '2', NULL, NULL, NULL),
(92, 87, 'Kamalia', 'KML', '2', NULL, NULL, NULL),
(93, 88, 'Kambar', 'KMB', '2', NULL, NULL, NULL),
(94, 89, 'Khuzdar', 'KDD', '2', NULL, NULL, NULL),
(95, 90, 'NokKundi', 'NSR', '2', NULL, NULL, NULL),
(96, 91, 'Nowshera', 'NHS', '2', NULL, NULL, NULL),
(97, 92, 'Nushki', 'OKC', '2', NULL, NULL, NULL),
(98, 93, 'OkaraCantt', 'OKR', '2', NULL, NULL, NULL),
(99, 94, 'Okara', 'OKR', '2', NULL, NULL, NULL),
(100, 95, 'Ormara', 'ORW', '2', NULL, NULL, NULL),
(101, 96, 'Pabbi', 'PBI', '2', NULL, NULL, NULL),
(102, 97, 'Panjgur', 'PJG', '2', NULL, NULL, NULL),
(103, 98, 'PanoAqil', 'PNL', '2', NULL, NULL, NULL),
(104, 217, 'ParaChinar', 'PAJ', '2', NULL, NULL, NULL),
(105, 100, 'Pasni', 'PSI', '2', NULL, NULL, NULL),
(106, 101, 'Pattoki', 'PTO', '2', NULL, NULL, NULL),
(107, 102, 'Quetta', 'UET', '2', NULL, NULL, NULL),
(108, 103, 'ShamsiAirfield', 'SHI', '2', NULL, NULL, NULL),
(109, 104, 'Sheikhupura', 'SKP', '2', NULL, NULL, NULL),
(110, 105, 'Shikarpur', 'SWV', '2', NULL, NULL, NULL),
(111, 106, 'ShorkotCantt', 'SRK', '2', NULL, NULL, NULL),
(112, 107, 'Shorkot', 'SKO', '2', NULL, NULL, NULL),
(113, 108, 'Sui', 'SUL', '2', NULL, NULL, NULL),
(114, 109, 'Taftan', 'TFT', '2', NULL, NULL, NULL),
(115, 110, 'Turbat', 'TUK', '2', NULL, NULL, NULL),
(116, 111, 'Vehari', 'VHR', '2', NULL, NULL, NULL),
(117, 112, 'Zhob', 'PZH', '2', NULL, NULL, NULL),
(118, 113, 'Ziarat', 'ZRT', '2', NULL, NULL, NULL),
(119, 114, 'Gilgit', 'GLT', '3', NULL, NULL, NULL),
(120, 115, 'Bannu', 'BNP', '4', NULL, NULL, NULL),
(121, 116, 'BhaiPheru', 'BPR', '4', NULL, NULL, NULL),
(122, 117, 'Bhakkar', 'BKR', '4', NULL, NULL, NULL),
(123, 118, 'Chitral', 'CJL', '4', NULL, NULL, NULL),
(124, 119, 'DeraIsmailKhan', 'DSK', '4', NULL, NULL, NULL),
(125, 120, 'Dir', 'DIR', '4', NULL, NULL, NULL),
(126, 121, 'Faizabad', 'FZB', '4', NULL, NULL, NULL),
(127, 122, 'GarhiHabibullahKhan', 'GHB', '4', NULL, NULL, NULL),
(128, 123, 'Ghakhar', 'GHK', '4', NULL, NULL, NULL),
(129, 124, 'Hayatabad', 'HAY', '4', NULL, NULL, NULL),
(130, 125, 'Hub', 'HUB', '4', NULL, NULL, NULL),
(131, 126, 'Khwazakhela', 'KHW', '4', NULL, NULL, NULL),
(132, 127, 'KohatCantt', 'OHT', '4', NULL, NULL, NULL),
(133, 128, 'Kohat', 'KHC', '4', NULL, NULL, NULL),
(134, 129, 'Kohlu', 'KLU', '4', NULL, NULL, NULL),
(135, 130, 'KotAddu', 'ADK', '4', NULL, NULL, NULL),
(136, 131, 'KotLakhpat', 'LPK', '4', NULL, NULL, NULL),
(137, 132, 'Kotli', 'KOL', '4', NULL, NULL, NULL),
(138, 133, 'Kotri', 'KOT', '4', NULL, NULL, NULL),
(139, 134, 'PeshawarCantt', 'PSH', '4', NULL, NULL, NULL),
(140, 135, 'PeshawarCity', 'PEW', '4', NULL, NULL, NULL),
(141, 136, 'Poonch', 'PON', '4', NULL, NULL, NULL),
(142, 137, 'SaiduSharif', 'SDT', '4', NULL, NULL, NULL),
(143, 138, 'Samasata', 'SMA', '4', NULL, NULL, NULL),
(144, 139, 'Sambrial', 'SMB', '4', NULL, NULL, NULL),
(145, 140, 'Sammundri', 'SMD', '4', NULL, NULL, NULL),
(146, 141, 'Sanghar', 'SNG', '4', NULL, NULL, NULL),
(147, 142, 'SanglaHill', 'SLL', '4', NULL, NULL, NULL),
(148, 143, 'Sargodha', 'SGI', '4', NULL, NULL, NULL),
(149, 144, 'SaviRagha', 'ZEO', '4', NULL, NULL, NULL),
(150, 145, 'Swat', 'SWT', '4', NULL, NULL, NULL),
(151, 146, 'Tarbela', 'TLB', '4', NULL, NULL, NULL),
(152, 147, 'Taxilla', 'TXA', '4', NULL, NULL, NULL),
(153, 148, 'Thatta', 'THT', '4', NULL, NULL, NULL),
(154, 149, 'Timargara', 'TMG', '4', NULL, NULL, NULL),
(155, 150, 'TobaTekSingh', 'TTS', '4', NULL, NULL, NULL),
(156, 151, 'Torkham', 'TOR', '4', NULL, NULL, NULL),
(157, 152, 'Warsak', 'WSK', '4', NULL, NULL, NULL),
(158, 153, 'Dadu', 'DDU', '6', NULL, NULL, NULL),
(159, 154, 'Daharki', 'DRK', '6', NULL, NULL, NULL),
(160, 155, 'Ghotki', 'GHT', '6', NULL, NULL, NULL),
(161, 156, 'Hyderabad', 'HDD', '6', NULL, NULL, NULL),
(162, 157, 'Jacobabad', 'JAG', '6', NULL, NULL, NULL),
(163, 158, 'Jalalpur', 'JLP', '6', NULL, NULL, NULL),
(164, 159, 'JamShoro', 'JAM', '6', NULL, NULL, NULL),
(165, 160, 'Jamrud', 'JRD', '6', NULL, NULL, NULL),
(166, 161, 'Jaranwala', 'JNW', '6', NULL, NULL, NULL),
(167, 162, 'JhalMagsi', 'JLM', '6', NULL, NULL, NULL),
(168, 163, 'JhangCity', 'JHC', '6', NULL, NULL, NULL),
(169, 164, 'Jhang', 'JNG', '6', NULL, NULL, NULL),
(170, 165, 'KarachiCity', 'KZL', '6', NULL, NULL, NULL),
(171, 166, 'Karachi', 'KHI', '6', NULL, NULL, NULL),
(172, 167, 'Kashmor', 'KZL', '6', NULL, NULL, NULL),
(173, 168, 'Kasur', 'KUS', '6', NULL, NULL, NULL),
(174, 169, 'KetiBandar', 'KBA', '6', NULL, NULL, NULL),
(175, 170, 'KetiBunder', 'KBU', '6', NULL, NULL, NULL),
(176, 171, 'Khairpur', 'KHP', '6', NULL, NULL, NULL),
(177, 172, 'Khanewal', 'KWL', '6', NULL, NULL, NULL),
(178, 173, 'Khanpur', 'KPR', '6', NULL, NULL, NULL),
(179, 174, 'Kharian', 'QKH', '6', NULL, NULL, NULL),
(180, 175, 'Khewra', 'KWA', '6', NULL, NULL, NULL),
(181, 176, 'Khipro', 'KPO', '6', NULL, NULL, NULL),
(182, 177, 'Khokhropar', 'KRB', '6', NULL, NULL, NULL),
(183, 178, 'Khushab', 'KHB', '6', NULL, NULL, NULL),
(184, 179, 'Kiamari', 'KIA', '6', NULL, NULL, NULL),
(185, 180, 'MirpurKhas', 'MPD', '6', NULL, NULL, NULL),
(186, 181, 'MirpurMathelo', 'MRP', '6', NULL, NULL, NULL),
(187, 182, 'Mohenjodaro', 'MJD', '6', NULL, NULL, NULL),
(188, 183, 'Moro', 'MOR', '6', NULL, NULL, NULL),
(189, 184, 'MuhammadBinQasim', 'BQM', '6', NULL, NULL, NULL),
(190, 185, 'Nawabshah', 'NZB', '6', NULL, NULL, NULL),
(191, 186, 'Nazimabad', 'NDD', '6', NULL, NULL, NULL),
(192, 187, 'Sawan', 'SAW', '6', NULL, NULL, NULL),
(193, 188, 'SehwenSharif', 'SYW', '6', NULL, NULL, NULL),
(194, 189, 'SeraiAlamgir', 'SXG', '6', NULL, NULL, NULL),
(195, 190, 'Shabqadar', 'SQR', '6', NULL, NULL, NULL),
(196, 191, 'Shahdadkot', 'SHK', '6', NULL, NULL, NULL),
(197, 192, 'Shahdadpur', 'SDU', '6', NULL, NULL, NULL),
(198, 193, 'ShahdaraBagh', 'SDR', '6', NULL, NULL, NULL),
(199, 194, 'Sukkur', 'SKZ', '6', NULL, NULL, NULL),
(200, 195, 'Swabi', 'SWB', '6', NULL, NULL, NULL),
(201, 196, 'Abbottabad', 'ABT', '12', NULL, NULL, NULL),
(202, 197, 'Ahmadpur', 'ADP', '12', NULL, NULL, NULL),
(203, 198, 'Arifwala', 'ARF', '12', NULL, NULL, NULL),
(204, 199, 'AttockCity', 'ATG', '12', NULL, NULL, NULL),
(205, 200, 'Attock', 'ATY', '12', NULL, NULL, NULL),
(206, 201, 'Badin', 'BDN', '12', NULL, NULL, NULL),
(207, 202, 'Bagh', 'BAG', '12', NULL, NULL, NULL),
(208, 203, 'Bahawalnagar', 'WGB', '12', NULL, NULL, NULL),
(209, 204, 'Bhimber', 'BHM', '12', NULL, NULL, NULL),
(210, 205, 'Bhurban', 'BHC', '12', NULL, NULL, NULL),
(211, 206, 'Burewala', 'BRW', '12', NULL, NULL, NULL),
(212, 207, 'Campbellpore', 'CWP', '12', NULL, NULL, NULL),
(213, 208, 'Chakwal', 'CKW', '12', NULL, NULL, NULL),
(214, 209, 'Muzaffarabad', 'MZJ', '12', NULL, NULL, NULL),
(215, 210, 'RawalaKot', 'RAZ', '12', NULL, NULL, NULL),
(216, 211, 'RekoDiq', 'REQ', '12', NULL, NULL, NULL),
(217, 212, 'RisalpurCantt', 'RCS', '12', NULL, NULL, NULL),
(218, 213, 'Rohri', 'ROH', '12', NULL, NULL, NULL),
(219, 214, 'Sadikabad', 'SDK', '12', NULL, NULL, NULL),
(220, 215, 'Sahiwal', 'SWN', '12', NULL, NULL, NULL),
(221, 216, 'Skardu', 'KDU', '12', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contractors`
--

DROP TABLE IF EXISTS `contractors`;
CREATE TABLE IF NOT EXISTS `contractors` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `sort_order` int NOT NULL,
  `contractor_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `contractor_ntn` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contractor_city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `contractor_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `contractor_landline` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contractor_details` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contractors_contractor_name_index` (`contractor_name`(250))
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contractors`
--

INSERT INTO `contractors` (`id`, `sort_order`, `contractor_name`, `contractor_ntn`, `contractor_city`, `contractor_address`, `contractor_landline`, `contractor_details`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Geo Technical Associates', 'Geo Technical Associates', '2', 'Dhok Mustaqeem Road, Opposite Shalbandi Drabar, Pirwadhai Morr, Peshawar Road, Rawalpindi', '+92-151-54-64-601', NULL, '2020-11-05 17:04:17', '2021-01-26 20:16:24', NULL),
(2, 2, 'Abbot Laboratories Pak Ltd', 'Abbot Laboratories Pak Ltd', '3', 'Abbott Laboratories (PAK) Ltd, [Diagnostic Division] Opp. Radio Pakistan Transmission Centre, Hyderabad Road, Landhi, Karachi', '+92-211-122-268-81', NULL, '2020-11-05 17:05:12', '2020-11-12 14:18:10', NULL),
(3, 3, 'Sind Medical Stores', 'Sind Medical Stores', '3', 'Sind Medical Stores, 13-B, Block-6, P.E.C.H.S., Shahrah-e-Faisal, Karachi', '+92-34-520-035-37', NULL, '2020-11-05 17:06:13', '2020-11-05 17:17:22', NULL),
(4, 4, 'Interex Company', 'Interex Company', '3', 'Interex Company 195, Block 7/8, KMCHS, Justice Inamullah Road, Karachi 74800, Pakistan', '+92-34-533-873-4', NULL, '2020-11-05 17:07:04', '2020-11-05 17:17:34', NULL),
(5, 5, 'Hospicare System - Zaavia', '0368358-3', '3', 'Hospicare Systems - Zaavia, Ground Floor, Rabbiya Garden, Plot # 3, Block-3, M.C.H.S, Shaheed-e-Millat Road, Karachi', '+92-213-492-191-3', NULL, '2020-11-05 17:11:52', '2020-11-05 17:11:52', NULL),
(6, 6, 'Technology Links', '0944309-6', '3', 'Technology Links Pvt Ltd, 4/11-12, Rimpa Plaza, M.A. Jinnah Road, Karachi, Pakistan', '+92-213-273-426-061', NULL, '2020-11-05 17:12:29', '2020-11-05 17:12:29', NULL),
(7, 7, 'Megaplus Pakistan', NULL, '1', 'Megaplus, 211, Street 33, F-10/1, Islamabad,', '+92-512-111-526-29', NULL, '2020-11-05 17:13:19', '2020-11-05 17:13:19', NULL),
(9, 8, 'developer', 'developer', '1', 'kot lakh path lahore', NULL, NULL, '2020-11-09 12:29:10', '2020-11-09 12:41:34', NULL),
(10, 9, 'Pro-Med Enterprises JV Zodiac International', NULL, '1', 's CentrZodiac International, Suite # 482, St # 98, G-9/4, Islamabad. Pakistan', NULL, NULL, '2020-11-09 12:45:18', '2020-11-09 12:45:18', NULL),
(12, 10, 'M/s Railway Construction Co (RAILCOP)', '0657151-4', '1', 'Railcop House, Pakistan Railway Carriage Factory, Sector i-11/1,  P.O Box. 1922, Islamabad, Pakistan', '051- 92-781-31', 'Railway Construction Pakistan Limited', '2020-11-27 11:09:01', '2021-02-19 16:00:30', NULL),
(13, 11, 'M/S Capital Builders Islamabad', NULL, '1', '14, Ground Floor, Abu Dhabi Tower, Sector F - 11 Markaz, Islamabad.', '051- 21-1 3-744', NULL, '2020-11-30 11:37:00', '2020-11-30 11:37:00', NULL),
(14, 12, 'M/s Haji Shash & Sons, Quetta', NULL, '7', 'Second Floor, Falt No 4, Gillani complex, Kasi Road, Quetta, Balochistan', '+92- 82-3 6-10 -793', NULL, '2020-11-30 14:06:46', '2020-11-30 14:06:46', NULL),
(15, 13, 'M/s Bilal & Brothers', NULL, '14', '105/B, dar plaza, Nabi Bazar, Gilgit, Gilgit Baltistan', '058-11 -454- 52-7', NULL, '2020-11-30 14:44:37', '2021-04-12 10:50:15', NULL),
(16, 345, 'saeed', '123', '2', 'sdf', '234', 'sd', '2021-01-26 20:18:02', '2021-01-26 20:19:35', '2021-01-26 20:19:35'),
(17, 223112, 'SFDRB55yW3', 'noTTEou8rM', '5', 'c6XRDvZKPq', 'BaQ5IyeAD3', '32KocPHsaZ', '2021-04-12 10:51:30', '2021-04-12 10:51:30', NULL),
(18, 22311233, 'od93GzGeoc', '0ZcbFKumBI', '5', 'V3mLbHdlYr', 'RHlS2sEHYZ', '1GjUS145Qg', '2021-04-12 10:52:02', '2021-04-12 10:52:02', NULL),
(19, 11223355, 'P01zbmeY0g', '3byWQQQLHG', '6', 'AvXssaXD21', 'GLsSTLPQHz', 'pvjGpGdtEY', '2021-04-12 10:52:28', '2021-04-12 10:52:28', NULL),
(20, 65432, 'MUI5wlOWve', '9MKnryU0Eh', '14', 'SuZHq9oZQb', 'MlZXTHug0G', 'VXZSvU2rIr', '2021-04-12 10:53:57', '2021-04-12 10:53:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contractor_contact_information`
--

DROP TABLE IF EXISTS `contractor_contact_information`;
CREATE TABLE IF NOT EXISTS `contractor_contact_information` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `contractor_id` bigint UNSIGNED NOT NULL,
  `contact_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_designation` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_role` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_mobile_no` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_landline` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contractor_contact_information_contractor_id_index` (`contractor_id`),
  KEY `contractor_contact_information_contact_name_index` (`contact_name`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contractor_contact_information`
--

INSERT INTO `contractor_contact_information` (`id`, `contractor_id`, `contact_name`, `contact_designation`, `contact_role`, `contact_email`, `contact_mobile_no`, `contact_landline`, `created_at`, `updated_at`) VALUES
(19, 12, 'Arshad Salam Khattak', 'Managing Director', '', '', ' 0345 741 0015', ' 051 927 8136', '2021-02-19 16:00:30', '2021-02-19 16:00:30'),
(7, 13, 'Muhammad Iqbal', 'CEO', '', 'capitalbuilders@ymail.com', '0300 500 0670', '', '2020-11-30 11:37:00', '2020-11-30 11:37:00'),
(8, 13, 'Ehsan Ahmad Khokhar', 'G.M ( Projects )', '', 'G.M ( Projects )capitalbuilders@ymail.com', '0333 533 4602', '', '2020-11-30 11:37:00', '2020-11-30 11:37:00'),
(9, 14, 'Abdullah Kakar', 'Owner', '', '', '0321 801 5104', '', '2020-11-30 14:06:46', '2020-11-30 14:06:46'),
(26, 15, 'Muhammad Bilal', 'Owner', '', 'mdbilalbrothers@gmail.com', '0346 511 9057', '', '2021-04-12 10:50:15', '2021-04-12 10:50:15'),
(14, 1, 'hassan', 'software engineer', 'engineer', 'jkhads', '76575', '675', '2021-01-26 20:16:24', '2021-01-26 20:16:24'),
(27, 17, '3288490870', '1723832168', '6230789003', 'test@test.com', '6169560937', '9716021475', '2021-04-12 10:51:30', '2021-04-12 10:51:30'),
(28, 18, '4764072847', '1270389974', '4396322370', 'test@test.com', '6462145108', '9911401770', '2021-04-12 10:52:02', '2021-04-12 10:52:02'),
(29, 19, '2136764762', '0233693694', '7016630102', 'test@test.com', '6770431217', '6567470613', '2021-04-12 10:52:28', '2021-04-12 10:52:28'),
(30, 20, '8426993157', '3279970987', '6049323233', 'test@test.com', '9165549257', '7157145062', '2021-04-12 10:53:57', '2021-04-12 10:53:57');

-- --------------------------------------------------------

--
-- Table structure for table `contractor_financial_details`
--

DROP TABLE IF EXISTS `contractor_financial_details`;
CREATE TABLE IF NOT EXISTS `contractor_financial_details` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `contractor_id` bigint UNSIGNED NOT NULL,
  `bank_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_no` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iban` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` int DEFAULT NULL,
  `swift_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contractor_financial_details_contractor_id_index` (`contractor_id`),
  KEY `contractor_financial_details_bank_name_index` (`bank_name`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contractor_financial_details`
--

INSERT INTO `contractor_financial_details` (`id`, `contractor_id`, `bank_name`, `account_no`, `iban`, `branch_name`, `branch_code`, `city_id`, `swift_code`, `branch_address`, `created_at`, `updated_at`) VALUES
(13, 12, 'Meezan Bank Limited', '1801 0273 5232', 'PK86 MEZN 0008 1801 0273 5232', '', '', NULL, NULL, '21212', '2021-02-19 16:00:30', '2021-02-19 16:00:30'),
(6, 13, 'Bank Alfalah Limited', '5547 0050 0011 2498', 'PK08 ALFH 5547 0050 0011 2498', '', '', NULL, NULL, '2121', '2020-11-30 11:37:00', '2020-11-30 11:37:00'),
(20, 3, '21', '212', '121', '21', '212', 12, '21', '212121', '2021-04-12 10:50:15', '2021-04-12 10:50:15'),
(21, 3, 'Meezan Bank Limited', '0098 3301 0267 5376', 'PK65 MEZN 0098 3301 0267 5376', '', '', 12, '212', '', '2021-04-12 10:50:15', '2021-04-12 10:50:15'),
(22, 3, 'Habib Metropolitan Bank Limited', '0262 0271 4011 1379', 'PK57 MBPL 0262 0271 4011 1379', '', '', 15, '1', '', '2021-04-12 10:50:15', '2021-04-12 10:50:15'),
(23, 3, 'wb22cLuXjj', '124333', 'JjMFbOjRgP', '855ntWrLim', '0kVSegQcCh', 26, 'y9Vif6FQH1', 'XPPmWNZdEb', '2021-04-12 10:53:57', '2021-04-12 10:53:57');

-- --------------------------------------------------------

--
-- Table structure for table `contracts`
--

DROP TABLE IF EXISTS `contracts`;
CREATE TABLE IF NOT EXISTS `contracts` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `contract_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `contractor_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `orginal_amount` double DEFAULT NULL,
  `orginal_amount_pkr` double DEFAULT NULL,
  `amount_pkr` double DEFAULT NULL,
  `payment_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exchange_rate_id` int DEFAULT NULL,
  `contract_date` date NOT NULL,
  `contract_start_date` date DEFAULT NULL,
  `contract_end_date` date DEFAULT NULL,
  `planned_start_date` date DEFAULT NULL,
  `planned_end_date` date DEFAULT NULL,
  `contract_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `performance_security_file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ps_amount` double DEFAULT NULL,
  `ps_validity` date DEFAULT NULL,
  `ps_currency` int DEFAULT NULL,
  `instrument_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `performance_security_file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `contract_file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contracts`
--

INSERT INTO `contracts` (`id`, `contract_title`, `contractor_id`, `amount`, `orginal_amount`, `orginal_amount_pkr`, `amount_pkr`, `payment_id`, `currency_id`, `exchange_rate_id`, `contract_date`, `contract_start_date`, `contract_end_date`, `planned_start_date`, `planned_end_date`, `contract_description`, `performance_security_file`, `ps_amount`, `ps_validity`, `ps_currency`, `instrument_type`, `performance_security_file_name`, `status`, `created_at`, `updated_at`, `deleted_at`, `contract_file`) VALUES
(1, 'test1', '1', 7244.24645, 5123.24645, 632464.774253, 894302.224253, '1', '2', 24, '2121-03-31', NULL, NULL, '2121-04-01', '2121-04-22', 'testing exchange rate', NULL, 76.8007, '2121-04-06', 1, 'Bank Guarantee', NULL, 0, '2021-04-08 05:10:17', '2021-04-08 06:26:03', NULL, NULL),
(2, 'home one212w', '3', 2142.00002, 2142.00002, 925344.00864, 925344.00864, '2', '2', 31, '2021-05-01', '2021-01-21', '2021-05-22', '2021-05-01', '2021-05-02', 'e2khQSlZgH', NULL, 58589, '2021-02-23', 2, 'CBR', NULL, 0, '2021-04-12 05:09:34', '2021-04-12 05:09:34', NULL, NULL),
(3, 'home one212w22', '2', 2142.3685, 2142.3685, 44989.7385, 44989.7385, '2', '2', 26, '2021-05-01', '2021-01-21', '2021-05-22', '2021-05-01', '2021-05-02', 'f3NUMvkGuq', NULL, 58589, '2021-02-23', 2, 'CBR', NULL, 0, '2021-04-12 05:10:25', '2021-04-12 05:10:25', NULL, NULL),
(4, 'home one212w22', '3', 2121, 2121, 44541, 44541, '2', '2', 26, '2021-05-01', '2021-01-21', '2021-05-22', '2021-05-01', '2021-05-02', '0G6gwMzZ59', NULL, 58589, '2021-02-23', 2, 'CBR', NULL, 0, '2021-04-12 05:11:05', '2021-04-12 05:11:05', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contracts_has_files`
--

DROP TABLE IF EXISTS `contracts_has_files`;
CREATE TABLE IF NOT EXISTS `contracts_has_files` (
  `id` int NOT NULL AUTO_INCREMENT,
  `contract_id` int DEFAULT NULL,
  `file` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `category` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `contract_file_amount` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contracts_has_files`
--

INSERT INTO `contracts_has_files` (`id`, `contract_id`, `file`, `file_name`, `category`, `description`, `contract_file_amount`, `created_at`, `updated_at`) VALUES
(1, NULL, '1617970579.pdf', 'screencapture-epos-test3-purika-pk-admin-contract-create-2021-04-09-11_28_45 (1).pdf', NULL, '', 0, '2021-04-09 12:16:19', '2021-04-09 12:16:19'),
(2, 1, '1617971100.rar', 'GOPA.rar', 'select', '', 0, '2021-04-09 12:25:00', '2021-04-09 12:25:00'),
(3, 2, '1618204169.pdf', 'screencapture-epos-test3-purika-pk-admin-contract-create-2021-04-09-11_28_45 (1).pdf', 'Quotation', '2121', 0, '2021-04-12 05:09:29', '2021-04-12 05:09:34'),
(4, 3, '1618204191.pdf', 'screencapture-epos-test3-purika-pk-admin-contract-edit-2-2021-04-09-11_29_43 (1).pdf', 'Quotation', '2121', 4, '2021-04-12 05:09:51', '2021-04-12 05:10:25');

-- --------------------------------------------------------

--
-- Table structure for table `contracts_has_retain_files`
--

DROP TABLE IF EXISTS `contracts_has_retain_files`;
CREATE TABLE IF NOT EXISTS `contracts_has_retain_files` (
  `id` int NOT NULL AUTO_INCREMENT,
  `contract_id` int DEFAULT NULL,
  `file` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `retain_file_date` varchar(255) DEFAULT NULL,
  `retain_file_amount` double DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contracts_has_retain_files`
--

INSERT INTO `contracts_has_retain_files` (`id`, `contract_id`, `file`, `file_name`, `retain_file_date`, `retain_file_amount`, `description`, `created_at`, `updated_at`) VALUES
(1, 2, '1618204166.pdf', 'screencapture-epos-test3-purika-pk-admin-contract-edit-2-2021-04-09-11_29_43.pdf', '2021-03-30', 5000, '4EJuEbBEsz', '2021-04-12 05:09:26', '2021-04-12 05:09:34');

-- --------------------------------------------------------

--
-- Table structure for table `contracts_payment`
--

DROP TABLE IF EXISTS `contracts_payment`;
CREATE TABLE IF NOT EXISTS `contracts_payment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `contract_id` int NOT NULL,
  `schedule_date` date NOT NULL,
  `percentage` double NOT NULL,
  `amount` double NOT NULL,
  `amount_pkr` double DEFAULT NULL,
  `delete_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contracts_payment`
--

INSERT INTO `contracts_payment` (`id`, `contract_id`, `schedule_date`, `percentage`, `amount`, `amount_pkr`, `delete_at`, `created_at`, `updated_at`) VALUES
(1, 2, '2021-04-14', 50, 1071.00001, 462672.00432, NULL, '2021-04-12 05:09:34', '2021-04-12 05:09:34'),
(2, 2, '2021-03-10', 50, 1071.00001, 462672.00432, NULL, '2021-04-12 05:09:34', '2021-04-12 05:09:34'),
(3, 3, '2021-03-10', 100, 2142.3685, 44989.7385, NULL, '2021-04-12 05:10:25', '2021-04-12 05:10:25'),
(4, 4, '2021-03-24', 100, 2121, 44541, NULL, '2021-04-12 05:11:05', '2021-04-12 05:11:05');

-- --------------------------------------------------------

--
-- Table structure for table `contract_has_provinces`
--

DROP TABLE IF EXISTS `contract_has_provinces`;
CREATE TABLE IF NOT EXISTS `contract_has_provinces` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `contract_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `province_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contract_has_provinc_divisions`
--

DROP TABLE IF EXISTS `contract_has_provinc_divisions`;
CREATE TABLE IF NOT EXISTS `contract_has_provinc_divisions` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `contract_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `measure_id` int DEFAULT NULL,
  `project_id` int NOT NULL,
  `sub_category_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pea_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `budget` double NOT NULL,
  `budget_pkr` double DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contract_has_provinc_divisions`
--

INSERT INTO `contract_has_provinc_divisions` (`id`, `contract_id`, `measure_id`, `project_id`, `sub_category_id`, `province_id`, `pea_id`, `city_id`, `budget`, `budget_pkr`, `description`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1, '1', 11, 6, NULL, '2', '2', '77', 123.123, 15199.53435, '', '2021-04-08 05:10:17', '2021-04-08 05:42:55', '2021-04-08 05:42:55'),
(2, '1', 11, 6, NULL, '2', '2', '78', 5000.12345, 617265.239903, '', '2021-04-08 05:10:17', '2021-04-08 05:42:55', '2021-04-08 05:42:55'),
(3, '1', 11, 6, NULL, '2', '2', '77', 123.123, 15199.53435, '', '2021-04-08 05:42:55', '2021-04-08 06:26:03', '2021-04-08 06:26:03'),
(4, '1', 11, 6, NULL, '2', '2', '78', 5000.12345, 617265.239903, '', '2021-04-08 05:42:55', '2021-04-08 06:26:03', '2021-04-08 06:26:03'),
(5, '1', 11, 6, NULL, '2', '2', '77', 123.123, 15199.53435, '', '2021-04-08 06:26:03', NULL, '2021-04-08 06:26:03'),
(6, '1', 11, 6, NULL, '2', '2', '78', 5000.12345, 617265.239903, '', '2021-04-08 06:26:03', NULL, '2021-04-08 06:26:03'),
(7, '1', 12, 9, NULL, '4', '4', '127', 2121, 261837.45, '', '2021-04-08 06:26:03', NULL, '2021-04-08 06:26:03'),
(8, '2', 14, 6, NULL, '2', '2', '90', 21.00002, 9072.00864, '', '2021-04-12 05:09:34', NULL, '2021-04-12 05:09:34'),
(9, '2', 7, 9, NULL, '4', '4', '135', 2121, 916272, '', '2021-04-12 05:09:34', NULL, '2021-04-12 05:09:34'),
(10, '3', 14, 6, NULL, '2', '2', '83', 2121, 44541, '', '2021-04-12 05:10:25', NULL, '2021-04-12 05:10:25'),
(11, '3', 11, 6, NULL, '2', '2', '85', 21.3685, 448.7385, '', '2021-04-12 05:10:25', NULL, '2021-04-12 05:10:25'),
(12, '4', 17, 5, NULL, '12', '1', '208', 2121, 44541, '', '2021-04-12 05:11:05', NULL, '2021-04-12 05:11:05');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
CREATE TABLE IF NOT EXISTS `currencies` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `sort_order` int NOT NULL,
  `currency_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_symbol` varchar(255) CHARACTER SET utf32 COLLATE utf32_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `sort_order`, `currency_name`, `currency_code`, `currency_symbol`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Euro', 'EUR', '€', '2020-10-22 06:58:33', '2021-01-26 20:32:33', NULL),
(2, 2, 'Pakistani Rupee', 'PKR', 'PKR', '2020-10-22 06:59:17', '2020-10-26 04:33:53', NULL),
(3, 3, 'canadain', 'CAD', '$', '2020-10-26 04:31:16', '2021-04-08 07:17:36', NULL),
(4, 4, 'Dollar', 'USD', '$', '2020-10-28 05:35:14', '2021-04-08 07:16:08', NULL),
(8, 6, 'Taka', 'TK', '元', '2020-11-12 12:17:52', '2021-04-08 07:17:15', NULL),
(10, 7, 'Indian Rupees', 'IND', '$', '2021-01-26 20:33:21', '2021-01-26 20:33:51', '2021-01-26 20:33:51');

-- --------------------------------------------------------

--
-- Table structure for table `exchange_rates`
--

DROP TABLE IF EXISTS `exchange_rates`;
CREATE TABLE IF NOT EXISTS `exchange_rates` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `sort_order` int NOT NULL,
  `exchange_rate` double NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `currency_from` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_to` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exchange_rates`
--

INSERT INTO `exchange_rates` (`id`, `sort_order`, `exchange_rate`, `start_date`, `end_date`, `currency_from`, `currency_to`, `created_at`, `updated_at`, `deleted_at`) VALUES
(35, 43433, 32, '2021-04-10', '2021-04-16', '4', '2', '2021-04-10 04:26:27', '2021-04-10 04:26:27', NULL),
(34, 2212, 313, '2021-03-30', '2021-03-31', '1', '3', '2021-04-09 05:42:21', '2021-04-09 05:42:21', NULL),
(33, 34, 32, '2021-04-14', '2021-04-17', '1', '4', '2021-04-09 05:41:58', '2021-04-09 05:41:58', NULL),
(32, 568, 87, '2021-04-01', '2021-04-21', '3', '8', '2021-04-09 05:40:51', '2021-04-09 05:40:51', NULL),
(31, 43432, 432, '2021-04-05', '2021-04-14', '1', '2', '2021-04-09 05:40:18', '2021-04-09 05:40:18', NULL),
(30, 4343, 432, '2021-04-05', '2021-04-14', '1', '2', '2021-04-09 05:40:08', '2021-04-09 05:40:08', NULL),
(29, 434, 432, '2021-04-05', '2021-04-14', '1', '2', '2021-04-09 05:38:44', '2021-04-09 05:38:44', NULL),
(28, 8654, 756, '2021-04-07', '2021-04-15', '2', '3', '2021-04-09 05:37:36', '2021-04-09 05:37:36', NULL),
(27, 865, 756, '2021-04-07', '2021-04-15', '2', '3', '2021-04-09 05:36:08', '2021-04-09 05:36:08', NULL),
(26, 21, 21, '2021-03-29', '2021-04-08', '1', '2', '2021-04-09 05:35:10', '2021-04-09 05:35:10', NULL),
(25, 87, 35, '2021-04-05', '2021-04-23', '1', '4', '2021-04-09 05:34:11', '2021-04-09 05:34:11', NULL),
(24, 1, 123.69, '2021-04-01', '2021-04-10', '1', '2', '2021-04-08 12:54:38', '2021-04-08 12:54:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `file_categories`
--

DROP TABLE IF EXISTS `file_categories`;
CREATE TABLE IF NOT EXISTS `file_categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `measures`
--

DROP TABLE IF EXISTS `measures`;
CREATE TABLE IF NOT EXISTS `measures` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `sort_order` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `measures_name_index` (`name`(250))
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `measures`
--

INSERT INTO `measures` (`id`, `sort_order`, `name`, `code`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Civil Works', '1', NULL, '2020-11-05 16:54:15', '2020-12-10 11:45:18', '2020-12-10 11:45:18'),
(2, 2, 'Equipment & Furniture', '2', NULL, '2020-11-05 16:54:24', '2021-01-28 17:54:42', NULL),
(3, 3, 'Management information system', '3', NULL, '2020-11-05 16:54:30', '2020-12-11 13:09:03', '2020-12-11 13:09:03'),
(4, 4, 'Donor recruitment campaign', '4', NULL, '2020-11-05 16:54:37', '2020-12-10 11:44:23', '2020-12-10 11:44:23'),
(5, 5, 'FC accompanying measures', '5', NULL, '2020-11-05 16:54:46', '2020-12-10 11:46:19', '2020-12-10 11:46:19'),
(6, 6, 'Consulting Services', '6', NULL, '2020-11-05 16:54:52', '2020-12-10 11:46:04', '2020-12-10 11:46:04'),
(7, 7, 'Contingencies', '8', NULL, '2020-11-05 16:54:59', '2020-12-11 15:16:49', NULL),
(9, 8, 'Waste Management', '8', NULL, '2020-11-30 12:18:05', '2020-12-11 13:15:25', '2020-12-11 13:15:25'),
(10, 9, 'Support to NGOs Blood Banks', '9', NULL, '2020-11-30 12:31:27', '2020-12-10 11:46:49', '2020-12-10 11:46:49'),
(11, 10, 'Upgrading of HBBs', '3', NULL, '2020-12-10 11:42:42', '2020-12-10 11:42:42', NULL),
(12, 11, 'CivilWork', '1', NULL, '2020-12-10 11:49:10', '2020-12-30 18:23:52', NULL),
(13, 12, 'CivilWork', '1', NULL, '2020-12-10 11:49:10', '2020-12-10 11:49:24', '2020-12-10 11:49:24'),
(14, 13, 'Management Information System for HBBs', '4', NULL, '2020-12-11 13:14:50', '2020-12-11 13:14:50', NULL),
(15, 14, 'Waste disposal equipment', '5', NULL, '2020-12-11 13:17:22', '2020-12-11 13:17:22', NULL),
(16, 16, 'Trainings', '6', NULL, '2020-12-11 13:17:50', '2020-12-23 09:36:18', NULL),
(17, 15, 'Support to Public/Private Sector/NGOs (BDOs, Blood Banks)', '7', NULL, '2020-12-11 13:18:27', '2020-12-11 13:18:27', NULL),
(21, 332, 'test', 'b1', NULL, '2021-01-26 19:58:17', '2021-01-26 20:00:03', '2021-01-26 20:00:03'),
(22, 212, 'Furniture', '33', NULL, '2021-01-26 20:20:43', '2021-01-26 20:21:09', '2021-01-26 20:21:09'),
(23, 18, 'Extension cost for Consulting services up to 31.12.20', '8a', NULL, '2021-01-28 18:05:38', '2021-01-28 18:05:38', NULL),
(24, 19, 'Remaining Contingencies', '8b', NULL, '2021-01-28 18:06:25', '2021-01-28 18:06:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(31, '2014_10_12_000000_create_users_table', 14),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_10_19_053130_create_vendors_table', 1),
(4, '2020_10_19_095839_create_province_regions_table', 1),
(30, '2020_10_20_060900_create_sub_categories_table', 13),
(6, '2020_10_20_061132_create_measures_table', 2),
(7, '2020_10_20_105841_create_cities_table', 3),
(20, '2020_10_20_105914_create_exchange_rates_table', 9),
(9, '2020_10_20_105937_create_currencies_table', 3),
(10, '2020_10_20_110019_create_payment_methods_table', 3),
(12, '2020_10_22_063335_create_contractors_table', 4),
(13, '2020_10_22_094759_create_p_e_as_table', 5),
(14, '2020_10_24_104122_create_projects_table', 6),
(15, '2020_10_24_111826_create_projects_files_table', 7),
(16, '2020_10_24_111901_create_projects_has_provinces_table', 7),
(17, '2020_10_26_061433_create_contracts_table', 8),
(18, '2020_10_26_062935_create_contract_has_provinces_table', 8),
(19, '2020_10_26_063347_create_contract_has_provinc_divisions_table', 8),
(24, '2020_10_27_065805_create_payment_has_files_table', 10),
(25, '2020_10_27_090712_create_payments_table', 10),
(26, '2020_10_27_093044_create_payments_has_subs_table', 10),
(27, '2020_10_28_050103_create_permission_tables', 11);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` bigint UNSIGNED NOT NULL,
  `model_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` bigint UNSIGNED NOT NULL,
  `model_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint UNSIGNED NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 3),
(1, 'App\\User', 5),
(2, 'App\\User', 4),
(2, 'App\\User', 7),
(2, 'App\\User', 8),
(2, 'App\\User', 9),
(2, 'App\\User', 14),
(3, 'App\\User', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(250))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `contractor_id` int NOT NULL,
  `contract_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_amount` double NOT NULL,
  `invoice_currency` int NOT NULL,
  `invoice_date` date NOT NULL,
  `invoice_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_less_retention` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_less_retention_amount` double NOT NULL,
  `invoice_withdraw_application` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_performance_security_validity` date NOT NULL,
  `invoice_net_payable` double NOT NULL,
  `invoice_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `beneficiary_account_id` int NOT NULL,
  `payment_details_date` date NOT NULL,
  `payment_details_amount` double NOT NULL,
  `payment_details_currency` int NOT NULL,
  `payment_details_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_details_remarks` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `contractor_id`, `contract_id`, `invoice_amount`, `invoice_currency`, `invoice_date`, `invoice_no`, `invoice_less_retention`, `invoice_less_retention_amount`, `invoice_withdraw_application`, `invoice_performance_security_validity`, `invoice_net_payable`, `invoice_description`, `beneficiary_account_id`, `payment_details_date`, `payment_details_amount`, `payment_details_currency`, `payment_details_code`, `payment_details_remarks`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, '4', 21212, 1, '2021-04-06', '434343', '4343', 434, 'bXU2jhjgY7', '2021-04-01', 0, NULL, 20, '2021-04-14', 21232.000003, 1, '21041aa', 'EU7nlsMuNa', 0, '2021-04-13 05:21:51', '2021-04-13 05:21:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payments_has_subs`
--

DROP TABLE IF EXISTS `payments_has_subs`;
CREATE TABLE IF NOT EXISTS `payments_has_subs` (
  `payment_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `measure_id` int DEFAULT NULL,
  `sub_cate_contracts` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `province_contracts` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_cate_peas` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_cate_city` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_amount` double DEFAULT NULL,
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_has_files`
--

DROP TABLE IF EXISTS `payment_has_files`;
CREATE TABLE IF NOT EXISTS `payment_has_files` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `payment_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

DROP TABLE IF EXISTS `payment_methods`;
CREATE TABLE IF NOT EXISTS `payment_methods` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `sort_order` int NOT NULL,
  `currency_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_method_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_rate` tinyint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `sort_order`, `currency_id`, `payment_method_name`, `short_code`, `exchange_rate`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '8', 'Direct Disbursement', '55', 0, '2020-10-22 07:00:07', '2021-01-26 20:27:52', NULL),
(2, 3, '1', 'Disposition FUND EPOS Germany', NULL, 0, '2020-10-22 07:00:55', '2020-11-05 16:57:55', NULL),
(3, 2, '2', 'Disposition Fund (DF) Account Pakistan', NULL, 1, '2020-10-22 07:01:41', '2020-11-05 16:58:25', NULL),
(11, 4, '2', 'Debit', '7', 1, '2021-01-26 20:29:13', '2021-01-26 20:29:25', '2021-01-26 20:29:25');

-- --------------------------------------------------------

--
-- Table structure for table `peas_contact_information`
--

DROP TABLE IF EXISTS `peas_contact_information`;
CREATE TABLE IF NOT EXISTS `peas_contact_information` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `pea_id` bigint UNSIGNED NOT NULL,
  `contact_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_designation` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_role` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_mobile_no` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_landline` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `peas_contact_information_pea_id_index` (`pea_id`),
  KEY `peas_contact_information_contact_name_index` (`contact_name`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `peas_contact_information`
--

INSERT INTO `peas_contact_information` (`id`, `pea_id`, `contact_name`, `contact_designation`, `contact_role`, `contact_email`, `contact_mobile_no`, `contact_landline`, `created_at`, `updated_at`) VALUES
(18, 9, 'ali', 'dev', 'sup', 'ali@ali.com', '0365421815', '042125125', '2020-11-13 10:50:55', '2020-11-13 10:50:55');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'role-list', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(2, 'role-create', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(3, 'role-edit', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(4, 'role-delete', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(5, 'city-list', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(6, 'city-create', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(7, 'city-edit', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(8, 'city-delete', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(9, 'exchange-rate-list', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(10, 'exchange-rate-create', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(11, 'exchange-rate-edit', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(12, 'exchange-rate-delete', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(13, 'currency-list', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(14, 'currency-create', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(15, 'currency-edit', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(16, 'currency-delete', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(17, 'payment-method-list', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(18, 'payment-method-create', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(19, 'payment-method-edit', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(20, 'payment-method-delete', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(21, 'sub-categories-list', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(22, 'sub-categories-create', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(23, 'sub-categories-edit', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(24, 'sub-categories-delete', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(25, 'measures-list', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(26, 'measures-create', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(27, 'measures-edit', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(28, 'measures-delete', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(29, 'contractors-list', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(30, 'contractors-create', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(31, 'contractors-edit', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(32, 'contractors-delete', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(33, 'project-execution-agency-list', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(34, 'project-execution-agency-create', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(35, 'project-execution-agency-edit', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(36, 'project-execution-agency-delete', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(37, 'province-region-list', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(38, 'province-region-create', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(39, 'province-region-edit', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(40, 'province-region-delete', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(41, 'payments-list', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(42, 'payments-create', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(43, 'payments-edit', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(44, 'payments-delete', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(45, 'projects-list', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(46, 'projects-create', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(47, 'projects-edit', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(48, 'projects-delete', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(49, 'contracts-list', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(50, 'contracts-create', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(51, 'contracts-edit', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(52, 'contracts-delete', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(53, 'users-list', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(54, 'users-create', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(55, 'users-edit', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(56, 'users-delete', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(57, 'user-show', 'web', '2020-10-29 02:55:52', '2020-10-29 02:55:52'),
(58, 'projects-status', 'web', NULL, NULL),
(59, 'dashboard', 'web', NULL, NULL),
(60, 'role-show', 'web', '2020-11-24 19:00:00', '2020-11-24 19:00:00'),
(61, 'contracts-show', 'web', '2020-11-04 19:00:00', '2020-11-04 19:00:00'),
(62, 'projects-show', 'web', NULL, NULL),
(63, 'payments-show', 'web', NULL, NULL),
(64, 'dashboard-project-count', 'web', NULL, NULL),
(65, 'dashboard-user-count', 'web', NULL, NULL),
(66, 'dashboard-contracts-count', 'web', NULL, NULL),
(67, 'dashboard-payments-count', 'web', NULL, NULL),
(68, 'dashboard-project-budget-distribution-province-wise', 'web', NULL, NULL),
(69, 'dashboard-overall-fund-dispositions-EUR', 'web', NULL, NULL),
(70, 'dashboard-fund-dispositions', 'web', NULL, NULL),
(71, 'dashboard-fund-dispositions-AJK', 'web', NULL, NULL),
(72, 'dashboard-fund-dispositions-balochistan', 'web', NULL, NULL),
(73, 'dashboard-fund-dispositions-gilgit-baltistan', 'web', NULL, NULL),
(74, 'dashboard-fund-dispositions-khyber-pakhtunkhwa', 'web', NULL, NULL),
(75, 'dashbard-fund-dispositions-punjab', 'web', NULL, NULL),
(76, 'dashboard-fund-dispositions-sindh', 'web', NULL, NULL),
(77, 'dashboard-fund-dispositions-ICT', 'web', NULL, NULL),
(78, 'projects-restore', 'web', NULL, NULL),
(79, 'contracts-restore', 'web', NULL, NULL),
(80, 'payments-restore', 'web', NULL, NULL),
(81, 'province-region-restore', 'web', NULL, NULL),
(82, 'project-execution-agency-restore', 'web', NULL, NULL),
(83, 'contractors-restore', 'web', NULL, NULL),
(84, 'measures-restore', 'web', NULL, NULL),
(85, 'sub-categories-restore', 'web', NULL, NULL),
(86, 'payment-method-restore', 'web', NULL, NULL),
(87, 'currency-restore', 'web', NULL, NULL),
(88, 'exchange-rate-restore', 'web', NULL, NULL),
(89, 'city-restore', 'web', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
CREATE TABLE IF NOT EXISTS `projects` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `donor_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `donor_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `budget` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bugdet_revised` double DEFAULT NULL,
  `revised_expected_completion_date` date DEFAULT NULL,
  `currency_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pea_id` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `planned_start_date` date DEFAULT NULL,
  `planned_end_date` date DEFAULT NULL,
  `project_start_date` date DEFAULT NULL,
  `project_end_date` date DEFAULT NULL,
  `project_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int NOT NULL DEFAULT '0',
  `is_approved` int DEFAULT NULL,
  `approval_file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approval_file_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_title_index` (`title`(250))
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `title`, `donor_number`, `donor_name`, `budget`, `bugdet_revised`, `revised_expected_completion_date`, `currency_id`, `province_id`, `pea_id`, `planned_start_date`, `planned_end_date`, `project_start_date`, `project_end_date`, `project_description`, `status`, `is_approved`, `approval_file`, `approval_file_title`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Qui totam nobis non', '786', '8', '25000.00', 45000, '2008-04-09', '4', '1', '5', '1985-09-25', '2011-03-11', '1998-12-07', '2020-11-03', 'Est eius assumenda q', 0, 1, '1611741347.png', 'screencapture-localhost-8000-admin-contract-edit-10-2021-01-25-10_44_25 (1).png', '2021-01-28 17:53:47', '2021-01-27 19:51:36', '2021-01-28 17:53:47'),
(2, 'test', NULL, '8', '50000.00', 55000, '2021-01-20', '1', '1', '5', '2020-12-29', '2020-12-14', '2021-01-20', NULL, NULL, 1, 1, '1611812124.txt', 'New Text Document.txt', '2021-01-28 22:56:46', '2021-01-28 15:27:27', '2021-01-28 22:56:46'),
(3, 'Deserunt velit aut a', '824', '8', '22.00', 0, '1990-12-11', '3', '6', NULL, '2021-01-28', NULL, '1992-07-08', '2000-05-06', 'Est enim dolores vol', 0, NULL, NULL, NULL, '2021-01-28 17:26:35', '2021-01-28 17:21:56', '2021-01-28 17:26:35'),
(4, 'Ut ipsam quaerat bea', '497', '8', '78000.00', 0, '2009-06-14', '2', '8', NULL, '1997-03-25', '2010-01-28', '1991-06-06', '1973-10-30', 'Saepe ad temporibus', 1, 1, NULL, NULL, '2021-01-28 17:53:57', '2021-01-28 17:50:25', '2021-01-28 17:53:57'),
(5, 'AJK - Safe Blood Transfusion Project Phase II', '2013 67 416', '8', '98000.00', 98000, '2021-12-31', '1', '12', '1', '2016-09-01', '2020-08-31', '2016-09-01', NULL, NULL, 1, 1, '1612785288.pdf', 'Asif Iqbal Ledger (Jan-2021).pdf', NULL, '2021-01-28 20:32:45', '2021-02-08 21:54:57'),
(6, 'Balochistan - Safe Blood Transfusion Project Phase II', '2013 67 416', '8', '326000.00', 326000, '2020-12-31', '1', '2', '2', '2016-09-01', '2020-08-31', '2016-09-01', NULL, NULL, 1, 1, '1611830993.pdf', 'HP Multimedia Screen .pdf', NULL, '2021-01-28 20:45:49', '2021-02-08 15:47:47'),
(7, 'Gilgit Baltistan - Safe Blood Transfusion Project Phase II', '2013 67 416', '8', '333000.00', 333000, '2020-12-31', '1', '3', '3', '2016-09-01', '2020-08-31', '2016-09-01', NULL, NULL, 1, 1, '1612171397.pdf', 'HP Multimedia Screen .pdf', NULL, '2021-01-28 20:54:21', '2021-02-08 15:47:59'),
(8, 'ICT - Safe Blood Transfusion Project Phase II', '2013 67 416', '8', '1591000.00', 1591000, '2020-12-31', '1', '8', '7', '2016-09-01', '2020-08-31', '2016-09-01', NULL, NULL, 1, 1, '1612172475.pdf', 'HP Multimedia Screen .pdf', NULL, '2021-02-01 19:35:33', '2021-02-08 15:48:09'),
(9, 'KP - Safe Blood Transfusion Project Phase  II', '2013 67 416', '8', '3414000.00', 3414000, '2020-12-31', '1', '4', '4', '2016-09-01', '2020-08-31', '2016-09-01', NULL, NULL, 1, 1, '1612173637.pdf', 'HP Multimedia Screen .pdf', NULL, '2021-02-01 19:51:59', '2021-02-08 15:48:16'),
(10, 'Punjab - Safe Blood Transfusion Project Phase II', '2013 67 416', '8', '2314000.00', 2314000, '2020-12-31', '1', '1', '5', '2016-09-01', '2020-08-31', '2016-09-01', NULL, NULL, 1, 1, '1612174240.pdf', 'HP Multimedia Screen .pdf', NULL, '2021-02-01 20:06:48', '2021-02-19 15:58:46'),
(11, 'Sindh - Safe Blood Transfusion Project Phase II', '2013 67 416', '8', '674000.00', 674000, '2020-12-31', '1', '6', '6', '2016-09-01', '2020-08-31', '2016-09-01', NULL, NULL, 1, 1, '1612174863.pdf', 'HP Multimedia Screen .pdf', NULL, '2021-02-01 20:18:05', '2021-02-08 15:48:30');

-- --------------------------------------------------------

--
-- Table structure for table `projects_files`
--

DROP TABLE IF EXISTS `projects_files`;
CREATE TABLE IF NOT EXISTS `projects_files` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `project_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects_files`
--

INSERT INTO `projects_files` (`id`, `project_id`, `file`, `category`, `description`, `created_at`, `updated_at`) VALUES
(1, '1', 'screencapture-localhost-8000-admin-contract-edit-10-2021-01-25-10_44_25 (1).png', 'Quotation', NULL, '2021-01-27 19:51:36', '2021-01-27 19:51:36'),
(2, '2', 'New Text Document.txt', 'Contract', NULL, '2021-01-28 15:27:27', '2021-01-28 15:27:27'),
(3, '2', 'New Text Document.txt', 'Contract', NULL, '2021-01-28 15:27:27', '2021-01-28 15:27:27');

-- --------------------------------------------------------

--
-- Table structure for table `projects_has_provinces`
--

DROP TABLE IF EXISTS `projects_has_provinces`;
CREATE TABLE IF NOT EXISTS `projects_has_provinces` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `project_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `measure_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_category_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `budget` double NOT NULL,
  `revise_amount` double DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revise_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_has_provinces_project_id_index` (`project_id`(250))
) ENGINE=MyISAM AUTO_INCREMENT=165 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects_has_provinces`
--

INSERT INTO `projects_has_provinces` (`id`, `project_id`, `measure_id`, `sub_category_id`, `budget`, `revise_amount`, `description`, `revise_date`, `created_at`, `updated_at`) VALUES
(3, '1', '7', '25', 20000, 25000, '', '2021-01-27 05:00:00', '2021-01-27 19:55:47', '2021-01-27 19:55:47'),
(4, '1', '7', '27', 5000, 20000, '', '2021-01-27 05:00:00', '2021-01-27 19:55:47', '2021-01-27 19:55:47'),
(7, '2', '2', NULL, 40000, 40000, '', '2021-01-26 05:00:00', '2021-01-28 15:35:24', '2021-01-28 15:35:24'),
(8, '2', '7', NULL, 10000, 15000, '', '2021-01-26 05:00:00', '2021-01-28 15:35:24', '2021-01-28 15:35:24'),
(10, '3', '15', NULL, 22, 0, '', NULL, '2021-01-28 17:26:24', '2021-01-28 17:26:24'),
(12, '4', '15', NULL, 78000, 0, '', NULL, '2021-01-28 17:50:36', '2021-01-28 17:50:36'),
(163, '5', '23', NULL, 0, 3000, '', '2020-11-30 05:00:00', '2021-02-08 21:54:48', '2021-02-08 21:54:48'),
(162, '5', '7', NULL, 8000, 0, '', '2020-11-30 05:00:00', '2021-02-08 21:54:48', '2021-02-08 21:54:48'),
(161, '5', '17', NULL, 30000, 0, '', '2020-11-30 05:00:00', '2021-02-08 21:54:48', '2021-02-08 21:54:48'),
(160, '5', '16', NULL, 25000, 10000, '', '2020-11-30 05:00:00', '2021-02-08 21:54:48', '2021-02-08 21:54:48'),
(158, '5', '14', NULL, 10000, 5000, '', '2020-11-30 05:00:00', '2021-02-08 21:54:48', '2021-02-08 21:54:48'),
(159, '5', '15', NULL, 25000, 5000, '', '2020-11-30 05:00:00', '2021-02-08 21:54:48', '2021-02-08 21:54:48'),
(34, '6', '15', NULL, 20000, 15000, '', '2020-11-30 05:00:00', '2021-01-28 20:49:54', '2021-01-28 20:49:54'),
(33, '6', '14', NULL, 10000, 5000, '', '2020-11-30 05:00:00', '2021-01-28 20:49:54', '2021-01-28 20:49:54'),
(32, '6', '11', NULL, 200000, 210000, '', '2020-11-30 05:00:00', '2021-01-28 20:49:54', '2021-01-28 20:49:54'),
(35, '6', '16', NULL, 20000, 20000, '', '2020-11-30 05:00:00', '2021-01-28 20:49:54', '2021-01-28 20:49:54'),
(36, '6', '17', NULL, 50000, 50000, '', '2020-11-30 05:00:00', '2021-01-28 20:49:54', '2021-01-28 20:49:54'),
(37, '6', '7', NULL, 26000, 0, '', '2020-11-30 05:00:00', '2021-01-28 20:49:54', '2021-01-28 20:49:54'),
(38, '6', '23', NULL, 0, 11000, '', '2020-11-30 05:00:00', '2021-01-28 20:49:54', '2021-01-28 20:49:54'),
(39, '6', '24', NULL, 0, 15000, '', '2020-11-30 05:00:00', '2021-01-28 20:49:54', '2021-01-28 20:49:54'),
(74, '7', '7', NULL, 27000, 0, '', '2020-11-30 05:00:00', '2021-02-01 19:23:18', '2021-02-01 19:23:18'),
(73, '7', '17', NULL, 30000, 0, '', '2020-11-30 05:00:00', '2021-02-01 19:23:18', '2021-02-01 19:23:18'),
(72, '7', '16', NULL, 23000, 5000, '', '2020-11-30 05:00:00', '2021-02-01 19:23:18', '2021-02-01 19:23:18'),
(71, '7', '15', NULL, 23000, 4000, '', '2020-11-30 05:00:00', '2021-02-01 19:23:18', '2021-02-01 19:23:18'),
(69, '7', '2', NULL, 100000, 100000, '', '2020-11-30 05:00:00', '2021-02-01 19:23:18', '2021-02-01 19:23:18'),
(70, '7', '14', NULL, 30000, 45000, '', '2020-11-30 05:00:00', '2021-02-01 19:23:18', '2021-02-01 19:23:18'),
(68, '7', '12', NULL, 100000, 166000, '', '2020-11-30 05:00:00', '2021-02-01 19:23:17', '2021-02-01 19:23:17'),
(75, '7', '23', NULL, 0, 11000, '', '2020-11-30 05:00:00', '2021-02-01 19:23:18', '2021-02-01 19:23:18'),
(76, '7', '24', NULL, 0, 2000, '', '2020-11-30 05:00:00', '2021-02-01 19:23:18', '2021-02-01 19:23:18'),
(87, '8', '15', NULL, 23000, 5000, '', '2020-11-30 05:00:00', '2021-02-01 19:41:16', '2021-02-01 19:41:16'),
(86, '8', '14', NULL, 80000, 60000, '', '2020-11-30 05:00:00', '2021-02-01 19:41:16', '2021-02-01 19:41:16'),
(85, '8', '2', NULL, 738000, 539000, '', '2020-11-30 05:00:00', '2021-02-01 19:41:16', '2021-02-01 19:41:16'),
(84, '8', '12', NULL, 550000, 880000, '', '2020-11-30 05:00:00', '2021-02-01 19:41:16', '2021-02-01 19:41:16'),
(88, '8', '16', NULL, 23000, 10000, '', '2020-11-30 05:00:00', '2021-02-01 19:41:16', '2021-02-01 19:41:16'),
(89, '8', '17', NULL, 50000, 0, '', '2020-11-30 05:00:00', '2021-02-01 19:41:16', '2021-02-01 19:41:16'),
(90, '8', '7', NULL, 127000, 0, '', '2020-11-30 05:00:00', '2021-02-01 19:41:16', '2021-02-01 19:41:16'),
(91, '8', '23', NULL, 0, 53000, '', '2020-11-30 05:00:00', '2021-02-01 19:41:16', '2021-02-01 19:41:16'),
(92, '8', '24', NULL, 0, 44000, '', '2020-11-30 05:00:00', '2021-02-01 19:41:16', '2021-02-01 19:41:16'),
(103, '9', '15', NULL, 64500, 15000, '', '2020-11-30 05:00:00', '2021-02-01 20:00:37', '2021-02-01 20:00:37'),
(102, '9', '14', NULL, 170000, 140000, '', '2020-11-30 05:00:00', '2021-02-01 20:00:37', '2021-02-01 20:00:37'),
(101, '9', '2', NULL, 1892000, 1666000, '', '2020-11-30 05:00:00', '2021-02-01 20:00:37', '2021-02-01 20:00:37'),
(100, '9', '12', NULL, 900000, 1300000, '', '2020-11-30 05:00:00', '2021-02-01 20:00:37', '2021-02-01 20:00:37'),
(104, '9', '16', NULL, 64500, 20000, '', '2020-11-30 05:00:00', '2021-02-01 20:00:37', '2021-02-01 20:00:37'),
(105, '9', '17', NULL, 50000, 0, '', '2020-11-30 05:00:00', '2021-02-01 20:00:37', '2021-02-01 20:00:37'),
(106, '9', '7', NULL, 273000, 0, '', '2020-11-30 05:00:00', '2021-02-01 20:00:37', '2021-02-01 20:00:37'),
(107, '9', '23', NULL, 0, 113000, '', '2020-11-30 05:00:00', '2021-02-01 20:00:37', '2021-02-01 20:00:37'),
(108, '9', '24', NULL, 0, 160000, '', '2020-11-30 05:00:00', '2021-02-01 20:00:37', '2021-02-01 20:00:37'),
(120, '10', '14', NULL, 160000, 160000, '', '2020-11-30 05:00:00', '2021-02-01 20:10:40', '2021-02-01 20:10:40'),
(119, '10', '11', NULL, 500000, 500000, '', '2020-11-30 05:00:00', '2021-02-01 20:10:40', '2021-02-01 20:10:40'),
(118, '10', '2', NULL, 720000, 709000, '', '2020-11-30 05:00:00', '2021-02-01 20:10:40', '2021-02-01 20:10:40'),
(117, '10', '12', NULL, 450000, 700000, '', '2020-11-30 05:00:00', '2021-02-01 20:10:40', '2021-02-01 20:10:40'),
(121, '10', '15', NULL, 64500, 15000, '', '2020-11-30 05:00:00', '2021-02-01 20:10:40', '2021-02-01 20:10:40'),
(122, '10', '16', NULL, 64500, 45000, '', '2020-11-30 05:00:00', '2021-02-01 20:10:40', '2021-02-01 20:10:40'),
(123, '10', '17', NULL, 170000, 0, '', '2020-11-30 05:00:00', '2021-02-01 20:10:40', '2021-02-01 20:10:40'),
(124, '10', '7', NULL, 185000, 0, '', '2020-11-30 05:00:00', '2021-02-01 20:10:40', '2021-02-01 20:10:40'),
(125, '10', '23', NULL, 0, 77000, '', '2020-11-30 05:00:00', '2021-02-01 20:10:40', '2021-02-01 20:10:40'),
(126, '10', '24', NULL, 0, 108000, '', '2020-11-30 05:00:00', '2021-02-01 20:10:40', '2021-02-01 20:10:40'),
(135, '11', '15', NULL, 80000, 15000, '', '2020-12-31 05:00:00', '2021-02-01 20:21:04', '2021-02-01 20:21:04'),
(134, '11', '14', NULL, 40000, 155000, '', '2020-12-31 05:00:00', '2021-02-01 20:21:04', '2021-02-01 20:21:04'),
(133, '11', '11', NULL, 300000, 335000, '', '2020-12-31 05:00:00', '2021-02-01 20:21:04', '2021-02-01 20:21:04'),
(136, '11', '16', NULL, 80000, 15000, '', '2020-12-31 05:00:00', '2021-02-01 20:21:04', '2021-02-01 20:21:04'),
(137, '11', '17', NULL, 120000, 100000, '', '2020-12-31 05:00:00', '2021-02-01 20:21:04', '2021-02-01 20:21:04'),
(138, '11', '7', NULL, 54000, 0, '', '2020-12-31 05:00:00', '2021-02-01 20:21:04', '2021-02-01 20:21:04'),
(139, '11', '23', NULL, 0, 22000, '', '2020-12-31 05:00:00', '2021-02-01 20:21:04', '2021-02-01 20:21:04'),
(140, '11', '24', NULL, 0, 32000, '', '2020-12-31 05:00:00', '2021-02-01 20:21:04', '2021-02-01 20:21:04'),
(157, '5', '11', NULL, 0, 70000, '', '2020-11-30 05:00:00', '2021-02-08 21:54:48', '2021-02-08 21:54:48'),
(164, '5', '24', NULL, 0, 5000, '', '2020-11-30 05:00:00', '2021-02-08 21:54:48', '2021-02-08 21:54:48');

-- --------------------------------------------------------

--
-- Table structure for table `projects_has_provinces_history`
--

DROP TABLE IF EXISTS `projects_has_provinces_history`;
CREATE TABLE IF NOT EXISTS `projects_has_provinces_history` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `row_id` int NOT NULL,
  `project_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `measure_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_category_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `budget` double NOT NULL,
  `revise_amount` double DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revise_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_has_provinces_project_id_index` (`project_id`(250))
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects_has_provinces_history`
--

INSERT INTO `projects_has_provinces_history` (`id`, `row_id`, `project_id`, `measure_id`, `sub_category_id`, `budget`, `revise_amount`, `description`, `revise_date`, `created_at`, `updated_at`) VALUES
(1, 1, '1', '7', '25', 20000, 0, '', NULL, '2021-01-27 19:55:47', '2021-01-27 19:55:47'),
(2, 1, '1', '7', '27', 5000, 0, '', NULL, '2021-01-27 19:55:47', '2021-01-27 19:55:47'),
(3, 2, '2', '2', NULL, 40000, 0, '', NULL, '2021-01-28 15:35:24', '2021-01-28 15:35:24'),
(4, 2, '2', '7', NULL, 10000, 0, '', NULL, '2021-01-28 15:35:24', '2021-01-28 15:35:24'),
(5, 3, '5', '14', NULL, 10000, 0, '', NULL, '2021-01-28 20:38:41', '2021-01-28 20:38:41'),
(6, 3, '5', '15', NULL, 25000, 0, '', NULL, '2021-01-28 20:38:41', '2021-01-28 20:38:41'),
(7, 3, '5', '16', NULL, 25000, 0, '', NULL, '2021-01-28 20:38:41', '2021-01-28 20:38:41'),
(8, 3, '5', '17', NULL, 30000, 0, '', NULL, '2021-01-28 20:38:41', '2021-01-28 20:38:41'),
(9, 3, '5', '7', NULL, 8000, 0, '', NULL, '2021-01-28 20:38:41', '2021-01-28 20:38:41'),
(10, 4, '6', '11', NULL, 200000, 0, '', NULL, '2021-01-28 20:49:54', '2021-01-28 20:49:54'),
(11, 4, '6', '14', NULL, 10000, 0, '', NULL, '2021-01-28 20:49:54', '2021-01-28 20:49:54'),
(12, 4, '6', '15', NULL, 20000, 0, '', NULL, '2021-01-28 20:49:54', '2021-01-28 20:49:54'),
(13, 4, '6', '16', NULL, 20000, 0, '', NULL, '2021-01-28 20:49:54', '2021-01-28 20:49:54'),
(14, 4, '6', '17', NULL, 50000, 0, '', NULL, '2021-01-28 20:49:54', '2021-01-28 20:49:54'),
(15, 4, '6', '7', NULL, 26000, 0, '', NULL, '2021-01-28 20:49:54', '2021-01-28 20:49:54'),
(16, 5, '7', '12', NULL, 100000, 0, '', NULL, '2021-01-28 21:03:03', '2021-01-28 21:03:03'),
(17, 5, '7', '2', NULL, 100000, 0, '', NULL, '2021-01-28 21:03:03', '2021-01-28 21:03:03'),
(18, 5, '7', '14', NULL, 30000, 0, '', NULL, '2021-01-28 21:03:03', '2021-01-28 21:03:03'),
(19, 5, '7', '15', NULL, 23000, 0, '', NULL, '2021-01-28 21:03:03', '2021-01-28 21:03:03'),
(20, 5, '7', '16', NULL, 23000, 0, '', NULL, '2021-01-28 21:03:03', '2021-01-28 21:03:03'),
(21, 5, '7', '17', NULL, 30000, 0, '', NULL, '2021-01-28 21:03:03', '2021-01-28 21:03:03'),
(22, 5, '7', '7', NULL, 27000, 0, '', NULL, '2021-01-28 21:03:03', '2021-01-28 21:03:03'),
(23, 6, '8', '12', NULL, 550000, 0, '', NULL, '2021-02-01 19:41:16', '2021-02-01 19:41:16'),
(24, 6, '8', '2', NULL, 738000, 0, '', NULL, '2021-02-01 19:41:16', '2021-02-01 19:41:16'),
(25, 6, '8', '14', NULL, 80000, 0, '', NULL, '2021-02-01 19:41:16', '2021-02-01 19:41:16'),
(26, 6, '8', '15', NULL, 23000, 0, '', NULL, '2021-02-01 19:41:16', '2021-02-01 19:41:16'),
(27, 6, '8', '16', NULL, 23000, 0, '', NULL, '2021-02-01 19:41:16', '2021-02-01 19:41:16'),
(28, 6, '8', '17', NULL, 50000, 0, '', NULL, '2021-02-01 19:41:16', '2021-02-01 19:41:16'),
(29, 6, '8', '7', NULL, 127000, 0, '', NULL, '2021-02-01 19:41:16', '2021-02-01 19:41:16'),
(30, 7, '9', '12', NULL, 900000, 0, '', NULL, '2021-02-01 20:00:37', '2021-02-01 20:00:37'),
(31, 7, '9', '2', NULL, 1892000, 0, '', NULL, '2021-02-01 20:00:37', '2021-02-01 20:00:37'),
(32, 7, '9', '14', NULL, 170000, 0, '', NULL, '2021-02-01 20:00:37', '2021-02-01 20:00:37'),
(33, 7, '9', '15', NULL, 64500, 0, '', NULL, '2021-02-01 20:00:37', '2021-02-01 20:00:37'),
(34, 7, '9', '16', NULL, 64500, 0, '', NULL, '2021-02-01 20:00:37', '2021-02-01 20:00:37'),
(35, 7, '9', '17', NULL, 50000, 0, '', NULL, '2021-02-01 20:00:37', '2021-02-01 20:00:37'),
(36, 7, '9', '7', NULL, 273000, 0, '', NULL, '2021-02-01 20:00:37', '2021-02-01 20:00:37'),
(37, 8, '10', '12', NULL, 450000, 0, '', NULL, '2021-02-01 20:10:40', '2021-02-01 20:10:40'),
(38, 8, '10', '2', NULL, 720000, 0, '', NULL, '2021-02-01 20:10:40', '2021-02-01 20:10:40'),
(39, 8, '10', '11', NULL, 500000, 0, '', NULL, '2021-02-01 20:10:40', '2021-02-01 20:10:40'),
(40, 8, '10', '14', NULL, 160000, 0, '', NULL, '2021-02-01 20:10:40', '2021-02-01 20:10:40'),
(41, 8, '10', '15', NULL, 64500, 0, '', NULL, '2021-02-01 20:10:40', '2021-02-01 20:10:40'),
(42, 8, '10', '16', NULL, 64500, 0, '', NULL, '2021-02-01 20:10:40', '2021-02-01 20:10:40'),
(43, 8, '10', '17', NULL, 170000, 0, '', NULL, '2021-02-01 20:10:40', '2021-02-01 20:10:40'),
(44, 8, '10', '7', NULL, 185000, 0, '', NULL, '2021-02-01 20:10:40', '2021-02-01 20:10:40'),
(45, 9, '11', '11', NULL, 300000, 0, '', NULL, '2021-02-01 20:21:04', '2021-02-01 20:21:04'),
(46, 9, '11', '14', NULL, 40000, 0, '', NULL, '2021-02-01 20:21:04', '2021-02-01 20:21:04'),
(47, 9, '11', '15', NULL, 80000, 0, '', NULL, '2021-02-01 20:21:04', '2021-02-01 20:21:04'),
(48, 9, '11', '16', NULL, 80000, 0, '', NULL, '2021-02-01 20:21:04', '2021-02-01 20:21:04'),
(49, 9, '11', '17', NULL, 120000, 0, '', NULL, '2021-02-01 20:21:04', '2021-02-01 20:21:04'),
(50, 9, '11', '7', NULL, 54000, 0, '', NULL, '2021-02-01 20:21:04', '2021-02-01 20:21:04'),
(51, 10, '5', '24', NULL, 0, 5000, '', '2020-11-30 05:00:00', '2021-02-08 21:54:48', '2021-02-08 21:54:48'),
(52, 10, '5', '23', NULL, 0, 3000, '', '2020-11-30 05:00:00', '2021-02-08 21:54:48', '2021-02-08 21:54:48'),
(53, 10, '5', '7', NULL, 8000, 0, '', '2020-11-30 05:00:00', '2021-02-08 21:54:48', '2021-02-08 21:54:48'),
(54, 10, '5', '17', NULL, 30000, 0, '', '2020-11-30 05:00:00', '2021-02-08 21:54:48', '2021-02-08 21:54:48'),
(55, 10, '5', '16', NULL, 25000, 10000, '', '2020-11-30 05:00:00', '2021-02-08 21:54:48', '2021-02-08 21:54:48'),
(56, 10, '5', '15', NULL, 25000, 5000, '', '2020-11-30 05:00:00', '2021-02-08 21:54:48', '2021-02-08 21:54:48'),
(57, 10, '5', '14', NULL, 10000, 5000, '', '2020-11-30 05:00:00', '2021-02-08 21:54:48', '2021-02-08 21:54:48'),
(58, 10, '5', '11', NULL, 0, 70000, '', '2020-11-30 05:00:00', '2021-02-08 21:54:48', '2021-02-08 21:54:48');

-- --------------------------------------------------------

--
-- Table structure for table `projects_history`
--

DROP TABLE IF EXISTS `projects_history`;
CREATE TABLE IF NOT EXISTS `projects_history` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `project_id` int NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `donor_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `donor_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `budget` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bugdet_revised` double DEFAULT NULL,
  `revised_expected_completion_date` date DEFAULT NULL,
  `currency_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` int NOT NULL,
  `pea_id` int DEFAULT NULL,
  `planned_start_date` date DEFAULT NULL,
  `planned_end_date` date DEFAULT NULL,
  `project_start_date` date DEFAULT NULL,
  `project_end_date` date DEFAULT NULL,
  `project_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int NOT NULL DEFAULT '0',
  `is_approved` int DEFAULT NULL,
  `approval_file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approval_file_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_title_index` (`title`(250))
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects_history`
--

INSERT INTO `projects_history` (`id`, `project_id`, `title`, `donor_number`, `donor_name`, `budget`, `bugdet_revised`, `revised_expected_completion_date`, `currency_id`, `province_id`, `pea_id`, `planned_start_date`, `planned_end_date`, `project_start_date`, `project_end_date`, `project_description`, `status`, `is_approved`, `approval_file`, `approval_file_title`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Qui totam nobis non', '786', 'KFW', '25000', 0, NULL, '4', 1, 5, '1985-09-25', '2011-03-11', '1998-12-07', '2020-11-03', 'Est eius assumenda q', 1, NULL, NULL, NULL, NULL, '2021-01-27 19:55:47', '2021-01-27 19:55:47'),
(2, 2, 'test', NULL, 'KFW', '50000', 0, NULL, '1', 1, 5, '2020-12-29', '2020-12-14', '2021-01-20', NULL, NULL, 1, NULL, NULL, NULL, NULL, '2021-01-28 15:35:24', '2021-01-28 15:35:24'),
(3, 5, 'AJK - Safe Blood Transfusion Project Phase II', '2013 67 416', 'KFW', '98000', 0, NULL, '1', 12, 1, '2016-09-01', '2020-08-31', '2016-09-01', NULL, NULL, 1, NULL, NULL, NULL, NULL, '2021-01-28 20:38:41', '2021-01-28 20:38:41'),
(4, 6, 'Balochistan - Safe Blood Transfusion Project Phase II', '2013 67 416', 'KFW', '326000', 0, NULL, '1', 2, 2, '2016-09-01', '2020-08-31', '2016-09-01', NULL, NULL, 1, NULL, NULL, NULL, NULL, '2021-01-28 20:49:54', '2021-01-28 20:49:54'),
(5, 7, 'Gilgit Baltistan - Safe Blood Transfusion Project Phase II', '2013 67 416', 'KFW', '333000', 0, NULL, '1', 3, 3, '2016-09-01', '2020-08-31', '2016-09-01', NULL, NULL, 1, NULL, NULL, NULL, NULL, '2021-01-28 21:03:03', '2021-01-28 21:03:03'),
(6, 8, 'ICT - Safe Blood Transfusion Project Phase II', '2013 67 416', 'KFW', '1591000', 0, NULL, '1', 8, 7, '2016-09-01', '2020-08-31', '2016-09-01', NULL, NULL, 1, NULL, NULL, NULL, NULL, '2021-02-01 19:41:16', '2021-02-01 19:41:16'),
(7, 9, 'KP - Safe Blood Transfusion Project Phase  II', '2013 67 416', 'KFW', '3414000', 0, NULL, '1', 4, 4, '2016-09-01', '2020-08-31', '2016-09-01', NULL, NULL, 1, NULL, NULL, NULL, NULL, '2021-02-01 20:00:37', '2021-02-01 20:00:37'),
(8, 10, 'Punjab - Safe Blood Transfusion Project Phase II', '2013 67 416', 'KFW', '2314000', 0, NULL, '1', 1, 5, '2016-09-01', '2020-08-31', '2016-09-01', NULL, NULL, 1, NULL, NULL, NULL, NULL, '2021-02-01 20:10:40', '2021-02-01 20:10:40'),
(9, 11, 'Sindh - Safe Blood Transfusion Project Phase II', '2013 67 416', 'KFW', '674000', 0, NULL, '1', 6, 6, '2016-09-01', '2020-08-31', '2016-09-01', NULL, NULL, 1, NULL, NULL, NULL, NULL, '2021-02-01 20:21:04', '2021-02-01 20:21:04'),
(10, 5, 'AJK - Safe Blood Transfusion Project Phase II', '2013 67 416', '8', '98000.00', 98000, NULL, '1', 12, 1, '2016-09-01', '2020-08-31', '2016-09-01', NULL, NULL, 1, NULL, '1612178684.pdf', 'Kopie von Revised budget Phase 2 SA annex 2 2020-12_v03.pdf', NULL, '2021-02-08 21:54:48', '2021-02-08 21:54:48');

-- --------------------------------------------------------

--
-- Table structure for table `project_reviseds`
--

DROP TABLE IF EXISTS `project_reviseds`;
CREATE TABLE IF NOT EXISTS `project_reviseds` (
  `id` int NOT NULL AUTO_INCREMENT,
  `project_id` int NOT NULL,
  `line_reference_id` int NOT NULL,
  `user_id` int NOT NULL,
  `date` datetime NOT NULL,
  `revised_budget` double NOT NULL,
  `status` int NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `province_regions`
--

DROP TABLE IF EXISTS `province_regions`;
CREATE TABLE IF NOT EXISTS `province_regions` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `sort_order` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `province_regions_name_index` (`name`(250))
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `province_regions`
--

INSERT INTO `province_regions` (`id`, `sort_order`, `name`, `short_code`, `detail`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Punjab', 'PB', NULL, '2020-11-04 04:28:41', '2020-11-12 14:28:06', NULL),
(2, 2, 'Balochistan', 'BA', NULL, '2020-11-04 04:28:46', '2020-11-04 04:28:46', NULL),
(3, 3, 'Gilgit Baltistan', 'GB ', NULL, '2020-11-04 04:28:57', '2020-11-04 04:28:57', NULL),
(4, 4, 'Khyber Pakhtunkhwa', 'KP', NULL, '2020-11-04 04:28:57', '2020-11-04 04:29:35', NULL),
(6, 6, 'Sindh', 'SD', NULL, '2020-11-04 04:29:11', '2020-11-04 04:29:11', NULL),
(8, 7, 'ICT', 'ICT', NULL, NULL, NULL, NULL),
(9, 8, 'M/o NHS,R&C', NULL, NULL, NULL, NULL, NULL),
(10, 9, 'South Punjab', 'SP', NULL, NULL, NULL, NULL),
(12, 10, 'Azad Kashmir', 'AJK', NULL, '2020-11-18 10:28:49', '2020-11-18 10:28:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `p_e_as`
--

DROP TABLE IF EXISTS `p_e_as`;
CREATE TABLE IF NOT EXISTS `p_e_as` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `sort_order` int NOT NULL,
  `pea_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `province_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pea_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `p_e_as_pea_name_index` (`pea_name`(250))
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `p_e_as`
--

INSERT INTO `p_e_as` (`id`, `sort_order`, `pea_name`, `province_id`, `pea_description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Department of Health, Govt of AJK', '12', NULL, '2020-11-05 16:51:50', '2021-01-26 20:09:53', NULL),
(2, 2, 'Department of Health, Govt of Balochistan', '2', NULL, '2020-11-05 16:51:59', '2020-11-05 16:51:59', NULL),
(3, 3, 'Department of Health, Govt. of Gilgit Baltistan', '3', NULL, '2020-11-05 16:52:09', '2020-11-05 16:52:09', NULL),
(4, 4, 'Department of Health, Govt of Khyber Pakhtunkhwa', '4', NULL, '2020-11-05 16:52:19', '2020-11-05 16:52:19', NULL),
(5, 5, 'Department of Health, Gov of Punjab', '1', 'Test 1', '2020-11-05 16:52:27', '2020-12-14 16:58:52', NULL),
(6, 6, 'Department of Health, Govt of Sindh', '6', NULL, '2020-11-05 16:52:33', '2020-11-12 14:22:51', NULL),
(7, 7, 'Ministry of NHS,R&C Islamabad', '8', NULL, '2020-11-05 16:52:46', '2020-11-05 16:52:46', NULL),
(8, 8, 'Ministry of NHS,R&C Islamabad', '9', NULL, '2020-11-05 16:52:55', '2020-11-05 16:52:55', NULL),
(12, 9, 'fdg', '6', 'asd', '2021-01-26 20:11:47', '2021-01-26 20:13:39', '2021-01-26 20:13:39');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2020-10-29 04:37:38', '2020-11-12 12:32:50'),
(2, 'Accountant', 'web', '2020-10-29 05:04:39', '2020-10-29 05:04:39'),
(3, 'CEO', 'web', '2020-11-12 11:42:19', '2020-11-12 11:42:19');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE IF NOT EXISTS `role_has_permissions` (
  `permission_id` bigint UNSIGNED NOT NULL,
  `role_id` bigint UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 3),
(2, 1),
(2, 3),
(3, 1),
(3, 3),
(4, 1),
(4, 3),
(5, 1),
(5, 2),
(5, 3),
(6, 1),
(6, 2),
(6, 3),
(7, 1),
(7, 2),
(7, 3),
(8, 1),
(8, 3),
(9, 1),
(9, 2),
(9, 3),
(10, 1),
(10, 2),
(10, 3),
(11, 1),
(11, 2),
(11, 3),
(12, 1),
(12, 3),
(13, 1),
(13, 2),
(13, 3),
(14, 1),
(14, 2),
(14, 3),
(15, 1),
(15, 2),
(15, 3),
(16, 1),
(16, 3),
(17, 1),
(17, 2),
(17, 3),
(18, 1),
(18, 2),
(18, 3),
(19, 1),
(19, 2),
(19, 3),
(20, 1),
(20, 3),
(21, 1),
(21, 2),
(21, 3),
(22, 1),
(22, 2),
(22, 3),
(23, 1),
(23, 2),
(23, 3),
(24, 1),
(24, 3),
(25, 1),
(25, 2),
(25, 3),
(26, 1),
(26, 2),
(26, 3),
(27, 1),
(27, 2),
(27, 3),
(28, 1),
(28, 3),
(29, 1),
(29, 2),
(29, 3),
(30, 1),
(30, 2),
(30, 3),
(31, 1),
(31, 2),
(31, 3),
(32, 1),
(32, 3),
(33, 1),
(33, 2),
(33, 3),
(34, 1),
(34, 2),
(34, 3),
(35, 1),
(35, 2),
(35, 3),
(36, 1),
(36, 3),
(37, 1),
(37, 2),
(37, 3),
(38, 1),
(38, 2),
(38, 3),
(39, 1),
(39, 2),
(39, 3),
(40, 1),
(40, 3),
(41, 1),
(41, 2),
(41, 3),
(42, 1),
(42, 2),
(42, 3),
(43, 1),
(43, 3),
(44, 1),
(44, 3),
(45, 1),
(45, 2),
(45, 3),
(46, 1),
(46, 2),
(46, 3),
(47, 1),
(47, 2),
(47, 3),
(48, 1),
(48, 3),
(49, 1),
(49, 2),
(49, 3),
(50, 1),
(50, 2),
(50, 3),
(51, 1),
(51, 2),
(51, 3),
(52, 1),
(52, 3),
(53, 1),
(53, 3),
(54, 1),
(54, 3),
(55, 1),
(55, 3),
(56, 1),
(56, 3),
(57, 1),
(57, 3),
(60, 1),
(60, 3),
(61, 1),
(61, 3),
(62, 1),
(62, 3),
(63, 1),
(63, 3),
(64, 1),
(64, 3),
(65, 1),
(65, 3),
(66, 1),
(66, 3),
(67, 1),
(67, 3),
(68, 1),
(68, 3),
(69, 1),
(69, 3),
(70, 1),
(70, 3),
(71, 1),
(71, 3),
(72, 1),
(72, 3),
(73, 1),
(73, 3),
(74, 1),
(74, 3),
(75, 1),
(75, 3),
(76, 1),
(76, 3),
(77, 1),
(77, 3),
(78, 1),
(78, 3),
(79, 1),
(79, 3),
(80, 1),
(80, 3),
(81, 1),
(81, 3),
(82, 1),
(82, 3),
(83, 1),
(83, 3),
(84, 1),
(84, 3),
(85, 1),
(85, 3),
(86, 1),
(86, 3),
(87, 1),
(87, 3),
(88, 1),
(88, 3),
(89, 1),
(89, 3);

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

DROP TABLE IF EXISTS `sub_categories`;
CREATE TABLE IF NOT EXISTS `sub_categories` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `sequence_number` int NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `measure_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sub_categories_name_index` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `sequence_number`, `name`, `measure_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 26, 'Soil investigation and topographic surveys', '1', NULL, '2020-11-05 16:56:27', '2020-11-05 16:56:27', NULL),
(2, 55, 'Lot 3 - Laboratory Items (Technology)', '2', NULL, '2020-11-05 16:56:47', '2021-01-26 20:24:32', NULL),
(3, 5, 'Lot 4 - Analyser (PKR) - Abbott', '2', NULL, '2020-11-05 16:56:56', '2020-11-05 16:56:56', NULL),
(4, 6, 'Lot 5 - Analyser ELISA (Sind Medical)', '2', NULL, '2020-11-05 16:57:07', '2020-11-05 16:57:07', NULL),
(5, 4, 'Lot 6 - Apheresis (Interex)', '2', NULL, '2020-11-05 16:57:17', '2020-11-05 16:57:17', NULL),
(6, 24, 'Lot 8 - Meter Haemoglobin (Interex)', '2', NULL, '2020-11-05 16:57:28', '2020-11-05 16:57:28', NULL),
(7, 18, 'Lot 1 Furniture (Asian)', '2', NULL, '2020-11-05 16:57:40', '2020-11-05 16:57:40', NULL),
(8, 19, 'Lot 2 General Medical Equipment (Strongman)', '2', NULL, '2020-11-05 16:57:50', '2020-11-05 16:57:50', NULL),
(9, 20, 'Supply of Tube Stripers (6/RBC) HospiCare', '2', NULL, '2020-11-05 16:58:05', '2020-11-05 16:58:05', NULL),
(10, 21, 'BBMIS Software (Zaavia)', '3', NULL, '2020-11-05 16:58:21', '2020-11-05 16:58:21', NULL),
(11, 22, 'Lot 1 ICT Hardware (MegaPlus)', '3', NULL, '2020-11-05 16:58:39', '2020-11-05 16:58:39', NULL),
(12, 23, 'Lot 2 ICT Networking Equipment (Jaffer)', '3', NULL, '2020-11-05 16:58:51', '2020-11-05 16:58:51', NULL),
(13, 10, 'Lot 4 Telephony Equipment (Phono)', '3', NULL, '2020-11-05 16:59:02', '2020-11-05 16:59:02', NULL),
(14, 11, 'MDF (Phono)', '3', NULL, '2020-11-05 16:59:12', '2020-11-05 16:59:12', NULL),
(16, 13, 'Video Documentary (Black Box)', '4', NULL, '2020-11-05 16:59:35', '2020-11-11 09:59:01', NULL),
(17, 14, 'Printed Material (Midas)', '4', NULL, '2020-11-05 16:59:49', '2020-11-05 16:59:49', NULL),
(18, 15, 'Video Documentary KP (Creater)', '4', NULL, '2020-11-05 17:00:00', '2020-11-05 17:00:00', NULL),
(19, 16, 'On-job training', '5', NULL, '2020-11-05 17:00:12', '2020-11-05 17:00:12', NULL),
(20, 17, 'RBC staff training at Indus Khi', '5', NULL, '2020-11-05 17:00:27', '2020-11-05 17:00:27', NULL),
(21, 97, 'RBC Managers Training (Indus&AFIT-AJK&KP)', '5', NULL, '2020-11-05 17:00:52', '2020-11-11 10:01:15', NULL),
(22, 88, 'HBB staff training (Khi&Isd)', '5', NULL, '2020-11-05 17:01:01', '2020-11-12 14:12:39', NULL),
(24, 40, 'Start up support', '5', NULL, '2020-11-05 17:01:24', '2020-11-05 17:01:24', NULL),
(25, 11, 'Admin Cost (bank fees, etc.)', '7', NULL, '2020-11-05 17:01:36', '2020-11-11 10:01:08', NULL),
(26, 33, 'Tender Fee', '7', NULL, '2020-11-05 17:01:47', '2020-11-05 17:01:47', NULL),
(27, 34, 'External Audit', '7', NULL, '2020-11-05 17:02:00', '2020-11-05 17:02:00', NULL),
(28, 35, 'Supply of Data Loggers', '2', NULL, '2020-11-05 17:02:11', '2020-11-05 17:02:11', NULL),
(29, 36, 'Supply of Social Media Services', '4', NULL, '2020-11-05 17:02:22', '2020-11-05 17:02:22', NULL),
(30, 37, 'Accounting Software', '7', NULL, '2020-11-05 17:02:33', '2020-11-05 23:59:43', NULL),
(36, 66, 'Construction', '12', NULL, '2021-01-26 20:25:55', '2021-01-26 20:26:57', '2021-01-26 20:26:57'),
(32, 1001, 'Construction of Regional Blood Centre', '1', NULL, '2020-11-27 11:05:46', '2020-11-27 11:05:46', NULL),
(33, 1002, 'Waste Management ProMed&Zodiac', '9', NULL, '2020-11-30 12:20:30', '2020-11-30 12:20:30', NULL),
(34, 1003, '2.1 Lot 1 - Blood Collection - KRI', '2', NULL, '2020-11-30 12:25:08', '2020-11-30 12:25:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint NOT NULL DEFAULT '0',
  `ip_address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` date DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userss_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `company`, `phone`, `email`, `email_verified_at`, `password`, `status`, `ip_address`, `last_login`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'admin', NULL, 'EPOS', '03324123549', 'admin@admin.com', NULL, '$2y$10$ZqxEkDa8BUq8dXWBJGdloOV7zbMnYBE81iMXnaN0ng1dOBRFqzARC', 0, '127.0.0.1', '2021-04-13', 'ckXQ4kHQKwFEa52QvklRCJYtCtZoSayw2FHGSgFKrKCC96kDYmdtU7g220CY', '2020-11-04 04:22:23', '2021-04-13 04:37:34'),
(2, 'CEO', NULL, 'EPOS', '03324123549', 'ceo@ceo.com', NULL, '$2y$10$c4/XQ8N7RZmnj.E14ACzwufAuy6CE78fbj/nR.hK9SL40MeCgxI4K', 0, '127.0.0.1', '2021-02-25', NULL, '2020-11-04 04:21:21', '2021-02-25 16:44:50'),
(4, 'accountant', NULL, 'EPOS', '398521477', 'accountant@accountant.com', NULL, '$2y$10$H93agWADztusfN0vMUiIG.Nl/YubeRk9DcU35FAvt4Kg6R51Yk0Um', 0, '127.0.0.1', '2020-12-08', NULL, '2020-11-04 04:23:05', '2020-12-08 00:23:06'),
(5, 'developers', NULL, 'sagacious system', '033-241-235-49', 'developer@gmail.com', NULL, '$2y$10$C/O5tP1a7nUZwZV2wIKES.7HzW1p3hceD.Fq2.kqkxtvNr0ckr1IS', 0, '127.0.0.1', NULL, NULL, '2020-11-04 06:04:59', '2020-11-18 10:02:34');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
