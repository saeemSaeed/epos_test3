<?php 

	return [
		'supporting_documents' => [
			'0' => 'Invoice',
			'1' => 'Bill of Lading',
			'2' => 'Warranty Certificate',
			'3' => 'Packing List',
			'4' => 'insurance Certificate',
			'5' => 'Inspection Certificate',
			'6' => 'Delivery Certificate',
			'7' => 'Packing List',
			'8' => 'Certificate of Origin',
			'10' => 'Pre Commissioning Document',
			'11' => 'Delivery Challan ',
			'12' => 'Withdrawal Application',
			'20' => 'Other'
		],

		'payment_supporting_documents' => [
			'0' => 'Withdrawal Application',
			
			'9' => 'Other',
		],
	];